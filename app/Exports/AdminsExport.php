<?php

namespace App\Exports;


use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AdminsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::where('my_status','active')->orderBy('created_at')->get();
    }
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            '#',
            'id',
            'name',
            'avatarcover',
            'avatar',
            'username',
            'last_name',
            'first_name',
            'full_name',
            'phone',
            'Role',
            'email',
            'Date birthday',
            'Birthday',
            'Sex',
            'Color favorite',
            'Country',
            'ip',

        ];
    }
}
