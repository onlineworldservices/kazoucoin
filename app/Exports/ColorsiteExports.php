<?php

namespace App\Exports;

use App\Model\user\partial\color;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ColorsiteExports implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Color::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            '#',
            'color_name',
            'slug',
            'Color_slug',
            'status',
            'ID edited by',
            'Name edited by',
            'Created_at',

        ];
    }
}
