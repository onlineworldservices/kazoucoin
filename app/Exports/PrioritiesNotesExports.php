<?php


namespace App\Exports;

use App\Model\admin\priority;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PrioritiesNotesExports implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return priority::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            '#',
            'priority_name',
            'slug',
            'priority_icon',
            'priority_color',
            'status',
            'ID edited by',
            'Name edited by',
            'Created_at',
        ];
    }
}
