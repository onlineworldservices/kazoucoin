<?php


namespace App\Exports;

use App\Model\admin\status;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class StatusTasksExports implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return status::all();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            '#',
            'status_name',
            'slug',
            'status_color',
            'status',
            'ID edited by',
            'Name edited by',
            'Created_at',
        ];
    }
}
