<?php

namespace App\Exports;

use App\Model\admin\subscribemail;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SubscribemailsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Subscribemail::all();
    }
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            '#',
            '#',
            'email',
            'date subscription',

        ];
    }
}
