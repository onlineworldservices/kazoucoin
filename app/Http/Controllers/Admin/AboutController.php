<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\AboutResource;
use App\Model\admin\about;
use App\Model\admin\presentation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Http\Requests\Admin\AboutRequest;
use Symfony\Component\HttpFoundation\Response;

class AboutController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.about.index');
    }

    public function api()
    {
        return AboutResource::collection(about::with('user')->latest()->get());
    }

    public function show($id)
    {
        $about = new AboutResource(about::where('id', $id)->findOrFail($id));
        return $about;
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(\request()->all()); // pour tester les donner qui entre dans la base de donner
        $this->validate($request, [
            'fullname' => 'required|string|unique:abouts|max:255',
            'body' => 'required',
            'role' => 'required',
            'photo' => 'required',
        ]);

        $about = new About;
        
        $about->role = $request->role;
        $about->body = $request->body;
        $about->fblink = $request->fblink;
        $about->twlink = $request->twlink;
        $about->instlink = $request->instlink;
        $about->fullname = $request->fullname;
        $about->linklink = $request->linklink;
        $about->googlelink = $request->googlelink;
        $about->dribbblelink = $request->dribbblelink;
        $about->status = '0';
        
        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile. '.' . explode('/', explode(':', substr($request->photo, 0, strpos
                ($request->photo, ';')))[1])[1];

            $dir = 'assets/img/about/';
            if (!file_exists($dir)) {
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/about/{$name}");
            Image::make($request->photo)->fit(400,400)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/about/{$name}";
            $about->photo = $myfilename;
        }
        $about->save();

        return response('Created', Response::HTTP_CREATED);
    }

    /**
     * cette partie consite a activer et a desactiver
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function disable($id)
    {
        DB::table('abouts')
            ->where('id', $id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active($id)
    {
        DB::table('abouts')
            ->where('id', $id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);

        return response('Activated', Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fullname' => "required|string|max:255|unique:abouts,fullname,{$id}",
            'body' => 'required',
            'role' => 'required',
        ]);
        
        $about = About::findOrFail($id);

        $currentPhoto = $about->photo;

        if ($request->photo != $currentPhoto){

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/about/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->fit(400,400)->save(public_path('assets/img/about/').$name);

            $request->merge(['photo' =>  "/assets/img/about/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }

        $about->update($request->all());

        return ['message' => 'updated successfully'];
    }

    public function edit($id)
    {
        $about = about::where('id', $id)->findOrFail($id);

        return view('admin.about.edit', compact('about'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $about = About::findOrFail($id);
        $oldFilename = $about->photo;
        File::delete(public_path($oldFilename));

        $about->delete();

        return ['message' => 'Deleted successfully'];
    }
}
