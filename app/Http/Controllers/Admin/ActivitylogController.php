<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\management\ActivitylogResource;
use App\Model\admin\activitylog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivitylogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partials.activity_log.index');
    }

    public function api()
    {
        return ActivitylogResource::collection(activitylog::with('user')->latest()->get());
    }
}
