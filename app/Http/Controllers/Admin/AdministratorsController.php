<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\UserResource;
use App\Exports\AdminsExport;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;
use Spatie\Permission\Models\Role;
use Spatie\Searchable\Search;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


class AdministratorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partials.administrator.index');
    }

    public function datatables()
    {
        return view('admin.partials.administrator.index');
    }

    public function api()
    {
        return UserResource::collection(User::with('my_country')
            ->where('my_status','active')
            ->latest()->get());
    }

    public function user(Request $request)
    {

      //if ($search = $request->get('q')){

      //    $users = UserResource::collection(User::where(function ($query) use ($search){
      //        $query->where('username','LIKE',"%$search%")
      //               ->orWhere('email','LIKE',"%$search%");
      //    })->get());
      //}else{

      //    //$users = UserResource::collection(User::orderBy('created_at','DESC')->get());
      //}

        $users = (new Search())
            ->registerModel(User::class, ['name', 'username','email','avatar'])
            ->search($request->input('q'));

        return response()->json($users);

        //return UserResource::collection(User::orderBy('created_at','DESC')->get());
    }

    /**
     * @return BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new AdminsExport(), 'administrators.csv');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get()->pluck('name', 'name');
        return view('admin.partials.administrator.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all());
        $this->validate($request, [

            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            "sex" => "required|in:Female,Male",
            'my_status' => 'required',
            'roles' => 'required'
        ]);
        User::create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return UserResource|\Illuminate\Http\Response
     */
    public function view($id)
    {
        $user = new UserResource(user::where('id', $id)->findOrFail($id));
        return $user;
    }

    public function show($username)
    {
        if($username) {
            $user = User::where('username', $username)->firstOrFail();
        } else {
            $user = User::findOrFail(auth()->user()->id);
        }
        return view("admin.partials.administrator.view")->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::get()->pluck('name', 'name');
        return view('admin.partials.administrator.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd(request()->all());
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            "sex" => "required|in:Female,Male",
            'my_status' => 'required',
            'roles' => 'required',
            //'role_user' => 'required',
        ]);

        $user = user::find($id);
        $user->name = $request->name;
        $user->my_status = $request->my_status;
        $user->email = $request->email;
        $user->sex = $request->sex;
        $user->edited_by = auth()->user()->name;

        $roles = $request->input('roles') ? $request->input('roles') : [];
        $permissions = $request->input('permissions') ? $request->input('permissions') : [];
        $user->role_user = $request->input('roles'); //Update role_user in users datatables
        $user->syncRoles($permissions);
        $user->syncRoles($roles);

        $user->save();

        return ['message' => 'user has ben updated'];
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return ['message' => 'user deleted '];
    }

}
