<?php

namespace App\Http\Controllers\Admin\Aide;

use App\Http\Resources\AidecontactthemeResource;
use App\Model\admin\aidecontacttheme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class AidecontactthemeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.aide_contact_theme.index');
    }

    public function api()
    {
        return AidecontactthemeResource::collection(Aidecontacttheme::with('user')->latest()->get());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
   {
       $this->validate($request,[
           'title'=>'required|string|min:2|max:100|unique:aidecontactthemes',
       ]);

        $aidecontacttheme = new Aidecontacttheme;
        $aidecontacttheme->title = $request->title;
        $aidecontacttheme->status = '0';
        
        $aidecontacttheme->save();
        
        return response('Created', Response::HTTP_CREATED);
   }

    /**
     * cette partie consite a activer et a desactiver
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function disable($id)
    {
        DB::table('aidecontactthemes')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated',Response::HTTP_ACCEPTED);
    }

    public function active($id)
    {
        DB::table('aidecontactthemes')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);

        return response('Activated',Response::HTTP_ACCEPTED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'title'=> "required|string|min:2|max:100|unique:aidecontactthemes,title,{$id}",
        ]);
        $aidecontacttheme = aidecontacttheme::findOrFail($id);
        $aidecontacttheme->update($request->all());

        return ['message' => 'updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aidecontacttheme = Aidecontacttheme::findOrFail($id);
        $aidecontacttheme->delete();

        return ['message' => 'Deleted successfully '];
    }
}
