<?php

namespace App\Http\Controllers\Admin\Categories;


use App\Http\Resources\Category\CategoryeventResource;
use App\Model\admin\categories\categoryevent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class CategoryeventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.categoryevent');
    }

    public function api()
    {
        return CategoryeventResource::collection(categoryevent::with('user')->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|min:2|max:200|unique:categoryevents,name',
            'link_category'=>'required|string|min:2|max:200|unique:categoryevents,link_category',
            'color_name'=>'required',
            'icon'=>'required',
        ]);

        $categoryevent = new categoryevent;
        
        $categoryevent->icon = $request->icon;
        $categoryevent->name = $request->name;
        $categoryevent->color_name = $request->color_name;
        $categoryevent->link_category = $request->link_category;
        $categoryevent->number_category = $request->number_category;
        $categoryevent->link_category_create = $request->link_category_create;
        $categoryevent->status = '0';

        $categoryevent->save();

        return response('Created',Response::HTTP_CREATED);
    }

    public function unactive_category($id)
    {
        DB::table('categoryevents')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('Deactivated',Response::HTTP_ACCEPTED);
    }

    public function active_category($id)
    {
        DB::table('categoryevents')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        return response('Activated',Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            "name" =>"required|string|unique:categoryevents,name,{$id}",
            "link_category" =>"required|string|unique:categoryevents,link_category,{$id}",
            'color_name'=>'required',
            'icon'=>'required',
        ]);

        $categoryevent = categoryevent::findOrFail($id);

        $categoryevent->icon = $request->icon;
        $categoryevent->name = $request->name;
        $categoryevent->color_name = $request->color_name;
        $categoryevent->link_category = $request->link_category;
        $categoryevent->number_category = $request->number_category;
        $categoryevent->link_category_create = $request->link_category_create;
        $categoryevent->status = $request->status;
        $categoryevent->slug = null;

        $categoryevent->save();

        return ['message' => 'categoryevent faq has ben updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoryevent = categoryevent::findOrFail($id);
        $categoryevent->delete();

        return ['message' => 'category event deleted '];
    }
}
