<?php

namespace App\Http\Controllers\Admin\Categories;


use App\Http\Resources\Category\CategoryorderResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\admin\categories\categoryorder;
use Symfony\Component\HttpFoundation\Response;

class CategoryorderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.categoryorder');
    }

    public function api()
    {
        return CategoryorderResource::collection(categoryorder::with('user')->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|min:2|max:200|unique:categoryorders,name',
            'link_category'=>'required|string|min:2|max:200|unique:categoryorders,link_category',
            'color_name'=>'required',
            'icon'=>'required',
        ]);
        
        $categoryorder = new categoryorder;
        
        $categoryorder->name = $request->name;
        $categoryorder->link_category = $request->link_category;
        $categoryorder->link_category_create = $request->link_category_create;
        $categoryorder->color_name = $request->color_name;
        $categoryorder->icon = $request->icon;
        $categoryorder->status = '0';
        
        $categoryorder->save();

        return response('Created',Response::HTTP_CREATED);
    }

    /**
     * disable
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */

    public function disable($id)
    {
        DB::table('categoryorders')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('Deactivated',Response::HTTP_ACCEPTED);
    }

    /**
     * Active
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function active($id)
    {
        DB::table('categoryorders')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);

        return response('Activated',Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=> "required|string|min:2|max:200|unique:categoryorders,name,{$id}",
            'link_category'=> "required|string|min:2|max:200|unique:categoryorders,link_category,{$id}",
            'color_name'=>'required',
            'icon'=>'required',
        ]);

        $categoryorder = categoryorder::findOrFail($id);

        $categoryorder->name = $request->name;
        $categoryorder->link_category = $request->link_category;
        $categoryorder->link_category_create = $request->link_category_create;
        $categoryorder->color_name = $request->color_name;
        $categoryorder->icon = $request->icon;
        $categoryorder->status = $request->status;
        $categoryorder->slug = null;

        $categoryorder->save();

        return ['message' => 'categoryorder faq has ben updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoryorder = categoryorder::findOrFail($id);
        $categoryorder->delete();

        return ['message' => 'category order deleted '];
    }
}
