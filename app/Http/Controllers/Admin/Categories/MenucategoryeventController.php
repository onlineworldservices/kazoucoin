<?php

namespace App\Http\Controllers\Admin\Categories;


use App\Http\Resources\Category\MenucategoryeventResource;
use App\Model\admin\categories\categoryevent;
use App\Model\admin\categories\menucategoryevent;
use App\Model\user\partial\color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class MenucategoryeventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.menucategoryevent');
    }

    public function api()
    {
        return MenucategoryeventResource::collection(menucategoryevent::with('user','categoryevent')->latest()->get());
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|unique:menucategoryevents,name',
            'categoryevent_name'=>'required',
            'categoryevent_id'=>'required',
            'color_name'=>'required',
            'icon'=>'required',
        ]);

        $menucategoryevent = new menucategoryevent;
        
        $menucategoryevent->slug = $request->slug;
        $menucategoryevent->name = $request->name;
        $menucategoryevent->icon = $request->icon;
        $menucategoryevent->color_name = $request->color_name;
        $menucategoryevent->categoryevent_id = $request->categoryevent_id;
        $menucategoryevent->categoryevent_name = $request->categoryevent_name;
        $menucategoryevent->status = '0';
        
        $menucategoryevent->save();

        return response('Created',Response::HTTP_CREATED);
    }

    public function unactive_category($id)
    {
        DB::table('menucategoryevents')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('Deactivated',Response::HTTP_ACCEPTED);
    }

    public function active_category($id)
    {
        DB::table('menucategoryevents')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        return response('Activated',Response::HTTP_ACCEPTED);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "name" =>"required|string|unique:menucategoryevents,name,{$id}",
            "icon" =>"required",
            'color_name'=>'required',
            'categoryevent_name'=>'required',
        ]);

        $menucategoryevent = menucategoryevent::findOrFail($id);
        $menucategoryevent->update($request->all());

        return ['message' => 'menu category order has ben updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menucategoryevent = menucategoryevent::findOrFail($id);
        $menucategoryevent->delete();

        return ['message' => 'category deleted '];
    }
}
