<?php

namespace App\Http\Controllers\Admin\Categories;


use App\Http\Resources\Category\MenucategoryorderResource;
use App\Model\admin\categories\categoryorder;
use App\Model\admin\categories\menucategoryorder;
use App\Model\user\partial\color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class MenucategoryorderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.menucategoryorder');
    }

    public function api()
    {
        return MenucategoryorderResource::collection(menucategoryorder::with('user','categoryorder')->latest()->get());
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=> "required|string|min:2|max:200|unique:menucategoryorders,name",
            'color_name'=>'required',
            'categoryorder_id'=>'required',
            'categoryorder_name'=>'required',
            'icon'=>'required',
        ]);
        
        $menucategoryorder = new menucategoryorder;
        
        $menucategoryorder->name = $request->name;
        $menucategoryorder->color_name = $request->color_name;
        $menucategoryorder->categoryorder_id = $request->categoryorder_id;
        $menucategoryorder->categoryorder_name = $request->categoryorder_name;
        $menucategoryorder->icon = $request->icon;
        $menucategoryorder->status = '0';
        
        $menucategoryorder->save();
        
        return response('Created',Response::HTTP_CREATED);
    }

    /**
     * disable
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */

    public function disable($id)
    {
        DB::table('menucategoryorders')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('Deactivated',Response::HTTP_ACCEPTED);
    }

    /**
     * Active
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function active($id)
    {
        DB::table('menucategoryorders')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        return response('Activated',Response::HTTP_ACCEPTED);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'name'=> "required|string|min:2|max:200|unique:menucategoryorders,name,{$id}",
            'color_name'=>'required',
            //'categoryorder_id'=>'required',
            'categoryorder_name'=>'required',
            'icon'=>'required',
        ]);
        
        $menucategoryorder= menucategoryorder::findOrFail($id);
        $menucategoryorder->update($request->all());

        return ['message' => 'menu category order has ben updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menucategoryorder = menucategoryorder::findOrFail($id);
        $menucategoryorder->delete();

        return ['message' => 'category deleted '];
    }
}
