<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Image\CoverpageResource;
use App\Model\admin\coverpage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class CoverpageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.profiles.cover_page.index');
    }


    public function api()
    {
        return CoverpageResource::collection(coverpage::with('user')->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CoverpageResource|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all()); // pour tester les donner qui entre dans la base de donner

        // Bon a savoir cette technologie functione avec vuejs
        $coverpage = new coverpage;
        $coverpage->status = '0';
        
        if ($request->photo){

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                                                                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/coverpage/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/coverpage/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/coverpage/{$name}";
            $coverpage->photo = $myfilename;
        }

        $coverpage->save();

        return new CoverpageResource($coverpage);
    }


    /**
     * cette partie consite a activer et a desactiver
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function disable($id)
    {
        DB::table('coverpages')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated',Response::HTTP_ACCEPTED);
    }

    public function active($id)
    {
        DB::table('coverpages')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);

        return response('Activated',Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\admin\coverpage  $coverpage
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $coverpage =  coverpage::findOrFail($id);

        $currentPhoto = $coverpage->photo;

        if ($request->photo != $currentPhoto){

            $namefile = sha1(date('YmdHis') . str_random(30));
            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                                                                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/coverpage/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->save(public_path('assets/img/coverpage/').$name);

            $request->merge(['photo' =>  "/assets/img/coverpage/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }

        $coverpage->update($request->all());
        return ['message' => 'coverpage has ben updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $coverpage = Coverpage::findOrFail($id);
        $oldFilename = $coverpage->photo;
        File::delete(public_path($oldFilename));

        $coverpage->delete();

        return ['message' => 'Deleted successfully '];
    }
}
