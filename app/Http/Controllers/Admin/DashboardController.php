<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Model\user\partial\contact;
use App\Model\user\partial\work;
use App\Model\user\article;
use App\Model\user\event;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    public function index()
    {
        $users = user::all();
        $contacts = contact::all();
        $user = user::orderBy('updated_at','desc')->take(1)->get();
        $contact = Contact::orderBy('updated_at','desc')->take(1)->get();
        $works = Work::orderBy('updated_at','desc')->get();
        return view('admin.dashboard',compact('user', 'users', 'contacts','contact', 'works'));
    }
}
