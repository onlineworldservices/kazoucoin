<?php

namespace App\Http\Controllers\Admin\Info;

use App\Model\admin\condition;
use Carbon\Carbon;
use App\Http\Resources\TermsconditionsResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\Response;

class ConditionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partials.conditions.index');
    }

    public function api()
    {
        return TermsconditionsResource::collection(Condition::with('user')->latest()->get());
    }
    
    public function trash()
    {
        $trash = Condition::onlyTrashed()->get();
        return view('admin.partials.conditions.trash',compact('trash'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partials.conditions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return TermsconditionsResource|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all());
        $this->validate($request,[
            'title'=>'required|string',
            'body'=>'required|string',
            'photo' =>'required',
        ]);

        $condition = new Condition;
        $condition->title = $request->title;
        $condition->slug = $request->slug;
        $condition->body = $request->body;
        $condition->photo = $request->photo;
        $condition->status = '0';

        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name =   $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/condition/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/condition/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/condition/{$name}";
            $condition->photo = $myfilename;
        }

        $condition->save();

        return new TermsconditionsResource($condition);
    }

    public function unactive_condition($id)
    {
        DB::table('conditions')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
                ]);
        
        return response('deactivated', Response::HTTP_ACCEPTED);;
    }

    public function active_condition($id)
    {
        DB::table('conditions')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
                ]);
        
        return response('activated', Response::HTTP_ACCEPTED);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return TermsconditionsResource|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $condition = new TermsconditionsResource(condition::where('id', $id)->findOrFail($id));
        return $condition;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $condition = Condition::where('id',$id)->first();
        return view('admin.partials.conditions.edit',compact('condition'));
    }


    public function view($slug)
    {
        $condition = new TermsconditionsResource(condition::where('slug',$slug)->firstOrFail());
        return $condition;
    }



    public function vector(condition $condition)
    {
        return view('admin.partials.conditions.view', [
            'condition' => $condition,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'body'=>'required',
            'title'=>'required',
            'photo' =>'required',
        ]);

        $condition = Condition::find($id);

        $currentPhoto = $condition->photo;

        if ($request->photo != $currentPhoto){

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name =   $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/condition/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->fit(400,400)->save(public_path('assets/img/condition/').$name);


            $request->merge(['photo' =>  "/assets/img/condition/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }
        $condition->update($request->all());

        return ['message' => 'updated successfully'];
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $condition = Condition::findOrFail($id);
        $oldFilename = $condition->photo;
        File::delete(public_path($oldFilename));

        $condition->delete();

        return ['message' => 'Deleted successfully '];
    }
}
