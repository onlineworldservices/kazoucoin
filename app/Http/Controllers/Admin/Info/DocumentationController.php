<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Resources\Info\DocumentationResource;
use App\Model\admin\info\documentation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DocumentationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.info.documentation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.info.documentation.create');
    }

    public function api()
    {
        return DocumentationResource::collection(documentation::with('user')->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name'=>'required|string',
        ]);

        $documentation = new documentation();
        $documentation->name = $request->name;
        $documentation->save();

        if ($request->name_doc) {
            $file_name_doc = $request->name_doc;
            $file_name_doc_name = ''.$file_name_doc->getClientOriginalName();
            Storage::disk('public')->putFileAs(
                $documentation->getUploadPath(),
                $file_name_doc,
                $file_name_doc_name
            );
            $documentation->name_doc = $file_name_doc_name;
            $documentation->save();
        }
        return redirect()->route('documentations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request, documentation $documentation)
    {
        try {
            Storage::disk('public')->delete($documentation->getUploadPath().$documentation->name_doc);
            // Delete user
            $documentation->delete();

        } catch (\Illuminate\Database\QueryException $e) {
            Log::error($e);
            dd($e);
        }

        return ['message' => 'condition deleted'];
    }


    public function getDocumentation(documentation $documentation)
    {
        if ($documentation->name_doc)
        {
            return Storage::disk('public')->download($documentation->getUploadPath().$documentation->name_doc);
        }
        return abort(404);
    }
}
