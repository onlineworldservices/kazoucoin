<?php

namespace App\Http\Controllers\Admin\Info;

use App\Http\Controllers\Controller;
use App\Http\Resources\InformationResource;
use App\Model\admin\information;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\Response;

class InformationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return view('admin.partials.information.index');
    }

    public function api()
    {
        return InformationResource::collection(information::with('user')->latest()->get());
    }
    
    public function trash()
    {
        $trash = information::onlyTrashed()->get();
        return view('admin.partials.information.trash',compact('trash'));
    }
     
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function vector(information $information)
    {
        return view('admin.partials.information.view', [
            'information' => $information,
        ]);
    }

    public function view($slug)
    {
        $information = new InformationResource(information::where('slug',$slug)->firstOrFail());
        return $information;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partials.information.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(request()->all());
        $this->validate($request,[
            'title'=>'required|string',
            'body'=>'required|string',
            'color_name'=>'required|string',
            'icon'=>'required|string',
        ]);

        $information = new information;
        $information->body = $request->body;
        $information->icon = $request->icon;
        $information->slug = $request->slug;
        $information->title = $request->title;
        $information->color_name = $request->color_name;
        //$information->user_id = auth()->user()->id;
        $information->status = '0';

        $information->save();

        return response('Created',Response::HTTP_CREATED);
		//alert()->success('Success', "Information Created Successfully");
        //toastr()->success('<b>Information Created Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        //return redirect(route('information.index'));
    }


    public function unactive_information($id)
    {
        DB::table('informations')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        toastr()->success('<b>Unactivated Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return back();

    }

    public function active_information($id)
    {
        DB::table('informations')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        toastr()->success('<b>Activated Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\admin\howtoregister  $howtoregister
     * @return \Illuminate\Http\Response
     */
    public function show($id)
     {
      $information = new InformationResource(information::where('id', $id)->findOrFail($id));
        return $information;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\admin\howtoregister  $howtoregister
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = information::where('id',$id)->findOrFail($id);

        return view('admin.partials.information.edit',compact('information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\admin\information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'title'=>'required|string',
            'body'=>'required|string',
            'icon'=>'required|string',
            'color_name'=>'required|string',
        ]);


        $information = information::find($id);

        $information->body = $request->body;
        $information->icon = $request->icon;
        $information->slug = $request->slug;
        $information->title = $request->title;
        $information->color_name = $request->color_name;
        $information->user_id = auth()->user()->id;

        $information->save();

        return ['message' => 'information has ben updated'];
		//alert()->success('Success', "Information Updated Successfully");
        //toastr()->success('<b>Info Updated Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        //return redirect()->route('how_register.index',$information->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\admin\howtoregister  $howtoregister
     * @return \Illuminate\Http\Response
     */
    public function delete_information(Request $request,$slug)
    {
        DB::table('informations')
            ->where('slug',$slug)
            ->update([
                'deleted_at' => Carbon::now(),
                'user_id' => auth()->user()->id,
            ]);

        toastr()->success('<b>Deleted Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return back();

    }

    public function delete_trash_force_information(Request $request)
    {
        $information = information::onlyTrashed()->findOrFail($request->item_id);
        if($information->forceDelete()){
            alert()->success('Success', "Deleted Forever Successfully");
            //toastr()->success('<b>Deleted Forever Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
            return redirect()->back();
        }else{
            Alert::error('Deleted!', 'Oop!');
            return redirect()->back();
        }
    }

    public function restore(Request $request)
    {
        $information = information::onlyTrashed()->find($request->item_id);
        if ($information->restore()){
            alert()->success('Success', "Restore Successfully");
            //toastr()->success('<b>Restore Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
            return redirect()->back();
        }else{
            Alert::error('Deleted!', 'Oop!');
            return redirect()->back();
        }

    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $information = information::findOrFail($id);
        $information->delete();

        return ['message' => 'Information deleted '];
    }
}
