<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\LinkRequest;
use App\Http\Resources\LinkResource;
use App\Model\admin\link;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\Response;

class LinkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.link.index');
    }

    public function api()
    {
        return LinkResource::collection(Link::with('user')->latest()->get());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partials.link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return LinkResource
     */
    public function store(LinkRequest $request)
    {

        $link = new Link;
        $link->link  =  $request->link;
        $link->slug  =  $request->slug;
        $link->name  =  $request->name;

        $link->save();

        return new LinkResource($link);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return LinkResource|\Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        return new LinkResource($link);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $link = Link::where('id',$id)->first();
        return view('admin.partials.link.edit',compact('link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return LinkResource|array|\Illuminate\Http\Response
     */
    public function update(LinkRequest $request, Link $link)
    {


        $link->update($request->all());;

        return new LinkResource($link);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $link= link::findOrFail($id);
        $link->delete();

        return ['message' => 'link deleted'];
    }
}
