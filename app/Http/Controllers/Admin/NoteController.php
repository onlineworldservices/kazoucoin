<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\NoteResource;
use App\Model\admin\note;
use App\Model\user\view;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\Response;

class NoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partials.note.index');
    }

    public function api()
    {
        return NoteResource::collection(Note::with('user')->latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required',
        ]);

        Note::create($request->all());

        return response('Created',Response::HTTP_CREATED);
    }


    /**
     * cette parti consite a activer et a desactiver
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function disable($id)
    {
        DB::table('notes')
            ->where('id',$id)
            ->update([
                'status' => 2,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated',Response::HTTP_ACCEPTED);

    }

    public function active($id)
    {
        DB::table('notes')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);

        return response('Activated',Response::HTTP_ACCEPTED);

    }

    public function taskconfirm(note $note, $id)
    {
        $note = note::where('id', $id)->findOrFail($id);
        $note->update([
            'status_task' => 1,
        ]);
        return response('Confirm successfully',Response::HTTP_ACCEPTED);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Model\admin\note  $note
     * @return \Illuminate\Http\Response
     */
    public function show(note $note)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\admin\note  $note
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\admin\note  $note
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required',
        ]);
        $note = note::findOrFail($id);
        //auth()->user()->question()->create($request->all());
        $note->update($request->all());

        return ['message' => 'note has been updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\admin\note  $note
     * @return array
     */

    public function destroy($id)
    {
        $note = Note::findOrFail($id);
        $note->delete();

        return ['message' => 'note deleted '];
    }
}
