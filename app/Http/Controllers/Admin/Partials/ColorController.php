<?php

namespace App\Http\Controllers\Admin\Partials;

use App\Exports\ColorsiteExports;
use App\Http\Resources\ColorResource;
use App\Model\user\partial\color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\Response;

class ColorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$trash = color::onlyTrashed()->get();
        return view('admin.color.index');
    }

    public function api()
    {
       //$expiresAt = now()->addSeconds(60);
       //$colors = Cache::remember('colors', $expiresAt, function () {
       //    return ColorResoure::collection(Color::with('user')->latest()->get());

       //});
       //return $colors;
        return ColorResource::collection(Color::with('user')->latest()->get());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new ColorsiteExports(), 'Color-sites.csv');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

   public function store(Request $request)
   {
       $this->validate($request,[
           'color_name'=>'required|string|unique:colors',
       ]);

       $color = new Color;
       $color->slug = $request->slug;
       $color->color_name = $request->color_name;
       $color->color_slug = $request->color_slug;

       $color->save();


       return response('Created',Response::HTTP_CREATED);
   }


    public function status(color $color, $id)
    {
        $color = color::where('id', $id)->findOrFail($id);
        $color->update(['status' => !$color->status]);

        return response('Update', Response::HTTP_ACCEPTED);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $color = Color::findOrFail($id);

        return view('admin.partials.color.show', ['color' => $color]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'color_name'=> "required|string|min:2|max:25|unique:colors,color_name,{$id}",
        ]);

        $color = color::findOrFail($id);
        $color->update($request->all());

        return ['message' => 'color has been updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy($id)
    {
        $color = color::findOrFail($id);
        $color->delete();

        return ['message' => 'color deleted '];
    }

}
