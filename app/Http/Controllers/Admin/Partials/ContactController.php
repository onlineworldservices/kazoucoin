<?php

namespace App\Http\Controllers\Admin\partials;


use App\Http\Resources\ContactResource;
use App\Model\user\partial\contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partials.contact.contact_page.index');
    }

    public function api()
    {
        return ContactResource::collection(contact::with('user')
            ->orderBy('created_at','DESC')->paginate(10));
    }

    /**
     * cette parti consite a activer et a desactiver
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function disable($slug)
    {
        DB::table('contacts')
            ->where('slug',$slug)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated message read',Response::HTTP_ACCEPTED);
    }

    public function active($id)
    {
        DB::table('contacts')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);

        return response('Message read',Response::HTTP_ACCEPTED);
    }



    public function view($slug)
    {
        $contact = new ContactResource(contact::where('slug',$slug)->firstOrFail());
        return $contact;
    }


    public function contact(contact $contact)
    {
        return view('admin.partials.contact.contact_page.view', [
            'contact' => $contact,
        ]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy($id)
    {
        $contact = contact::findOrFail($id);
        $contact->delete();

        return ['message' => 'contact deleted '];
    }
}
