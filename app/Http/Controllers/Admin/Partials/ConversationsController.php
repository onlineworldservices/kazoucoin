<?php

namespace App\Http\Controllers\Admin\Partials;

use App\Model\admin\admin;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ConversationsController extends Controller
{
    /**
     * @var ConversationRepository
     */
    private $r;
    /**
     * @var AuthManager
     */
    private $auth;

    /**

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $admins = admin::all();
        return view('admin.partials.conversation.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //public function show(Admin $admin) {
    //    $me = $this->auth->user();
    //    $messages = $this->r->getMessagesFor($me->id, $admin->id)->paginate(50);
    //    $unread = $this->r->unreadCount($me->id);
    //    if (isset($unread[$admin->id])) {
    //        $this->r->readAllFrom($admin->id, $me->id);
    //        unset($unread[$admin->id]);
    //    }
    //    return view('admin.partials.conversation.show', [
    //        '$admins' => $this->r->getConversations($me->id),
    //        '$admin' => $admin,
    //        'messages' => $messages,
    //        'unread'=> $unread
    //    ]);
    //}

    public function show(Admin $admin)
    {
        return view('admin.partials.conversation.show',[
            'admins' => $this->r->getConversations($this->auth->user()->id),
            'admin' => $admin,
            'messages' => $this->r->getMessagesFor($this->auth->user()->id,$admin->id)->get()
            //'unread'=> $this->r->unreadCount($this->auth->user()->id)
        ]);
    }


        public function store (Admin $admin,Request $request) {
            $this->r->createMessage(
                $request->get('content'),
                $this->auth->user()->id,
                $admin->id
            );
            return redirect(route('admin.partials.conversation.show', ['id' => $admin->id]));
        }


    //public function store (Admin $admin, StoreMessageRequest $request) {
    //    $message = $this->r->createMessage(
    //        $request->get('content'),
    //        $this->auth->user()->id,
    //        $admin->id
    //    );
    //    $admin->notify(new MessageReceived($message));
    //    return redirect(route('admin.partials.conversation.show', ['id' => $admin->id]));
    //}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
