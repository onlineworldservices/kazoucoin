<?php

namespace App\Http\Controllers\Admin\Partials;

use App\Http\Resources\DeviseResource;
use App\Model\admin\devise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class DevisesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$trash = color::onlyTrashed()->get();
        return view('admin.devise.index');
    }

    public function api()
    {
        return DeviseResource::collection(Devise::with('user')->latest()->get());
    }
    public function devise()
    {
        return DeviseResource::collection(devise::with('user')
            ->where('status', 1)
            ->latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:25|unique:devises',
            'code'=>'required|string|max:25|unique:devises',
            'symbol'=>'required|string|max:25|unique:devises',
        ]);

        Devise::create($request->all());

        return response('Created',Response::HTTP_CREATED);
    }


    public function status(devise  $devise,$id)
    {
        $devise = devise::where('id', $id)->findOrFail($id);
        $devise->update(['status' => !$devise->status]);

        return response('Update', Response::HTTP_ACCEPTED);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $devise = devise::findOrFail($id);

        return view('admin.partials.devise.show', ['devise' => $devise]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'name'=> "required|string|max:25|unique:devises,name,{$id}",
            'code'=> "required|string|max:25|unique:devises,code,{$id}",
            'symbol'=> "required|string|max:25|unique:devises,symbol,{$id}",
        ]);

        $devise = devise::findOrFail($id);
        $devise->update($request->all());

        return ['message' => 'devise has been updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array
     */
    public function destroy($id)
    {
        $devise = devise::findOrFail($id);
        $devise->delete();

        return ['message' => 'devise deleted '];
    }
}
