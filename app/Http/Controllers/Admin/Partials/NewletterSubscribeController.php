<?php

namespace App\Http\Controllers\Admin\Partials;

use App\Model\admin\subscribemail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use RealRashid\SweetAlert\Facades\Alert;

class NewletterSubscribeController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[

            'user_email'=>'required|string|email|unique:subscribemails,user_email',
            
        ]);

        $subscribemail= new Subscribemail;
        $subscribemail->user_email = $request->user_email;


        if ($subscribemail->save()){
            alert()->success('Success', "You are subscribed to our newletters Successfully");
            return redirect()->back();
        } else {
            //toastr()->error('You are already subscribed','',["positionClass" => "toast-top-right"]);
            alert()->success('Information!', 'You are already subscribed to our newletters');
            return redirect()->back();
        }
        
    }

}
