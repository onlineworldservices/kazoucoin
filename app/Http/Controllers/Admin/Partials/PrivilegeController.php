<?php

namespace App\Http\Controllers\Admin\Partials;

use App\Http\Controllers\Controller;
use App\Model\admin\privilege;
use Illuminate\Http\Request;
use App\Http\Resources\PrivilegeResource;

class PrivilegeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function api($name_privilege)
    {
        return PrivilegeResource::collection(privilege::where('name_privilege', $name_privilege)
            ->latest()
            ->get()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\admin\privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function show(privilege $privilege)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\admin\privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function edit(privilege $privilege)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\admin\privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, privilege $privilege)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\admin\privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function destroy(privilege $privilege)
    {
        //
    }
}
