<?php

namespace App\Http\Controllers\Admin\Partials\Profiles;

use App\Http\Resources\Image\HomeprofileResource;
use App\Http\Controllers\Controller;
use App\Model\admin\profiles\homepageprofile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class HomepageprofileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.profiles.profile_home.index');
    }

    public function api()
    {
        return HomeprofileResource::collection(homepageprofile::with('user')->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'photo' => 'required'
        ]);

        $homepageprofile = new homepageprofile;

        $homepageprofile->slug = $request->slug;
        $homepageprofile->status = '0';

       if ($request->photo){

           $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                                                                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/homeprofile/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/homeprofile/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/homeprofile/{$name}";
            $homepageprofile->photo = $myfilename;
        }

        $homepageprofile->save();

        return new HomeprofileResource($homepageprofile);
    }

    public function unactive_home_page($id)
    {
        DB::table('homepageprofiles')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('Deactivated',Response::HTTP_ACCEPTED);
    }

    public function active_home_page($id)
    {
        DB::table('homepageprofiles')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        return response('Activated',Response::HTTP_ACCEPTED);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\admin\homepageprofile  $homepageprofile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'photo' => 'required'
        ]);

        $homepageprofile = homepageprofile::findOrFail($id);

        $currentPhoto = $homepageprofile->photo;

        if ($request->photo != $currentPhoto){

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                                                                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/homeprofile/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->save(public_path('assets/img/homeprofile/').$name);

            $request->merge(['photo' =>  "/assets/img/homeprofile/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }

        $homepageprofile->update($request->all());
        return ['message' => 'coverpage has ben updated'];
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $homepageprofile = homepageprofile::findOrFail($id);
        $oldFilename = $homepageprofile->photo;
        File::delete(public_path($oldFilename));

        $homepageprofile->delete();

        return ['message' => 'Deleted successfully '];
    }

}
