<?php

namespace App\Http\Controllers\Admin\Partials\Slides;

use App\Http\Resources\Slides\SlidesAboutResource;
use App\Model\admin\slides\slidesabout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\Response;

class SlidesaboutController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slides.slide_about.index');
    }

    public function api()
    {
        return SlidesAboutResource::collection(slidesabout::with('user')->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(\request()->all()); // pour tester les donner qui entre dans la base de donner

        $slidesabout = new Slidesabout;

        $slidesabout->slug = $request->slug;
        $slidesabout->slide_title = $request->slide_title;
        $slidesabout->slide_subtitle = $request->slide_subtitle;
        $slidesabout->status = '0';

        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));
            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_about/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/slides/slides_about/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/slides/slides_about/{$name}";
            $slidesabout->photo = $myfilename;
        }

        $slidesabout->save();

        return new SlidesAboutResource($slidesabout);
    }

    public function unactive_slide_about($id)
    {
        DB::table('slidesabouts')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active_slide_about($id)
    {
        DB::table('slidesabouts')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        return response('activated', Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request,[
        //    'slide_title' => 'required|string',
        //    'slide_subtitle' => 'required|string',
        //    'photo' => 'required',
        //]);

        $slidesabout = Slidesabout::findOrFail($id);

        $currentPhoto = $slidesabout->photo;

        if ($request->photo != $currentPhoto) {

            $namefile = sha1(date('YmdHis') . str_random(30));
            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_about/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->save(public_path('assets/img/slides/slides_about/').$name);

            $request->merge(['photo' =>  "/assets/img/slides/slides_about/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }
        $slidesabout->update($request->all());

        return ['message' => 'updated successfully'];
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $slidesabout = Slidesabout::findOrFail($id);
        $oldFilename = $slidesabout->photo;
        File::delete(public_path($oldFilename));

        $slidesabout->delete();

        return ['message' => 'Deleted successfully'];
    }
}
