<?php

namespace App\Http\Controllers\Admin\Partials\Slides;

use App\Model\admin\slides\slidescontact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Http\Resources\Slides\SlidesContactResource;
use Symfony\Component\HttpFoundation\Response;

class SlidescontactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slides.slide_contact.index');
    }

    public function api()
    {
        return SlidesContactResource::collection(Slidescontact::with('user')->latest()->get());
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return SlidesContactResource|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(\request()->all()); // pour tester les donner qui entre dans la base de donner
        $this->validate($request,[
            'slide_title' => 'required|string',
            //'slide_subtitle' => 'required|string',
            //'photo' => 'required'
        ]);

        $slidescontact = new Slidescontact;

        $slidescontact->slug = $request->slug;
        $slidescontact->slide_title = $request->slide_title;
        $slidescontact->slide_subtitle = $request->slide_subtitle;
        $slidescontact->status = '0';

        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_contact/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/slides/slides_contact/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/slides/slides_contact/{$name}";
            $slidescontact->photo = $myfilename;
        }

        $slidescontact->save();

        return new SlidesContactResource($slidescontact);
    }

    public function unactive_slide($id)
    {
        DB::table('slidescontacts')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
                ]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active_slide($id)
    {
        DB::table('slidescontacts')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
                ]);
        return response('activated', Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'slide_title' => 'required|string',
            //'slide_subtitle' => 'required|string',
            //'photo' => 'required'
        ]);

        //dd(\request()->all());
        $slidescontact = Slidescontact::find($id);

        $currentPhoto = $slidescontact->photo;

        if ($request->photo != $currentPhoto) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_contact/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->save(public_path('assets/img/slides/slides_contact/').$name);

            $request->merge(['photo' =>  "/assets/img/slides/slides_contact/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }
        $slidescontact->update($request->all());

        return ['message' => 'updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $slidescontact = Slidescontact::findOrFail($id);
        $oldFilename = $slidescontact->photo;
        File::delete(public_path($oldFilename));

        $slidescontact->delete();

        return ['message' => 'Deleted successfully'];
    }
}
