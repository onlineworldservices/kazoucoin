<?php

namespace App\Http\Controllers\Admin\Partials\Slides;

use App\Http\Resources\Slides\SlidesEventResource;
use App\Model\admin\slides\slidesevenement;
use Illuminate\Http\Request;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class SlidesevenementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slides.slide_evenement.index');
    }
    
    public function api()
    {
        return SlidesEventResource::collection(Slidesevenement::with('user')->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(\request()->all()); // pour tester les donner qui entre dans la base de donner
        $this->validate($request,[
            'slide_title' => 'required|string',
            'slide_subtitle' => 'required|string',
            'photo' => 'required',
            'slide_btn_link' => 'required|string',
            'slide_btn_title' => 'required|string',
            'slide_btn_style1' => 'required|string',
            'slide_btn_style' => 'required|string',
            'slide_btn_color' => 'required|string',
        ]);

        $slidesevenement = new Slidesevenement;

        $slidesevenement->slug = $request->slug;
        $slidesevenement->slide_title = $request->slide_title;
        $slidesevenement->slide_subtitle = $request->slide_subtitle;
        $slidesevenement->slide_btn_style = $request->slide_btn_style;
        $slidesevenement->slide_btn_style1 = $request->slide_btn_style1;
        $slidesevenement->slide_btn_color = $request->slide_btn_color;
        $slidesevenement->slide_btn_link = $request->slide_btn_link;
        $slidesevenement->slide_btn_title = $request->slide_btn_title;
        $slidesevenement->slide_btn_status = $request->slide_btn_status;
        $slidesevenement->status = '0';

        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_event/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/slides/slides_event/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/slides/slides_event/{$name}";
            $slidesevenement->photo = $myfilename;
        }

        $slidesevenement->save();

        return new SlidesEventResource($slidesevenement);
    }

    public function unactive_slide($id)
    {
        DB::table('slidesevenements')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
                ]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active_slide($id)
    {
        DB::table('slidesevenements')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
                ]);
        return response('activated', Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'slide_title' => 'required|string',
            'slide_subtitle' => 'required|string',
            'photo' => 'required',
            'slide_btn_link' => 'required|string',
            'slide_btn_title' => 'required|string',
            'slide_btn_style1' => 'required|string',
            'slide_btn_style' => 'required|string',
            'slide_btn_color' => 'required|string',
        ]);

        $slidesevenement = Slidesevenement::findOrFail($id);

        $slidesevenement->slug = $request->slug;
        $slidesevenement->slide_title = $request->slide_title;
        $slidesevenement->slide_subtitle = $request->slide_subtitle;
        $slidesevenement->slide_btn_style = $request->slide_btn_style;
        $slidesevenement->slide_btn_style1 = $request->slide_btn_style1;
        $slidesevenement->slide_btn_color = $request->slide_btn_color;
        $slidesevenement->slide_btn_link = $request->slide_btn_link;
        $slidesevenement->slide_btn_title = $request->slide_btn_title;
        $slidesevenement->slide_btn_status = $request->slide_btn_status;

        $currentPhoto = $slidesevenement->photo;

        if ($request->photo != $currentPhoto){

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_event/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->save(public_path('assets/img/slides/slides_event/').$name);

            $request->merge(['photo' =>  "/assets/img/slides/slides_event/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }
        $slidesevenement->update($request->all());

        return ['message' => 'updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $slidesevenement = Slidesevenement::findOrFail($id);
        $oldFilename = $slidesevenement->photo;
        File::delete(public_path($oldFilename));

        $slidesevenement->delete();

        return ['message' => 'Deleted successfully '];
    }
}
