<?php

namespace App\Http\Controllers\Admin\Partials\Slides;

use App\Http\Resources\Slides\SlidesHomeResource;
use App\Model\admin\slides\slideshome;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class SlideshomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slides.slide_home.index');
    }

    public function api()
    {
        return SlidesHomeResource::collection(Slideshome::with('user')->latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slides.slide_home.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return SlidesHomeResource|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all()); // pour tester les donner qui entre dans la base de donner
        $this->validate($request,[
            'slide_title' => 'required|string',
            'body' => 'required',
            'slide_btn_position' => 'required',
            'photo' => 'required',
            'slide_btn_link' => 'required|string',
            'slide_btn_title' => 'required|string',
            'slide_btn_style1' => 'required|string',
            'slide_btn_style' => 'required|string',
            'slide_btn_color' => 'required|string',
        ]);

        $slideshome = new Slideshome;

        $slideshome->body                         = $request->body;
        $slideshome->slug                         = $request->slug;
        $slideshome->slide_title                  = $request->slide_title;
        $slideshome->slide_fblink                 = $request->slide_fblink;
        $slideshome->slide_twlink                 = $request->slide_twlink;
        $slideshome->slide_youtubelink            = $request->slide_youtubelink;
        $slideshome->slide_btn_icon               = $request->slide_btn_icon;
        $slideshome->slide_btn_link               = $request->slide_btn_link;
        $slideshome->slide_subtitle               = $request->slide_subtitle;
        $slideshome->slide_instalink              = $request->slide_instalink;
        $slideshome->slide_btn_title              = $request->slide_btn_title;
        $slideshome->slide_btn_style              = $request->slide_btn_style;
        $slideshome->slide_btn_color              = $request->slide_btn_color;
        $slideshome->slide_btn_style1             = $request->slide_btn_style1;
        $slideshome->slide_btn_status             = $request->slide_btn_status;
        $slideshome->slide_youtubelink            = $request->slide_youtubelink;
        $slideshome->slide_btn_position           = $request->slide_btn_position;
        $slideshome->slide_btn_style_fblink       = $request->slide_btn_style_fblink;
        $slideshome->slide_btn_style_twlink       = $request->slide_btn_style_twlink;
        $slideshome->slide_btn_style_instalink    = $request->slide_btn_style_instalink;
        $slideshome->slide_btn_style_youtubelink  = $request->slide_btn_style_youtubelink;
        $slideshome->status = '0';

        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_home/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/slides/slides_home/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/slides/slides_home/{$name}";
            $slideshome->photo = $myfilename;
        }

        $slideshome->save();

        return new SlidesHomeResource($slideshome);
    }

    public function unactive_slide($id)
    {
        DB::table('slideshomes')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
                ]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active_slide($id)
    {
        DB::table('slideshomes')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
                ]);
        return response('activated', Response::HTTP_ACCEPTED);
    }

    public function show($id)
    {
        $slideshome = new SlidesHomeResource(slideshome::where('id', $id)->findOrFail($id));
        return $slideshome;
    }

    public function edit($id)
    {
        $slideshome = Slideshome::where('id',$id)->first();
        return view('admin.slides.slide_home.edit', compact('slideshome'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->validate($request,[
            'slide_title' => 'required|string',
            'body' => 'required',
            'slide_btn_position' => 'required',
            'photo' => 'required',
            'slide_btn_link' => 'required|string',
            'slide_btn_title' => 'required|string',
            'slide_btn_style1' => 'required|string',
            'slide_btn_style' => 'required|string',
            'slide_btn_color' => 'required|string',
        ]);

        $slideshome = Slideshome::findOrFail($id);
        
        $slideshome->body                         = $request->body;
        $slideshome->slug                         = $request->slug;
        $slideshome->slide_title                  = $request->slide_title;
        $slideshome->slide_fblink                 = $request->slide_fblink;
        $slideshome->slide_twlink                 = $request->slide_twlink;
        $slideshome->slide_youtubelink            = $request->slide_youtubelink;
        $slideshome->slide_btn_icon               = $request->slide_btn_icon;
        $slideshome->slide_btn_link               = $request->slide_btn_link;
        $slideshome->slide_subtitle               = $request->slide_subtitle;
        $slideshome->slide_instalink              = $request->slide_instalink;
        $slideshome->slide_btn_title              = $request->slide_btn_title;
        $slideshome->slide_btn_style              = $request->slide_btn_style;
        $slideshome->slide_btn_color              = $request->slide_btn_color;
        $slideshome->slide_btn_style1             = $request->slide_btn_style1;
        $slideshome->slide_btn_status             = $request->slide_btn_status;
        $slideshome->slide_youtubelink            = $request->slide_youtubelink;
        $slideshome->slide_btn_position           = $request->slide_btn_position;
        $slideshome->slide_btn_style_fblink       = $request->slide_btn_style_fblink;
        $slideshome->slide_btn_style_twlink       = $request->slide_btn_style_twlink;
        $slideshome->slide_btn_style_instalink    = $request->slide_btn_style_instalink;
        $slideshome->slide_btn_style_youtubelink  = $request->slide_btn_style_youtubelink;

        $currentPhoto = $slideshome->photo;

        if ($request->photo != $currentPhoto){

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_home/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->save(public_path('assets/img/slides/slides_home/').$name);

            $request->merge(['photo' =>  "/assets/img/slides/slides_home/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }
        $slideshome->update($request->all());

        return ['message' => 'updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $slideshome = Slideshome::findOrFail($id);
        $oldFilename = $slideshome->photo;
        File::delete(public_path($oldFilename));

        $slideshome->delete();

        return ['message' => 'Deleted successfully '];
    }
}
