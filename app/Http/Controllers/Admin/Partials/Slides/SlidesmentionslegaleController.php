<?php

namespace App\Http\Controllers\Admin\Partials\Slides;

use App\Http\Resources\Slides\SlidesMentionslegalesResource;
use App\Model\admin\slides\slidesmentionslegale;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class SlidesmentionslegaleController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slides.slide_mentionslegale.index');
    }
	
	public function api()
    {
        return SlidesMentionslegalesResource::collection(Slidesmentionslegale::with('user')->latest()->get());
    }
	
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return SlidesMentionslegalesResource|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(\request()->all()); // pour tester les donner qui entre dans la base de donner
        $this->validate($request,[
            'slide_title' => 'required|string',
            //'slide_subtitle' => 'required|string',
            //'photo' => 'required'
        ]);

        $slidesmentionslegale = new Slidesmentionslegale;

        $slidesmentionslegale->slug = $request->slug;
        $slidesmentionslegale->slide_title = $request->slide_title;
        $slidesmentionslegale->slide_subtitle = $request->slide_subtitle;
        $slidesmentionslegale->status = '0';

        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_mentionslegales/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/slides/slides_mentionslegales/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/slides/slides_mentionslegales/{$name}";
            $slidesmentionslegale->photo = $myfilename;
        }

        $slidesmentionslegale->save();

        return new SlidesMentionslegalesResource($slidesmentionslegale);
    }
	
	public function unactive_slide($id)
    {
        DB::table('slidesmentionslegales')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active_slide($id)
    {
        DB::table('slidesmentionslegales')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        return response('activated', Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'slide_title' => 'required|string',
            //'slide_subtitle' => 'required|string',
            //'photo' => 'required'
        ]);

        $slidesmentionslegale = Slidesmentionslegale::findOrFail($id);

        $currentPhoto = $slidesmentionslegale->photo;

        if ($request->photo != $currentPhoto) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_mentionslegales/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->save(public_path('assets/img/slides/slides_mentionslegales/').$name);

            $request->merge(['photo' =>  "/assets/img/slides/slides_mentionslegales/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }
        $slidesmentionslegale->update($request->all());

        return ['message' => 'updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $slidesmentionslegale = Slidesmentionslegale::findOrFail($id);
        $oldFilename = $slidesmentionslegale->photo;
        File::delete(public_path($oldFilename));

        $slidesmentionslegale->delete();

        return ['message' => 'Deleted successfully'];
    }
}
