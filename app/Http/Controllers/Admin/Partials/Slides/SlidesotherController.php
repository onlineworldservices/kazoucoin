<?php

namespace App\Http\Controllers\Admin\Partials\Slides;

use App\Http\Resources\Slides\SlidesOthersResource;
use App\Model\admin\slides\slidesother;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use File;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class SlidesotherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slides.slide_other.index');
    }

    public function api()
    {
        return SlidesOthersResource::collection(Slidesother::with('user')->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(\request()->all()); // pour tester les donner qui entre dans la base de donner
        $this->validate($request,[
            'slide_title' => 'required|string',
            'slide_subtitle' => 'required|string',
            'photo' => 'required',
            'slide_btn_link' => 'required|string',
            'slide_btn_title' => 'required|string',
            'slide_btn_style1' => 'required|string',
            'slide_btn_style' => 'required|string',
            'slide_btn_color' => 'required|string',
        ]);

        $slidesother = new Slidesother;

        $slidesother->slug = $request->slug;
        $slidesother->slide_title = $request->slide_title;
        $slidesother->slide_subtitle = $request->slide_subtitle;
        $slidesother->slide_btn_style = $request->slide_btn_style;
        $slidesother->slide_btn_style1 = $request->slide_btn_style1;
        $slidesother->slide_btn_color = $request->slide_btn_color;
        $slidesother->slide_btn_link = $request->slide_btn_link;
        $slidesother->slide_btn_icon = $request->slide_btn_icon;
        $slidesother->slide_btn_title = $request->slide_btn_title;
        $slidesother->slide_btn_status = $request->slide_btn_status;
        $slidesother->status = '0';

        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_others/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/slides/slides_others/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/slides/slides_others/{$name}";
            $slidesother->photo = $myfilename;
        }

        $slidesother->save();

        return new SlidesOthersResource($slidesother);
    }

    public function unactive_slide($id)
    {
        DB::table('slidesothers')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
                ]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active_slide($id)
    {
        DB::table('slidesothers')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
                ]);
        return response('activated', Response::HTTP_ACCEPTED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'slide_title' => 'required|string',
            'slide_subtitle' => 'required|string',
            'photo' => 'required',
            'slide_btn_link' => 'required|string',
            'slide_btn_title' => 'required|string',
            'slide_btn_style1' => 'required|string',
            'slide_btn_style' => 'required|string',
            'slide_btn_color' => 'required|string',
        ]);

        $slidesother = Slidesother::findOrFail($id);

        $slidesother->slug = $request->slug;
        $slidesother->slide_title = $request->slide_title;
        $slidesother->slide_subtitle = $request->slide_subtitle;
        $slidesother->slide_btn_style = $request->slide_btn_style;
        $slidesother->slide_btn_style1 = $request->slide_btn_style1;
        $slidesother->slide_btn_color = $request->slide_btn_color;
        $slidesother->slide_btn_link = $request->slide_btn_link;
        $slidesother->slide_btn_icon = $request->slide_btn_icon;
        $slidesother->slide_btn_title = $request->slide_btn_title;
        $slidesother->slide_btn_status = $request->slide_btn_status;

        $currentPhoto = $slidesother->photo;

        if ($request->photo != $currentPhoto){

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile .'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/slides/slides_others/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->save(public_path('assets/img/slides/slides_others/').$name);

            $request->merge(['photo' =>  "/assets/img/slides/slides_others/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }
        $slidesother->update($request->all());

        return ['message' => 'updated successfully'];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $slidesother = Slidesother::findOrFail($id);
        $oldFilename = $slidesother->photo;
        File::delete(public_path($oldFilename));

        $slidesother->delete();

        return ['message' => 'Deleted successfully '];
    }
}
