<?php

namespace App\Http\Controllers\Admin\Partials;

use App\Model\user\partial\speciality;
use Illuminate\Http\Request;
use App\Http\Resources\SpecialitiesResource;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class SpecialityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partials.contact.speciality.index');
    }

    public function api()
    {
        return SpecialitiesResource::collection(speciality::with('user')->latest()->get());
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'speciality_name'=>'required|string|unique:specialities',
        ]);
        
        $speciality = new speciality;
        $speciality->speciality_name = $request->speciality_name;
        $speciality->status = '0';
        
        $speciality->save();

        return response('Created',Response::HTTP_CREATED);
        
        //alert()->success('Success', "Speciality Created Successfully");
        //toastr()->success('<b>Created Successfully !</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        //return back();
    }

    public function unactive_speciality($id)
    {
        DB::table('specialities')
            ->where('id',$id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
       
        return response('deactivated',Response::HTTP_ACCEPTED);
    }

    public function active_speciality($id)
    {
        DB::table('specialities')
            ->where('id',$id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
      
        return response('Activated',Response::HTTP_ACCEPTED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $speciality = Speciality::findOrFail($id);

        return view('admin.partials.contact.speciality.show', ['speciality' => $speciality]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'speciality_name'=> "required|string|min:2|max:100|unique:specialities,speciality_name,{$id}",
        ]);
        
        $speciality = Speciality::findOrFail($id);
        $speciality->update($request->all());

        return ['message' => 'speciality has ben updated'];

        //$speciality->slug = $request->slug;
        //$speciality->speciality_name = $request->speciality_name;
        //$speciality->admin_id = auth()->user()->id;
        //$speciality->save();


        //alert()->success('Success', "Speciality Updated Successfully");
        //toastr()->success('<b>Update Successfully !</b> ','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>',['timeOut'=>5000]);
        //return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $speciality = Speciality::findOrFail($id);
        $speciality->delete();


        //toastr()->success('<b>Delete Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        //return back();
    }
}
