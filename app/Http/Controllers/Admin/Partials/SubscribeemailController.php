<?php

namespace App\Http\Controllers\Admin\Partials;

use App\Exports\SubscribemailsExport;
use App\Http\Resources\SubscribeemailResource;
use App\Model\admin\subscribemail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class SubscribeemailController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partials.subscribe.index');
    }

    public function api()
    {
        return SubscribeemailResource::collection(subscribemail::latest()->get());
    }

    /**
     * @return BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new SubscribemailsExport(), 'Subscribemails.csv');
    }
}
