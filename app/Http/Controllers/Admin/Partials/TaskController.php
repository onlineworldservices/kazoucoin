<?php

namespace App\Http\Controllers\Admin\Partials;

use App\Model\admin\task;
use App\Services\Admin\MailTaskService;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\TaskResource;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.partials.tasks.index');
    }

    public function api()
    {
        return TaskResource::collection(Task::with('user','administrator','note')
            ->orderBy('created_at','DESC')->paginate(6));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'administrator_id'=>'required',
            'note_id'=>'required|unique:tasks',
        ]);

        $task = new Task;
        $task->administrator_id = $request->administrator_id;
        $task->note_id = $request->note_id;
        $task->user_id = Auth::user()->id;

        $task->save();

        MailTaskService::newTask($request);
        return response('Created',Response::HTTP_CREATED);
    }


    /**
     * cette parti consite a ajouter le progress
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function updateProgress(Request $request, $id)
    {
        $this->validate($request,[
            'progress'=> "nullable|integer|min:1|max:100|digits_between: 1,9",
        ]);

        $task = task::findOrFail($id);
        $task->progress = $request->progress;
        $task->user_id = Auth::user()->id;

        $this->authorize('update',$task);
        $task->save();

        return response('Progress Task update', Response::HTTP_ACCEPTED);
    }


    public function updateDescription(Request $request, $id)
    {
        $this->validate($request,[
            'description'=> 'required',
        ]);

        $task = task::findOrFail($id);
        $task->description = $request->description;
        $task->user_id = Auth::user()->id;

        $this->authorize('update',$task);
        $task->save();

        return response('Description Task update', Response::HTTP_ACCEPTED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($username)
    {
        if($username) {
            $user = User::where('username', $username)->firstOrFail();
        } else {
            $user = User::findOrFail(auth()->user()->id);
        }
        $usertasks = $user->tasks()->latest()->get();
        return view("admin.partials.tasks.view",compact('usertasks'))->withUser($user);
    }

    public function usertask($username)
    {
        if($username) {
            $user = User::where('username', $username)->firstOrFail();
        } else {
            $user = User::findOrFail(auth()->user()->id);
        }
        $usertasks = TaskResource::collection($user->tasks()
            ->orderBy('updated_at','DESC')->paginate(6));
        return $usertasks;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'administrator_id'=>'required',
            'note_id'=>"required|unique:tasks,note_id,{$id}",
        ]);

        $task = task::findOrFail($id);

        $task->administrator_id = $request->administrator_id;
        $task->note_id = $request->note_id;
        $task->user_id = Auth::user()->id;

        $task->save();

        return ['message' => 'color has ben updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return ['message' => 'task deleted '];
    }
}
