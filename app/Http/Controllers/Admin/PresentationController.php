<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\PresentationResource;
use App\Model\user\partial\color;
use App\Model\admin\presentation;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\Response;


class PresentationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.presentation.index');
    }

    public function api()
    {
        return PresentationResource::collection(presentation::with('user')->latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.presentation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return PresentationResource|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all()); // pour tester les donner qui entre dans la base de donner
        $this->validate($request,[
            'title'=>'required',
            'icon'=>'required',
            'color_name'=>'required',
            'body'=>'required',
            'photo'=>'required',
        ]);

        $presentation = new Presentation;

        $presentation->title = $request->title;
        $presentation->color_name = $request->color_name;
        $presentation->mySlug = $request->mySlug;
        $presentation->icon = $request->icon;
        $presentation->body = $request->body;
        $presentation->status = '0';
        
        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/presentation/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/presentation/{$name}");
            Image::make($request->photo)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/presentation/{$name}";
            $presentation->photo = $myfilename;
        }

        $presentation->save();

        return new PresentationResource($presentation);
    }

    /**
     * cette partie consite a activer et a desactiver
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function disable($id)
    {
        DB::table('presentations')
            ->where('id', $id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active($id)
    {
        DB::table('presentations')
            ->where('id', $id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);

        return response('Activated', Response::HTTP_ACCEPTED);
    }

    public function show($id)
    {
        $presentation = new PresentationResource(presentation::where('id', $id)->findOrFail($id));
        return $presentation;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function vector(presentation $presentation)
    {
        return view('admin.presentation.view', [
            'presentation' => $presentation,
        ]);
    }

    public function view($slug)
    {
        $presentation = new PresentationResource(presentation::where('mySlug',$slug)->firstOrFail());
        return $presentation;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $presentation = presentation::where('id',$id)->findOrFail($id);

        return view('admin.presentation.edit', compact('presentation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd(request()->all());
        $this->validate($request,[
            'title'=>'required',
            'icon'=>'required',
            'body'=>'required',
        ]);

        $presentation = Presentation::findOrFail($id);

        $currentPhoto = $presentation->photo;

        if ($request->photo != $currentPhoto){

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile.'.' . explode('/',explode(':',substr($request->photo,0,strpos
                ($request->photo,';')))[1])[1];

            $dir = 'assets/img/presentation/';
            if(!file_exists($dir)){
                mkdir($dir, 0775, true);
            }
            Image::make($request->photo)->fit(400,400)->save(public_path('assets/img/presentation/').$name);


            $request->merge(['photo' =>  "/assets/img/presentation/{$name}"]);

            // Ici on suprimme l'image existant
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }
        $presentation->update($request->all());

        return ['message' => 'updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $presentation = Presentation::findOrFail($id);
        $oldFilename = $presentation->photo;
        File::delete(public_path($oldFilename));

        $presentation->delete();

        return ['message' => 'Deleted successfully '];
    }
}
