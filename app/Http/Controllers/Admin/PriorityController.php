<?php

namespace App\Http\Controllers\Admin;

use App\Exports\PrioritiesNotesExports;
use App\Http\Controllers\Controller;
use App\Http\Resources\PriorityResource;
use App\Model\admin\priority;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response;

class PriorityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.priority.index');
    }

    public function api()
    {
        return PriorityResource::collection(priority::with('user')->latest()->get());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new PrioritiesNotesExports(), 'Priorities-Notes.csv');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'priority_name'=>'required|string|unique:priorities',
        ]);

        $priority = new priority;
        $priority->slug = $request->slug;
        $priority->status = '0';
        $priority->priority_name = $request->priority_name;
        $priority->priority_color = $request->priority_color;
        $priority->priority_icon = $request->priority_icon;

        $priority->save();


        return response('Created',Response::HTTP_CREATED);
    }


    public function status(priority  $priority,$id)
    {
        $priority = priority::where('id', $id)->findOrFail($id);
        $priority->update(['status' => !$priority->status]);

        return response('Update', Response::HTTP_ACCEPTED);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\admin\priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function show(priority $priority)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\admin\priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function edit(priority $priority)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\admin\priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'priority_name'=> "required|string|min:2|max:25|unique:priorities,priority_name,{$id}",
        ]);

        $priority = priority::findOrFail($id);
        $priority->update($request->all());

        return ['message' => 'Priority has been updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\admin\priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $priority = priority::findOrFail($id);
        $priority->delete();

        return ['message' => 'Priority deleted '];
    }
}
