<?php

namespace App\Http\Controllers\Admin;

use App\Exports\StatusTasksExports;
use App\Http\Controllers\Controller;
use App\Http\Resources\StatusResource;
use App\Model\admin\status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response;

class StatusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.status.index');
    }

    public function api()
    {
        return StatusResource::collection(status::with('user')->latest()->get());
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new StatusTasksExports(), 'Status-Tasks.csv');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'status_name'=>'required|string|unique:statuses',
        ]);

        $status = new status;
        $status->slug = $request->slug;
        $status->status = '0';
        $status->status_name = $request->status_name;
        $status->status_color = $request->status_color;

        $status->save();

        return response('Created',Response::HTTP_CREATED);
    }

    public function status(status  $status,$id)
    {
        $status = status::where('id', $id)->findOrFail($id);
        $status->update(['status' => !$status->status]);

        return response('Update', Response::HTTP_ACCEPTED);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\admin\priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function show(priority $priority)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\admin\priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function edit(priority $priority)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\admin\priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'status_name'=> "required|string|min:2|max:25|unique:statuses,status_name,{$id}",
        ]);

        $status = status::findOrFail($id);
        $status->update($request->all());

        return ['message' => 'Status has been updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\admin\priority  $priority
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = status::findOrFail($id);
        $status->delete();

        return ['message' => 'Status deleted'];
    }
}
