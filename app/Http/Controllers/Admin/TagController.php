<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\TagResource;
use App\Model\user\tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tag.index');
    }

    public function api()
    {
        return TagResource::collection(Tag::with('user')->latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return TagResource
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);

        $tag = new Tag;
        $tag->slug = $request->slug;
        $tag->name = $request->name;
        $tag->status = '0';
        //$tag->admin_id = auth()->user()->id;
        $tag->save();

        return response('Created',Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    
    public function status(tag  $tag,$id)
    {
        $tag = tag::where('id', $id)->findOrFail($id);
        $tag->update(['status' => !$tag->status]);

        return response('Update', Response::HTTP_ACCEPTED);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=> "required",
            'slug'=> "required"
        ]);

        $tag = Tag::findOrFail($id);

        $tag->update($request->all());

        return ['message' => 'tag has ben updated'];

        //$tag->name = $request->name;
        //$tag->slug = $request->slug;
        //$tag->admin_name = auth()->user()->name;
        //$tag->save();


        //alert()->success('Success', "Tag Updated Successfully");
        //return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();

        //session()->put('success','Item created successfully.');
        //toastr()->success('<b>Deleted Successfully</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
       //Alert::success('Deleted!', 'Your file has been deleted.');
       //return redirect()->back();
    }

    public function deleteMultiple(Request $request){

        $ids = $request->ids;

        //About::whereIn('id',explode(",",$ids))->delete();

        DB::table("tags")->whereIn('id',explode(",",$ids))->delete();

        return response()->json(['status'=>true,'message'=>"Message Tag deleted successfully."]);

    }
}
