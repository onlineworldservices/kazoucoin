<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\TestimonialResource;
use App\Model\admin\testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class TestimonialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.testimonial.index');
    }

    public function api()
    {
        return TestimonialResource::collection(Testimonial::with('user')->latest()->get());
    }

    public function show($id)
    {
        $testimonial = new TestimonialResource(testimonial::where('id',$id)->firstOrFail());
        return $testimonial;
    }
   
    public function create()
    {
        return view('admin.testimonial.create');
    }

    public function edit($id)
    {
        $testimonial = testimonial::where('id', $id)->findOrFail($id);
        return view('admin.testimonial.edit', compact('testimonial'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(\request()->all()); // pour tester les donner qui entre dans la base de donner
        $this->validate($request,[
            'fullname' => 'required|string|unique:abouts|max:255',
            'body' => 'required',
            'stars_evaluation' => 'required',
            'role' => 'required|string|max:100',
            'photo' => 'required',
        ]);

        $testimonial = new Testimonial;

        $testimonial->body = $request->body;
        $testimonial->role = $request->role;
        $testimonial->fullname = $request->fullname;
        $testimonial->stars_evaluation = $request->stars_evaluation;
        $testimonial->status = '0';

        if ($request->photo) {

            $namefile = sha1(date('YmdHis') . str_random(30));

            $name = $namefile. '.' . explode('/', explode(':', substr($request->photo, 0, strpos
                ($request->photo, ';')))[1])[1];

            $dir = 'assets/img/testimonial/';
            if (!file_exists($dir)) {
                mkdir($dir, 0775, true);
            }
            $destinationPath = public_path("assets/img/testimonial/{$name}");
            Image::make($request->photo)->fit(400,400)->save($destinationPath);

            //Save Image to database
            $myfilename = "/assets/img/testimonial/{$name}";
            $testimonial->photo = $myfilename;
        }

        $testimonial->save();

        return response('Created', Response::HTTP_CREATED);
    }

    public function disable($id)
    {
        DB::table('testimonials')
            ->where('id',$id)
            ->update(['status' => 0]);
        return response('deactivated', Response::HTTP_ACCEPTED);
    }

    public function active($id)
    {
        DB::table('testimonials')
            ->where('id',$id)
            ->update(['status' => 1]);
        return response('activated', Response::HTTP_ACCEPTED);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function vector(testimonial $testimonial)
    {
        return view('admin.testimonial.view', [
            'testimonial' => $testimonial,
        ]);
    }

    public function view($slug)
    {
        $testimonial = new TestimonialResource(testimonial::where('slug',$slug)->firstOrFail());
        return $testimonial;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            //'fullname' => "required|string|max:255|unique:abouts,fullname,{$id}",
            'body' => 'required',
            'role' => 'required|string|max:100',
        ]);

        $testimonial = Testimonial::findOrFail($id);

        $testimonial->body = $request->body;
        $testimonial->role = $request->role;
        $testimonial->fullname = $request->fullname;

        $testimonial->save();

        return ['message' => 'updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $testimonial = Testimonial::findOrFail($id);

        $testimonial->delete();

        return ['message' => 'Deleted successfully'];
    }

}
