<?php

namespace App\Http\Controllers\Admin;


use App\Http\Resources\UserResource;
use App\User;
use App\Model\user\login;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class UsersregisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.partials.user.index');
    }

    public function api()
    {
        return UserResource::collection(User::with('my_country')
            ->latest()->get());
    }
     
    public function user()
    {
        return UserResource::collection(User::with('my_country')
            ->where('my_status',null)
            ->latest()->get());
    }

    /**
     * @return BinaryFileResponse
     */
   public function export()
   {
       return Excel::download(new UsersExport(), 'users.csv','Html');
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',

        ]);

        if ($request->has('password') && !empty($request->password)) {
           $password = trim($request->password);
        } else {
            $length = 10;
            $keyspace = '2y$10$d4gjolCjg/VLPbpbRkNuNOKneeSEoP3dQao6iEHEiY65S31QTAAvW';
            $str= '';
            $max = mb_strlen($keyspace,'8bit') -1;
            for ($i = 0; $i< $length; ++$i){
             $str .= $keyspace[random_int(0,$max)];
            }
            $password =  $str;
        }

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($password);

        if ($user->save()){
            return redirect()->route('users.index', $user->id);
        }else{
            alert()->error('Oops...', 'Quelque chose s\'est mal passé  avec la ceation de l\'utilisateur!');
            return redirect()->route('users.create');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email'=> "required|string|email|min:2|max:255|unique:users,email,{$id}",
            'username'=> "required|string|min:2|max:25|unique:users,username,{$id}",
        ]);

        $user = User::findOrFail($id);
        $user->update($request->all());

        return ['message' => 'user has ben updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user= User::findOrFail($id);
        $user->delete();

        return ['message' => 'user deleted'];
    }
}
