<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\AidecontactdemandeResource;
use App\Http\Resources\AidecontactmessageResource;
use App\Http\Resources\AidecontactthemeResource;
use App\Model\admin\aidecontactmessage;
use App\Model\admin\aidecontacttheme;
use App\Model\admin\aidecontactdemande;
use App\Model\user\partial\assistancemessage;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Response;

class AidecontactmessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $auth;

    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('admin.partials.contact.assistance.index');
    }

    public function api()
    {
        return AidecontactmessageResource::collection(Aidecontactmessage::with('user', 'aidecontactdemande', 'aidecontacttheme')->latest()->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $user = $this->auth->user();
        return view('site.page.aide_contact',compact('user'));
    }
    
    public function themes()
    {
        return AidecontactthemeResource::collection(Aidecontacttheme::with('user')->where('status', 1)->latest()->get());
    }

    public function demandes()
    {
        return AidecontactdemandeResource::collection(Aidecontactdemande::with('user')->where('status', 1)->latest()->get());
    }


    /**
     * cette parti consite a activer et a desactiver
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function active($id)
    {
        DB::table('aidecontactmessages')
            ->where('id', $id)
            ->update([
                'status' => 1,
                'user_id' => auth()->user()->id,
            ]);
        return response('Message red', Response::HTTP_ACCEPTED);
    }

    public function disable($id)
    {
        DB::table('aidecontactmessages')
            ->where('id', $id)
            ->update([
                'status' => 0,
                'user_id' => auth()->user()->id,
            ]);
        return response('deactivated message red', Response::HTTP_ACCEPTED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all());
        $this->validate($request, [
            'object' => 'required|max:100',
            'body' => 'required|min:3',
            'aidecontacttheme_id' => 'required',
            'aidecontactdemande_id' => 'required',
        ]);
        
        $aidecontactmessage = new Aidecontactmessage;
        
        $aidecontactmessage->object = $request->object;
        $aidecontactmessage->body = $request->body;
        $aidecontactmessage->aidecontacttheme_id = $request->aidecontacttheme_id;
        $aidecontactmessage->aidecontactdemande_id = $request->aidecontactdemande_id;
        $aidecontactmessage->status = '0';
            
        $aidecontactmessage->save();
        return response('Created', Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array
     */
    public function destroy($id)
    {
        $aidecontactmessage = Aidecontactmessage::findOrFail($id);
        $aidecontactmessage->delete();

        return ['message' => 'message deleted '];
    }
}
