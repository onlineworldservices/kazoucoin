<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Model\admin\slides\slidescontact;
use App\Model\user\partial\contact;
use App\Rules\Recaptcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;


Class ContactController extends Controller
{
    public function create()
    {
        $expiresAt = now()->addSeconds(10);
        $slidescontacts = Cache::remember('slidescontacts', $expiresAt, function () {
            return Slidescontact::where('status',1)->orderBy('created_at','DESC')->get();
        });
        return view('site.contact.create',compact('slidescontacts'));
    }

    /**
     * Store a newly created resource in storage.
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request,Recaptcha $recaptcha)
    {
        $this->validate($request,[
            'name'    => 'required|max:100',
            'lastname'    => 'required|max:100',
            'email'   => 'required|email',
            'subject' => 'required|min:2|max:200',
            'msg'     => 'required|min:2|max:50000',
            'recaptcha' => ['required', $recaptcha],
        ]);
        $contact = new Contact;

        $contact->name = $request->name;
        $contact->lastname = $request->lastname;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->slug = $request->slug;
        $contact->subject = $request->subject;
        $contact->msg = $request->msg;
        $contact->status = '0';


        $contact->save();

        Mail::send('emails.contact',
            array(
                'name' => $request->get('name'),
                'lastname' => $request->get('lastname'),
                'phone' => $request->get('phone'),
                'subject' => $request->get('subject'),
                'email' => $request->get('email'),
                'msg' => $request->get('msg')
            ),
            function ($message) {
                $message->from('hello@outlined.co');
                $message->to('dasgivemoi@gmail.com', 'kazoucoin')
                    ->subject('New Message send from contact page');
            });

        alert()->success('Success', "Thank you for your message");
        return back();
    }

}
