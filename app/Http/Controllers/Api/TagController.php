<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\TagResource;
use App\Model\user\tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return TagResource::collection(Tag::latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return TagResource|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[

            'name'=>'required',

        ]);

        return Tag::create([
            /// bon a savoir le slug se cree automatiquement pas besoin
           'name' => $request['name'],
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tag $tag)
    {
        $this->validate($request,[

            'name'=>'required',

        ]);
        $tag->update(request()->all());

        return ['message' => 'tag has been updated'];
    }



    public function status(tag  $tag,$id)
    {
        $tag = tag::where('id', $id)->findOrFail($id);
        $tag->update(['status' => !$tag->status]);

        return response('Update', Response::HTTP_ACCEPTED);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = tag::findOrFail($id);
        $tag->delete();

        return ['message' => 'tag deleted '];
    }
}
