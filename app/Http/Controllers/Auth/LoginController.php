<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\admin\coverpage;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;




    //protected $redirectTo = 'back';

    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $expiresAt = now()->addMinutes(5);
        $coverpages = Cache::remember('coverpages', $expiresAt, function () {
            return Coverpage::where('status',1)->orderBy('created_at')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                ];
            })->toArray();
        });
        return view('auth.login',compact('coverpages'));
    }

    protected function sendFailedLoginResponse(Request $request)
    {

        if ( ! User::where('username', $request->username)->first() ) {
            throw ValidationException::withMessages([
                $this->username() => Lang::get('auth.username'),
            ]);
        }


        if ( ! User::where('username', $request->username)->where('password', bcrypt($request->password))->first() ) {
            throw ValidationException::withMessages([
                'password' => Lang::get('auth.password'),
            ]);
        }


    }

    function authenticated(Request $request, $user)
    {
        $user->update([

            'last_login_ip' => $request->getClientIp()
        ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','UserLogout');
    }



    public function username()
    {
        $loginType = request()->input('username');
        $this->username = filter_var($loginType, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$this->username => $loginType]);

        return property_exists($this, 'username') ? $this->username : 'email';
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);


        //toastr()->success('<strong>Welcome</strong>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>', ['timeOut'=>5000]);
        //Alert::success('Welcome', 'Welcome to you'.' '.Auth::user()->username.'');
        Alert::success('Welcome ' .Auth::user()->username, '');
        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }


    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UserLogout()
    {
        Auth::guard('web')->logout();



       // toastr()->success('See you soon!', '',['timeOut'=>5000]);
        //toastr()->success('<b>See you soon !</b> <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>','',['timeOut'=>5000]);
        toast('You are successfully disconnected','success','top-right');

        return back();
    }

}









