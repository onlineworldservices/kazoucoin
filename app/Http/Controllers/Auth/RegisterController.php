<?php

namespace App\Http\Controllers\Auth;

use App\Mail\Welcome;
use App\Model\admin\coverpage;
use App\Model\admin\presentation;
use App\Model\admin\profiles\profilelogin;
use App\User;
use App\Notifications\RegisteredUsers;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use RealRashid\SweetAlert\Facades\Alert;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {

        $expiresAt = now()->addMinutes(1);
        $coverpages = Cache::remember('coverpages', $expiresAt, function () {
            return Coverpage::where('status',1)->orderBy('created_at')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                ];
            })->toArray();
        });
        $presentations = Presentation::where('status',1)->with('user')->orderBy('created_at','ASC')->get();

        return view('auth.register',compact('presentations','coverpages'));
    }
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';



    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        alert()->success('Success','Votre compte a été bien créer bien vouloir vérifier votre address mail pour la confirmation!')->persistent('Close Me!');
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|alpha_dash|string|unique:users,username',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            "status" => "required|in:Yes",
            //'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
