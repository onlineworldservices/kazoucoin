<?php

namespace App\Http\Controllers;


use App\Model\admin\profiles\homepageprofile;
use App\Model\admin\slides\slideshome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expiresAt = now()->addSeconds(5);
        $slideshomes = Cache::remember('slideshomes', $expiresAt, function () {
            return Slideshome::where('status',1)->orderBy('created_at','DESC')->get();
        });

        $homepageprofiles = Cache::remember('homepageprofiles', $expiresAt, function () {
            return Homepageprofile::where('status',1)->orderBy('created_at','DESC')->get();
        });
        return view('site.index',compact('slideshomes','homepageprofiles'));
    }
}
