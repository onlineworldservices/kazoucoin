<?php

namespace App\Http\Controllers;


use App\Http\Resources\Site\Orders\BeauteResource;
use App\Model\admin\categories\categoryevent;
use App\Model\admin\categories\categoryorder;
use App\Model\user\article;
use App\Model\user\event;
use App\Model\user\myaccount;
use App\Model\user\order\beaute;
use App\Model\user\tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;


class MyaccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['only' => [
            'index',
            'toggle',
            'home',

        ]]);
    }


    public function index()
    {

        return redirect()->route('myaccount.home');
    }



    public function toggle(Request $request)
    {

        $user = User::find($request->user_id);
        $data= auth()->user()->toggleFollow($user);
        return response()->json(['success'=>$data]);
    }



    public function home()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);


        return view('site.home', [

            'events'=> $user->events,
            'articles'=> $user->articles,

        ]);
    }

    // View Profile
    public function viewProfile($username) {

        if($username) {
            $user = User::where('username', $username)->firstOrFail();
        } else {
            $user = User::findOrFail(auth()->user()->id);
        }

        $categoryevents = categoryevent::where('status',1)->orderBy('created_at')->get();
        $categoryorders = categoryorder::where('status',1)->orderBy('created_at')->get();
        return view('site.user.account', [

            'user' => $user,
            'categoryevents' => $categoryevents,
            'categoryorders' => $categoryorders,
            'events' => $user->events,



        ]);

    }


    /**
     * @param $username
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    //Ici nous accedons au lien
    public function orders($username)
    {
        if($username) {
            $user = User::where('username', $username)->firstOrFail();
        } else {
            $user = User::findOrFail(auth()->user()->id);
        }

        $userbeautes = $user->beautes()->latest()->get();
        return view('site.order.beaute.user_orders',compact('user','userbeautes'));

    }

    //Ici nous recuperon tous les infirmation de l'utilisateur a traver l'API
    public function GetOrdersByUser($username)
    {
        if($username) {
            $user = User::where('username', $username)->firstOrFail();
        } else {
            $user = User::findOrFail(auth()->user()->id);
        }
        $userbeautes = BeauteResource::collection($user->beautes()->latest()->paginate(30));
        return $userbeautes;

    }

}
