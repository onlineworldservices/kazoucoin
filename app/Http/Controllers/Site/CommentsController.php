<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\StoreCommentRequest;
use App\Model\admin\categories\menucategoryorder;
use App\Model\user\comment;
use App\Model\user\event;
use App\Model\user\order\informatique;
use App\Notifications\NewCommentEvent;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;


class CommentsController extends Controller
{

    public function __construct() {
        //$this->middleware('auth');

        $this->middleware('auth',['except' => ['index','show']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(menucategoryorder $menucategoryorder,informatique $informatique)
    {
        $comments = $informatique->comments()->with('user')->latest()->get();
        return $comments;
       // $comments = Comment::allFor(Input::get('type'), Input::get('id'));
       // return Response::json($comments,200,[],JSON_NUMERIC_CHECK);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(menucategoryorder $menucategoryorder,informatique $informatique)
    {

        $comment = $informatique->comments()->create(request()->all());
        return $comment->load('user');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(menucategoryorder $menucategoryorder,informatique $informatique,comment $comment)
    {
        $comment->update(request()->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(menucategoryorder $menucategoryorder,informatique $informatique,comment $comment)
    {
        $comment->delete();
        return response("Goood job commentaire suprimer");

    }
}
