<?php

namespace App\Http\Controllers\Site\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\BeauteRequest;
use App\Http\Resources\Category\CategoryorderResource;
use App\Http\Resources\Category\MenucategoryorderResource;
use App\Http\Resources\ColorResource;
use App\Http\Resources\DeviseResource;
use App\Http\Resources\Site\Orders\BeauteResource;
use App\Model\admin\categories\categoryorder;
use App\Model\admin\categories\menucategoryorder;
use App\Model\admin\devise;
use App\Model\admin\slides\slidesorder;
use App\Model\user\order\beaute;
use App\Model\user\order\beautes_photos;
use App\Model\user\partial\color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class BeauteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','getbeautes','categoryorder','menucategoryorder','getItembycategory','getcategory','view','show']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expiresAt = now()->addSeconds(10);
        $slidesorders = Cache::remember('slidesorders', $expiresAt, function () {
            return Slidesorder::where('status', 1)->orderBy('created_at', 'DESC')->get();
        });
        return view('site.order.beaute.index', compact('slidesorders'));
    }

    /**
     * Dashboard get beaute
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        return view('admin.site.beaute');
    }

    /**
     *Recuperation de tous les informations dans le dashboard admin/orders/beautes
     *Ici on recuperation des donnees pour le reutiliser sour forme de Json
     */
    public function getbeautedashboard()
    {
        $beautes = BeauteResource::collection(beaute::with('user', 'menucategoryorder', 'country','devise')->latest()->get());
        return $beautes;
    }

    /**
     * Ici on recupere tous les articles dans le orders/beautes
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getbeautes()
    {
        $beautes = BeauteResource::collection(beaute::with('user', 'menucategoryorder', 'country','devise')
            ->where('status_user', 1)
            ->where('status_admin', 1)
            ->latest()->get());
        return $beautes;
    }

    /**
     * @param $slug
     * Recuperation des articles par category
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getItembycategory($slug)
    {


        $beautes = BeauteResource::collection(menucategoryorder::whereSlug($slug)->firstOrFail()->beautes()
            ->with('user', 'menucategoryorder', 'country','devise')
            ->where('status_user', 1)
            ->where('status_admin', 1)
            ->latest()->paginate(30));
        return $beautes;
    }

    public function getcategory($slug)
    {

        $expiresAt = now()->addSeconds(10);
        $slidesorders = Cache::remember('slidesorders', $expiresAt, function () {
            return Slidesorder::where('status', 1)->orderBy('created_at', 'DESC')->get();
        });

        $categoryorders = categoryorder::where('status', 1)->get();
        $menucategoryorders = menucategoryorder::with('categoryorder')->where('status', 1)->get();
        $beautes = menucategoryorder::whereSlug($slug)->firstOrFail()->beautes()->with('user', 'menucategoryorder', 'country','devise')->latest()->paginate(3);
        return view('site.order.beaute.CategoryTag', compact('slidesorders', 'beautes', 'menucategoryorders', 'categoryorders'));
    }


    public function categoryorder()
    {
        $categoryorder = CategoryorderResource::collection(categoryorder::with('user')
            ->where('status', 1)
            ->latest()->get());

        return $categoryorder;
    }

    public function menucategoryorder()
    {
        return MenucategoryorderResource::collection(menucategoryorder::with('user', 'categoryorder')
            ->where('status', 1)
            ->where('categoryorder_name', 'beautes')
            ->latest()->get());
    }

    public function color()
    {
        return ColorResource::collection(color::with('user')
            ->where('status', 1)
            ->latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('site.order.beaute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
  ////public function store(BeauteRequest $request)
  //{

  //    $beaute= new beaute;

  //    $beaute->create($request->all());

  //    return response('Created', Response::HTTP_CREATED);
  //}


   //public function store(Request $request)
   //{
   //    $beaute = beaute::create($request->all());

   //    foreach ($request->photos as $photo){

   //        $dir = 'assets/img/beaute/';
   //        if(!file_exists($dir)){
   //            mkdir($dir, 0775, true);
   //        }
   //        $destinationPath = 'assets/img/beaute';
   //        $filename = $photo->move($destinationPath,$photo->getClientOriginalName());
   //        beautes_photos::create([
   //            'beaute_id' => $beaute->id,
   //            'filename' => $filename,
   //        ]);
   //    }
   //    return response('Created', Response::HTTP_CREATED);
   //}

    public function store(BeauteRequest $request)
    {
        //$beaute = beaute::create($request->all());
        auth()->user()->beautes()->create($request->all());

        //$imageName = time().'.'.$request->file->getClientOriginalExtension();

        //$request->file->move(public_path('assets/img/beaute'), $imageName);
        //$request->file->move(public_path('assets/img/beaute'),$request->file->getClientOriginalName());


        //$photo = $request->file;

       //$dataArr = is_array($request->file('file') ) ? $request->file()  : array($request->file('file') );
       //foreach ( $dataArr as $photo){


       //    $name = sha1(date('YmdHis') . str_random(30));
       //    $save_name = $name . '.' . $photo->getClientOriginalExtension();

       //    $dir = 'assets/img/beaute/';
       //    if(!file_exists($dir)){
       //        mkdir($dir, 0775, true);
       //    }
       //    $destinationPath = public_path('assets/img/beaute/');
       //    $photo->move($destinationPath,$save_name);

       //    $myfilename = "/assets/img/beaute/{$save_name}";
       //    return beautes_photos::create([
       //        //'beaute_id' => $beaute->id,
       //        'filename' => $myfilename,
       //    ]);
       //}


        return response()->json(['success'=>'You have successfully upload file.']);
    }


    public function upluoad()
    {

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Model\user\order\beaute $beaute
     * @return BeauteResource
     */
    // public function show(Beaute $beaute)
    // {
    //     //return new BeauteResource(beaute::where('id',$id)->findOrFail($id));
    //     return new BeauteResource($beaute);
    // }
    public function show($id)
    {
        //$beaute = new BeauteResource(beaute::where('id',$id)->findOrFail($id));
        return new BeauteResource(beaute::where('id', $id)->findOrFail($id));
        //return ($beaute);
    }


    public function view(menucategoryorder $menucategoryorder, beaute $beaute)
    {
        //conter les visiteurs sur le site
        $beaute->visits()->increment();

        $beautes = beaute::with('user')
            ->with('menucategoryorder')
            ->take(4)
            ->get();

        $slidesorders = slidesorder::where('status', 1)->orderBy('created_at', 'DESC')->get();

        return view('site.order.beaute.view', [

            'beaute' => $beaute,
            'slidesorders' => $slidesorders,
            'beautes' => $beautes,

        ]);
    }

    /**
     * cette parti consite a donner la permission a l'administrateur d'activer et a desactiver le post
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function admindisable(beaute $beaute,$id)
    {
        $beaute = beaute::where('id', $id)->findOrFail($id);
            $beaute->update([
                'status_admin' => 0,
            ]);
        return response('deactivated', Response::HTTP_ACCEPTED);

    }

    public function adminactive($id)
    {
        DB::table('beautes')
            ->where('id',$id)
            ->update([
                'status_admin' => 1,
            ]);

        return response('Activated', Response::HTTP_ACCEPTED);

    }

    /**
     * Cette partie consite a donner la permission a l'utilisatue d'activer et a desactiver son post
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function userdisable($id)
    {
        $beaute = beaute::where('id', $id)->findOrFail($id);
        if(Auth::user()->id === $beaute->user_id){

            DB::table('beautes')
                ->where('id',$id)
                ->update([
                    'status_user' => 0,
                    'user_id' => auth()->user()->id,
                ]);
            return response('deactivated', Response::HTTP_ACCEPTED);
        }
        return response('Error disable', Response::HTTP_ACCEPTED);

    }

    public function useractive($id)
    {

        $beaute = beaute::where('id', $id)->findOrFail($id);
        if(Auth::user()->id === $beaute->user_id){

            DB::table('beautes')
                ->where('id',$id)
                ->update([
                    'status_user' => 1,
                    'user_id' => auth()->user()->id,
                ]);
            return response('Activated', Response::HTTP_ACCEPTED);
        }
        return response('Error activated', Response::HTTP_ACCEPTED);

    }


    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function likePost(Request $request,$id)
    {

        $beaute = beaute::where('id', $id)->findOrFail($id);

        $response = auth()->user()->toggleLike($beaute);
        return response()->json(['success'=>$response]);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\user\order\beaute $beaute
     * @return BeauteResource
     */
    public function edit($id)
    {
        $beaute = beaute::where('id', $id)->findOrFail($id);
        $this->authorize('update',$beaute);
        if(auth()->user()->id === $beaute->user_id) {
            //return new BeauteResource(beaute::where('id',$id)->findOrFail($id));
            return view('site.order.beaute.edit', compact('beaute'));
        }else{
            toastr()->error('<strong>Unauthorized</strong>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>', ['timeOut' => 5000]);
            return back()
                ->with('error',"Unauthorized edit this article contact Author.");
        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Model\user\order\beaute $beaute
     * @return BeauteResource|array|\Illuminate\Http\Response
     */
    public function update(BeauteRequest $request, $id)
    {

        $beaute = beaute::findOrFail($id);
        $beaute->update($request->all());

        return ['message' => 'Beaute has ben updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\user\order\beaute $beaute
     * @return array|\Illuminate\Http\Response
     */

    public function destroy(Request $request, $id)
    {
        $beaute = beaute::findOrFail($id);
        //$oldFilename = $beaute->photo;
        //File::delete(public_path($oldFilename));
        $beaute->delete();

        return ['message' => 'Deleted successfully '];
    }
}
