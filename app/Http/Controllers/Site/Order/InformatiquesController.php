<?php

namespace App\Http\Controllers\Site\Order;

use App\Model\admin\categories\categoryorder;
use App\Model\admin\categories\menucategoryorder;
use App\Model\admin\slides\slidesorder;
use App\Model\user\order\informatique;
use App\Model\user\partial\color;
use App\Model\user\tag;
use App\Model\user\upload;
use App\Model\user\view;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;


class InformatiquesController extends Controller
{
    private $photos_path;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show','getcategory', 'view','informatique']]);

        $this->photos_path = public_path('/images');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->renderIndex((new Informatique())->newQuery());
    }

    public function renderIndex($informatiqueQuery)
    {
        $slidesorders = slidesorder::where('status',1)->orderBy('created_at','DESC')->get();
        $categoryorders = categoryorder::where('status',1)->get();
        $menucategoryorders = menucategoryorder::with('categoryorder')->where('status',1)->get();
        $informatiques = $informatiqueQuery
            ->with(['user','menucategoryorder'=>function($query){
                $query->where('status',1);
            }])->orderBy('created_at','desc')->paginate(12);

        return view('site.order.informatique.index',[

            'slidesorders' => $slidesorders,
            'categoryorders' => $categoryorders,
            'menucategoryorders' => $menucategoryorders

        ]);
    }


    public function getcategory($slug)
    {
        $expiresAt = now()->addSeconds(10);
        $slidesorders = Cache::remember('slidesorders', $expiresAt, function () {
            return Slidesorder::where('status',1)->orderBy('created_at','DESC')->get();
        });
        $categoryorders = categoryorder::where('status',1)->get();
        $menucategoryorders = menucategoryorder::with('categoryorder')->where('status',1)->get();

        $informatiques = menucategoryorder::whereSlug($slug)->firstOrFail()->informatiques()->with('user','menucategoryorder')->orderBy('created_at','desc')->paginate(12);

        return view('site.order.informatique.index',compact('informatiques','slidesorders','categoryorders','menucategoryorders'));


    }

    public function view(menucategoryorder $menucategoryorder,informatique $informatique)
    {

       //conter les visiteurs sur le site
       $informatique->visits()->increment();

       $informatiques = informatique::with('user')
           ->with('menucategoryorder')
           ->take(4)
           ->get();

       $slidesorders = slidesorder::where('status',1)->orderBy('created_at','DESC')->get();

       return view('site.order.informatique.view',[

           'informatique'=> $informatique,
           'slidesorders' => $slidesorders,
           'informatiques' => $informatiques,

       ]);
    }


    public function tag(tag $tag)
    {
        $informatiques = $tag->informatiques();;
        return view('site.test',compact('informatiques'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $menucategoryorders = DB::table('menucategoryorders')
            ->where('categoryorder_id', 'informatique')
            ->where('status', 1)
            ->get();

        return view('site.order.informatique.create',[

            'menucategoryorders' => $menucategoryorders,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(request()->all());
        $this->validate($request,[
            //'cover_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

        ]);

        $informatique = new informatique;

        $informatique->title = $request->title;
        $informatique->body = $request->body;
        $informatique->phone = $request->phone;
        $informatique->title = $request->title;
        $informatique->slug = $request->slug;
        $informatique->summary = $request->summary;
        $informatique->color_id = $request->color_id;
        $informatique->color_name = $request->color_name;
        $informatique->menucategoryorder_id = $request->menucategoryorder_id;
        $informatique->user_id = Auth::user()->id;

        $informatique->save();
        $informatique->saveTags($request->get('tags'));


        alert()->success('Good Job','Article create with success');
        //Toastr::success('Article create with success','', ["positionClass" => "toast-top-center"]);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$colors =color::all();
        $menucategoryorders = menucategoryorder::where('status',1)->orderBy('created_at')->get();
        $informatique = informatique::where('id',$id)->first();

        return view('site.order.informatique.edit',compact('informatique','menucategoryorders'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {


    }


}
