<?php

namespace App\Http\Controllers\User;

use App\Model\admin\about;
use App\Model\admin\presentation;
use App\Model\admin\slides\slidesabout;
use App\Model\user\partial\speciality;
use App\Model\user\partial\work;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller
{
    /**
     * Show the testimonials page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expiresAt = now()->addSeconds(60);
        $slidesabouts = Cache::remember('slidesabouts', $expiresAt, function () {
            return Slidesabout::where('status',1)->orderBy('created_at','DESC')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                    'slide_title' => $item->slide_title,
                    'slide_subtitle' => $item->slide_subtitle,
                ];
            })->toArray();
        });
        $specialities = Speciality::where('status',1)->orderBy('created_at')->get();
        $abouts = About::where('status',1)->orderBy('created_at','DESC')->get();

        $presentations = Presentation::where('status',1)->orderBy('created_at','DESC')->get();
        return view('site.page.about',compact('abouts','presentations','specialities','slidesabouts'));
    }

    public function save_work(Request $request)
    {
        //dd(\request()->all());
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'speciality_id' => 'required',
        ]);

        $work = Work::create($request->except('speciality'));
        $work->save();

        alert()->success('Success', "Your request has been sent successfully");
        //toastr()->success('<b>Good Job !</b>','<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>');
        return back();
    }
}
