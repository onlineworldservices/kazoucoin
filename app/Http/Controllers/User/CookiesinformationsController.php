<?php

namespace App\Http\Controllers\User;

use App\Http\Resources\CookieResource;
use App\Model\admin\cookie;
use Illuminate\Http\Request;
use App\Model\admin\slides\slidescookie;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Response;

class CookiesinformationsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['user']]);
        // Middleware lock account
        //$this->middleware('auth.lock');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        $expiresAt = now()->addSeconds(10);
        $slidescookies = Cache::remember('slidescookies', $expiresAt, function () {
            return Slidescookie::where('status',1)->orderBy('created_at','DESC')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                    'slide_title' => $item->slide_title,
                    'slide_subtitle' => $item->slide_subtitle,
                ];
            })->toArray();
        });
        $cookies = cookie::where('status',1)->orderBy('created_at','DESC')->get();
        return view('site.page.cookies',compact('slidescookies','cookies'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('admin.info.cookie.index');
    }


    public function api()
    {
        return CookieResource::collection(cookie::with('user')->latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.info.cookie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'color_name'=>'required',
            'title'=>'required',
            'body'=>'required',
        ]);

        $cookie = new cookie;
        $cookie->title = $request->title;
        $cookie->body = $request->body;
        $cookie->color_name = $request->color_name;

        $cookie->save();

        return response('Created',Response::HTTP_CREATED);
    }

    /**
     * cette partie consite a activer et a desactiver
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function disable($id)
    {

        $cookie = cookie::where('id', $id)->findOrFail($id);

        $cookie->update([
            'status' => 1,
        ]);
        return response('Activated', Response::HTTP_ACCEPTED);

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return CookieResource|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $cookie = new CookieResource(cookie::where('id', $id)->findOrFail($id));
        return $cookie;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cookie = cookie::where('id', $id)->findOrFail($id);
        return view('admin.info.cookie.edit', compact('cookie'));

    }


    public function view($slug)
    {
        $cookie = new CookieResource(cookie::where('slug',$slug)->firstOrFail());
        return $cookie;
    }



    public function vector(cookie $cookie)
    {
        return view('admin.info.cookie.view', [
            'cookie' => $cookie,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'color_name'=>'required',
            'title'=>'required',
            'body'=>'required',
        ]);

        $cookie = cookie::findOrFail($id);

        $cookie->title = $request->title;
        $cookie->body = $request->body;
        $cookie->color_name = $request->color_name;
        $cookie->user_id = auth()->user()->id;

        $cookie->save();

        return ['message' => 'updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array|\Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $cookie = cookie::findOrFail($id);
        $cookie->delete();

        return ['message' => 'Deleted successfully '];
    }
}
