<?php

namespace App\Http\Controllers\User;

use App\Model\admin\categories\categoryevent;
use App\Model\admin\categories\menucategoryevent;
use App\Model\admin\slides\slidesevenement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class EvenementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryevents = categoryevent::where('status',1)->orderBy('created_at')->get();
        $menucategoryevents = menucategoryevent::where('status',1)->orderBy('created_at')->get();


        $expiresAt = now()->addSeconds(10);
        $slidesevenements = Cache::remember('slidesevenements', $expiresAt, function () {
            return slidesevenement::where('status',1)->orderBy('created_at','DESC')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                    'slide_title' => $item->slide_title,
                    'slide_btn_title' => $item->slide_btn_title,
                    'slide_btn_icon' => $item->slide_btn_icon,
                    'slide_subtitle' => $item->slide_subtitle,
                    'slide_btn_link' => $item->slide_btn_link,
                    'slide_btn_color' => $item->slide_btn_color,
                    'slide_btn_style1' => $item->slide_btn_style1,
                    'slide_btn_style' => $item->slide_btn_style,
                    'slide_btn_status' => $item->slide_btn_status,
                ];
            })->toArray();
        });
        return view('site.page.evenement',compact('slidesevenements','categoryevents','menucategoryevents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
