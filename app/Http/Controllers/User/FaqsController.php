<?php

namespace App\Http\Controllers\User;

use App\Model\admin\categories\categoryfaq;
use App\Model\admin\faq;
use App\Model\admin\slides\slidesfaq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expiresAt = now()->addSeconds(60);
        $faqs = Cache::remember('faqs', $expiresAt, function () {
            return faq::where('status',1)->orderBy('created_at','DESC')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'title' => $item->title,
                    'body' => $item->body,
                    'status' => $item->status
                ];
            })->toArray();
        });

        $slidesfaqs = Cache::remember('slidesfaqs', $expiresAt, function () {
            return slidesfaq::where('status',1)->orderBy('created_at','DESC')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                    'slide_title' => $item->slide_title,
                    'slide_btn_title' => $item->slide_btn_title,
                    'slide_btn_icon' => $item->slide_btn_icon,
                    'slide_subtitle' => $item->slide_subtitle,
                    'slide_btn_link' => $item->slide_btn_link,
                    'slide_btn_color' => $item->slide_btn_color,
                    'slide_btn_style1' => $item->slide_btn_style1,
                    'slide_btn_style' => $item->slide_btn_style,
                    'slide_btn_status' => $item->slide_btn_status,
                ];
            })->toArray();
        });

        $categoryfaqs = Cache::remember('categoryfaqs', 5, function () {
            return categoryfaq::with('faqs')->where('status',1)->orderBy('created_at','DESC')->get();
        });

        return view('site.page.faqs',compact('slidesfaqs','faqs','categoryfaqs'));
    }

    public function categoryfaq(Categoryfaq $categoryfaq,$slug)
    {

        $expiresAt = now()->addSeconds(10);
        $slidesfaqs = Cache::remember('slidesfaqs', $expiresAt, function () {
            return Slidesfaq::where('status',1)->orderBy('created_at','DESC')->get();
        });
        $expiresAt = now()->addSeconds(10);
        $categoryfaqs = Cache::remember('categoryfaqs', $expiresAt, function () {
            return categoryfaq::with('faqs')->where('status',1)->orderBy('created_at','DESC')->get();
        });

        $faqs = Categoryfaq::whereSlug($slug)->firstOrFail()->faqs()->where('status',1)->orderBy('created_at','DESC')->get();
        return view('site.page.faqs',compact('slidesfaqs','faqs','categoryfaqs'));
    }

}
