<?php

namespace App\Http\Controllers\User;

use App\Model\admin\about;
use App\Model\admin\howtoregister;
use App\Model\admin\information;
use App\Model\admin\legal_mention\legalnotice;
use App\Model\admin\legal_mention\licence;
use App\Model\admin\legal_mention\policyprivacy;
use App\Model\admin\slides\slidesconcept;
use App\Model\admin\slides\slideslicence;
use App\Model\admin\slides\slidesmentionslegale;
use App\Model\admin\slides\slidespolicy;
use App\Model\user\partial\speciality;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class LegalmentionpagesController extends Controller
{

    public function concept_page()
    {
        $informations= information::where('status',1)->orderBy('created_at','ASC')->get();
        $howtoregisters= howtoregister::where('status',1)->orderBy('created_at','ASC')->get();
        $slidesconcepts = Slidesconcept::where('status',1)->orderBy('created_at','DESC')->get();
        $abouts = About::where('status',1)->orderBy('created_at','DESC')->get();
        $specialities = Speciality::where('status',1)->orderBy('created_at')->get();
        return view('site.page.concept_exposition',compact('slidesconcepts', 'abouts', 'specialities','howtoregisters','informations'));
    }
    
    public function information_page()
    {
        $informations= information::where('status',1)->orderBy('created_at','ASC')->get();
        $slidesconcepts = Slidesconcept::where('status',1)->orderBy('created_at','DESC')->get();
        return view('site.page.informations',compact('slidesconcepts','informations'));
    }

    public function legal_notice_page()
    {
        $expiresAt = now()->addSeconds(10);
        $slidesmentionslegales = Cache::remember('slidesmentionslegales', $expiresAt, function () {
            return slidesmentionslegale::where('status',1)->orderBy('created_at','DESC')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                    'slide_title' => $item->slide_title,
                    'slide_subtitle' => $item->slide_subtitle,
                ];
            })->toArray();
        });
        $legalnotices= legalnotice::where('status',1)->orderBy('created_at','DESC')->get();
        return view('site.page.mentions_legales',compact('slidesmentionslegales','legalnotices'));
    }
    
    public function licence_page()
    {
        $expiresAt = now()->addSeconds(10);
        $slideslicences = Cache::remember('slideslicences', $expiresAt, function () {
            return slideslicence::where('status',1)->orderBy('created_at','DESC')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                    'slide_title' => $item->slide_title,
                    'slide_subtitle' => $item->slide_subtitle,
                ];
            })->toArray();
        });
        $licences= licence::where('status',1)->orderBy('created_at','DESC')->get();
        return view('site.page.licences',compact('slideslicences','licences'));
    }
    
    public function policy_privacy_page()
    {
        $expiresAt = now()->addSeconds(10);
        $slidespolicies = Cache::remember('slideslicences', $expiresAt, function () {
            return slidespolicy::where('status',1)->orderBy('created_at','DESC')->get()->map(function ($item){
                return[
                    'id' => $item->id,
                    'photo' => $item->photo,
                    'slide_title' => $item->slide_title,
                    'slide_subtitle' => $item->slide_subtitle,
                ];
            })->toArray();
        });
        $policyprivacies= policyprivacy::where('status',1)->orderBy('created_at','DESC')->get();
        return view('site.page.privacy',compact('slidespolicies','policyprivacies'));
    }
    
    public function registration_info_page()
    {
        $howtoregisters= howtoregister::where('status',1)->orderBy('created_at','ASC')->get();
        $slidesconcepts = Slidesconcept::where('status',1)->orderBy('created_at','DESC')->get();
        return view('site.page.registration',compact('slidesconcepts','howtoregisters'));
    }
}
