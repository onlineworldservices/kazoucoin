<?php

namespace App\Http\Controllers\User;

use App\Model\admin\presentation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PresentationController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest',['except' => ['presentation']]);

    }

    /**
     * Show the testimonials page
     *
     * @return \Illuminate\Http\Response
     */

    public function presentation(presentation $presentation)
    {
        return view('site.page.presentation',compact('presentation'));
    }

}
