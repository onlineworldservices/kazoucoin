<?php

namespace App\Http\Controllers;



use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $auth;

    public function __construct(Guard $auth){
        $this->middleware('auth');
        $this->auth = $auth;
    }

    /**
     * Show the application Profile
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function edit()
    {
        $user = $this->auth->user();
        return view('site.user.edit_profile',compact('user'));
    }


}
