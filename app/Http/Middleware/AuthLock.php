<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 15/11/18
 * Time: 15:04
 */

namespace App\Http\Middleware;


use Closure;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;

class AuthLock
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
   public function handle($request, Closure $next)
   {
       // If the user does not have this feature enabled, then just return next.

        if($request->session()->has('locked')){

            return redirect('/admin/lock');
        }

       return $next($request);

     }

}