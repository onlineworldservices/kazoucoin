<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

class BeauteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $init_price = Input::get('price_no_sold');
        return [
            'title' => 'required',
            'body' => 'required',
            'color_name' => 'required',
            'devise_id' => 'required',
            'country_id' => 'required',
            'menucategoryorder_id' => 'required',

            'price_no_sold' => 'nullable:price_sold|integer|min:1|digits_between: 1,9',
            'price_sold' => 'nullable:price_no_sold|integer|max:'. ($init_price) .'|digits_between:1,9',
        ];
    }
}
