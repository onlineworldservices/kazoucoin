<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AboutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'role' => $this->role,
            'body' => $this->body,
            'fblink' => $this->fblink,
            'status' => $this->status,
            'photo' => $this->photo,
            'twlink' => $this->twlink,
            'instlink' => $this->instlink,
            'fullname' => $this->fullname,
            'linklink' => $this->linklink,
            'googlelink' => $this->googlelink,
            'dribbblelink' => $this->dribbblelink,
            'user' => $this->user,
            'statusOnline' => $this->user->isOnline(),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
