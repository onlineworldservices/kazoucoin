<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AidecontactmessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'body' => $this->body,
            'ip' => $this->ip,
            'slug' => $this->slug,
            'object' => $this->object,
            'status' => $this->status,
            'user' => $this->user,
            'statusOnline' => $this->user->isOnline(),
            'aidecontactdemande' => $this->aidecontactdemande,
            'aidecontacttheme' => $this->aidecontacttheme,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
