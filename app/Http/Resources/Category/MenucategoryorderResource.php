<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class MenucategoryorderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'icon' => $this->icon,
            'user' => $this->user,
            'statusOnline' => $this->user->isOnline(),
            'status' => $this->status,
            'color_name' => $this->color_name,
            'categoryorder_id' => $this->categoryorder_id,
            'categoryorder_name' => $this->categoryorder_name,
            'categoryorder' => $this->categoryorder,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
