<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PresentationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'photo' => $this->photo,
            'user' => $this->user,
            'statusOnline' => $this->user->isOnline(),
            'icon' => $this->icon,
            'slug' => $this->slug,
            'mySlug' => $this->mySlug,
            'status' => $this->status,
            'color_name' => $this->color_name,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
