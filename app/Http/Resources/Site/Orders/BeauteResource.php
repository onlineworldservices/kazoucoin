<?php

namespace App\Http\Resources\Site\Orders;

use awssat\Visits\Tests\User;
use Illuminate\Http\Resources\Json\JsonResource;

class BeauteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ip' => $this->ip,
            'site' => $this->site,
            'contact' => $this->contact,
            'email' => $this->email,
            'phone' => $this->phone,
            'title' => $this->title,
            'user' => $this->user,
            'country' => $this->country,
            'city' => $this->city,
            'devise' => $this->devise,
            'slug' => $this->slug,
            'color_name' => $this->color_name,
            'body' => $this->body,
            'images' => $this->images,
            'read_time' => $this->read_time,
            'country_id' => $this->country_id,
            'status_admin' => $this->status_admin,
            'status_user' => $this->status_user,
            'description' => $this->description,
            'price_sold' => $this->price_sold,
            'information' => $this->information,
            'likers' => $this->likers()->get()->count(),
            'detail' => $this->detail,
            'visits' => $this->visits()->count(),
            'quantity' => $this->quantity,
            'color_product' => $this->color_product,
            'devise_id' => $this->devise_id,
            'price_no_sold' => $this->price_no_sold,
            'menucategoryorder_id' => $this->menucategoryorder_id,
            'menucategoryorder' => $this->menucategoryorder,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'expired_at' => (string) $this->expired_at->diffInDays(),
        ];
    }
}
