<?php

namespace App\Http\Resources\Slides;

use Illuminate\Http\Resources\Json\JsonResource;

class SlidesEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ip' => $this->ip,
            'slide_title' => $this->slide_title,
            'slide_subtitle' => $this->slide_subtitle,
            'slide_btn_status' => $this->slide_btn_status,
            'slide_btn_link' => $this->slide_btn_link,
            'slide_btn_icon' => $this->slide_btn_icon,
            'slide_btn_title' => $this->slide_btn_title,
            'slide_btn_style' => $this->slide_btn_style,
            'slide_btn_style1' => $this->slide_btn_style1,
            'slide_btn_color' => $this->slide_btn_color,
            'image' => $this->image,
            'photo' => $this->photo,
            'slug' => $this->slug,
            'status' => $this->status,
            'user' => $this->user,
            'statusOnline' => $this->user->isOnline(),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
