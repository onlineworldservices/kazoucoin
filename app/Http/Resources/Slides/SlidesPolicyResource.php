<?php

namespace App\Http\Resources\Slides;

use Illuminate\Http\Resources\Json\JsonResource;

class SlidesPolicyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ip' => $this->ip,
            'slide_title' => $this->slide_title,
            'slide_subtitle' => $this->slide_subtitle,
            'image' => $this->image,
            'photo' => $this->photo,
            'slug' => $this->slug,
            'status' => $this->status,
            'user' => $this->user,
            'statusOnline' => $this->user->isOnline(),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
