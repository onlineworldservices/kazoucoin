<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserLastSignInAt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user = $event->user;


        $user->last_sign_in_at =  $user->current_sign_in_at ? $user->current_sign_in_at : Carbon::now();
        $user->current_sign_in_at = Carbon::now()->toDateTimeString();
        $user->ip = request()->getClientIp();
        $user->save();
    }
}
