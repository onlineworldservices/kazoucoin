<?php

namespace App\Model\admin\agenda;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
     protected $fillable = ['title','start_date','end_date'];
}
