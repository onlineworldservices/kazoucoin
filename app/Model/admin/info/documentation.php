<?php

namespace App\Model\admin\info;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Spatie\Activitylog\Traits\LogsActivity;

class documentation extends Model
{
    use LogsActivity;

    protected static $logAttributes = ['name','name_doc'];
    protected $table = 'documentations';

    protected $fillable = [
        'name',
        'name_doc',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    /**
     * save and deleting image
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            if (auth()->check()){
                $model->user_id = auth()->id();
            }
        });
        static::updating(function($model){
            if (auth()->check()){
                $model->user_id = auth()->id();
            }
        });
    }


    public function getUploadPath()
    {
        //return 'documentation/'. $this->id .'/';
        return 'documentation' . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR;
    }

}
