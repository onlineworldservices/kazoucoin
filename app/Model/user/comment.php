<?php

namespace App\Model\user;

use App\Model\admin\admin;
use App\Model\user\order\informatique;
use App\User;
use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $fillable = ['commentable_id','body','user_id','ip','commentable_type','admin_id'];

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        self::creating(function ($model){

           if (auth()->check()){
               $model->user_id = auth()->id();
           }
            $model->ip = request()->ip();
        });
    }

    public function commentable()
    {

        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
