<?php

namespace App\Model\user;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class image extends Model
{
    protected $fillable = [
        'image', 'imageable_type', 'imageable_id',

    ];

    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'image',
                'separator' => '+'
            ]
        ];
    }

    public function imageable()
    {
        return $this->morphTo();
    }
}
