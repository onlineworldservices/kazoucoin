<?php

namespace App\Model\user;

use App\User;
use Illuminate\Database\Eloquent\Model;

class login extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
