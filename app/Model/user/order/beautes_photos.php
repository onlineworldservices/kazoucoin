<?php

namespace App\Model\user\order;

use Illuminate\Database\Eloquent\Model;

class beautes_photos extends Model
{
    protected $fillable = ['beaute_id','filename'];
    // Table Name
    protected $table = 'beautes_photos';

    // Primary Key

    public $primaryKey = 'id';


    public function beaute()
    {
        return $this->belongsTo(beaute::class);
    }
}
