<?php

namespace App\Model\user\order;


use App\Model\admin\admin;
use App\Model\admin\categories\categoryorder;
use App\Model\user\comment;
use App\Model\user\favorite;
use App\Model\user\tag;
use App\User;
use App\Model\user\Concern\Taggable;
use App\Model\admin\categories\menucategoryorder;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class informatique extends Model
{
    use Taggable,SoftDeletes,LogsActivity;

    protected $dates = ['deleted_at'];

    protected static $logAttributes = [
        'user_id',
        'body',
        'phone',
        'slug',
        'title',
        'summary',
        'color_id',
        'color_name',
        'menucategoryorder_id',
        ];


    protected $fillable = [
        'body',
        'phone',
        'slug',
        'title',
        'summary',
        'color_id',
        'color_name',
        'menucategoryorder_id',

    ];



    public function visits()
    {
        return visits($this);
    }

    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
                'separator' => '_'
            ]
        ];
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }
    // Table Name
    protected $table = 'informatiques';

    // Primary Key

    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;


    //public function favorited()
    //{
    //    return (bool) favorite::where('user_id',Auth::id())
    //        ->where('informatique_id',$this->id)
    //        ->first();
    //}

    public function comments()
    {
        return $this->morphMany(comment::class,'commentable');
    }

    public function path()
    {
        return "/orders/informatiques/{$this->menucategoryorder->slug}/show/{$this->slug}";
    }

    public function user()
    {
        return $this->belongsTo(user::class);
    }

    public function categoryorder()
    {
        return $this->belongsTo(categoryorder::class);
    }

    public function menucategoryorder()
    {
        return $this->belongsTo(menucategoryorder::class);

    }

    public function category()
    {
        return $this->belongsTo(menucategoryorder::class);
    }


    public function tags()
    {
        return $this->belongsToMany(tag::class,'informatique_tags');
    }

}
