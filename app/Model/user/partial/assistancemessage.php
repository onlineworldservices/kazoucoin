<?php

namespace App\Model\user\partial;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class assistancemessage extends Model
{
   protected $fillable = [
        'name', 'lastname', 'email', 'theme', 'demande', 'msg', 
    ];
    
    protected $table = 'assistancemessages';
    
    use Sluggable;
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'separator' => '+'
            ]

        ];
    }
}
