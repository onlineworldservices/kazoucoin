<?php

namespace App\Model\user;

use Cviebrock\EloquentSluggable\Sluggable;
use Laratrust\Models\LaratrustRole;
use App\User;
    
class role extends  LaratrustRole
{

    public function role()
    {
        return $this->hasOne(User::class);
    }

}