<?php

namespace App\Policies;

use App\User;
use App\Model\user\order\beaute;
use Illuminate\Auth\Access\HandlesAuthorization;

class BeautePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the beaute.
     *
     * @param  \App\User  $user
     * @param  \App\Model\user\order\beaute  $beaute
     * @return mixed
     */
    public function view(User $user, beaute $beaute)
    {
        //
    }

    /**
     * Determine whether the user can create beautes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the beaute.
     *
     * @param  \App\User  $user
     * @param  \App\Model\user\order\beaute  $beaute
     * @return mixed
     */
    public function update(User $user, beaute $beaute)
    {
        return $user->id === $beaute->user_id;
    }

    /**
     * Determine whether the user can delete the beaute.
     *
     * @param  \App\User  $user
     * @param  \App\Model\user\order\beaute  $beaute
     * @return mixed
     */
    public function delete(User $user, beaute $beaute)
    {
        //
    }

    /**
     * Determine whether the user can restore the beaute.
     *
     * @param  \App\User  $user
     * @param  \App\Model\user\order\beaute  $beaute
     * @return mixed
     */
    public function restore(User $user, beaute $beaute)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the beaute.
     *
     * @param  \App\User  $user
     * @param  \App\Model\user\order\beaute  $beaute
     * @return mixed
     */
    public function forceDelete(User $user, beaute $beaute)
    {
        //
    }
}
