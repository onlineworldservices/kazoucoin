<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id')->unique()->index();
            $table->string('fullname')->unique();
            $table->string('slug')->nullable();
            $table->text('body');
            $table->string('photo')->nullable();
            $table->string('role');
            $table->integer('status')->nullable()->default('2');
            $table->string('fblink')->nullable();
            $table->string('googlelink')->nullable();
            $table->string('instlink')->nullable();
            $table->string('twlink')->nullable();
            $table->string('linklink')->nullable();
            $table->string('dribbblelink')->nullable();
            $table->string('avatar')->nullable();
            $table->boolean('image')->nullable()->default(true);
            $table->integer('admin_id')->unsigned()->nullable();
            $table->string('ip')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
