<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slidesabouts', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('admin_id')->unsigned()->nullable();
        $table->string('slug')->nullable();
        $table->string('slide_about')->nullable();
            $table->integer('status')->nullable()->default('2');
        $table->string('slide_title')->nullable();
        $table->string('slide_subtitle')->nullable();
            $table->string('image')->nullable()->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        $table->string('admin_name')->nullable();
            $table->string('ip')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        $table->timestamps();
        $table->softDeletes();

         });


        Schema::create('slidescontacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->longText('body')->nullable();
            $table->string('slide_contact')->nullable();
            $table->integer('status')->nullable()->default('2');
            $table->string('slide_title')->nullable();
            $table->string('ip')->nullable();
            $table->string('slide_subtitle')->nullable();
            $table->string('image')->nullable()->default('/assets/img/image.jpg');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('slidestestimonials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->longText('body')->nullable();
            $table->string('slide_testimonial')->nullable();
            $table->string('ip')->nullable();
            $table->integer('status')->nullable()->default('2');
            $table->string('slide_title')->nullable();
            $table->string('slide_subtitle')->nullable();
            $table->string('image')->nullable()->default('/assets/img/image.jpg');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slidesabouts');
        Schema::dropIfExists('slidescontacts');
        Schema::dropIfExists('slidestestimonials');
    }
}
