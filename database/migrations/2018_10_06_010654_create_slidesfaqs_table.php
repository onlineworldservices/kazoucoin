<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesfaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slidesfaqs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('slide_faq')->nullable();
            $table->integer('status')->nullable()->default('2');
            $table->string('slide_title')->nullable();
            $table->longText('body')->nullable();
            $table->longText('slide_subtitle')->nullable();
            $table->integer('slide_btn_status')->nullable();
            $table->string('slide_btn_link')->nullable();
            $table->string('slide_btn_icon')->nullable();
            $table->string('slide_btn_title')->nullable();
            $table->string('slide_btn_style')->nullable();
            $table->string('slide_btn_style1')->nullable();
            $table->string('slide_btn_color')->nullable()->default('rose');
            $table->string('image')->nullable()->default('https://www.kazoucoin.com/assets/img/photo.jpg');
            $table->string('ip')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slidesfaqs');
    }
}
