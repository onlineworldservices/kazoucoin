<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slidesorders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->string('slug')->nullable();
            $table->string('slide_oder')->nullable();
            $table->integer('status')->nullable()->default('2');
            $table->string('slide_title')->nullable();
            $table->longText('slide_subtitle')->nullable();
            $table->integer('slide_btn_status')->nullable();
            $table->string('slide_btn_link')->nullable();
            $table->string('slide_btn_icon')->nullable();
            $table->string('slide_btn_title')->nullable();
            $table->string('slide_btn_style')->nullable();
            $table->string('slide_btn_style1')->nullable();
            $table->string('slide_btn_color')->nullable()->default('rose');
            $table->string('image')->nullable()->default('https://www.kazoucoin.com/assets/img/photo.jpg');
            $table->string('admin_name')->nullable();
            $table->string('ip')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slidesorders');
    }
}
