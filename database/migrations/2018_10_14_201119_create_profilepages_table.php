<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilepagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('profilelogins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->integer('status')->nullable();
            $table->integer('admin_id')->unsigned()->nullable();
            $table->string('admin_name')->nullable();
            $table->string('ip')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->boolean('image')->nullable()->default(true);
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('profilepasswords');
        Schema::dropIfExists('profileregisters');
        Schema::dropIfExists('profilelogins');
    }
}
