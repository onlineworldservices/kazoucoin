<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryfaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoryfaqs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('color_name')->nullable();
            $table->string('icon')->nullable();
            $table->string('slug')->nullable();
            $table->boolean('status')->default(true)->nullable();
            $table->string('link_category')->nullable();
            $table->string('ip')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoryfaqs');
    }
}
