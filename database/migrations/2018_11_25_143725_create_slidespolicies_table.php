<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidespoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slidespolicies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slide_title')->nullable();
			$table->string('slide_subtitle')->nullable();
            $table->string('slug')->nullable();
            $table->string('image')->nullable()->default('https://www.kazoucoin.com/assets/img/photo.jpg');
            $table->integer('status')->nullable()->default('2');
            $table->string('ip')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slidespolicies');
    }
}
