<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('ip_address');
            $table->timestamp('created_at');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('last_login_id')->nullable()->after('updated_at');
            $table->integer('last_login_ip')->nullable()->after('last_sign_in_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logins');
        Schema::dropIfExists('users');
    }
}
