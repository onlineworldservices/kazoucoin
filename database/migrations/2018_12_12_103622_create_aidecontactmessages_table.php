<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAidecontactmessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aidecontactmessages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('slug')->nullable();
            $table->string('object')->nullable();
            $table->longText('body')->nullable();
            $table->integer('status')->nullable()->default('2');
            $table->string('ip')->nullable();
            $table->timestamps();
            $table->softDeletes();



            $table->unsignedInteger('aidecontacttheme_id')->nullable();
            $table->foreign('aidecontacttheme_id')->references('id')->on('aidecontactthemes')->onDelete('cascade');
            $table->unsignedInteger('aidecontactdemande_id')->nullable();
            $table->foreign('aidecontactdemande_id')->references('id')->on('aidecontactdemandes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aidecontactmessages');
    }
}
