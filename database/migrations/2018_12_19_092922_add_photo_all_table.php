<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('presentations', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/image.jpg');
        });
        Schema::table('conditions', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/image.jpg');
        });
        Schema::table('testimonials', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/image.jpg');
        });
        Schema::table('slidesabouts', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidesconcepts', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidescookies', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidescontacts', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidesevenements', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidesfaqs', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slideshomes', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slideslicences', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidesmentionslegales', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidesorders', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidesothers', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidespolicies', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
        Schema::table('slidestestimonials', function (Blueprint $table) {
            $table->string('photo')->nullable()->after('image')->default('https://www.kazoucoin.com/assets/img/photo.jpg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('presentations');
        Schema::dropIfExists('conditions');
        Schema::dropIfExists('testimonials');
        Schema::dropIfExists('slidesabouts');
        Schema::dropIfExists('slidesconcepts');
        Schema::dropIfExists('slidescontacts');
        Schema::dropIfExists('slidescookies');
        Schema::dropIfExists('slidesevenements');
        Schema::dropIfExists('slidesfaqs');
        Schema::dropIfExists('slideshomes');
        Schema::dropIfExists('slideslicences');
        Schema::dropIfExists('slidesmentionslegales');
        Schema::dropIfExists('slidesorders');
        Schema::dropIfExists('slidesothers');
        Schema::dropIfExists('slidespolicies');
        Schema::dropIfExists('slidestestimonials');




    }
}
