<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('admin_id')->nullable()->index();
            $table->unsignedInteger('commentable_id')->nullable()->index();
            $table->string('commentable_type',50)->nullable();
            $table->string('username',50)->nullable();
            $table->string('name',50)->nullable();
            $table->string('email',50)->nullable();
            $table->integer('reply')->nullable()->default(0);
            $table->longText('body')->nullable();
            $table->string('ip')->nullable()->default('127.0.0.1');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
