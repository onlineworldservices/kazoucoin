<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeautesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beautes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('summary')->nullable();
            $table->string('read_time')->nullable();
            $table->string('photo')->nullable();
            $table->string('title')->nullable();
            $table->longText('body')->nullable();
            $table->longText('description')->nullable();
            $table->integer('status_admin')->nullable()->default('1');
            $table->integer('status_user')->nullable()->default('1');
            $table->integer('price_sold')->nullable();
            $table->integer('price_no_sold')->nullable();
            $table->string('slug')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('site')->nullable();
            $table->string('contact')->nullable();
            $table->string('ip')->nullable();
            $table->timestamps();

            $table->string('color_name')->nullable();
            $table->unsignedInteger('country_id')->nullable()->index();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('menucategoryorder_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beautes');
    }
}
