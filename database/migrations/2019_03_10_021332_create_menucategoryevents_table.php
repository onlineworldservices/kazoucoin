<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenucategoryeventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menucategoryevents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('icon')->nullable();
            $table->string('slug')->nullable();
            $table->integer('status')->nullable()->default('2');
            $table->string('color_name')->nullable();
            $table->string('categoryevent_name')->nullable();
            $table->string('ip')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('categoryevent_id')->nullable()->unsigned()->index();
            $table->foreign('categoryevent_id')->references('id')->on('categoryevents')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menucategoryevents');
    }
}
