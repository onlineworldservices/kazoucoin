<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformatiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informatiques', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('summary')->nullable();
            $table->string('read_time')->nullable();
            $table->string('image')->nullable();

            $table->string('title')->nullable();
            $table->longText('body')->nullable();
            $table->string('slug')->nullable();
            $table->string('phone')->nullable();
            $table->string('contact')->nullable();

            $table->boolean('status_user')->nullable();
            $table->integer('color_id')->unsigned()->nullable();
            $table->string('user_name')->nullable();


            $table->timestamps();
            $table->softDeletes();

            $table->string('color_name')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('menucategoryorder_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informatiques');
    }
}
