<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfoInBeautesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beautes', function (Blueprint $table) {
            $table->timestamp('expired_at')->default(\Carbon\Carbon::now()->addDays(7))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beautes', function (Blueprint $table) {
            //
        });
    }
}
