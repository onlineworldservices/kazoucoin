<?php

use Illuminate\Database\Seeder;
use App\Model\admin\agenda\Agenda;
    
class AddDummyEvent extends Seeder
{
    public function run()
    {
        $data = [
        	['title'=>'Demo Event-1', 'start_date'=>'2018-10-23', 'end_date'=>'2018-10-24'],
        	['title'=>'Demo Event-2', 'start_date'=>'2018-10-24', 'end_date'=>'2018-10-25'],
        	['title'=>'Demo Event-3', 'start_date'=>'2018-10-25', 'end_date'=>'2018-10-26'],
        	['title'=>'Demo Event-3', 'start_date'=>'2018-10-26', 'end_date'=>'2018-10-26'],
        ];
        foreach ($data as $key => $value) {
        	Agenda::create($value);
        }
    }
}