<?php

use App\Model\user\partial\message;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class CompileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addDefaultUser();

        if (config('app.env') !== 'production') {
            $this->addTestUser();
        }
    }

    private function addDefaultUser()
    {
        // Truncate table
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        $god = User::create([
            'username' =>'bokino12',
            'name' =>'Boclair Temgoua',
            'my_status' =>'active',
            'email' => "temgoua2012@gmail.com",
            "password" => bcrypt('0000000'),
            'created_at' => Carbon\Carbon::now(),
            'email_verified_at' => Carbon\Carbon::now(),
        ]);
        $god->syncRoles('super-admin');

        $pat = User::create([
            'username' =>'randrino17',
            'name' =>'Nzeukang',
            'my_status' =>'active',
            'email' => "nzeukangrandrin@gmail.com",
            "password" => bcrypt('123456789'),
            'created_at' => Carbon\Carbon::now(),
            'email_verified_at' => Carbon\Carbon::now(),
        ]);
        $pat->syncRoles('super-admin');
    }

    private function addTestUser()
    {

        factory(User::class, 5)->create();

        // Output
        $this->command->info('Test user added.');
    }
}
