<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        //A seeder ensemble
        $this->call(PermissionTableSeeder::class);
        $this->call(CompileTableSeeder::class);
        $this->call(AddDummyEvent::class);

        //$this->call(RolesTableSeedeer::class);
        $this->call(CountriesTableSeeder::class);


    }

}
