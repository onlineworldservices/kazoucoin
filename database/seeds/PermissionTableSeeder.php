<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        //Create Roles (Please don't delete this it's very important !)
        Role::create(['guard_name' => 'web', 'name' => 'admin']);
        Role::create(['guard_name' => 'web', 'name' => 'advertiser']);
        Role::create(['guard_name' => 'web', 'name' => 'editor']);
        Role::create(['guard_name' => 'web', 'name' => 'moderator']);
        Role::create(['guard_name' => 'web', 'name' => 'visitor']);

        //Create Permissions Administrations
        Permission::create(['guard_name' => 'web' , 'name' => 'create-administrator']);
        Permission::create(['guard_name' => 'web' , 'name' => 'view-administrator']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-administrator']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-administrator']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-admin-administrator']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-administrator']);

        //Create Permissions Testimonials
        Permission::create(['guard_name' => 'web' , 'name' => 'create-testimonial']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-testimonial']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-testimonial']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-testimonial']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-testimonial']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-testimonial']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-testimonial']);

        //Create Permissions Tasks
        Permission::create(['guard_name' => 'web' , 'name' => 'create-task']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-task']);
        Permission::create(['guard_name' => 'web' , 'name' => 'assign-task']);

        //Create Permissions Categories
        Permission::create(['guard_name' => 'web' , 'name' => 'create-category']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-category']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-category']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-category']);

        //Create Permissions Colors
        Permission::create(['guard_name' => 'web' , 'name' => 'create-color']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-color']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-color']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-color']);

        //Create Permissions Priorities
        Permission::create(['guard_name' => 'web' , 'name' => 'create-priority']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-priority']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-priority']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-priority']);

        //Create Permissions Status
        Permission::create(['guard_name' => 'web' , 'name' => 'create-status']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-status']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-status']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-status']);

        //Create Permissions Faqs
        Permission::create(['guard_name' => 'web' , 'name' => 'create-faq']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-faq']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-faq']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-faq']);

        //Create Permissions Aide & Contact Themes
        Permission::create(['guard_name' => 'web' , 'name' => 'create-aide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-aide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-aide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-aide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-aide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-aide']);
        
        //Create Permissions Evenements
        Permission::create(['guard_name' => 'web' , 'name' => 'create-eventment']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-eventment']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-eventment']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-eventment']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-eventment']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-eventment']);

        //Create Permissions Messages Assistance/Contact Us/Work With Us
        Permission::create(['guard_name' => 'web' , 'name' => 'read-message']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-message']);

        //Create Permissions Message-contacts
        Permission::create(['guard_name' => 'web' , 'name' => 'all-contact_message']);
        Permission::create(['guard_name' => 'web' , 'name' => 'view-contact_message']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-contact_message']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-contact_message']);

        //Create Permissions Conditions and Utilisations
        Permission::create(['guard_name' => 'web' , 'name' => 'create-condition_utilisation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-condition_utilisation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-condition_utilisation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-condition_utilisation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-condition_utilisation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-condition_utilisation']);

        //Create Permissions Abouts
        Permission::create(['guard_name' => 'web' , 'name' => 'create-about']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-about']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-about']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-about']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-about']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-about']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-about']);

        //Create Permissions Tags
        Permission::create(['guard_name' => 'web' , 'name' => 'create-tag']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-tag']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-tag']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-tag']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-tag']);

        //Create Permissions Presentations
        Permission::create(['guard_name' => 'web' , 'name' => 'create-presentation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-presentation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-presentation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-presentation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-presentation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-presentation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-presentation']);
        Permission::create(['guard_name' => 'web' , 'name' => 'view-presentation']);

        //Create Permissions Informations
        Permission::create(['guard_name' => 'web' , 'name' => 'create-informations']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-informations']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-informations']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-informations']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-informations']);

        //Create Permissions Registration
        Permission::create(['guard_name' => 'web' , 'name' => 'create-registration']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-registration']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-registration']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-registration']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-registration']);

        //Create Permissions User
        Permission::create(['guard_name' => 'web' , 'name' => 'create-user']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-user']);
        Permission::create(['guard_name' => 'web' , 'name' => 'view-user']);

        //Create Permissions Links
        Permission::create(['guard_name' => 'web' , 'name' => 'create-link']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-link']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit_by-link']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-link']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-link']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-link']);

        //Create Permissions Countries
        Permission::create(['guard_name' => 'web' , 'name' => 'create-country']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-country']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit_by-country']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-country']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-country']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-country']);

        //Create Permissions Currencies
        Permission::create(['guard_name' => 'web' , 'name' => 'create-currency']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-currency']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit_by-currency']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-currency']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-currency']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-currency']);

        //Create Permissions Specialities
        Permission::create(['guard_name' => 'web' , 'name' => 'create-speciality']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-speciality']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit_by-speciality']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-speciality']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-speciality']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-speciality']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-speciality']);

        //Create Permissions Works
        Permission::create(['guard_name' => 'web' , 'name' => 'create-work']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-work']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit_by-work']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-work']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-work']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-work']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-work']);

        //Create Permissions Roles
        Permission::create(['guard_name' => 'web' , 'name' => 'all-role']);
        Permission::create(['guard_name' => 'web' , 'name' => 'create-role']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-role']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-role']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-role']);

        //Create Permissions Slides
        Permission::create(['guard_name' => 'web' , 'name' => 'all-slide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'create-slide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-slide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-slide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-slide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-slide']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-slide']);

        //Create Permissions Permissions
        Permission::create(['guard_name' => 'web' , 'name' => 'all-permission']);
        Permission::create(['guard_name' => 'web' , 'name' => 'create-permission']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-permission']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-permission']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-multiple-permission']);
        
        //View Subscribe Newletters Permissions
        Permission::create(['guard_name' => 'web' , 'name' => 'view-email']);

        //Create Permissions Notes
        Permission::create(['guard_name' => 'web' , 'name' => 'create-note']);

        //Create Permissions Legal Notice
        Permission::create(['guard_name' => 'web' , 'name' => 'create-legal']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-legal']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-legal']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-legal']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-legal']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-legal']);

        //Create Permissions Licence Site
        Permission::create(['guard_name' => 'web' , 'name' => 'create-licence']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-licence']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-licence']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-licence']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-licence']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-licence']);

        //Create Permissions Policy & Privacy
        Permission::create(['guard_name' => 'web' , 'name' => 'create-privacy']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-privacy']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-privacy']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-privacy']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-privacy']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-privacy']);

        //Create Permissions Cookies
        Permission::create(['guard_name' => 'web' , 'name' => 'create-cookie']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-cookie']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-cookie']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-cookie']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-cookie']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-cookie']);

        //Create Permissions Cover Page
        Permission::create(['guard_name' => 'web' , 'name' => 'create-coverpage']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edit-coverpage']);
        Permission::create(['guard_name' => 'web' , 'name' => 'edited_by-coverpage']);
        Permission::create(['guard_name' => 'web' , 'name' => 'delete-coverpage']);
        Permission::create(['guard_name' => 'web' , 'name' => 'publish-coverpage']);
        Permission::create(['guard_name' => 'web' , 'name' => 'unpublish-coverpage']);

        //Create Permissions Download Excel
        Permission::create(['guard_name' => 'web' , 'name' => 'download_excel']);

        //Create Permissions to see section page in Narbar Menu
        Permission::create(['guard_name' => 'web' , 'name' => 'access_permission']);

        //Create Roles and Assign created permissions
        $role = Role::create(['guard_name' => 'web', 'name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}
