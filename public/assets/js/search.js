$(document).ready(function () {
    $('.search .search-wrap__icon').on('click', function () {
        $('.search-wrap__content').addClass('is-visible');
    })
    $('.search-wrap__content__head .close-search').on('click', function () {
        $('.search-wrap__content').removeClass('is-visible');
    })
});
