
try {
    var map;
    function initMapSingle() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: new google.maps.LatLng(45.468576, 9.194453),
            mapTypeId: 'roadmap',
            fullscreenControl: false,
            mapTypeControl: false,
            streetViewControl: false,
            styles: [{
                featureType: "poi",
                elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            }]
        });

        var iconBase = '../images/';
        var icons = {
            info: {
                icon: iconBase + 'kazoucoin-marker.png'
            }
        };

        var features = [{
            position: new google.maps.LatLng(45.468576, 9.194453),
            type: 'info'
        }];

        var contentString = '<div id="content" xmlns="http://www.w3.org/1999/html">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<br class="bodyContent">' +
            '<h2 id="firstHeading" class="firstHeading ">Kazoucoin Srl - Siège Social</h2>' +
            '<p>Bld Mihail Kogalniceanu, nr. 8, 16134 Genova, Italy</p></br>' +
            '<p> Tel: 02-83549386</p>' +
            '</div>' +
            '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,
        });

        var marker;
        // Create markers.
        features.forEach(function(feature) {
            marker = new google.maps.Marker({
                position: feature.position,
                icon: icons[feature.type].icon,
                map: map
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
            var latLng = marker.getPosition(); // returns LatLng object
            map.setCenter(latLng); // setCenter takes a LatLng object
        });
    }
} catch (error) {
    console.error(error);
}
