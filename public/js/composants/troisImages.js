$('.trois-images-wrapper').find('.outbox-trois-images').click(function() {
    $(this).find('.toggle-arrow-trois-images').toggleClass('toggle-arrow-trois-images-active');
    $(this).find('.rectangle-trois-images').toggleClass('rectangle-trois-images-active');
    $(this).toggleClass('outbox-trois-images-active');
    $(this).find('.trois-images-title').toggleClass('trois-images-title-active');
    $(this).find('.trois-images-mask').toggleClass('trois-images-mask-active');
});

$('.trois-images-wrapper').find('.outbox-trois-images').off('click').on('keydown mousedown', function(event) {
    $(this).find('.toggle-arrow-trois-images').toggleClass('toggle-arrow-trois-images-active');
    $(this).find('.rectangle-trois-images').toggleClass('rectangle-trois-images-active');
    $(this).toggleClass('outbox-trois-images-active');
    $(this).find('.trois-images-title').toggleClass('trois-images-title-active');
    $(this).find('.trois-images-mask').toggleClass('trois-images-mask-active');

    if (event.shiftKey && event.keyCode === 9) {
        console.log(event.shiftKey);
        $('.outbox-trois-images a').attr('tabindex', '-1');
    } else {
        if ($(".outbox-trois-images").is(":focus")) {
            $('.outbox-trois-images a').attr('tabindex', '0');
        } else {
            $('.outbox-trois-images a').attr('tabindex', '-1');
        }
    }
});
