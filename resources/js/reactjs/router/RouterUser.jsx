import React from "react";
import {Route, Switch,withRouter} from 'react-router-dom';
import IndexSite from "../components/user/IndexSite";



const RouterUser = props => (

    <Switch>
        <Route exact path="/" strict component={IndexSite}/>
    </Switch>
);
export default RouterUser;
