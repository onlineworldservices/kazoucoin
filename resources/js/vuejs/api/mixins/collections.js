import axios from 'axios';

export default {
    ShowUser(username) {
        return axios.get(`/admin/api/profile/${username}`);
    },
    userID(id) {
        return axios.get(`/admin/administrators/profile/${id}`);
    },
    contactshow(contact) {
        return axios.get(`/admin/all-contact/view/${contact}`);
    },
    messageshow(message) {
        return axios.get(`/admin/messages/view/${message}`);
    },
    cookieID(id) {
        return axios.get(`/admin/cookies-site/${id}`);
    },
    cookieSlug(cookie) {
        return axios.get(`/admin/cookies-site/view/${cookie}`);
    },
    licenceID(id) {
        return axios.get(`/admin/licence_site/${id}`);
    },
    licenceSlug(licence) {
        return axios.get(`/admin/licence_site/view/${licence}`);
    },
    conditionID(id) {
        return axios.get(`/admin/conditions/${id}`);
    },
    conditionSlug(condition) {
        return axios.get(`/admin/conditions/view/${condition}`);
    },
    legalnoticeID(id) {
        return axios.get(`/admin/legal_notice/${id}`);
    },
    legalnoticeSlug(legalnotice) {
        return axios.get(`/admin/legal_notice/view/${legalnotice}`);
    },
    policyprivacyID(id) {
        return axios.get(`/admin/policy_privacy/${id}`);
    },
    policyprivacySlug(policyprivacy) {
        return axios.get(`/admin/policy_privacy/view/${policyprivacy}`);
    },
    faqID(id) {
        return axios.get(`/admin/faqs/${id}`);
    },
    aboutID(id) {
        return axios.get(`/admin/abouts/${id}`);
    },
    presentationID(id) {
        return axios.get(`/admin/presentations/${id}`);
    },
    presentationSlug(presentation) {
        return axios.get(`/admin/presentations/view/${presentation}`);
    },
    //presentationSlug(presentation) {
    //    return axios.get(`/admin/presentations/view/${presentation}`);
    //},
    informationID(id) {
        return axios.get(`/admin/informations/${id}`);
    },
    informationSlug(information) {
        return axios.get(`/admin/informations/view/${information}`);
    },
    howtoregisterID(id) {
        return axios.get(`/admin/how_register/${id}`);
    },
    howtoregisterSlug(howtoregister) {
        return axios.get(`/admin/how_register/view/${howtoregister}`);
    },
    tasksbyuser(username) {
        return axios.get(`/admin/api/tasks/u/${username}`);
    },
    SlidesHomeID(id) {
        return axios.get(`/admin/slide_home/${id}`);
    },
    testimonialSlug(testimonial) {
        return axios.get(`/admin/testimonials/view/${testimonial}`);
    },
    testimonialID(id) {
        return axios.get(`/admin/testimonials/${id}`);
    }
};
