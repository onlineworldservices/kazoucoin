import axios from 'axios';

export default {
    all() {
        return axios.get('/api/orders/beautes');
    },
    ordersbyuser(username) {
        return axios.get(`/api/store/orders/orders-user/${username}`);
    },
    ordersbycategory(menucategoryorder) {
        return axios.get(`/api/beautes/topic/${menucategoryorder}`);
    },
    find(id) {
        return axios.get(`/api/orders/beautes/${id}`);
    },
};
