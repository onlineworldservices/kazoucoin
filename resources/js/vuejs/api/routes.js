import Login from "../components/auth/Login";
import DashboardIndex from "../components/admin/DashboardIndex";
import UserShowProfile from "../components/account/UserShowProfile";
import BeautesByCategory from "../components/user/orders/beautes/BeautesByCategory";
import BeautesUpdate from "../components/user/orders/beautes/BeautesUpdate";
import BeautesOrdersByUser from "../components/user/orders/beautes/BeautesOrdersByUser";
import Beautes from "../components/user/orders/beautes/Beautes";
import UserProfile from "../components/account/UserProfile";
import ChangePassword from "../components/account/ChangePassword";
import AdminBeautes from "../components/admin/sites/orders/AdminBeautes";
import Devises from "../components/admin/Devises";
import Colors from "../components/admin/Colors";
import Priorities from "../components/admin/Priorities";
import Status from "../components/admin/Status";
import Countries from "../components/admin/Countries";
import Administrator from "../components/admin/partials/administrator/Administrator";
import AdministratorEdit from "../components/admin/partials/administrator/AdministratorEdit";
import AdministratorDatatables from "../components/admin/partials/administrator/AdministratorDatatables";
import AdminAccount from "../components/account/AdminAccount";
import AdminProfileEdit from "../components/account/AdminProfileEdit";
import AdminChangePassword from "../components/account/AdminChangePassword";
import Links from "../components/admin/Links";
import Tags from "../components/admin/Tags";
import Users from "../components/admin/Users";
import Notes from "../components/admin/Notes";
import TaskIndex from "../components/admin/partials/task/TaskIndex";
import TaskUser from "../components/admin/partials/task/TaskUser";
import Subscribemail from "../components/admin/partials/Subscribemail";
import Permissions from "../components/admin/Permissions";
import Roles from "../components/admin/Roles";
import Activity_log from "../components/admin/partials/activity_log/Activity_log";
import Faqs from "../components/admin/faq/Faqs";
import FaqViewSites from "../components/admin/faq/FaqViewSites";
import FaqsCreate from "../components/admin/faq/FaqsCreate";
import FaqsEdit from "../components/admin/faq/FaqsEdit";
import Category_faq from "../components/admin/categories/Category_faq";
import Category_event from "../components/admin/categories/Category_event";
import Category_order from "../components/admin/categories/Category_order";
import Menu_category_event from "../components/admin/categories/Menu_category_event";
import Menu_category_order from "../components/admin/categories/Menu_category_order";
import Messages from "../components/admin/contact/contactus/Messages";
import Assistance from "../components/admin/contact/Assistance";
import Work_with_us from "../components/admin/contact/Work_with_us";
import Specialities from "../components/admin/partials/speciality/Specialities";
import AboutCreate from "../components/admin/about/AboutCreate";
import AboutEdit from "../components/admin/about/AboutEdit";
import Abouts from "../components/admin/about/Abouts";
import Informations from "../components/admin/partials/information/Informations";
import InformationsCreate from "../components/admin/partials/information/InformationsCreate";
import InformationsEdit from "../components/admin/partials/information/InformationsEdit";
import InformationsView from "../components/admin/partials/information/InformationsView";
import Testimonial from "../components/admin/partials/testimonials/Testimonial";
import TestimonialCreate from "../components/admin/partials/testimonials/TestimonialCreate";
import TestimonialEdit from "../components/admin/partials/testimonials/TestimonialEdit";
import TestimonialView from "../components/admin/partials/testimonials/TestimonialView";
import Howregister from "../components/admin/partials/stepsregistration/Howregister";
import HowregisterCreate from "../components/admin/partials/stepsregistration/HowregisterCreate";
import HowregisterEdit from "../components/admin/partials/stepsregistration/HowregisterEdit";
import HowregisterView from "../components/admin/partials/stepsregistration/HowregisterView";
import Aidecontacttheme from "../components/admin/Aidecontacttheme";
import Aidecontactdemande from "../components/admin/Aidecontactdemande";
import SlidesAbout from "../components/admin/slides/slide_about/SlidesAbout";
import SlidesConcept from "../components/admin/slides/slide_concept/SlidesConcept";
import SlidesContact from "../components/admin/slides/slide_contact/SlidesContact";
import SlidesCookies from "../components/admin/slides/slide_cookies/SlidesCookies";
import SlidesEvents from "../components/admin/slides/slide_events/SlidesEvents";
import SlidesFaqs from "../components/admin/slides/slide_faqs/SlidesFaqs";
import SlidesHome from "../components/admin/slides/slide_home/SlidesHome";
import SlidesHomeCreate from "../components/admin/slides/slide_home/SlidesHomeCreate";
import SlidesHomeEdit from "../components/admin/slides/slide_home/SlidesHomeEdit";
import SlidesLicences from "../components/admin/slides/slide_licences/SlidesLicences";
import SlidesMentionslegales from "../components/admin/slides/slide_mentionslegales/SlidesMentionslegales";
import SlidesOrders from "../components/admin/slides/slide_orders/SlidesOrders";
import SlidesOthers from "../components/admin/slides/slide_others/SlidesOthers";
import SlidesPolicy from "../components/admin/slides/slide_policy/SlidesPolicy";
import SlidesTestimonial from "../components/admin/slides/slide_testimonial/SlidesTestimonial";
import Policyprivacy from "../components/admin/info/policyprivacy/Policyprivacy";
import BeautesCreate from "../components/user/orders/beautes/BeautesCreate";
import MessageShow from "../components/admin/contact/contactus/MessageShow";
import SitePlan from "../components/user/SitePlan";
import Blog from "../components/user/Blog";

/* Legal information dans la cartalla info */
import LegalnoticeIndex from "../components/admin/info/legalnotice/Legalnotice";
import LegalnoticeCreate from "../components/admin/info/legalnotice/LegalnoticeCreate";
import LegalnoticeEdit from "../components/admin/info/legalnotice/LegalnoticeEdit";
import LegalnoticeView from "../components/admin/info/legalnotice/LegalnoticeView";
import LicenceIndex from "../components/admin/info/licence/LicenceIndex";
import LicenceCreate from "../components/admin/info/licence/LicenceCreate";
import LicenceEdit from "../components/admin/info/licence/LicenceEdit";
import LicenceView from "../components/admin/info/licence/LicenceView";
import CookieIndex from "../components/admin/info/cookie/CookieIndex";
import CookieCreate from "../components/admin/info/cookie/CookieCreate";
import CookieEdit from "../components/admin/info/cookie/CookieEdit";
import CookieView from "../components/admin/info/cookie/CookieView";
import TermsconditionsIndex from "../components/admin/info/conditions/TermsconditionsIndex";
import TermsconditionsCreate from "../components/admin/info/conditions/TermsconditionsCreate";
import TermsconditionsEdit from "../components/admin/info/conditions/TermsconditionsEdit";
import TermsconditionsView from "../components/admin/info/conditions/TermsconditionsView";
import PolicyprivacyCreate from "../components/admin/info/policyprivacy/PolicyprivacyCreate";
import PolicyprivacyEdit from "../components/admin/info/policyprivacy/PolicyprivacyEdit";
import PolicyprivacyView from "../components/admin/info/policyprivacy/PolicyprivacyView";
import MessageIndex from "../components/user/message/MessageIndex";
import MessageSend from "../components/user/message/MessageSend";
import MessageView from "../components/user/message/MessageView";

/* Presentation dans la cartalla presentation */
import PresentationCreate from "../components/admin/presentations/PresentationCreate";
import PresentationEdit from "../components/admin/presentations/PresentationEdit";
import PresentationView from "../components/admin/presentations/PresentationView";
import PresentationIndex from "../components/admin/presentations/PresentationIndex";
import AdministratorShow from "../components/admin/partials/administrator/AdministratorShow";
import DocumentationIndex from "../components/admin/info/documentation/DocumentationIndex";

export const routes = [
    { path: '/admin',  component: DashboardIndex },
    { path: '/admin/countries/', name: 'admin.countries', component: Countries },
    { path: '/admin/account/', name: 'admin.account', component: AdminAccount },
    { path: '/admin/edit/profile/', name: 'admin.edit_profile', component: AdminProfileEdit },
    { path: '/admin/change_password/', name: 'admin.change_password', component: AdminChangePassword },
    { path: '/admin/devises/', name: 'admin.devises', component: Devises },
    { path: '/admin/tag/', component: Tags },
    { path: '/admin/subscribe_email/', component: Subscribemail },
    { path: '/admin/faqs/', name: 'faqs.index', component: Faqs },
    { path: '/admin/faqs/v/sites', name: 'faqs.admin_sites', component: FaqViewSites },
    { path: '/admin/faqs/create', name: 'faqs.create', component: FaqsCreate },
    { path: '/admin/faqs/:id/edit', name: 'faqs.edit', component: FaqsEdit },
    { path: '/admin/aide/contact_themes/', name: 'contact_themes.index', component: Aidecontacttheme },
    { path: '/admin/demande/contact_demandes/', name: 'contact_demandes.index', component: Aidecontactdemande },
    { path: '/admin/category-event', component: Category_event },
    { path: '/admin/category-faq', component: Category_faq },
    { path: '/admin/category-order', component: Category_order },
    { path: '/admin/category-menu-event', component: Menu_category_event },
    { path: '/admin/category-menu-order', component: Menu_category_order },
    { path: '/admin/users', name: 'users.index',component: Users },
    { path: '/admin/notes', name: 'notes.index',component: Notes },
    { path: '/admin/tasks', name: 'tasks.index',component: TaskIndex },
    { path: '/admin/tasks/u/:username', name: 'tasks.view',component: TaskUser },
    { path: '/admin/permissions', component: Permissions },
    { path: '/admin/roles', component: Roles },
    { path: '/admin/all-activity-log-page', component: Activity_log },
    { path: '/admin/colors', component: Colors },
    { path: '/admin/priorities', component: Priorities },
    { path: '/admin/status', component: Status },
    { path: '/admin/links', component: Links },
    { path: '/admin/all-contact', name: 'all-contact.index', component: Messages },
    { path: '/admin/all-contact/msg/:contact', component: MessageShow },
    { path: '/admin/demande/contact/assistance/all-assistance', component: Assistance },
    { path: '/admin/all-work_with-us', component: Work_with_us },
    { path: '/admin/abouts', name: 'about.index', component: Abouts },
    { path: '/admin/abouts/create', name: 'about.create', component: AboutCreate },
    { path: '/admin/abouts/:id/edit', name: 'about.edit', component: AboutEdit },
    { path: '/admin/informations', name: 'informations.index', component: Informations },
    { path: '/admin/informations/create', name: 'informations.create', component: InformationsCreate },
    { path: '/admin/informations/:id/edit', name: 'informations.edit', component: InformationsEdit },
    { path: '/admin/informations/inf/:information', name: 'informations.view', component: InformationsView },
    { path: '/admin/testimonials', name: 'testimonials.index', component: Testimonial },
    { path: '/admin/testimonials/create', name: 'testimonials.create', component: TestimonialCreate },
    { path: '/admin/testimonials/:id/edit', name: 'testimonials.edit', component: TestimonialEdit },
    { path: '/admin/testimonials/tm/:testimonial', name: 'testimonials.view', component: TestimonialView },
    { path: '/admin/how_register', name: 'how_register.index', component: Howregister },
    { path: '/admin/how_register/create', name: 'how_register.create', component: HowregisterCreate },
    { path: '/admin/how_register/:id/edit', name: 'how_register.edit', component: HowregisterEdit },
    { path: '/admin/how_register/hw/:howtoregister', name: 'how_register.view', component: HowregisterView },
    { path: '/admin/contact/speciality', component: Specialities },
    { path: '/admin/administrators', name: 'administrators.index', component: AdministratorDatatables },
    { path: '/admin/administrators/u/datatable', name: 'administrators.datatable', component: Administrator },
    { path: '/admin/administrators/:id/edit', name: 'administrators.edit', component: AdministratorEdit },
    { path: '/admin/administrators/:username', name: 'administrators.profile', component: AdministratorShow },
    { path: '/user/change_password', component: ChangePassword },
    { path: '/account/profile', name: 'myaccount.profile', component: UserProfile },
    { path: '/login', name: 'login', component: Login },
    { path: '/admin/profile/:username', name: 'user.profile', component: UserShowProfile },
    { path: '/site_plan', name: 'site_plan', component: SitePlan },
    { path: '/blog', name: 'blog', component: Blog },



    /* Route beautes page site*/
    { path: '/orders/beautes', name: 'beautes.index', component: Beautes },
    { path: '/orders/beautes/create', name: 'beautes.create', component: BeautesCreate },
    { path: '/orders/store/:username', name: 'orders.users', component: BeautesOrdersByUser },
    { path: '/orders/beautes/:id/edit', name: 'beautes.edit', component: BeautesUpdate },
    { path: '/orders/beautes/topic/:menucategoryorder', name: 'category.beaute', component: BeautesByCategory },
    { path: '/orders/beautes/topic/:menucategoryorder/:beaute', name: 'view.beaute' },

    /* Slide route */
    { path: '/admin/slide-about-us', name: 'slide-about-us.index', component: SlidesAbout },
    { path: '/admin/slide_concept', name: 'slide_concept.index', component: SlidesConcept },
    { path: '/admin/slide_contact-us', name: 'slide_contact-us.index', component: SlidesContact, },
    { path: '/admin/slide_cookie', name: 'slide_cookie.index', component: SlidesCookies },
    { path: '/admin/slide_evenement', name: 'slide_evenement.index', component: SlidesEvents },
    { path: '/admin/slide_faq', name: 'slide_faq.index', component: SlidesFaqs },
    { path: '/admin/slide_home', name: 'slide_home.index', component: SlidesHome },
    { path: '/admin/slide_home/create/', name: 'slide_home.create', component: SlidesHomeCreate },
    { path: '/admin/slide_home/:id/edit', name: 'slide_home.edit', component: SlidesHomeEdit },
    { path: '/admin/slide_licence', name: 'slide_licence.index', component: SlidesLicences },
    { path: '/admin/slide_mentionslegale', name: 'slide_mentionslegale.index', component: SlidesMentionslegales },
    { path: '/admin/slide_order', name: 'slide_order.index', component: SlidesOrders },
    { path: '/admin/slide_other', name: 'slide_other.index', component: SlidesOthers },
    { path: '/admin/slide_policy', name: 'slide_policy.index', component: SlidesPolicy },
    { path: '/admin/slide_testimonial', name: 'slide_testimonial.index', component: SlidesTestimonial },

    /* Legal Mention route  */
    { path: '/admin/legal_notice', name: 'legal_notice.index', component: LegalnoticeIndex },
    { path: '/admin/legal_notice/create', name: 'legal_notice.create', component: LegalnoticeCreate },
    { path: '/admin/legal_notice/:id/edit', name: 'legal_notice.edit', component: LegalnoticeEdit },
    { path: '/admin/legal_notice/lm/:legalnotice', name: 'legal_notice.view', component: LegalnoticeView, },
    { path: '/admin/licence_site', name: 'licence_site.index', component: LicenceIndex },
    { path: '/admin/licence_site/create/', name: 'licence_site.create', component: LicenceCreate },
    { path: '/admin/licence_site/:id/edit', name: 'licence_site.edit', component: LicenceEdit },
    { path: '/admin/licence_site/lm/:licence', name: 'licence_site.view', component: LicenceView },
    { path: '/admin/policy_privacy', name: 'policy_privacy.index', component: Policyprivacy },
    { path: '/admin/policy_privacy/create', name: 'policy_privacy.create', component: PolicyprivacyCreate },
    { path: '/admin/policy_privacy/:id/edit', name: 'policy_privacy.edit', component: PolicyprivacyEdit },
    { path: '/admin/policy_privacy/lm/:policyprivacy', name: 'policy_privacy.view', component: PolicyprivacyView },
    { path: '/admin/conditions', name: 'conditions.index', component: TermsconditionsIndex },
    { path: '/admin/conditions/create', name: 'conditions.create', component: TermsconditionsCreate },
    { path: '/admin/conditions/:id/edit', name: 'conditions.edit', component: TermsconditionsEdit },
    { path: '/admin/conditions/lm/:condition', name: 'conditions.view', component: TermsconditionsView },
    { path: '/admin/cookies-site', name: 'cookies-site.index', component: CookieIndex },
    { path: '/admin/cookies-site/create', name: 'cookies-site.create', component: CookieCreate },
    { path: '/admin/cookies-site/:id/edit', name: 'cookies-site.edit', component: CookieEdit },
    { path: '/admin/cookies-site/lm/:cookie', name: 'cookies-site.view', component: CookieView },

    /* Presentation route */
    { path: '/admin/presentations', name: 'presentations.index', component: PresentationIndex },
    { path: '/admin/presentations/create', name: 'presentations.create', component: PresentationCreate },
    { path: '/admin/presentations/:id/edit', name: 'presentations.edit', component: PresentationEdit },
    { path: '/admin/presentations/lm/:presentation', name: 'presentations.view', component: PresentationView },

    /* Category site get route */
    { path: '/admin/orders/beautes', name: 'admin.beaute', component: AdminBeautes },

    /* Documentation site get route */
    { path: '/admin/documentations', name: 'documentations.index', component: DocumentationIndex },
    {
        path: '/admin/messages/',
        name: 'messages.index',
        component: MessageIndex,
        //children: [
        //    {path: 'send/', name: 'messages.send', component: MessageSend,},
        //]
    },
    {path: '/admin/messages/msg/:message/', name: 'messages.view', component: MessageView},
    {path: '/admin/messages/m/send/', name: 'messages.send', component: MessageSend}
];
