/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



require('./bootstrap');

window.Vue = require('vue');
Vue.use(require('vue-resource'));


import { Form, HasError, AlertError } from 'vform'

window.Form = Form;

Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

/**
 * Vue select
 */
import 'vue-search-select/dist/VueSearchSelect.css'
import vSelect from 'vue-select'
Vue.component('v-select', vSelect);

/**
 * Router vuejs
 */
import VueRouter from 'vue-router'
Vue.use(VueRouter);

/**
 * Stars Rating
 */
import rate from 'vue-rate';
Vue.use(rate);

import InfiniteLoading from 'vue-infinite-loading';
Vue.use(InfiniteLoading);

/**
 * VueEditor
 */
import {VueEditor, Quill} from "vue2-editor";
Vue.component('VueEditor', VueEditor);
Vue.component('Quill', Quill);


// ...

import {routes} from './api/routes';
const router = new VueRouter({
    routes,
    mode: 'history',
    linkActiveClass: "active"
    //linkExactActiveClass: "active" // active class for *exact* links.

});

/**
 * Ici c'est pour le number
 */
import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask);


import Vue2Filters from 'vue2-filters'
Vue.use(Vue2Filters);
/**
 * Ici c'est pour configurer les alerts surtout le sweetalert2
*/
import Swal from 'sweetalert2';
window.swal = swal;
const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.toast = toast;

/** Ici c'est pour configurer le progressbar se referer a la documentation
* https://github.com/hilongjw/vue-progressbar#requirements
*/
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
  color: 'rgba(255,158,15,1)',
  failedColor: 'red',
  thickness: '4px',
  height: '2px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300
  },
  autoRevert: true,
  location: 'top',
  inverse: false
});



Vue.filter('upText',function (text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
});


/** Ici c'est pour configurer les date se referer a la documentation
* pour plus d'information visiter ce lien
* https://momentjs.com
*/

import moment from 'moment'
require("moment/min/locales.min");
moment.locale('fr');

Vue.filter('myDate',function (created) {
    return moment(created).format('ll');
});

Vue.filter('ageDate',function (created) {
    return moment(created).format('l');
});

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY');
    }
});

Vue.filter('formatDateAndHour', function(value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY HH:mm:ss');
    }
});


Vue.filter('dateFormat',function (created) {
    return moment(created).format('lll'); // mars 2 2019, 12:32:56 pm
});


Vue.filter('dateAgo',function (created) {
    return moment(created).fromNow(); // date ago
});

Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('en-US', {

        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

window.Fire = new Vue();
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/CommentComponent.vue -> <example-component></example-component>
 */


Vue.component('email-verify', require('./components/auth/Verify').default);
Vue.component('comments', require('./components/comments/Comments.vue').default);
Vue.component('coverpage', require('./components/admin/image/Coverpage.vue').default);
Vue.component('homeprofile', require('./components/admin/image/Homeprofile.vue').default);
Vue.component('pagination', require('laravel-vue-pagination').default);
Vue.component('tags', require('./components/admin/Tags.vue').default);
Vue.component('login-modal', require('./components/auth/LoginModal').default);
Vue.component('register-user', require('./components/auth/RegisterUser').default);
Vue.component('homepageprofile', require('./components/admin/partials/Homepageprofile.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('pagination-link', require('./components/inc/vendor/PaginationComponent').default);
Vue.component('loader-ellipsis', require('./components/inc/animation/LoaderEllipsis').default);



// Nous somme ici sur la partie utilisateur du site
Vue.component('contact-page', require('./components/user/Contact.vue').default);
Vue.component('aide-contact-messages', require('./components/user/AideContactMessages.vue').default);
Vue.component('news-letter', require('./components/user/Newsletter').default);
Vue.component('verify-link', require('./components/auth/verify/VerifyLink').default);
Vue.component('testimonial-page', require('./components/user/Testimonial.vue').default);
Vue.component('chat-page', require('./components/user/Chat.vue').default);
Vue.component('site-plan', require('./components/user/SitePlan').default);

//Vue.component('add-comment', require('./components/comments/AddComment.vue'));

// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
