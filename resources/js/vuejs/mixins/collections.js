export default {
    data(){
        return {
            items: {}
        }
    },

    method:{
        add(item){
            return this.items.push(item);
        },
        remove(index){
            return this.items.splice(index,1);
        }
    }
}