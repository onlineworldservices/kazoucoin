@extends('inc.admin._app')
<?php $usernameTag = htmlspecialchars($user->name); ?>
@section('title',"- $usernameTag")
@section('sectionTitle', 'Admin Change Password')

@section('style')

@endsection

@section('content')

<router-view></router-view>
@endsection

@section('script')

@endsection