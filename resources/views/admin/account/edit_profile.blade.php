@extends('inc.admin._app')
<?php $usernameTag = htmlspecialchars($user->name); ?>
@section('title',"- $usernameTag")
@section('sectionTitle', 'Admin Edit Profile')
@section('style')
@endsection

@section('init')
<!-- Site wrapper -->
@endsection

@section('content')
    <router-view company="{{config("app.name")}}"></router-view>
@endsection
@section('script')

<script>
    $(document).ready(function () {
        $(".datepicker").datetimepicker({
            format: "DD/MM/YYYY",
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                today: "fa fa-screenshot",
                clear: "fa fa-trash",
                close: "fa fa-remove"
            }
        })
    });
</script>
@endsection

