@extends('inc.admin._app')
<?php $usernameTag = htmlspecialchars($user->name); ?>
@section('title',"- $usernameTag")
@section('sectionTitle', 'Profile')
@section('style')
    @parent
@endsection

@section('init')
    <!-- Site wrapper -->
@endsection

@section('content')
    <router-view company="{{config("app.name")}}"></router-view>
@endsection
@section('script')
    @parent
@endsection



