@extends('inc.admin._main')
@section('title', '- Admin Agenda')
@section('sectionTitle', 'My Agenda')
@section('style')

@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-info card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">event_available</i>
                        </div>
                        <h4 class="card-title"><b>My Agenda</b></h4>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-2 col-md-4 mr-auto ml-auto">
                            <button class="btn btn-primary btn-round" data-toggle="modal" data-target="#myModal">
                                <i class="material-icons">event_note</i> 
                                <b>Add Note</b>
                            </button>
                        </div>
                        <br>
                        {!! $calendar->calendar() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-120 ml-auto mr-auto">
                <div class="card card-calendar">
                    <div class="card-body ">
                        <div id="fullCalendar"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Classic Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add New Note into Agenda</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="material-icons">clear</i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="RegisterEvent" method="POST" action="#" accept-charset="UTF-8">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-lg-12 mr-auto">
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">description</i>
                                                </span>
                                            </div>
                                            <input id="title" type="text"
                                                   class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                   name="title" value="{{ old('title') }}" minLength="5" placeholder="Title ..." required="required"  >
                                            @if ($errors->has('username'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('startevent') ? ' has-error' : '' }}">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                    </div>
                                    <input type="text"
                                           class="form-control{{ $errors->has('startevent') ? ' is-invalid' : '' }} datepicker "
                                           id="startevent" name="startevent"
                                           value="{{ old('startevent') }}" placeholder="Start Event ..." required="required">
                                    @if ($errors->has('birthday'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('startevent') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('endevent') ? ' has-error' : '' }}">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">date_range</i>
                                        </span>
                                    </div>
                                    <input type="text"
                                           class="form-control{{ $errors->has('endevent') ? ' is-invalid' : '' }} datepicker "
                                           id="endevent" name="endevent"
                                           value="{{ old('endevent') }}" placeholder="End Event ..." required="required">
                                    @if ($errors->has('endevent'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('endevent') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <br>
                            <div class="submit text-center">
                                <button class="btn btn-warning btn-raised btn-round " type="submit">
                                    <span class="btn-label">
                                        <i class="material-icons">check</i>
                                    </span>
                                    <b>Add</b>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('inc.admin._footer')
<script type="text/javascript">
    $(document).ready(function() {
        md.initFullCalendar();
    });
</script>


@endsection
@section('script')

{!! $calendar->script() !!}

@endsection