@extends('inc.admin._app')
@section('title', '- Admin Colors')
@section('sectionTitle', 'Colors')
@section('style')

@endsection
@section('content')
@if(Auth::user()->my_status === 'active')
    <router-view></router-view>
@else
<div class="submit text-center">
    @include('inc.admin.components.alert_permission')
</div>
@endif
@endsection

@section('script')
<script type="text/javascript">

    /**** Script confirm Download ****/
    $("#myColorDownload").click(function() {
        sweetAlert({
            title:'Download Color File',
            text: 'Are you sure you want to Download this file ?',
            type:'question',
            animation: true,
            customClass: 'animated bounceIn',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            showCancelButton: true,
            reverseButtons: true
        },function(isConfirm){
            alert('ok');
        });
        $('.swal2-confirm').click(function(){
            window.location.href = "{{route('colors_export')}}";
        });
    });
</script>
@endsection
