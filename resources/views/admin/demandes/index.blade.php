@extends('inc.admin._app')
@section('title', '- Admin Aide Demandes')
@section('sectionTitle', 'Aide Demandes')

@section('style')

@endsection

@section('content')
@if(Auth::user()->my_status === 'active')
	<router-view></router-view>
@include('inc.admin._footer')
@else
<div class="submit text-center">
    @include('inc.admin.components.alert_permission')
</div>
@endif
@endsection

@section('script')

@endsection
