@extends('inc.admin._app')
<?php $titleTag = htmlspecialchars($user->name); ?>
@section('title',"- $titleTag")
@section('sectionTitle', 'Administrators')
@section('style')
@parent

@endsection

@section('init')
<!-- Site wrapper -->
@endsection

@section('content')
    @if(Auth::user()->my_status === 'active')
        <router-view></router-view>
    @else
        <div class="submit text-center">
            @include('inc.admin.components.alert_permission')
        </div>
    @endif
@endsection

@section('script')
@parent
@endsection



