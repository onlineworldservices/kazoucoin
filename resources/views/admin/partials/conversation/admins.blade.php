
<div class="col-md-3">

    <div class="list-group">
        @foreach($admins as $admin)
        <a class="list-group-item" href="{{ route('conversations.show', $admin->id )}}">{{ $admin->name }}</a>
        @endforeach
    </div>

</div>
