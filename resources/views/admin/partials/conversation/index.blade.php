@extends('inc.admin._main')
@section('title', '- Admin Message Conversation ')
@section('sectionTitle', 'Message Conversation')
@section('style')


@endsection

@section('content')

<div class="content">
    <div class="container-fluid">
        @include('inc.admin.components.status_admin')
        <div class="header text-center ml-auto mr-auto">
            <h3 class="title">Material Dashboard Heading</h3>
            <p class="category">Created using Roboto Font Family</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body" id="app">



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('inc.admin._footer')
@endsection

@section('script')
<script src="{{ asset('js/app.js') }}"></script>
@endsection
