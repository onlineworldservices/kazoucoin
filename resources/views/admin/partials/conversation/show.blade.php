@extends('inc.admin._main')
@section('title', '- Admin Message Conversation ')
@section('sectionTitle', 'Message Conversation')
@section('style')

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        @include('inc.admin.components.status_admin')
        <div class="row">
            <div class="col-md-12">
                <div class="row">

                    @include('admin.partials.conversation.admins', ['admins' => $admins ])

                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header">{{ $admin->name }}</div>
                            <div class="card-body conversations">

                                <form method="post" action="">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <textarea class="form-control" name="content" placeholder="placer un message"></textarea>

                                    </div>
                                    <button class="btn btn-instagram" type="submit">send</button>
                                </form>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



@include('inc.admin._footer')
@endsection

@section('script')

@endsection
