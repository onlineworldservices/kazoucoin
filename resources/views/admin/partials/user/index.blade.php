@extends('inc.admin._app')
@section('title', '- Admin Users')
@section('sectionTitle', 'Users')

@section('style')

@endsection

@section('content')
@can('view-administrator')
@if(Auth::user()->my_status === 'active')
    <router-view></router-view>
@else
<div class="submit text-center">
    @include('inc.admin.components.alert_permission')
</div>
@endif
@endcan
@endsection

@section('script')
<script>
    
    /**** Script confirm Download ****/
    $("#myUserDownload").click(function() {
        sweetAlert({
            title:'Download User File',
            text: 'Are you sure you want to Download this file ?',
            type:'question',
            customClass: 'animated bounceIn',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            showCancelButton: true,
            reverseButtons: true
        },function(isConfirm){
            alert('ok');
        });
        $('.swal2-confirm').click(function(){
            window.location.href = "{{route('users_export')}}";
        });
    });
</script>
@endsection
