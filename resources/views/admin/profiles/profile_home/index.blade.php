@extends('inc.admin._main')
@section('title',' - Admin Profile Image Site')
@section('sectionTitle', 'Home Profile Page Site')

@section('style')

@endsection

@section('content')
@if(Auth::user()->my_status === 'active')
<div class="content" id="app">
    <div class="container-fluid">
        <br>
        @include('inc.admin.components.status_admin')
        <br>
        <homeprofile></homeprofile>
        @include('inc.component-vue-js')
    </div>
</div>
@include('inc.admin._footer')
@else
<div class="submit text-center">
    @include('inc.admin.components.alert_permission')
</div>
@endif
@endsection

@section('script')

@endsection
