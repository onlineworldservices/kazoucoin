@extends('inc.admin._app')
@section('title', '- Admin Status')
@section('sectionTitle', 'Status')
@section('style')

@endsection
@section('content')
    @if(Auth::user()->my_status === 'active')
        <router-view></router-view>
    @else
        <div class="submit text-center">
            @include('inc.admin.components.alert_permission')
        </div>
    @endif
@endsection

@section('script')
    <script type="text/javascript">

        /**** Script confirm Download ****/
        $("#myStatusDownload").click(function() {
            sweetAlert({
                title:'Download Status File',
                text: 'Are you sure you want to Download this file ?',
                type:'question',
                animation: true,
                customClass: 'animated bounceIn',
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                showCancelButton: true,
                reverseButtons: true
            },function(isConfirm){
                alert('ok');
            });
            $('.swal2-confirm').click(function(){
                window.location.href = "{{route('status_export')}}";
            });
        });
    </script>
@endsection