@extends('inc.main')
@section('title', '| Login')


@section('style')
<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
@endsection

@section('content')
<div class="signup-page sidebar-collapse">

@if(count($coverpages) > 0)
@foreach($coverpages as $item)
<div class="page-header header-filter" style="background-image: url(&apos;{{ $item['photo'] }}&apos;); background-size: cover; background-position: top center;">
	@endforeach
	@else
	<div class="page-header header-filter" filter-color="warning" data-parallax="true">
		@endif
		<!-- <div class="page-header header-filter" filter-color="warning" data-parallax="true">-->
		<div class="container" id="app">
			<div class="row">
				<div class="col-lg-4 col-md-6 ml-auto mr-auto">
					<div class="card card-login">
						<div class="card-header card-header-warning text-center">
							<h4 class="card-title kazoucoin-subtitle title-white">Connect With</h4>
							<div class="social-line">
								<a href="{{ route('social.oauth', 'facebook') }}" class="btn btn-just-icon btn-facebook btn-round">
									<i class="fab fa-facebook-f"></i>
								</a>
								<a href="{{ route('social.oauth', 'google') }}" class="btn btn-just-icon btn-google btn-round">
									<i class="fab fa-google-plus"></i>
								</a>
								<a href="{{ route('social.oauth', 'github') }}" class="btn btn-just-icon btn-github btn-round" >
									<i class="fab fa-github"></i>
								</a>
							</div>
						</div>
                        <router-view></router-view>
					</div>
				</div>
			</div>
		</div>
		@include('inc._footer')
	</div>
</div>
@endsection
@section('scripts')

@endsection
