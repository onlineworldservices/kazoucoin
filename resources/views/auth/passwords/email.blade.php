@extends('inc.main')
@section('title', '| Mot de Passe oublié')

@section('content')

    @if(count($coverpages) > 0)
        @foreach($coverpages as $item)
            <div class="page-header header-filter header-small" data-parallax="true"
                 style="background-image: url(&apos;{{ $item['photo'] }}&apos;); background-size: cover; background-position: top center;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h2 class="title">Forgot your password ?</h2>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="page-header header-filter header-small" filter-color="warning" data-parallax="true"
             style="background-image: url(&apos;/assets/img/kit/password.jpg&apos;);">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title">Forgot your password ?</h2>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="main main-raised">
        <div class="container">
            <div class="pricing-2">
                <div class="row">
                    <div class="col-md-10 ml-auto mr-auto text-center">
                        <h4 class="kazoucoin-subtitle title-grey">Enter the e-mail address you indicated when
                            registering to receive a link to reset your password</h4>
                        @include('inc.alert')
                    </div>
                    <div class="col-md-6 ml-auto mr-auto">
                        @if (session('status'))
                            <div class="alert alert-success text-center">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <!-- <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="exampleInputEmails" class="bmd-label-floating">{{ __('E-mail address')}}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                 <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                                <div class="text-right">
                                    <a class="text-info"
                                       href="{{ route('login') }}">{{ __('Do your remember your password ?') }}</a>
                            </div>
                        </div> -->

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">mail</i>
                                    </span>
                                    </div>
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ $email ?? old('email') }}" required autofocus
                                           placeholder="E-mail address ...">

                                    @if ($errors->has('email'))
                                        <span style="padding-left: 20px;" class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="text-right">
                                    <a class="title-red"
                                       href="{{ route('login') }}">{{ __('Do your remember your password ?') }}</a>
                                </div>
                                <br>
                                <div class="submit text-center">
                                    <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }} text-center">
                                        {!! NoCaptcha::renderJs() !!}
                                        {!! NoCaptcha::display() !!}

                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                  <strong class="text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="submit text-center">
                                <button id="button_hover" class="btn btn-warning btn-raised btn-round title_hover"
                                        type="submit">
                                <span class="btn-label">
                                    <i class="material-icons">email</i>
                                  </span>
                                    <b class="title_hover">Send e-mail</b>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('inc.footer')
    </div>

@endsection
