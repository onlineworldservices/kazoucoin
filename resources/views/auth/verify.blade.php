@extends('inc.app')
@section('title', '- Confirmation Email')
@section('style')

@endsection
@section('content')
<div class="signup-page sidebar-collapse" id="app">
	@include('inc.nav_bar')
	<div class="page-header header-filter"
		 style="background-image: url(&apos;{{ url(Auth::user()->avatarcover)  }}&apos;); background-size: cover; background-position: top center;">
		<div class="container">
			<div class="col-md-6 ml-auto mr-auto">
				<div style="padding-top: -50px;" class="profile text-center ">
					<div class="avatar">
						<div class="fileinput fileinput-new text-center"
							 data-provides="fileinput">
							<div class="fileinput-new thumbnail img-circle img-raised">
								@if(Auth::user()->avatar)
									<img src="{{ url(Auth::user()->avatar)  }}" alt="{{ Auth::user()->email }}">
								@endif
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail img-circle img-raised"></div>
						</div>
					</div>
				</div>
			</div>
			<!--<div class="row" style="padding-top: 50px; padding-bottom: 50px;">
				<div class="col-md-6 mr-auto ml-auto">
					<div class="card">
						<div class="card-header text-center card-header-warning card-header-icon">
							<div class="card-icon">
								<i class="material-icons">mail_outline</i>
							</div>
							<h4 class="card-title">Email Verification</h4>
						</div>
						<div class="card-body">
							@if (session('resent'))
							<div class="alert alert-success" role="alert">
								{{ __('A fresh verification link has been sent to your email address.') }}
							</div>
							@endif
							<br>
							{{ __('Before proceeding in our platform, please check your registration email for a secure verification.') }}
							<br>
						</div>
						<div class="card-footer text-right">
							<div class="form-check mr-auto">
								{{ __('Have not received the confirm link ?') }}

							</div>
							<div class="submit text-center">
								<a href="{{ route('verification.resend') }}" class="btn btn-success btn-raised btn-round">
									<span class="btn-label">
										<i class="material-icons">email</i>
									</span>
									<b>Send Again</b>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>-->
			<email-verify></email-verify>
			<vue-progress-bar></vue-progress-bar>
		</div>
		<br>
		<br>
		@include('inc._footer')
	</div>
</div>

@endsection

@section('scripts')
@parent
@endsection
