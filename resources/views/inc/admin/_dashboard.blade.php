@if(Auth::user()->avatar)
<div class="sidebar" data-color="{{ Auth::user()->color_style }}" data-background-color="black" data-image="{{ Auth::user()->avatar }}">
	@endif
	<!--
Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
Tip 2: you can also add an image using data-image tag
-->
	<div class="logo">
		<a href="/" class="simple-text logo-mini" target="_blank">
			<b>KC</b>
		</a>
		<a href="/" class="simple-text logo-normal" target="_blank">
			<b style="font-size: 80%;">{{ config('app.name') }}</b>
		</a>
	</div>
	<div class="sidebar-wrapper">
		<div class="user">
			<div class="photo">
				@if(Auth::user()->avatar)
				<img src="{{ Auth::user()->avatar  }}" />
				@endif
			</div>
			<div class="user-info">
				<a data-toggle="collapse" href="#collapseExample" class="username">
					<span>
						<b>{{ Auth::user()->name }}</b>
						<b class="caret"></b>
					</span>
				</a>
				<div class="collapse" id="collapseExample">
					<ul class="nav">
						<li class="nav-item {{ setActive(['admin/profile']) }}">
							<a class="nav-link" href="{{ route('admin.account')}}">
								<span class="sidebar-mini"><b>MP</b></span>
								<span class="sidebar-normal"><b>My Profile</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/account']) }}">
							<a class="nav-link" href="{{route('admin.edit_profile')}}">
								<span class="sidebar-mini">EP</span>
								<span class="sidebar-normal"><b>Edit Profile</b></span>
							</a>
						</li>

						<li class="nav-item {{ setActive(['admin/change_password']) }}">
							<a class="nav-link" href="{{route('admin.change_password')}}">
								<span class="sidebar-mini">CP</span>
								<span class="sidebar-normal"><b>Change Password</b></span>
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" href="#">
								<span class="sidebar-mini"><b>MS</b></span>
								<span class="sidebar-normal"><b>Member since:
									{!! Auth::user()->created_at->format('\<\s\t\r\o\n\g\>d\</\s\t\r\o\n\g\> M Y') !!}
									</b>
								</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">
								<span class="sidebar-mini"><b>CL</b></span>
								<span class="sidebar-normal"><b>Current Login:
									{!! str_limit( \Carbon\Carbon::parse(Auth::user()->current_sign_in_at)->diffForHumans(), 30,'...')!!}</b>
								</span>
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" id="btnShowAlertLogout" style="cursor:pointer;">
								<span class="sidebar-mini"><b>L</b></span>
								<span class="sidebar-normal"><b>Logout</b></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<ul class="nav">
			<!-- Dashboard -->
			<li class="nav-item {{ setActive('admin') }}">
				<a class="nav-link" href="/admin">
					<i class="material-icons">dashboard</i>
					<p><b>Dashboard</b></p>
				</a>
			</li>
			<!-- Links -->
			<li class="nav-item {{ setActive('admin/links*') }}">
				<a class="nav-link" href="{{route('links.index')}}">
					<i class="material-icons">http</i>
					<p><b>Links</b></p>
				</a>
			</li>
            <!-- Countries -->
			<li class="nav-item {{ setActive('admin/countries*') }}">
				<a class="nav-link" href="{{ route('countries.index') }}">
					<i class="material-icons">flag</i>
					<p><b>Countries</b></p>
				</a>
			</li>
			<!-- Devises -->
			<li class="nav-item {{ setActive('admin/devises*') }}">
				<a class="nav-link" href="{{ route('devises.index') }}">
					<i class="material-icons">attach_money</i>
					<p><b>Currency</b></p>
				</a>
			</li>
			<!-- Colors -->
			<li class="nav-item {{ setActive('admin/color*') }}">
				<a class="nav-link" href="{{ route('colors.index') }}">
					<i class="material-icons">color_lens</i>
					<p><b>colors</b></p>
				</a>
			</li>
			<!-- Tags -->
			<li class="nav-item {{ setActive('admin/tag*') }}">
				<a class="nav-link" href="{{route('tag.index')}}">
					<i class="material-icons">local_offer</i>
					<p><b>Tags</b></p>
				</a>
			</li>
			<!-- Subscribe Newletters -->
			<li class="nav-item {{ setActive('admin/subscribe_email*') }}">
				<a class="nav-link" href="{{route('emailsubscribe')}}">
					<i class="material-icons">unsubscribe</i>
					<p><b>Newletters</b></p>
				</a>
			</li>
			<!-- Articles Site -->
			<li class="nav-item {{ Active::check(['admin/orders/beautes*']) }} ">
				<a class="nav-link" data-toggle="collapse" href="#ordersExamples">
					<i class="material-icons">card_giftcard</i>
					<p><b>Orders</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="ordersExamples">
					<ul class="nav">
						<li class="nav-item {{ setActive('admin/abouts*') }}">
							<a class="nav-link" href="{{route('abouts.index')}}">
								<span class="sidebar-mini"><b>IF</b></span>
								<span class="sidebar-normal"><b>Informatiques</b></span>
							</a>
						</li>
					</ul>
                    <ul class="nav">
						<li class="nav-item {{ setActive('admin/orders/beautes*') }}">
							<a class="nav-link" href="{{route('admin.beaute')}}">
								<span class="sidebar-mini"><b>BE</b></span>
								<span class="sidebar-normal"><b>Beautes</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
            <!-- Categories Faqs -->
			<li class="nav-item {{ setActive(['admin/category-faq*','admin/faqs*']) }}">
				<a class="nav-link" data-toggle="collapse" href="#faqsExamples">
					<i class="material-icons">contact_support</i>
					<p><b>FAQs</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="faqsExamples">
					<ul class="nav">
						<li class="nav-item {{ setActive(['admin/category-faq*']) }}">
							<a class="nav-link" href="{{route('category-faq.index')}}">
								<span class="sidebar-mini"><b>CF</b></span>
								<span class="sidebar-normal"><b>Category FAQs</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/faqs*']) }}">
							<a class="nav-link" href="{{route('faqs.index')}}">
								<span class="sidebar-mini"><b>FQ</b></span>
								<span class="sidebar-normal"><b>FAQs</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<!-- Pages -->
			<li class="nav-item {{ setActive(['admin/information*','admin/how_register*','admin/abouts*','admin/presentations*','admin/testimonial*','admin/contact/speciality*']) }}">
				<a class="nav-link" data-toggle="collapse" href="#pagesExamples">
					<i class="material-icons">style</i>
					<p><b>Pages</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="pagesExamples">
					<ul class="nav">
						<li class="nav-item {{ setActive(['admin/abouts*']) }}">
							<a class="nav-link" href="{{route('abouts.index')}}">
								<span class="sidebar-mini"><b>AM</b></span>
								<span class="sidebar-normal"><b>About Members</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/presentations*']) }}">
							<a class="nav-link" href="{{route('presentations.index')}}">
								<span class="sidebar-mini"><b>AP</b></span>
								<span class="sidebar-normal"><b>About Presentations</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/informations*']) }}">
							<a class="nav-link" href="{{ route('informations.index') }}">
								<span class="sidebar-mini"><b>IS</b></span>
								<span class="sidebar-normal"><b>Informations Site</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/how_register*']) }}">
							<a class="nav-link" href="{{ route('how_register.index') }}">
								<span class="sidebar-mini"><b>RI</b></span>
								<span class="sidebar-normal"><b>Registration infos</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/contact/speciality*']) }}">
							<a class="nav-link" href="{{ route('speciality.index') }}">
								<span class="sidebar-mini"><b>SP</b></span>
								<span class="sidebar-normal"><b>Specialities</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/testimonial*']) }}">
							<a class="nav-link" href="{{route('testimonials.index')}}">
								<span class="sidebar-mini"><b>TE</b></span>
								<span class="sidebar-normal"><b>Testimonials</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<!-- Aide & Contacts -->
			<li class="nav-item {{ Active::check(['admin/demande/contact_demandes', 'admin/aide/contact_themes']) }} ">
				<a class="nav-link" data-toggle="collapse" href="#aidesExamples">
					<i class="material-icons">info</i>
					<p><b>Aide & Contact</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="aidesExamples">
					<ul class="nav">
						<li class="nav-item {{ active_check('admin/aide/contact_themes') }}">
							<a class="nav-link" href="{{ route('contact_themes.index') }}">
								<span class="sidebar-mini"><b>AT</b></span>
								<span class="sidebar-normal"><b>Aide Themes</b></span>
							</a>
						</li>
						<li class="nav-item {{active_check('admin/demande/contact_demandes')}}">
							<a class="nav-link" href="{{ route('contact_demandes.index') }}">
								<span class="sidebar-mini"><b>AD</b></span>
								<span class="sidebar-normal"><b>Aide Demandes</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<!-- All Categories Orders & Events -->
			<li class="nav-item {{ setActive(['admin/category-order*','admin/category-menu-order*','admin/category-event*','admin/category-menu-event*']) }} ">
				<a class="nav-link" data-toggle="collapse" href="#aidesCategories">
					<i class="material-icons">category</i>
					<p><b>Categories</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="aidesCategories">
					<ul class="nav">
						<li class="nav-item {{ setActive(['admin/category-event*','admin/category-menu-event*']) }}">
							<a class="nav-link" data-toggle="collapse" href="#componentsEvent">
								<span class="sidebar-mini"><b>EV</b></span>
								<span class="sidebar-normal"><b>Events</b>
									<b class="caret"></b>
								</span>
							</a>
							<div class="collapse" id="componentsEvent">
								<ul class="nav">
									<li class="nav-item {{ setActive(['admin/category-event*']) }}">
										<a class="nav-link" href="{{route('category-event.index')}}">
											<span class="sidebar-mini"><b>CE</b></span>
											<span class="sidebar-normal"><b>Categories Events</b></span>
										</a>
									</li>

									<li class="nav-item {{ setActive(['admin/category-menu-event*']) }}">
										<a class="nav-link" href="{{ route('category-menu-event.index')}}">
											<span class="sidebar-mini"><b>ME</b></span>
											<span class="sidebar-normal"><b>Menu Categories Events</b></span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item {{ setActive(['admin/category-order*','admin/category-menu-order*']) }}">
							<a class="nav-link" data-toggle="collapse" href="#componentsOrders">
								<span class="sidebar-mini"><b>OD</b></span>
								<span class="sidebar-normal"><b>Orders</b>
									<b class="caret"></b>
								</span>
							</a>
							<div class="collapse" id="componentsOrders">
								<ul class="nav">
									<li class="nav-item {{ setActive(['admin/category-order*']) }}">
										<a class="nav-link" href="{{ route('category-order.index')}}">
											<span class="sidebar-mini"><b>CO</b></span>
											<span class="sidebar-normal"><b>Categories Orders</b></span>
										</a>
									</li>

									<li class="nav-item {{ setActive(['admin/category-menu-order*']) }}">
										<a class="nav-link" href="{{ route('category-menu-order.index')}}">
											<span class="sidebar-mini"><b>MO</b></span>
											<span class="sidebar-normal"><b>Menu Categories Orders</b></span>
										</a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</li>
			<!-- Slides Pages -->
			<li class="nav-item {{ setActive(['admin/slide_home*','admin/slide-about-us*','admin/slide_evenement*','admin/slide_faq*','admin/slide_order*','admin/slide_other*','admin/slide_contact-us*','admin/slide_testimonial*','admin/slide_concept*','admin/slide_policy*','admin/slide_cookie*','admin/slide_mentionslegale*','admin/slide_licence*']) }}">
				<a class="nav-link" data-toggle="collapse" href="#slideExamples">
					<i class="material-icons">perm_media</i>
					<p><b>Slides Page</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="slideExamples">
					<ul class="nav">
						<li class="nav-item {{ setActive(['admin/slide-about-us*']) }}">
							<a class="nav-link" href="{{ route('slide-about-us.index')}}">
								<span class="sidebar-mini"><b>AS</b></span>
								<span class="sidebar-normal"><b>About Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_concept*']) }}">
							<a class="nav-link" href="{{ route('slide_concept.index')}}">
								<span class="sidebar-mini"><b>CS</b></span>
								<span class="sidebar-normal"><b>Concept Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_contact-us*']) }}">
							<a class="nav-link" href="{{ route('slide_contact-us.index')}}">
								<span class="sidebar-mini"><b>CS</b></span>
								<span class="sidebar-normal"><b>Contact Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_cookie*']) }}">
							<a class="nav-link" href="{{ route('slide_cookie.index')}}">
								<span class="sidebar-mini"><b>CS</b></span>
								<span class="sidebar-normal"><b>Cookies Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_evenement*']) }}">
							<a class="nav-link" href="{{ route('slide_evenement.index')}}">
								<span class="sidebar-mini"><b>EV</b></span>
								<span class="sidebar-normal"><b>Events Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_faq*']) }}">
							<a class="nav-link" href="{{ route('slide_faq.index')}}">
								<span class="sidebar-mini"><b>FS</b></span>
								<span class="sidebar-normal"><b>FAQs Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_home*']) }}">
							<a class="nav-link" href="{{ route('slide_home.index')}}">
								<span class="sidebar-mini"><b>HS</b></span>
								<span class="sidebar-normal"><b>Home Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_licence*']) }}">
							<a class="nav-link" href="{{ route('slide_licence.index')}}">
								<span class="sidebar-mini"><b>LS</b></span>
								<span class="sidebar-normal"><b>Licences Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_mentionslegale*']) }}">
							<a class="nav-link" href="{{ route('slide_mentionslegale.index')}}">
								<span class="sidebar-mini"><b>MS</b></span>
								<span class="sidebar-normal"><b>Mentions Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_order*']) }}">
							<a class="nav-link" href="{{ route('slide_order.index')}}">
								<span class="sidebar-mini"><b>OS</b></span>
								<span class="sidebar-normal"><b>Orders Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_other*']) }}">
							<a class="nav-link" href="{{ route('slide_other.index')}}">
								<span class="sidebar-mini"><b>OS</b></span>
								<span class="sidebar-normal"><b>Others Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_policy*']) }}">
							<a class="nav-link" href="{{ route('slide_policy.index')}}">
								<span class="sidebar-mini"><b>PS</b></span>
								<span class="sidebar-normal"><b>Policy Slides</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/slide_testimonial*']) }}">
							<a class="nav-link" href="{{ route('slide_testimonial.index') }}">
								<span class="sidebar-mini"><b>TS</b></span>
								<span class="sidebar-normal"><b>Testimonial Slides</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
            <!-- Cover Page & Home Profile -->
			<li class="nav-item {{ setActive(['admin/homepageprofile*','admin/covers-page*'])}}">
				<a class="nav-link" data-toggle="collapse" href="#profilepageExamples">
					<i class="material-icons">photo_size_select_actual</i>
					<p><b>Profile page</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="profilepageExamples">
					<ul class="nav">
						<li class="nav-item {{ setActive('admin/homepageprofile*') }}">
							<a class="nav-link" href="{{ route('homepageprofile.index') }}" >
								<span class="sidebar-mini"><b>HO</b></span>
								<span class="sidebar-normal"><b>Home profile page</b></span>
							</a>
						</li>
					</ul>
					<ul class="nav">
						<li class="nav-item {{ setActive('admin/covers-page*') }}">
							<a class="nav-link" href="{{ route('covers-page.index')}}">
								<span class="sidebar-mini"><b>CP</b></span>
								<span class="sidebar-normal"><b>Cover page</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
            <!-- Maintenance Site -->
			<li class="nav-item {{ setActive(['admin/telescope/requests*'])}}">
				<a class="nav-link" data-toggle="collapse" href="#profileExamples">
					<i class="material-icons">build</i>
					<p><b>Maintenance site</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="profileExamples">
					<ul class="nav">
						<li class="nav-item {{ setActive('admin/telescope/requests*') }}">
							<a class="nav-link" href="telescope/requests" target="_blank">
								<span class="sidebar-mini"><b>DT</b></span>
								<span class="sidebar-normal"><b>Dashboard Telescope</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<!-- Messages -->
			<li class="nav-item {{ setActive(['admin/all-contact*','admin/contact/all-work_with-us*', 'admin/demande/contact/assistance/all-assistance*']) }}">
				<a class="nav-link" data-toggle="collapse" href="#contactExamples">
					<i class="material-icons">drafts</i>
					<p><b>Messages</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="contactExamples">
					<ul class="nav">
						<li class="nav-item {{ setActive('admin/demande/contact/assistance/all-assistance*') }}">
							<a class="nav-link" href="{{ route('all-assistance.index') }}">
								<span class="sidebar-mini"><b>AS</b></span>
								<span class="sidebar-normal"><b>Assistance</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive('admin/all-contact*') }}">
							<a class="nav-link" href="{{ route('all-contact.index') }}">
								<span class="sidebar-mini"><b>CM</b></span>
								<span class="sidebar-normal"><b>Contact Us</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive('admin/contact/all-work_with-us') }}">
							<a class="nav-link" href="{{ route('all-work_with-us.index') }}">
								<span class="sidebar-mini"><b>WS</b></span>
								<span class="sidebar-normal"><b>Work With Us</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<!-- Management Admin -->
			<li class="nav-item {{ setActive(['admin/administrators*','admin/users*','admin/permissions*','admin/roles*','admin/notes*','admin/all-activity-log-page*']) }}">
				<a class="nav-link" data-toggle="collapse" href="#mapsExamples">
					<i class="material-icons">person_outline</i>
					<p><b>Management Admin</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="mapsExamples">
					<ul class="nav">
						<li class="nav-item {{ setActive(['admin/all-activity-log-page*']) }}">
							<a class="nav-link" href="{{route('all_activity')}}">
								<span class="sidebar-mini"><b>AS</b></span>
								<span class="sidebar-normal"><b>Activity Site</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/administrators*']) }}">
							<a class="nav-link" href="{{route('administrators.index')}}">
								<span class="sidebar-mini"><b>AD</b></span>
								<span class="sidebar-normal"><b>Administrators</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/notes*']) }}">
							<a class="nav-link" href="{{route('notes.index')}}">
								<span class="sidebar-mini"><b>AN</b></span>
								<span class="sidebar-normal"><b>Notes</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/permissions*']) }}">
							<a class="nav-link" href="{{route('permissions.index')}}">
								<span class="sidebar-mini"><b>PE</b></span>
								<span class="sidebar-normal"><b>Permissions</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/roles*']) }}">
							<a class="nav-link" href="{{route('roles.index')}}">
								<span class="sidebar-mini"><b>RO</b></span>
								<span class="sidebar-normal"><b>Roles</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/users*']) }}">
							<a class="nav-link" href="{{route('users.index')}}">
								<span class="sidebar-mini"><b>US</b></span>
								<span class="sidebar-normal"><b>Users</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<!-- Legals Mentions -->
			<li class="nav-item {{ setActive(['admin/conditions*','admin/legal_notice*','admin/licence_site*','admin/policy_privacy*','admin/cookies-site*']) }}">
				<a class="nav-link" data-toggle="collapse" href="#legalsExamples">
					<i class="material-icons">looks</i>
					<p><b>Legal & Mention</b>
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="legalsExamples">
					<ul class="nav">
						<li class="nav-item {{ active_check('admin/legal_notice*') }}">
							<a class="nav-link" href="{{route('legal_notice.index')}}">
								<span class="sidebar-mini"><b>LN</b></span>
								<span class="sidebar-normal"><b>Legal Notice</b></span>
							</a>
						</li>
						<li class="nav-item {{ active_check('admin/licence_site*') }}">
							<a class="nav-link" href="{{route('licence_site.index')}}">
								<span class="sidebar-mini"><b>LS</b></span>
								<span class="sidebar-normal"><b>Licence Site</b></span>
							</a>
						</li>
						<li class="nav-item {{ active_check('admin/policy_privacy*') }}">
							<a class="nav-link" href="{{route('policy_privacy.index')}}">
								<span class="sidebar-mini"><b>PP</b></span>
								<span class="sidebar-normal"><b>Policy & Privacy</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/conditions*']) }}">
							<a class="nav-link" href="{{route('conditions.index')}}">
								<span class="sidebar-mini"><b>TC</b></span>
								<span class="sidebar-normal"><b>Terms & Conditions</b></span>
							</a>
						</li>
						<li class="nav-item {{ setActive(['admin/cookies-site*']) }}">
							<a class="nav-link" href="{{route('cookies-site.index')}}">
								<span class="sidebar-mini"><b>CO</b></span>
								<span class="sidebar-normal"><b>Cookies</b></span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<!-- Charts -->
			<!--<li class="nav-item">
<a class="nav-link" href="../examples/charts.html">
<i class="material-icons">timeline</i>
<p><b>Charts</b></p>
</a>
</li> -->

			<!-- Calendar -->
			<!--<li class="nav-item ">
<a class="nav-link" href="../examples/calendar.html">
<i class="material-icons">date_range</i>
<p><b>Calendar</b></p>
</a>
</li> -->
		</ul>
	</div>
</div>
