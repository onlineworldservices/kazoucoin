<footer class="footer">
    <div class="container">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="/" target="_blank">
                        {!! config('app.name') !!}
                    </a>
                </li>
                <li>
                    <a href="{{ route('about-us')}}" target="_blank">
                        About Us
                    </a>
                </li>
				<li>
                    <a href="/orders" target="_blank">
                        Services
                    </a>
                </li>
                <li>
                    <a href="{{ route('faqs') }}" target="_blank">
                        FAQs
                    </a>
                </li>
                <li>
                    <a href="/contact-us" target="_blank">
                        Contact Us
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright pull-right">
            <h6 class="title-grey">Copyright &copy; 2018 -
                <script>
                    document.write(new Date().getFullYear())
                </script>. Realized by <a href="/" class="title-red">{{ config('app.author') }}</a></h6>
        </div>
    </div>
</footer>
</div>


