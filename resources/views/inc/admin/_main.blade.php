<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.admin._head')

@yield('style')
</head>
<body>
<div class="wrapper" id="app">
@if(Auth::user()->my_status === 'active')


@include('inc.admin._dashboard')
@include('inc.admin._nav')



@section('content')


@show


    <!-- le if permert ici de restrendre l'accet au site -->
@else
    <div class="submit text-center">
        @include('inc.admin.components.alert_permission')
    </div>
@endif
</div>
@include('layouts.admin._script')
@yield('script')
</body>
</html>