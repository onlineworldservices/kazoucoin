<!-- Notice Modal Category -->
<div class="modal fade" id="noticeCategory" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-notice modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="description">
                                <b class="title-red"><i class="material-icons">filter_1</i></b>
                                <b style="color:blue;">Comment enregistrer une catègorie?</b>
                            </h4>
                            <ul>
                                <li>Cliquez sur "New Category Order/Event"</li>
                                <li>Dans le modal, donnez un nom à "Category Name"</li>
                                <li>Choisisez une icon sur ce lien: <a href="https://material.io/tools/icons/?icon=music_note&style=baseline" target="_blank">Matrials Icons</a>. Par exemple: home, delete, ...</li>
                                <li>Insérez le lien de creation: Par example informatique/create. <b>(Se renseignez d'abord aux chargés de Back End Développeurs sur les liens crées et disponibles)</b></li>
                                <li>Insérez la route de la catègorie <b>(Se renseignez d'abord aux chargés de Back End Développeurs sur les liens crées et disponibles)</b></li>
                                <li>Cliquez sur "Create" pour enregistrer ou "Cancel" pour ressortir</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="description">
                                <b class="title-red"><i class="material-icons">filter_2</i></b>
                                <b style="color:blue;">Comment "Activer" ou "Dèsactiver" une categorie?</b>
                            </h4>
                            <ul>
                                <li>Cliquez sur <span class="btn btn-link btn-danger btn-round btn-just-icon" title="Activation">
                                    <i class="material-icons">power_settings_new</i>
                                    </span> pour activer votre Categorie</li>
                                <li>Cliquez sur <span class="btn btn-link btn-info btn-round btn-just-icon" title="Dèsactivation">
                                    <i class="material-icons">power_settings_new</i>
                                    </span> pour désactiver votre Categorie</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="description">
                                <b class="title-red"><i class="material-icons">filter_3</i></b>
                                <b style="color:blue;">Comment "Modifier" ou "Supprimer Partiellement" une Categorie?</b>
                            </h4>
                            <ul>
                                <li>Cliquez sur <span class="btn btn-link btn-success btn-round btn-just-icon" title="Modification">
                                    <i class="material-icons">edit</i>
                                    </span> pour modifier les impostations de votre Categorie</li>
                                <li>Cliquez sur <span class="btn btn-link btn-danger btn-round btn-just-icon" title="Suppresion">
                                    <i class="material-icons">delete</i>
                                    </span> pour supprimer votre Categorie. Il n'est pas supprimée totalement. Vous pouvez le retrouver dans le "Trash (x)"</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="description">
                                <b class="title-red"><i class="material-icons">filter_4</i></b>
                                <b style="color:blue;">Comment "Supprimer Complètement" une Categorie?</b>
                            </h4>
                            <ul>
                                <li>Cliquez sur "Trash (x)"</li>
                                <li>Cliquez sur <span class="btn btn-link btn-danger btn-round btn-just-icon" title="Suppresion">
                                    <i class="material-icons">delete_forever</i>
                                    </span> pour supprimer complètement votre categorie</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-info btn-round" data-dismiss="modal">
                    <i class="material-icons">thumb_up</i>
                    <b>Merci</b>
                </button>
            </div>
        </div>
    </div>
</div>
