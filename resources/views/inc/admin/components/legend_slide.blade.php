<!-- Notice Modal Slide -->
<div class="modal fade" id="noticeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-notice modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="description">
                                <b class="title-red"><i class="material-icons">filter_1</i></b>
                                <b style="color:blue;">Comment enregistrer un Slide?</b>
                            </h4>
                            <ul>
                                <li>Cliquez sur "New Slide"</li>
                                <li>Dans la nouvelle fenetre, Cliquez sur "Select Slide"</li>
                                <li>Donnez un titre à votre Slide</li>
                                <li>Choisisez une icon sur ce lien: <a href="https://material.io/tools/icons/?icon=music_note&style=baseline" target="_blank">Matrials Icons</a>. Par exemple: home, delete, ...</li>
                                <li>Donnez un titre à votre icon</li>
                                <li>Vous pouvez rediriger votre icon sur une nouvelle page: par exemple: https://www.google.com/</li>
                                <li>Ecrivez un paragraphe qui s'affichera sur votre Slide</li>
                                <li>Cliquez sur "Show Icon Button" pour faire apparaitre votre button icon sur le Slide</li>
                                <li>Vous avez un libre choi de configuration: Couleur, position, style, réseaux sociaux et liens de redirection</li>
                                <li>Cliquez sur "Create" pour enregistrer ou "Back" pour ressortir</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="description">
                                <b class="title-red"><i class="material-icons">filter_2</i></b>
                                <b style="color:blue;">Comment "Activer" ou "Dèsactiver" un Slide?</b>
                            </h4>
                            <ul>
                                <li>Cliquez sur <span class="btn btn-link btn-danger btn-round btn-just-icon" title="Activation">
                                    <i class="material-icons">power_settings_new</i>
                                    </span> pour activer votre Slide</li>
                                <li>Cliquez sur <span class="btn btn-link btn-info btn-round btn-just-icon" title="Dèsactivation">
                                    <i class="material-icons">power_settings_new</i>
                                    </span> pour désactiver votre Slide</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="description">
                                <b class="title-red"><i class="material-icons">filter_3</i></b>
                                <b style="color:blue;">Comment "Modifier" ou "Supprimer Partiellement" un Slide?</b>
                            </h4>
                            <ul>
                                <li>Cliquez sur <span class="btn btn-link btn-success btn-round btn-just-icon" title="Modification">
                                    <i class="material-icons">edit</i>
                                    </span> pour modifier les impostations de votre slide</li>
                                <li>Cliquez sur <span class="btn btn-link btn-danger btn-round btn-just-icon" title="Suppresion">
                                    <i class="material-icons">delete</i>
                                    </span> pour supprimer votre Slide. Il n'est pas supprimé totalement. Vous pouvez le retrouver dans le "Trash (x)"</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="instruction">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="description">
                                <b class="title-red"><i class="material-icons">filter_4</i></b>
                                <b style="color:blue;">Comment "Supprimer Complètement" un Slide?</b>
                            </h4>
                            <ul>
                                <li>Cliquez sur "Trash (x)"</li>
                                <li>Cliquez sur <span class="btn btn-link btn-danger btn-round btn-just-icon" title="Suppresion">
                                    <i class="material-icons">delete_forever</i>
                                    </span> pour supprimer complètement votre slide</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-info btn-round" data-dismiss="modal">
                    <i class="material-icons">thumb_up</i>
                    <b>Merci</b>
                </button>
            </div>
        </div>
    </div>
</div>
