<div id="services" class="row multi-services">
    <div class="col-md-4 col-md-offset-4">
        <div class="material-button-anim">
            <ul class="list-inline" id="options">
                <li class="option">
                    <button class="material-button option1" type="button">
                        <a href="{{ route('contact_us') }}" title="Contact-us">
                            <i class="material-icons">perm_phone_msg</i>
                        </a>
                    </button>
                </li>
                <li class="option">
                    <button class="material-button option2" type="button">
                        <a href="#" data-toggle="modal" data-target="#messageModal" title="Send-us a message">
                            <i class="material-icons">email</i>
                        </a>
                    </button>
                </li>
                <li class="option">
                    <button class="material-button option3" type="button">
                        <a href="#" data-toggle="modal" data-target="#testimonialModal" title="Send your testimonial">
                            <i class="material-icons">feedback</i>
                        </a>
                    </button>
                </li>
                <li class="option">
                    <button class="material-button option4" type="button">
                        @guest
                            <a href="#" data-toggle="modal" data-target="#chatModal" title="Chat now">
                                <i class="material-icons">chat</i>
                            </a>
                        @else
                            <a href="{{ route('conversations') }}" title="Chat now">
                                <i class="material-icons">chat</i>
                            </a>
                        @endguest
                    </button>
                </li>
            </ul>
            <button class="material-button material-button-toggle" type="button" style="cursor:pointer;">
                <span class="fa fa-plus" aria-hidden="false"></span>
            </button>
        </div>
    </div>
</div>
