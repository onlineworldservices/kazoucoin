@hasrole('super-admin')
<div class="submit">
    <div class="text-center">
        <button class="btn btn-rose btn-raised btn-warning btn-lg btn-round">
            <i class="material-icons">supervisor_account</i>
            <b>Super Admin</b>
        </button>
    </div>
</div>
@endhasrole
@hasrole('admin')
<div class="submit text-center">
    <button class="btn btn-danger btn-raised btn-warning btn-lg btn-round">
        <i class="material-icons">supervisor_account</i>
        <b>Admin</b>
    </button>
</div>
@endhasrole
@hasrole('editor')
<div class="submit text-center">
    <button class="btn btn-success btn-raised btn-lg btn-round">
        <i class="material-icons">supervisor_account</i>
        <b>Editor</b>
    </button>
</div>
@endhasrole
@hasrole('moderator')
<div class="submit text-center">
    <button class="btn btn-info btn-raised btn-lg btn-round">
        <i class="material-icons">supervisor_account</i>
        <b>Moderator</b>
    </button>
</div>
@endhasrole
@hasrole('advertiser')
<div class="submit text-center">
    <button class="btn btn-primary btn-raised btn-lg btn-round">
        <i class="material-icons">supervisor_account</i>
        <b>Advertiser</b>
    </button>
</div>
@endhasrole
@hasrole('visitor')
<div class="submit text-center">
    <button class="btn btn-warning btn-raised btn-lg btn-round">
        <i class="material-icons">supervisor_account</i>
        <b>Visitor</b>
    </button>
</div>
@endhasrole