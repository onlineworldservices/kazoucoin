<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.site.head')
    </head>
    <body>
        @section('content')
        @show
        @include('layouts.site.script')
    </body>
</html>
