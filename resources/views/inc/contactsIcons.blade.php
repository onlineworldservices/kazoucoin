<section class="contactsIcons-wrapper">
    <div class="row">
        <div class="col-sm-12">
            <div class="container-icon">
                <a href="tel:800%20821%20021">
                    <div class="icon-phone">
                        <img src="../assets/images/contactIcons/svg/phone-call.svg" alt="" class="img-icon-phone">
                        <img src="../assets/images/contactIcons/svg/phone-call-over.svg" alt="" class="img-icon-phone-hover">
                        <span>800 821 021</span>
                    </div>
                </a>
                <a href="{{ route('contact_us') }}">
                    <div class="icon-contact">
                        <img src="../assets/images/contactIcons/svg/mail.svg" alt="" class="img-icon-contact">
                        <img src="../assets/images/contactIcons/svg/mail-over.svg" alt="" class="img-icon-contact-hover">
                        <span>NOUS CONTACTEZ</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
