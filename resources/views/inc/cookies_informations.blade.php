<div class="cookies-section cd-section section-notifications" id="cookies-notifications">
    <div class="alert alert-warning">
        <div class="container-cookies">
            <div class="row">
                <div class="col-md-10">
                    <div class="alert-icon">
                        <i class="material-icons">info</i>
                    </div>
                    <b class="cookie-color">Cookies Alert:</b> This site uses cookies and other tracking technologies to assist with navigation and your
                    ability to provide feedback, analyse your use of our products and services, assist with our promotional and marketing efforts,
                    and provide content from third parties.See our <a href="{{ route('user.cookies') }}"><strong class="cookie-color">Cookies Policies</strong></a> here.
                </div>
                <div class="col-md-2">
                    <div class="row">
                        <div class="col-md-12 mr-auto ml-auto">
                            <button id="button_hover" class="btn btn-success btn-sm btn-block btn-round">
                                <i class="material-icons">check</i>
                                <b class="title_hover">Accept Cookies</b>
                            </button>
                        </div>
                        <div class="col-md-12 mr-auto ml-auto">
                            <button id="button_hover" type="button" class="btn btn-danger btn-sm btn-block btn-round" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">
                                    <i class="material-icons">clear</i>
                                    <b class="title_hover">Close</b>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>