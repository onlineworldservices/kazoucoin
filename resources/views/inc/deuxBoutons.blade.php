<div class="container container-off">
    <div class="row ml-auto mr-auto">
        <div class="col-md-4">
            <h3 class="info-title">
                Prêt(e) à vous lancer ?
            </h3>
            <p class="description">Créez votre compte en une minute.</p>
        </div>
        <div class="col-md-8">
            <div class="text-center siteplan-right">
                <a id="button_hover" href="/orders"
                   class="btn btn-success btn-lg btn-raised btn-round">
                <span class="btn-label">
                    <i class="material-icons">remove_red_eye</i>
                </span>
                    <b class="title_hover">Visitez les vitrines</b>
                </a>
                <a id="new-user" href="/register"
                   class="btn btn-rose btn-raised btn-lg btn-round btn-center">
                    <b>Créez votre profil</b>
                    <i class="material-icons size-profil">keyboard_arrow_right</i>
                </a>
            </div>
        </div>
    </div>
</div>