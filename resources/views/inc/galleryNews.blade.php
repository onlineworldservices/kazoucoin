<section class="gallery-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="gallery-card gallery-card-x">
                    <p class="gallery-card__date">
                        22-22-2222
                    </p>
                    <div class="gallery-card__img" style="background-image: url(http://placehold.it/800x500);">
                        <p class="gallery-card__tag">
                            lorem
                        </p>
                    </div>
                    <h3 class="gallery-card__title">
                        Lorem ipsum dolor sit amet,  adipiscing elit.
                    </h3>
                    <p class="gallery-card__excerpt">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elitdol consectetuer adipiscing dolor sit amet.
                    </p>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="gallery-card gallery-card-x">
                    <p class="gallery-card__date">
                        22-22-2222
                    </p>
                    <div class="gallery-card__img" style="background-image: url(http://placehold.it/800x500);">
                        <p class="gallery-card__tag">
                            lorem
                        </p>
                    </div>
                    <h3 class="gallery-card__title">
                        Lorem ipsum dolor sit amet,  adipiscing elit.
                    </h3>
                    <p class="gallery-card__excerpt">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elitdol consectetuer adipiscing dolor sit amet.
                    </p>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="gallery-card gallery-card-x">
                    <p class="gallery-card__date">
                        22-22-2222
                    </p>
                    <div class="gallery-card__img" style="background-image: url(http://placehold.it/800x500);">
                        <p class="gallery-card__tag">
                            lorem
                        </p>
                    </div>
                    <h3 class="gallery-card__title">
                        Lorem ipsum dolor sit amet,  adipiscing elit.
                    </h3>
                    <p class="gallery-card__excerpt">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elitdol consectetuer adipiscing dolor sit amet.
                    </p>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="gallery-card gallery-card-x">
                    <p class="gallery-card__date">
                        22-22-2222
                    </p>
                    <div class="gallery-card__img" style="background-image: url(http://placehold.it/800x500);">
                        <p class="gallery-card__tag">
                            lorem
                        </p>
                    </div>
                    <h3 class="gallery-card__title">
                        Lorem ipsum dolor sit amet,  adipiscing elit.
                    </h3>
                    <p class="gallery-card__excerpt">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elitdol consectetuer adipiscing dolor sit amet.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
