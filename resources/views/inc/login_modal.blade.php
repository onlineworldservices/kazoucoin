<div class="modal fade" id="loginModal" tabindex="-1" role="" >
	<div class="modal-dialog modal-login" role="document">
		<div class="modal-content">
			<div class="card card-signup card-plain">
				<div class="modal-header">
					<div class="card-header card-header-warning text-center">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							<i class="material-icons">clear</i>
						</button>

						<h4 class="card-title kazoucoin-subtitle title-white">Connect With</h4>
						<div class="social-line">
							<a href="{{ route('social.oauth', 'facebook') }}" class="btn btn-just-icon btn-facebook btn-round">
								<i class="fa fa-facebook-f"></i>
							</a>
							<a href="{{ route('social.oauth', 'google') }}" class="btn btn-just-icon btn-google btn-round">
								<i class="fa fa-google-plus"></i>
							</a>
							<a href="{{ route('social.oauth', 'github') }}" class="btn btn-just-icon btn-github btn-round" >
								<i class="fa fa-github"></i>
							</a>
						</div>
					</div>
				</div>
				<login-modal></login-modal>
			</div>
		</div>
	</div>
</div>