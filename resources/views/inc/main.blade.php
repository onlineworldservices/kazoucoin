<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.site.head')
</head>
<body>

@include('inc.nav_registration')

@section('content')

@show

@include('layouts.site.script')

</body>
</html>
