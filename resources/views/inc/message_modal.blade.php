<div class="modal fade" id="messageModal" tabindex="-1" role="">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="card card-signup card-plain">
				<div class="modal-header">
					<div class="card-header card-header-warning text-center">
						<i class="material-icons">email</i>
					</div>
				</div>
				<div class="modal-body">
					<form id="RegisterValidation" accept-charset="UTF-8" action="{{ route('contact.store')}}"
						  method="POST" enctype="multipart/form-data">
						<h3 class="kazoucoin-title title-grey title-no-uppercase">Contact Message</h3>
						{{ csrf_field() }}

						<div class="card-body">
							<div class="submit text-center">
								@include('inc.alert')
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group label-floating is-empty">
										<label class="bmd-label-floating">Prenom *</label>
										<input type="text" class="form-control" name="lastname"
											   value="{{ old('lastname') }}" minLength="3" maxlength="20" required
											   autofocus>
										<span class="material-input"></span>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group label-floating is-empty">
										<label class="bmd-label-floating">Nom *</label>
										<input type="text" class="form-control" name="name"
											   value="{{ old('name') }}" minLength="3" maxlength="20" required
											   autofocus>
										<span class="material-input"></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group label-floating is-empty">
										<label class="bmd-label-floating">youremail@example.com *</label>
										<input type="text" name="email" class="form-control"
											   value="{{ old('email') }}" required>
										<span class="material-input"></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating is-empty">
										<label class="bmd-label-floating">Objet *</label>
										<input type="text" name="subject" class="form-control"
											   value="{{ old('subject') }}" required minLength="3" maxlength="100">
										<span class="material-input"></span>
									</div>
								</div>
							</div>
							<div class="form-group label-floating is-empty">
								<label class="bmd-label-floating">Your Message *</label>
								<textarea rows="8" name="msg" class="form-control"
										  style="height:200px;" required autofocus>{{ old('msg') }}</textarea>
								<span class="material-input"></span>
							</div>
							<br>
							<div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }} text-center">
								{!! NoCaptcha::renderJs() !!}
								{!! NoCaptcha::display() !!}

								@if ($errors->has('g-recaptcha-response'))
								<span class="help-block">
									<strong class="text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>
								</span>
								@endif
							</div>
							<div class="submit text-center">
								@guest
								<button id="button_hover" class="btn btn-success btn-raised btn-round" type="submit">
									<span class="btn-label">
										<i class="material-icons">drafts</i>
									</span>
									<b class="title_hover">Send</b>
								</button>
								@else
								<button id="button_hover" class="btn btn-{{ Auth::user()->color_name }} btn-raised btn-round" type="submit">
									<span class="btn-label">
										<i class="material-icons">drafts</i>
									</span>
									<b class="title_hover">Send</b>
								</button>
								@endguest
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
