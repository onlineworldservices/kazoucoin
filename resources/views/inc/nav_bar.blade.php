<nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top navbar-expand-lg bg-warning"
     color-on-scroll="100" id="sectionsNav">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/images/kazoucoin-logo.png" class="kazoucoin-logo">
                <img src="/images/kazoucoin-scroll-logo.png" class="kazoucoin-scroll-logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse position-navbar">
            <!-- <div class="row search-position-scroll">
                <form method="" action="">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons icon-search">search</i>
                                    </span>
                                    </div>
                                    <input type="text" value=""
                                           placeholder="Ex: Make Up"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons icon-search">location_on</i>
                                    </span>
                                    </div>
                                    <input type="text" value=""
                                           placeholder="Ex: Paris"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button id="button_hover" type="button"
                                    class="btn btn-success btn-raised btn-md btn-round">
                            <span class="btn-label">
                                <i class="material-icons">done_all</i>
                            </span>
                            </button>
                        </div>
                    </div>
                </form>
            </div> -->
            <ul class="navbar-nav ml-auto">
                <li class="dropdown nav-item {{ active_check('/') }}">
                    <a href="/" class=" nav-link">
                        <i class="material-icons">home_work</i> <b>Home</b>
                    </a>
                </li>
                <li class="dropdown nav-item {{ setActive(['orders*','evenements*','others*']) }}">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                        <i class="material-icons">view_day</i> <b>Services</b>
                    </a>
                    <div class="dropdown-menu dropdown-with-icons">
                        <a href="{{ route('orders')}}" class="dropdown-item {{ setActive('orders*') }}">
                            <i class="material-icons">card_giftcard</i> <b>Orders</b>
                        </a>
                        <a href="{{ route('evenements')}}" class="dropdown-item {{ setActive('evenements*') }}">
                            <i class="material-icons">event_available</i> <b>Events</b>
                        </a>
                        <a href="{{ route('others')}}" class="dropdown-item {{ setActive('others*') }}">
                            <i class="material-icons">bubble_chart</i> <b>Others</b>
                        </a>
                    </div>
                </li>
                <li class="dropdown nav-item {{ setActive('faqs*') }}">
                    <a href="{{ route('faqs') }}" class=" nav-link">
                        <i class="material-icons">contact_support</i> <b>FAQs</b>
                    </a>
                </li>
                <li class="header-main-utility__search search dropdown nav-item {{ setActive('search') }}">
                    <a href="javascript:void(0)" class="search-wrap__icon nav-link">
                        <i class="material-icons">search</i> <b>Recherche</b>
                    </a>
                    <div class="search-wrap__content">
                        <form role="search" class="search search-section" method="get" name="simpleSearch"
                              novalidate="novalidate">
                            <div class="search-wrap__content__head">
                                <span>Recherche</span>
                                <button class="close-search" type="reset">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M23.294 24a.71.71 0 0 1-.5-.206L.206 1.206a.708.708 0 0 1 0-1 .708.708 0 0 1 1 0l22.588 22.588a.708.708 0 0 1-.5 1.206z"
                                              fill="#fff"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M.706 24a.708.708 0 0 1-.5-1.206L22.794.206a.708.708 0 0 1 1 0 .708.708 0 0 1 0 1L1.206 23.794a.71.71 0 0 1-.5.206z"
                                              fill="#fff"></path>
                                    </svg>
                                </button>
                            </div>
                            <div class="search-wrap__content__body">
                                <!--<span>search</span>-->
                                <label class="visually-hidden" for="q">Search Catalog</label>
                                <input class="input-search" type="text" id="q" name="search" value="" autocomplete="off"
                                       aria-invalid="false">
                                <button class="search-submit" type="submit">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         id="new-search-input">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M23.06 23.8a.742.742 0 0 1-.523-.216l-7.52-7.521a.74.74 0 0 1 1.045-1.046l7.522 7.52a.741.741 0 0 1-.524 1.263z"
                                              fill="#fff"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M9.258 1.484c-4.287 0-7.774 3.487-7.774 7.773 0 4.287 3.487 7.776 7.774 7.776 4.288 0 7.775-3.489 7.775-7.776 0-4.286-3.487-7.773-7.775-7.773zm0 17.033C4.154 18.517 0 14.363 0 9.257 0 4.154 4.154 0 9.258 0c5.105 0 9.259 4.154 9.259 9.257 0 5.106-4.154 9.26-9.259 9.26z"
                                              fill="#fff"></path>
                                    </svg>
                                </button>
                                <!--<button class="close-search search-reset" type="reset">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M23.294 24a.71.71 0 0 1-.5-.206L.206 1.206a.708.708 0 0 1 0-1 .708.708 0 0 1 1 0l22.588 22.588a.708.708 0 0 1-.5 1.206z"
                                              fill="#fff"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M.706 24a.708.708 0 0 1-.5-1.206L22.794.206a.708.708 0 0 1 1 0 .708.708 0 0 1 0 1L1.206 23.794a.71.71 0 0 1-.5.206z"
                                              fill="#fff"></path>
                                    </svg>
                                </button> -->
                            </div>
                        </form>
                    </div>
                </li>
                <li class="dropdown nav-item {{ setActive('contact-us') }}">
                    <a href="{{ route('contact_us') }}" class=" nav-link">
                        <i class="material-icons">perm_phone_msg</i> <b>{{ __('Contact Us')}}</b>
                    </a>
                </li>
                @if(Auth::user())
                    <li class="messagerie-content dropdown nav-item {{ setActive('conversations') }}">
                        <a href="{{ route('conversations') }}" class=" nav-link">
                            <i class="material-icons">message</i> <b>Messagerie</b>
                            <span class="nav-item_badge">3</span>
                        </a>
                    </li>
                @endif
                <!-- Authentication Links -->
                @guest
                    <li class="dropdown nav-item login-kazoucoin">
                        <a href="#" class="nav-link">
                            <i class="material-icons">face</i> <b>Login / Register</b>
                        </a>
                        <div class="login-wrap__content">
                            <div class="login-wrap__content__logged-out">
                                <!--LOG IN -->
                                <a href="#" data-toggle="modal" data-target="#loginModal"
                                   class="btn btn-warning btn-round">
                                    <i class="material-icons">how_to_reg</i>
                                    <b>Login</b>
                                </a>
                                <!-- REGISTER -->
                                <a href="{{ route('register') }}" class="btn btn-warning btn-round">
                                    <i class="material-icons">person_add</i>
                                    <b>Register</b>
                                </a>
                            </div>
                        </div>
                    </li>
                @else
                    <li class="dropdown nav-item">
                        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"
                           style="position: relative; padding-left: 50px;">
                            @if(Auth::user()->avatar)
                                <img src="{{ asset(auth()->user()->avatar ) }}" alt="{{ Auth::user()->email }}"
                                     class="img-raised rounded-circle img-fluid text-center"
                                     style="width: 32px; height: 32px; position: absolute; top: 10px; left: 10px; border-radius: 50%">
                                <b>{!! str_limit(Auth::user()->email, 12,'...') !!}</b>
                            @endif
                        </a>
                        <div class="dropdown-menu dropdown-with-icons">
                            @if(Auth::user()->my_status === 'active')
                                <a href="{{ route('admin') }}" class="dropdown-item">
                                    <i class="material-icons">dashboard</i> <b>Dashboard</b>
                                </a>
                            @endif
                            <a href="{{route('orders.users',auth()->user()->username)}}" class="dropdown-item">
                                <i class="material-icons">card_giftcard</i> <b>@lang('My Orders')</b>
                            </a>
                            <a href="../sections.html#features" class="dropdown-item">
                                <i class="material-icons">event_available</i> <b>@lang('My Events')</b>
                            </a>
                            <a href="{{ route('aide_contact_messages') }}" class="dropdown-item">
                                <i class="material-icons">info</i> <b>Aide & Contact</b>
                            </a>
                            <a href="{{ route('/', Auth::user()->username) }}" class="dropdown-item">
                                <i class="material-icons">person</i> <b>Profile</b>
                            </a>
                            <a href="/account/profile" class="dropdown-item">
                                <i class="material-icons">settings</i> <b>Paramètres</b>
                            </a>
                            <a class="dropdown-item" href="{{ route('user.logout') }}">
                                <i class="material-icons">power_settings_new</i> <b>Deconnexion</b>
                            </a>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
@include('inc.login_modal')
