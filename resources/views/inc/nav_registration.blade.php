<nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-warning" color-on-scroll="100" id="sectionsNav">
	<div class="container">
		<div class="navbar-translate">
			<a class="navbar-brand" href="{{ url('/') }}">
				<img src="/images/kazoucoin-logo.png" class="kazoucoin-logo">
				<img src="/images/kazoucoin-scroll-logo.png" class="kazoucoin-scroll-logo">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="sr-only">Toggle navigation</span>
				<span class="navbar-toggler-icon"></span>
				<span class="navbar-toggler-icon"></span>
				<span class="navbar-toggler-icon"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			@guest
			<ul class="navbar-nav ml-auto">
				<li class="dropdown nav-item {{ setActive('login') }}">
					<a href="{{ route('login') }}"  class="nav-link">
						<i class="material-icons">how_to_reg</i>  <b>{{__('Sign In')}}</b>
					</a>
				</li>
				@if (Route::has('register'))
				<li class="dropdown nav-item {{ setActive('register') }}">
					<a href="{{ route('register') }}"  class="nav-link">
						<i class="material-icons">person_add</i> <b>{{ __('Sign up')}}</b>
					</a>
				</li>
				@endif
			</ul>
			@endguest
		</div>
	</div>
</nav>



