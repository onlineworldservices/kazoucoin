<div class="container">
    <div class="row position-top">
        <div class="col-md-4">
            <span class="home-guide_list-item">1</span>
            <h4 class="title list-item-position">J'effectue ma
                recherche Online en quelques clics pour repérer les
                e-Commerçants proches de chez moi.</h4>
        </div>
        <div class="col-md-4">
            <span class="home-guide_list-item">2</span>
            <h4 class="title list-item-position">Je reçois pour le
                meme service plusieurs propositions commerciales de
                diffèrents e-Commerçants.</h4>
        </div>
        <div class="col-md-4">
            <span class="home-guide_list-item">3</span>
            <h4 class="title list-item-position">Je communique
                directement en live ou en chat avec le plus
                proposant et je fixe les modalités de paiement et
                transport.</h4>
        </div>
    </div>
</div>
