<div class="modal fade" id="testimonialModal" tabindex="-1" role="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                    <div class="card-header card-header-warning text-center">
                        <i class="material-icons">feedback</i>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h3 class="kazoucoin-title title-no-uppercase title-grey">Post your testimonial</h3>
                    </div>
                    <br>
                    @guest
                        <div class="container feedback-position">
                            <div class="row text-center">
                                <div class="col-md-10 mr-auto ml-auto">
                                    <a id="new-user" href="{{ route('register')}}"
                                       class="btn btn-rose btn-raised btn-lg btn-round">
                                        <b>Register</b>
                                        <i class="material-icons size-profil">keyboard_arrow_right</i>
                                    </a>
                                    <a id="new-user" href="{{ route('login')}}"
                                       class="btn btn-success btn-raised btn-lg btn-round">
                                        <b>Login</b>
                                        <i class="material-icons size-profil">keyboard_arrow_right</i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @else
                        <testimonial-page></testimonial-page>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
