<section class="trois-images-wrapper container">
    <div class="row expanded">
        <div class="col-sm-12">
            <div class="trois-images">
                <div class="container-title-trois-images">
                    <!-- <div class="title-vertically-card-component">OUR BEST COFFEES OUR BEST COFFEES</div>
                    <div class="cta-alternate">
                        <a href="#" tabindex="0" class="deepLink">
                            <div class="deepLink-arrow">Scoprile tutte</div>
                        </a>
                    </div>
                </div> -->
                    <div class="module-main-content">
                        <!-- To create multiple rows of images, clone the element with 'container-trois-images' class -->
                        <div class="container-trois-images">
                            <div class="outbox-trois-images" tabindex="0">
                                <div class="rectangle-trois-images right-trois-images">
                                    <div class="toggle-arrow-trois-images"></div>
                                </div>
                                <div class="gradient-trois-images"></div>
                                <div class="img-trois-images">
                                    <img class="img-solo-trois-img" alt="First Image"
                                         src="../images/kazoucoin.jpeg">
                                </div>
                                <div class="trois-images-mask">
                                    <div class="title-rectangle-trois-images"><span
                                            class="home-guide_list-item">1</span></div>
                                    <div class="trois-images-mask-txt">J'effectue ma
                                        recherche Online en quelques clics pour repérer les
                                        e-Commerçants proches de chez moi.
                                    </div>
                                    <a href="#" tabindex="-1" aria-label="scopri di più"
                                       class="arrow-style arrow-trois-images">
                                        <div class="body-arrow white-body-arrow"></div>
                                        <div class="head-arrow white-head-arrow"></div>
                                    </a>
                                </div>
                            </div>
                            <div class="outbox-trois-images" tabindex="0">
                                <div class="rectangle-trois-images right-trois-images">
                                    <div class="toggle-arrow-trois-images"></div>
                                </div>
                                <div class="gradient-trois-images"></div>
                                <div class="img-trois-images">
                                    <img class="img-solo-trois-img" alt="Second Image"
                                         src="../images/kazoucoin.jpeg">
                                </div>
                                <div class="trois-images-mask">
                                    <div class="title-rectangle-trois-images"><span
                                            class="home-guide_list-item">2</span></div>
                                    <div class="trois-images-mask-txt">Je reçois pour le
                                        meme service plusieurs propositions commerciales de
                                        diffèrents e-Commerçants.
                                    </div>
                                    <a href="#" tabindex="-1" aria-label="scopri di più"
                                       class="arrow-style arrow-trois-images">
                                        <div class="body-arrow white-body-arrow"></div>
                                        <div class="head-arrow white-head-arrow"></div>
                                    </a>
                                </div>
                            </div>
                            <div class="outbox-trois-images" tabindex="0">
                                <div class="rectangle-trois-images right-trois-images">
                                    <div class="toggle-arrow-trois-images"></div>
                                </div>
                                <div class="gradient-trois-images"></div>
                                <div class="img-trois-images">
                                    <img class="img-solo-trois-img" alt="Third Image"
                                         src="../images/kazoucoin.jpeg">
                                </div>
                                <div class="trois-images-mask">
                                    <div class="title-rectangle-trois-images"><span
                                            class="home-guide_list-item">3</span></div>
                                    <div class="trois-images-mask-txt">Je communique
                                        directement en live ou en chat avec le plus
                                        proposant et je fixe les modalités de paiement et
                                        transport.
                                    </div>
                                    <a href="#" tabindex="-1" aria-label="scopri di più"
                                       class="arrow-style arrow-trois-images">
                                        <div class="body-arrow white-body-arrow"></div>
                                        <div class="head-arrow white-head-arrow"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
