@routes
<!--   Core JS Files   -->
<script src="/assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
<script src="/assets/js/plugins/moment.min.js"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="/assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--   Chat Section Js   -->
<script src="/assets/js/chat.js" type="text/javascript"></script>
<!--   Trois Images Js   -->
<script src="/js/composants/troisImages.js" type="text/javascript"></script>
<!--   Map Js   -->
<script src="/js/composants/map.js" type="text/javascript"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_k5oKeNuLvXbeDz0IDHATmV5u8IK8xYA&callback=initMapSingle"></script>
<!--   Slick Carousel Js   -->
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<!--   Search Js   -->
<script src="/assets/js/search.js" type="text/javascript"></script>
<!--  Google Maps Plugin
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
-->
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<!--
<script src="/assets/js/plugins/fullcalendar.min.js"></script>
-->
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="/assets/js/plugins/bootstrap-tagsinput.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="/assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="/assets/js/plugins/jasny-bootstrap.min.js" type="text/javascript"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!--	Plugin for Small Gallery in Product Page -->
<script src="/assets/js/plugins/jquery.flexisel.js"></script>
<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
<script src="/assets/js/material-kit.js?v=2.1.0" type="text/javascript"></script>
<!-- Sweet Alert 2 plugin -->
<script src="/assets/js/plugins/sweetalert2.js"></script>
<script src="/assets/js/plugins/toastr.min.js"></script>
<script src="/assets/js/plugins/zoom.min.js"></script>
<!-- Forms Validations Plugin -->
<script src="/assets/js/plugins/jquery.validate.min.js"></script>
<script src="{{ asset('assets/js/plugins/parsley.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="/assets/dashboard/assets/js/plugins/bootstrap-notify.js"></script>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="{{ mix('/js/app.js') }}"></script>

<script type="text/javascript">

	function setFormValidation(id){
		$(id).validate({
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
			},
			success: function(element) {
				$(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
			},
			errorPlacement : function(error, element) {
				$(element).append(error);
			},
		});
	}

	$(document).ready(function(){
		setFormValidation('#RegisterValidation');
		setFormValidation('#TypeValidation');
		setFormValidation('#LoginValidation');
		setFormValidation('#RangeValidation');
	});
</script>
<script>
	$(document).ready(function(){
		$('body').append('<a id="toTop" class="btn btn-outline-warning btn-sm" href="#" title="Go to Top Page"><i class="material-icons top-button">arrow_upward</i></a>');
		$(window).scroll(function () {
			if ($(this).scrollTop() != 0) {
				$('#toTop, #search-top').fadeIn();
			} else {
				$('#toTop, #search-top').fadeOut();
			}
		});
		$('#toTop, #search-top').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	});
</script>
<script>
	$(document).ready(function () {
		if ($(window).width() > 1023) {
		$(window).scroll(function () {
		   // Show multi-services button
			if ($(this).scrollTop() > 1000) {
				$('.multi-services').css('display', 'block');
				$('#services').fadeIn();
			} else {
				$('#services').fadeOut();
			}

			// Show search form in navbar
			if ($(this).scrollTop() > 650) {
				$('.search-position-scroll').css('display', 'block');
				$('.search-position-scroll').fadeIn();
			} else {
				if ($(this).scrollTop() < 600) {
					$('.search-position-scroll').css('display', 'none');
					$('.search-position-scroll').fadeOut();
				}
			}

			// Change color nav-link Menu
			if($(this).scrollTop() > 260) {
				$('.navbar .navbar-nav .nav-item .nav-link').addClass('navbar-transparent-color-link');
			} else {
				$('.navbar .navbar-nav .nav-item .nav-link').removeClass('navbar-transparent-color-link');
			}
		});

		$('.material-button-toggle').on("click", function () {
			$(this).toggleClass('open');
			$('.option').toggleClass('scale-on');
		});
		}
	});
</script>
<script>
    $(document).ready(function () {
        if(!($('.messagerie-content').length)) {
            $('.search .search-wrap__content').addClass('search-wrap__content-connected')
        }
    });
</script>
<script>
    $(document).ready(function() {
        var $chatbox = $('.chatbox'),
            $chatboxTitle = $('.chatbox__title'),
            $chatboxTitleClose = $('.chatbox__title__close'),
            $chatboxCredentials = $('.chatbox__credentials');
        $chatboxTitle.on('click', function() {
            $chatbox.toggleClass('chatbox--tray');
        });
        $chatboxTitleClose.on('click', function(e) {
            e.stopPropagation();
            $chatbox.addClass('chatbox--closed');
        });
        $chatbox.on('transitionend', function() {
            if ($chatbox.hasClass('chatbox--closed')) $chatbox.remove();
        });
    });
</script>
<script type="text/javascript" charset="utf-8">
	$(function () {
		$('#form-contact-us').submit(function () {
			return submitForm($(this));
		});

		/**
         * Helpers to submit the forms via ajax
         * @param form
         * @returns {boolean}
         */
		function submitForm($form) {
			var inputs = [];
			if (!FORM.validateForm($form, inputs)) {
				return false;
			}

			FORM.sendFormToServer($form, $form.serialize());
			return false;
		}
	});
</script>
<script>
	(function () {
		var e = document.getElementById("fooDiv");
		e.parentNode.removeChild(e);
	})();
</script>
<script>
        $(document).ready(function() {

            var slider2 = document.getElementById('sliderRefine');

            noUiSlider.create(slider2, {
                start: [101, 790],
                connect: true,
                range: {
                    'min': [30],
                    'max': [900]
                }
            });

            var limitFieldMin = document.getElementById('price-left');
            var limitFieldMax = document.getElementById('price-right');

            slider2.noUiSlider.on('update', function(values, handle) {
                if (handle) {
                    limitFieldMax.innerHTML = $('#price-right').data('currency') + Math.round(values[handle]);
                } else {
                    limitFieldMin.innerHTML = $('#price-left').data('currency') + Math.round(values[handle]);
                }
            });
        });
    </script>
@include('sweetalert::alert')
@toastr_js
@toastr_render
@section('scripts')


@show
