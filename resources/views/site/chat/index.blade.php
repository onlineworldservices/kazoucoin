@extends('inc.app')
@section('title', '- Chat Section')
@section('style')

@endsection

@section('content')
    <div class="signup-page sidebar-collapse">
        @include('inc.nav_bar')
        <div class="page-header header-filter"
             style="background-image: url(&apos;{{ $user->avatarcover }}&apos;); background-size: cover; background-position: top center;">
            <div id="app" class="container">
                <chat-page></chat-page>
            </div>
            @include('inc._footer')
        </div>
    </div>
@endsection
