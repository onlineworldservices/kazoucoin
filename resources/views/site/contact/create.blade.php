@extends('inc.app')
@section('title', '- Contact')

@section('style')

@endsection

@section('content')

    <div class="contact-content" id="app">
        @include('inc.nav_bar')
        @if(count($slidescontacts) > 0)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($slidescontacts as $item)
                        <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                            <div class="page-header header-filter header-small" data-parallax="true"
                                 style="background-image: url(&apos;{{ url($item['photo']) }}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 ml-auto mr-auto text-center">
                                            <h1 class="title">
                                                <b>{{ $item['slide_title'] }}</b>
                                            </h1>
                                            <h4>{{ $item['slide_subtitle']}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="page-header header-filter header-small" data-parallax="true"
                 style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h1 class="title">Contact Us</h1>
                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="main main-raised" style="padding-bottom: 15px; padding-top: 15px;">
            <div class="container">
                @guest

                @else
                    @if(Auth::user()->email_verified_at === null)
                        <verify-link></verify-link>
                    @endif
                @endguest
                <div class="row">
                    <div class="col-md-6 contact-section">
                        <h2 class="mt-5">
                            <b>Get in touch with us</b>
                        </h2>
                        <div class="description text-justify">
                            <p class="title-grey">You can contact us with anything related to our Products. We'll get in touch with you as
                                soon as possible.</p>
                        </div>
                        <div class="container-content-squared">
                            <div id="map" class="map-container mapId">
                            </div>
                        </div>
                    </div>
                    <contact-page></contact-page>
                </div>
                <br>
                <div class="row mr-auto ml-auto mb-5">
                    <div class="col-md-3 col-sm-6">
                        <div class="icon icon-warning">
                            <i class="material-icons">pin_drop</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Find us at the Office</h4>
                            <p class="title-grey"> Bld Mihail Kogalniceanu, nr. 8,
                                <br> 16134 Genova, Italy
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="icon icon-rose">
                            <i class="material-icons">phone</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Give us a Call</h4>
                            <p class="title-grey"> +39 3425712192 / +39 3296187465
                                <br> Lun - Ven, 8:00 - 22:00
                            </p>
                        </div>
                    </div>
                    <div class="service-contact col-md-3 col-sm-6">
                        <div class="icon icon-info">
                            <i class="material-icons">mail</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">E-mail Contact</h4>
                            <a href="mailto:dasgivemoi@gmail.com" class="title-grey">
                                <p>onlineworldservices@gmail.com</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="icon icon-success">
                            <i class="material-icons">business</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Legal Informations</h4>
                            <p class="title-grey"> {{ config('app.name') }} Srl, Milan / Italy
                                <br> IVA: 000.000.0000
                            </p>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    @include('inc.footer')
@endsection

@section('scripts')
    @parent
@endsection
