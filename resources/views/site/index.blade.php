@extends('inc.app')
@section('title', '- Site Marchante en ligne')
@section('style')
    <style>

    </style>
@endsection

@section('content')
    <div class="landing-page" id="app">
    @include('inc.nav_bar')
    <!-- <div class="page-header header-filter" data-parallax="true"
style=" background-image: url(&apos;/assets/img/kit/pro/bg8.jpg&apos;);">
<div class="container">
<div class="row">
<div class="col-md-12 ml-auto mr-auto text-center">
<h2 class="title"> {{ config('app.name') }}</h2>
@guest
        <h3 class="description title">Inscrivez-vous et profitez au plus vite des services du sites</h3>
        <a href="{{ route('register') }}" class="btn btn-warning btn-raised btn-round">
Ouvrir facilement un compte
</a>
@else
        <a href=" " class="btn btn-warning btn-raised btn-round">
        <i class="material-icons">play_arrow</i> Verifiez vos transaction ici
        </a>
@endguest
        </div>
        </div>
        </div>
        </div> -->

        <!-- Carousel Card -->

        @if(count($slideshomes) > 0)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <!--
<ol class="carousel-indicators">
@foreach($slideshomes as $item)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ ($loop->first)? 'active':'' }}"></li>
@endforeach
                </ol>
-->

                <div class="carousel-inner">
                    @foreach($slideshomes as $item)
                        <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                            <div class="page-header header-filter" data-parallax="true"
                                 style="background-image: url(&apos;{{ $item->photo}}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        @if($item->slide_btn_position === 'left')
                                            <div class="col-md-10 text-left">
                                                @endif
                                                @if($item->slide_btn_position === 'right')
                                                    <div class="col-md-10 ml-auto text-right">
                                                        @endif
                                                        @if($item->slide_btn_position === 'center')
                                                            <div class="col-md-10 ml-auto mr-auto text-center">
                                                                @endif
                                                                <h2 class="title">
                                                                    <b>{{ $item->slide_title }}</b>
                                                                </h2>
                                                                <h4 class="title">{!! htmlspecialchars_decode( $item->body) !!}</h4>
                                                                <br>
                                                                @if($item->slide_btn_status != null)
                                                                    <div class="buttons">
                                                                        <a id="button_hover"
                                                                           href="{{ $item->slide_btn_link }}"
                                                                           class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                                                                <span class="btn-label">
                                                                                    <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                                                                </span>
                                                                            <b class="title_hover">{{ $item->slide_btn_title }}</b>
                                                                        </a>
                                                                    </div>
                                                                @endif
                                                                <br>
                                                                @if($item->slide_fblink != null)
                                                                    <a href="{{ $item->slide_fblink }}"
                                                                       class="btn btn-just-icon btn-{{ $item->slide_btn_style_fblink }} btn-facebook btn-round">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                @endif
                                                                @if($item->slide_twlink != null)
                                                                    <a href="{{ $item->slide_twlink }}"
                                                                       class="btn btn-just-icon btn-{{ $item->slide_btn_style_twlink }} btn-twitter btn-round">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                @endif
                                                                @if($item->slide_instalink != null)
                                                                    <a href="{{ $item->slide_instalink }}"
                                                                       class="btn btn-just-icon btn-{{ $item->slide_btn_style_instalink }} btn-instagram btn-round">
                                                                        <i class="fa fa-instagram"></i>
                                                                    </a>
                                                                @endif
                                                                @if($item->slide_youtubelink != null)
                                                                    <a href="{{ $item->slide_youtubelink }}"
                                                                       class="btn btn-just-icon btn-{{ $item->slide_btn_style_youtubelink }} btn-google btn-round">
                                                                        <i class="fa fa-youtube"></i>
                                                                    </a>
                                                                @endif
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <!--<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <i class="material-icons">keyboard_arrow_left</i>
    <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <i class="material-icons">keyboard_arrow_right</i>
    <span class="sr-only">Next</span>
    </a>-->
                        </div>
                        @else
                            <div class="page-header header-filter header-small" data-parallax="true"
                                 style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 ml-auto mr-auto text-center">
                                            <h2 class="title">Home Page</h2>
                                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @include('inc.login_modal')
                        <vue-progress-bar></vue-progress-bar>
                        <!-- Welcome Page -->
                        @forelse($homepageprofiles as $item)
                            <div class="main main-raised"
                                 style="background-image: url(&apos;{{ url($item->photo)}}&apos;); background-size: cover; background-repeat: no-repeat;">
                                @empty
                                    <div class="main main-raised">
                                        @endforelse
                                        <div class="container">
                                            <div class="section text-center">
                                                <div class="row">
                                                    <div class="col-md-12 ml-auto mr-auto">
                                                        @if (session('status'))
                                                            <div class="alert alert-success">
                                                                {{ session('status') }}
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- Search Bar -->
                                                <div class="row">
                                                    <div class="col-md-10 ml-auto mr-auto position-search">
                                                        <div class="card card-raised card-form-horizontal">
                                                            <div class="card-body">
                                                                <form method="get" action="{{ route('search') }}"
                                                                      accept-charset="UTF-8">
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text">
                                                                                            <i class="material-icons icon-search">search</i>
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="text" value=""
                                                                                           placeholder="Essayez 'Make Up', 'High Tech', 'Smartphone'. ..."
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text">
                                                                                            <i class="material-icons icon-search">location_on</i>
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="text" value=""
                                                                                           placeholder="Pays, Région, Ville, ...."
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <button id="button_hover" type="submit"
                                                                                    class="btn btn-success btn-raised btn-lg btn-round">
                                                                                    <span class="btn-label">
                                                                                        <i class="material-icons">done_all</i>
                                                                                    </span>
                                                                                <b class="title_hover title_phone">Rechercher</b>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Welcome -->
                                                <div class="row">
                                                    <div class="col-md-12 ml-auto mr-auto">
                                                        <h3 class="kazoucoin-title title-grey"><span
                                                                class="title-red">{{ config('app.name') }}</span>
                                                            : De la recherche des articles et services jusqu'à la mise
                                                            en relation directe avec les particuliers</h3>
                                                    </div>
                                                    @include('inc.troisImages')
                                                    <div class="container">
                                                        <div class="col-md-12 ml-auto mr-auto position-top">
                                                            <div class="text-center">
                                                                <a id="button_hover" href="{{ route('concept') }}"
                                                                   class="btn btn-danger btn-raised btn-lg btn-round">
                                                                    <span class="btn-label">
                                                                        <i class="material-icons">present_to_all</i>
                                                                    </span>
                                                                    <b class="title_hover">Notre Concept</b>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Affaires du Jour -->
                                                <div class="row">
                                                    <div class="col-md-12 ml-auto mr-auto">
                                                        <div class="section-title text-left">
                                                            <div class="info info-horizontal"
                                                                 style="max-width: 1500px;">
                                                                <div class="description">
                                                                    <h3 class="kazoucoin-title title-red title-left">Les Affaires du Jour</h3>
                                                                    <div class="kazoucoin-subtitle title-grey">
                                                                        Nous vous proposons un choix étendu de
                                                                        références des articles, évenements et autres
                                                                        services des différents commercants du monde
                                                                        dans leurs déstockages et couvertes par des
                                                                        engagements rigoureux, notamment en termes de
                                                                        qualité, de service et d'expertise.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- carousel -->
                                                        <div class="row">
                                                            <div class="col-md-12 ml-auto mr-auto">
                                                                <!-- Carousel Card -->
                                                                <div id="carouselExampleIndicators"
                                                                     class="carousel slide" data-ride="carousel">
                                                                    <div class="carousel-inner">
                                                                        <div class="carousel-item active">
                                                                            <div class="row">
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/img-error/clint-mckoy.jpg">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/assets-for-demo/section-components/laptop-basics.png">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/assets-for-demo/section-components/responsive.png">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="carousel-item">
                                                                            <div class="row">
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/img-error/clint-mckoy.jpg">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/assets-for-demo/section-components/laptop-basics.png">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/assets-for-demo/section-components/responsive.png">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="carousel-item">
                                                                            <div class="row">
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/img-error/clint-mckoy.jpg">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/assets-for-demo/section-components/laptop-basics.png">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4 expo">
                                                                                    <div
                                                                                        class="card card-blog card-plain">
                                                                                        <div
                                                                                            class="card-header card-header-image">
                                                                                            <a href="#pablo">
                                                                                                <img
                                                                                                    class="img contain-height-img"
                                                                                                    src="../assets/assets-for-demo/section-components/responsive.png">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="badge badge-warning">
                                                                                                <b>Article 1</b></h6>
                                                                                            <a href="#pablo">
                                                                                                <h4 class="card-title">
                                                                                                    The Best
                                                                                                    Productivity Apps on
                                                                                                    Market</h4>
                                                                                            </a>
                                                                                            <br>
                                                                                            <p class="card-description">
                                                                                                Don't be scared of the
                                                                                                truth because we need to
                                                                                                restart the human
                                                                                                foundation in truth And
                                                                                                I love you like Kanye
                                                                                                loves Kanye I love Rick
                                                                                                Owens’ bed design but
                                                                                                the back is...
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="price-container">
                                                                                            <span
                                                                                                class="price price-old"> <b>€1,430</b></span>
                                                                                            <span
                                                                                                class="price price-new"> <b>€743</b></span>
                                                                                        </div>
                                                                                        <br>
                                                                                        <div class="card-footer">
                                                                                            <div class="author">
                                                                                                <a href="#pablo">
                                                                                                    <img
                                                                                                        src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                                                                        alt="..."
                                                                                                        class="avatar img-raised">
                                                                                                    <span
                                                                                                        class="description">Tania Andrew</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="stats ml-auto">
                                                                                                <i class="material-icons icon icon-info">thumb_up</i>
                                                                                                <b>2.4K</b> &#xB7;
                                                                                                <i class="material-icons icon icon-warning">visibility</i>
                                                                                                <b>45</b> &#xB7;
                                                                                                <i class="material-icons icon icon-success">chat</i>
                                                                                                <b>45</b>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <a class="carousel-control-prev"
                                                                       href="#carouselExampleIndicators" role="button"
                                                                       data-slide="prev">
                                                                        <i class="material-icons icon icon-danger">keyboard_arrow_left</i>
                                                                        <span class="sr-only">Previous</span>
                                                                    </a>
                                                                    <a class="carousel-control-next"
                                                                       href="#carouselExampleIndicators" role="button"
                                                                       data-slide="next">
                                                                        <i class="material-icons icon icon-danger">keyboard_arrow_right</i>
                                                                        <span class="sr-only">Next</span>
                                                                    </a>
                                                                </div>
                                                                <!-- End Carousel Card -->
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="text-center">
                                                            <a id="button_hover" href="{{ route('orders') }}"
                                                               class="btn btn-success btn-raised btn-lg btn-round">
                                                                <span class="btn-label">
                                                                    <i class="material-icons">remove_red_eye</i>
                                                                </span>
                                                                <b class="title_hover">Visitez les vitrines</b>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                </div>

                <!-- Tops Posters  -->
                <div class="col-md-12 ml-auto mr-auto">
                    <div class="container-fluid">
                        <div class="section-title text-left">
                            <div class="info info-horizontal" style="max-width: 1000px;">
                                <div class="description">
                                    <h3 class="kazoucoin-title title-grey">Top 3 des meilleurs e-Commerçants</h3>
                                    <div class="kazoucoin-subtitle title-red text-center">
                                        Nous vous proposons un choix étendu de références des artices, évenements et
                                        autres services des différents commercants du monde dans leurs déstockages et
                                        couvertes par des engagements rigoureux, notamment en termes de qualité, de
                                        service et d'expertise.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-4 expo">
                                <div class="card card-product">
                                    <div class="card-header card-header-image" data-header-animation="true">
                                        <a href="#pablo">
                                            <img class="img" src="https://www.kazoucoin.com/assets/img/photo.jpg">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a href="#pablo">Cozy 5 Stars Apartment</a>
                                        </h4>

                                        <div class="card-description">
                                            The place is close to Barceloneta Beach and bus stop just 2 min by walk and
                                            near to "Naviglio" where you can enjoy the.
                                        </div>
                                    </div>
                                    <br>

                                    <div class="card-footer" style="padding: 0.75rem 1.25rem;
                                                                    background-color: #fff;
                                                                    border-top: 1px solid rgba(0, 0, 0, 0.12);">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span>Lord Alex</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <div class="icon icon-success">
                                                <b><i class="material-icons">chat</i>5</b>
                                            </div>
                                            <div class="icon icon-danger">
                                                <b><i class="material-icons">place</i>Milan, Italy</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 expo">
                                <div class="card card-product">
                                    <div class="card-header card-header-image" data-header-animation="true">
                                        <a href="#pablo">
                                            <img class="img" src="https://www.kazoucoin.com/assets/img/photo.jpg">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a href="#pablo">Office Studio</a>
                                        </h4>

                                        <div class="card-description">
                                            The place is close to Metro Station and bus stop just 2 min by walk and near
                                            to "Naviglio" where you can enjoy the night life in London, UK.
                                        </div>
                                    </div>
                                    <br>

                                    <div class="card-footer" style="padding: 0.75rem 1.25rem;
                                                                    background-color: #fff;
                                                                    border-top: 1px solid rgba(0, 0, 0, 0.12);">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span>Lord Alex</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <div class="icon icon-success">
                                                <b><i class="material-icons">chat</i>5</b>
                                            </div>
                                            <div class="icon icon-danger">
                                                <b><i class="material-icons">place</i>Milan, Italy</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 expo">
                                <div class="card card-product">
                                    <div class="card-header card-header-image" data-header-animation="true">
                                        <a href="#pablo">
                                            <img class="img" src="https://www.kazoucoin.com/assets/img/photo.jpg">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <a href="#pablo">Beautiful Castle</a>
                                        </h4>

                                        <div class="card-description">
                                            The place is close to Metro Station and bus stop just 2 min by walk and near
                                            to "Naviglio" where you can enjoy the main.
                                        </div>
                                    </div>
                                    <br>

                                    <div class="card-footer" style="padding: 0.75rem 1.25rem;
                                                                    background-color: #fff;
                                                                    border-top: 1px solid rgba(0, 0, 0, 0.12);">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span>Lord Alex</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <div class="icon icon-success">
                                                <b><i class="material-icons">chat</i>5</b>
                                            </div>
                                            <div class="icon icon-danger">
                                                <b><i class="material-icons">place</i>Milan, Italy</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Sécurité Kazoucoin -->
            <div class="team-4 image-subcribe">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 ml-auto mr-auto text-center">
                            <h2 class="kazoucoin-title title-white title-no-uppercase">Vous travaillez en toute sécurité sur {{ config('app.name') }} ?</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info-horizontal">
                                <div class="icon icon-warning">
                                    <i class="material-icons">how_to_reg</i>
                                </div>
                                <div class="description">
                                    <h3 class="title">E-Commerçants vérifiés</h3>
                                    <div class="kazoucoin-subtitle title-yellow">Consultez les avis des visiteurs, clients et contactez les
                                        machants en règle.</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info-horizontal">
                                <div class="icon icon-warning">
                                    <i class="material-icons">security</i>
                                </div>
                                <div class="description">
                                    <h3 class="title">Compte sécurisé</h3>
                                    <div class="kazoucoin-subtitle title-yellow">Votre compte est unique et protegé par des accés
                                        sécurisé.</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info-horizontal">
                                <div class="icon icon-warning">
                                    <i class="material-icons">verified_user</i>
                                </div>
                                <div class="description">
                                    <h3 class="title">Communication live</h3>
                                    <div class="kazoucoin-subtitle title-yellow">Vous pouvez communiquer en toute libeté à travers différents
                                        canaux.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Testimonials -->
            <div class="col-md-12 ml-auto mr-auto">
                <div class="container-fluid">
                    <div class="section-title text-left">
                        <div class="info info-horizontal" style="max-width: 1000px;">
                            <div class="description">
                                <h3 class="kazoucoin-title title-red">Des Visiteurs et e-Commerçants Comblès</h3>
                                <div class="kazoucoin-subtitle title-grey text-center">
                                    Vous avez besoin de plus d'informations? Vérifiez ce que les autres personnes disent
                                    de l'équipe {{ config('app.name') }} et des services des différents posters du monde
                                    . Ils sont très contents de leur échange sur cette plateforme.
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-10 ml-auto mr-auto">
                            <div id="carouselExampleIndicatorss" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="card card-testimonial card-plain">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-avatar">
                                                        <a href="#pablo">
                                                            <img class="img"
                                                                 src="https://www.kazoucoin.com/assets/img/default-avatar.png">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <h4 class="card-title">Kendall Thompson</h4>
                                                    <h6 class="card-category text-muted">CEO @ Marketing Digital
                                                        Ltd.</h6>
                                                    <div class="footer">
                                                        <i class="material-icons text-warning">star</i>
                                                        <i class="material-icons text-warning">star</i>
                                                        <i class="material-icons text-warning">star</i>
                                                        <i class="material-icons text-warning">star</i>
                                                        <i class="material-icons text-warning">star</i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 ml-auto mr-auto">
                                                <div class="card-body">
                                                    <h5 class="card-description">"I speak yell scream directly at the
                                                        old guard on behalf of the future. I gotta say at that time I’d
                                                        like to meet Kanye I speak yell scream directly at the old guard
                                                        on behalf of the future. My brother Chance!!!
                                                        <br>Thank you for letting me work on this masterpiece. One of my
                                                        favorite people."
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="card card-testimonial card-plain">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="card-avatar">
                                                        <a href="#pablo">
                                                            <img class="img"
                                                                 src="https://www.kazoucoin.com/assets/img/default-avatar.png">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <h4 class="card-title">Christian Louboutin</h4>
                                                    <h6 class="card-category text-muted">Designer @ Louboutin &amp;
                                                        Co.</h6>
                                                    <div class="footer">
                                                        <i class="material-icons text-warning">star</i>
                                                        <i class="material-icons text-warning">star</i>
                                                        <i class="material-icons text-warning">star</i>
                                                        <i class="material-icons text-warning">star</i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 ml-auto mr-auto">
                                                <div class="card-body">
                                                    <h5 class="card-description">"Thank you Anna for the invite thank
                                                        you to the whole Vogue team Called I Miss the Old Kanye At the
                                                        God's last game Chop up the soul Kanye. I promise I will never
                                                        let the people down. I want a better life for all!!! Pablo Pablo
                                                        Pablo Pablo! Thank you Anna for the invite thank you to the
                                                        whole Vogue team."
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicatorss" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicatorss" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Latest News -->
            <div class="subscribe-line subscribe-line-image">
                <div class="col-md-10 ml-auto mr-auto">
                    <div class="container">
                        <div class="section-title text-left">
                            <div class="description text-center">
                                <h3 class="kazoucoin-title title-white">Soyez à l'affût de toutes les dernières
                                    Nouvelles</h3>
                                <div class="kazoucoin-subtitle title-white">
                                    Suivez en direct nos dernières nouveautès d'ajournements, de publications, d'évenements et bien
                                    d'autres choses.
                                </div>
                            </div>
                        </div>
                        @include('inc.galleryNews')
                        <div class="text-center">
                            <a id="button_hover" href="#" class="btn btn-warning btn-raised btn-lg btn-round">
                            <span class="btn-label">
                                <i class="material-icons">view_list</i>
                            </span>
                                <b class="title_hover">Tous les news</b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Nous rejoindre -->
            <div class="team-4 image-subcribe">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 ml-auto mr-auto text-center">
                            <h2 class="kazoucoin-title title-white title-no-uppercase">Vous souhaitez rejoindre la communauté {{ config('app.name') }} ?</h2>
                            <a id="new-user" href="{{ route('register')}}"
                               class="btn btn-rose btn-raised btn-lg btn-round">
                                <b>Créer mon profil</b>
                                <i class="material-icons size-profil">keyboard_arrow_right</i>
                            </a>
                            <a id="search-top" href="#"
                               class="btn btn-success btn-raised btn-lg btn-round">
                                <b>Rechercher un service</b>
                                <i class="material-icons size-profil">keyboard_arrow_right</i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Form Newletters -->
            <div class="subscribe-line subscribe-line-image" data-parallax="true"
                 style="background-image: url(&apos;../assets/img/about/1538132025.fond.jpg&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="kazoucoin-title title-no-uppercase title-left title-white">Envie de bonnes affaires?</h3>
                            <div class="kazoucoin-subtitle kazoucoin-subtitle-small title-white">
                                Inscrivez-vous gratuitement à notre newletters et soyez contacter par mail en premiers
                                chaque week-end pour les offres de prèmier choix.
                            </div>
                        </div>
                        @include('inc.alert')
                        <div class="col-md-6 ml-auto mr-auto">
                            <div class="card card-raised card-form-horizontal">
                                <div class="card-body">
                                    <form id="form-newsletter-subscribe" method="post"
                                          action="{{ route('newsletter.store') }}" accept-charset="UTF-8">
                                        {!! csrf_field() !!}
                                        <div class="row">
                                            <div id="fooDiv">
                                                <label for="foo">Ce champ m'evite les spam dans le server</label>
                                                <input type="text" name="foo" id="foo">
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="material-icons">mail</i>
                                                            </span>
                                                    </div>
                                                    <input id="user_email" type="email" name="user_email"
                                                           class="form-control{{ $errors->has('user_email') ? ' has-error' : '' }}"
                                                           placeholder=" Email ..." required>
                                                    @if ($errors->has('user_email'))
                                                        <span class="help-block">
                                                            <strong
                                                                class="text-center">{{ $errors->first('user_email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button id="button_hover" type="submit"
                                                        class="btn btn-rose btn-block btn-round">
                                                    <i class="material-icons">done_all</i>
                                                    <b class="title_hover">Subscribe</b>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    @include('inc.footer')
    @include('cookieConsent::index')
    @include('inc.contactsIcons')
    @include('inc.chat_section')
@endsection

@section('scripts')

@endsection
