@extends('inc.app')
@section('title', '- Create New Order Beauté')
@section('style')
    <style type="text/css">
        .main-section {
            margin: 0px;
            padding: 20px;
            margin-top: 100px;
            background-color: #fff;
            box-shadow: 0px 0px #cc0000;
        }
    </style>
@endsection

@section('content')
    <div id="app">
        @include('inc.nav_bar')
        <router-view></router-view>
    </div>
    @include('inc._footer')
@endsection

@section('scripts')
    @parent
@endsection

