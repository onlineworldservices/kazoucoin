@extends('inc.app')
<?php $titleTag = htmlspecialchars($beaute->title); ?>
@section('title',"-edit $titleTag")

@section('style')

@endsection

@section('content')
<div class="ecommerce-page sidebar-collapse" id="app">
     @include('inc.nav_bar')
     <router-view></router-view>
</div>
@include('inc._footer')
@endsection

@section('scripts')
    @parent



@endsection

