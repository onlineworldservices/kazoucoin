@extends('inc.app')

@section('title', '- Beautes')


@section('style')

@endsection


@section('content')

<div class="about-us" id="app">
    @include('inc.nav_bar')
@if(count($slidesorders) > 0)
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($slidesorders as $item)
                <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                    <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ url($item->photo) }}&apos;);">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 ml-auto mr-auto text-center">
                                    <h2 class="title">
                                        <b>{{ $item->slide_title }}</b>
                                        <h4>{{ $item->slide_subtitle }}</h4>
                                    </h2>
                                    <br>
                                    @if($item->slide_btn_status != null)
                                        <div class="buttons">
                                            <a href="{{ $item->slide_btn_link }}" class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                                <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                                <b>{{ $item->slide_btn_title }}</b>
                                            </a>
                                        </div>
                                    @endif
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@else
    <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
        <div class="container">
            <div class="row">
                <div class="col-md-10 ml-auto mr-auto text-center">
                    <h3 class="title">All Orders Beautes</h3>
                    <h6 class="title">Set Up your Slide in your Dashboard</h6>
                </div>
            </div>
        </div>
    </div>
@endif


<router-view auth="{{Auth::guest()}}" :user="{{json_encode(Auth::user())}}" ></router-view>

@include('inc.login_modal')
</div>

@include('inc.footer')

@endsection

@section('scripts')
    @parent

@endsection
