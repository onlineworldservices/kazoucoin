@extends('inc.app')
<?php $nameTag = htmlspecialchars($user->name); ?>
@section('title',"- Store $nameTag")
@section('style')

@endsection


@section('content')
<div id="app">
    @include('inc.nav_bar')

<router-view auth="{{Auth::guest()}}" authcheck="{{Auth::check()}}"></router-view>

</div>
@include('inc._footer')
@endsection


@section('scripts')

@endsection

