@extends('inc.app')
@section('title', '- Informatique')
@section('style')


<style type="text/css">

    .main-section{

        margin: 0px;
        padding: 20px;
        margin-top: 100px;
        background-color: #fff;
        box-shadow: 0px 0px #cc0000;
    }
</style>
@endsection
@section('navbar')

@guest
<nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-warning" color-on-scroll="100" id="sectionsNav">
    @else
    <nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-{{ Auth::user()->color_name }}" color-on-scroll="100" id="sectionsNav">
        @endguest
        @endsection
        @section('content')

<div class="landing-page">

            <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ URL::to('assets/img/image.jpg')}}&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h1 class="title">Informatique</h1>
                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main main-raised">
                <div class="main">
                    <div class="section">
                        <div class="container">


                            @include('inc.alert')

                            <form id="RegisterValidation"  role="form" method="POST" action="{{route('informatiques.store')}}" enctype="multipart/form-data" accept-charset="UTF-8">
                                {{ csrf_field() }}

                                @include('site.order.informatique.form',['informatique' => new \App\Model\user\order\informatique()])

                                <div class="row buttons-row">
                                    <div class="col-md-6 col-sm-6">
                                        <button class="btn btn-outline-danger btn-block btn-round">Cancel</button>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                       <button type="submit" class="btn btn-outline-primary btn-block btn-round">Save</button>
                                    </div>
                                    <!--<div class="col-md-4 col-sm-4">
                                        <button class="btn btn-primary btn-block btn-round">Save & Publish </button>
                                    </div>-->
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
</div>
@include('inc._footer')
@endsection

@section('scripts')
@parent



@endsection

