@extends('inc.app')
<?php $titleTag = htmlspecialchars($informatique->title); ?>
@section('title',"- edit $titleTag" )

@section('style')
    <script>
        window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
    </script>
@endsection
@section('navbar')

    @guest
        <nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-warning" color-on-scroll="100" id="sectionsNav">
            @else
                <nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-{{ Auth::user()->color_name }}" color-on-scroll="100" id="sectionsNav">
                    @endguest
                    @endsection
                    @section('content')

                        <div class="landing-page">
                            <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ URL::to('assets/img/image.jpg')}}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 ml-auto mr-auto text-center">
                                            <h1 class="title">Informatique</h1>
                                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="main main-raised">
                                <div class="main">
                                    <div class="section">
                                        <div class="container">
                                            <h3>Add Product</h3>
                                            {!! Form::model($informatique, ['files'=> 'true','method' => 'PATCH','route' => ['informatiques.update',$informatique->id], 'id' => 'RegisterValidation']) !!}

                                            @include('site.order.informatique.form')
                                                <div class="row buttons-row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <button class="btn btn-outline-danger btn-block btn-round">Cancel</button>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <button type="submit" class="btn btn-outline-primary btn-block btn-round">Save</button>
                                                    </div>
                                                    <!--<div class="col-md-4 col-sm-4">
                                                        <button class="btn btn-primary btn-block btn-round">Save & Publish </button>
                                                    </div>-->
                                                </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
     @include('inc._footer')
 @endsection

@section('scripts')
@parent

<script>
    var gear;
    var lastPosition;
    var refreshRate = 60; // fps; reduce for less overhead
    var animation = "rotate(*deg)"; // css style to apply [Wildcard is *]
    var link = 0.5; //what to multiply the scrollTop by

    function replace(){
        if(lastPosition != document.body.scrollTop){ // Prevents unnecessary recursion
            lastPosition = document.body.scrollTop; // updates the last position to current
            gear.style.transform = animation.replace("*", Math.round(lastPosition * link)); // applies new style to the gear
        }
    }

    function setup(){
        lastPosition = document.body.scrollTop;
        gear = document.querySelector(".gear");
        setInterval(replace, 1000 / refreshRate); // 1000 / 60 = 16.666... Invokes function "replace" to update for each frame
    }
    window.addEventListener("DOMContentLoaded", setup);

</script>

@endsection

