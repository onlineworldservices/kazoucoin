<div class="row">
    <div class="col-md-12">
        <div class="row price-row">
            <div class="col-sm-6 row-block">
                {!! Form::select('menucategoryorder_id', old('menucategoryorder_id') ? old('menucategoryorder_id') : $menucategoryorders = DB::table('menucategoryorders')
                ->where('categoryorder_name', 'informatiques')
                ->where('status', 1)->pluck('name','id')
                ,null, ['data-style'=>'select-with-transition', 'class' => 'form-control selectpicker', 'required' => '','title'=> 'Choose Category ...' ])
                !!}
                @if ($errors->has('menucategoryorder_id'))
                <span class="help-block">
                    <strong class="text-danger text-center">{{ $errors->first('menucategoryorder_id') }}</strong>
                </span>
                @endif

            </div>
            <div class="col-sm-6 row-block">
                {!! Form::select('color_name', old('color_name') ? old('color_name') : App\Model\user\partial\color::where('status',1)->pluck('color_name','slug'),null, ['data-style'=>'select-with-transition', 'class' => 'form-control selectpicker', 'required' => '','title'=> 'Choose Color ...' ]) !!}

                @if ($errors->has('color_name'))
                <span class="help-block">
                    <strong class="text-danger text-center">{{ $errors->first('color_name') }}</strong>
                </span>
                @endif
            </div>

        </div>
        <div class="form-group">
            <h6>Title
                <span class="icon-danger">*</span>
            </h6>
            {!! Form::text('title', null, ['id'=>'title','class' => 'form-control border-input', 'placeholder' => 'enter the product name here...', 'required' => 'required']) !!}
            @if ($errors->has('title'))
            <span class="help-block">
                <strong class="text-danger text-center">{{ $errors->first('title') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group">
            <h6>Tagline
                <span class="icon-danger">*</span>
            </h6>
            <input type="text" class="form-control border-input" placeholder="enter the product tagline here...">
        </div>
        <div class="row price-row">
            <div class="col-md-6">
                <h6>Contact
                    <span class="icon-danger">*</span>
                </h6>
                <div class="input-group border-input">
                    {!! Form::text('phone', null, ['id'=>'phone','class' => 'form-control border-input', 'placeholder' => 'enter the contact phone here...']) !!}
                    @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong class="text-danger text-center">{{ $errors->first('phone') }}</strong>
                    </span>
                    @endif
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="fa fa-phone"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h6>Phone</h6>
                <div class="input-group border-input">
                    <input type="text" value="" placeholder="enter phone or email" class="form-control border-input">
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row price-row">
            <div class="col-md-12">
                <div class="col-md-12 ">
                    <h6>Tags
                        <span class="icon-danger">*</span>
                    </h6>
                    <div id="tags">
                        <input type="text" value="{{ old('tags', $informatique->tagsList) }}" name="tags" class="tagsinput form-control{{ $errors->has('tags') ? ' is-invalid' : '' }}" data-role="tagsinput" data-color="rose" placeholder="Tags" >
                        <!-- You can change data-color="rose" with one of our colors primary | warning | info | danger | success -->
                        @if ($errors->has('tags'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('tags') }}</strong>
                            </span>
                        @endif
                    <!-- <div class="tagsinput-add"></div> -->
                        <!-- You can change "tag-primary" with with "tag-info", "tag-success", "tag-warning","tag-danger" -->
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <h6>Description</h6>
            {!! Form::textarea('body', null, ['id'=>'body','class' => 'form-control textarea-limited', 'placeholder' => 'This is a textarea limited to 150 characters.' , 'rows' =>"13", 'maxlength' => "150"]) !!}
            @if ($errors->has('body'))
            <span class="help-block">
                <strong class="text-danger text-center">{{ $errors->first('body') }}</strong>
            </span>
            @endif
            <h5>
                <small>
                    <span id="textarea-limited-message" class="pull-right">150 characters left</span>
                </small>
            </h5>
        </div>
        <div class="form-check">
            <label class="form-check-label pull-right">
                vous pouvez utiliser le
                <a href="https://help.github.com/articles/getting-started-with-writing-and-formatting-on-github/" class="text-danger">
                    markdown ici
                </a>

                <span class="form-check-sign"></span>
            </label>
        </div>
    </div>
</div>