@extends('inc.app')

@section('title', '- Informatique')


@section('style')

@endsection

@section('navbar')

@guest
<nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top  navbar-expand-lg bg-warning" color-on-scroll="100" id="sectionsNav">
    @else
    <nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top  navbar-expand-lg bg-{{ Auth::user()->color_name }}" color-on-scroll="100" id="sectionsNav">
        @endguest

        @endsection

        @section('content')

        <div class="about-us" id="app">

            @if(count($slidesorders) > 0)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($slidesorders as $item)
                    <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                        <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ URL::to('assets/img/slides/order/'.$item->photo) }}&apos;);">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-10 ml-auto mr-auto text-center">
                                        <h2 class="title">
                                            <b>{{ $item->slide_title }}</b>
                                            <h4>{{ $item->slide_subtitle }}</h4>
                                        </h2>
                                        <br>
                                        @if($item->slide_btn_status != null)
                                        <div class="buttons">
                                            <a href="{{ $item->slide_btn_link }}" class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                                <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                                <b>{{ $item->slide_btn_title }}</b>
                                            </a>
                                        </div>
                                        @endif
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @else
            <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ URL::to('assets/img/image.jpg')}}&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 ml-auto mr-auto text-center">
                            <h3 class="title">All Orders Information Technology</h3>
                            <h6 class="title">Set Up your Slide in your Dashboard</h6>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="main main-raised">
                <div class="container">

                    <!-- All Orders -->
                    <div class="cd-section" id="projects">
                        <div class="projects-1" id="projects-1">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-10 ml-auto mr-auto text-center">
                                        <h2 class="title">
                                            <b>Les Articles en Exposition</b>
                                        </h2>
                                        <br>
                                        <h5 class="description">Retrouvez à portée de main tous les meilleurs articles high-tech comme la gamme Samsung Galaxy, les mobiles Tecno, les smartphones ou encore les tablettes de poche, ainsi tout le nécessaire pour remplir et décorer votre maison, et aussi tout l'univers de la Mode : chaussures, vêtements et accessoires pour toute la famille.</h5>
                                    </div>
                                </div>
                            </div>

                            @include('site.order.nav_orders')

                            <div class="row">
                                <div class="col-md-12 ml-auto mr-auto text-center">
                                        @forelse($menucategoryorders as $item)
                                            @if($item->categoryorder->slug === 'informatiques')
                                            <a href="{{ route('topic.menucategoryorder', $item->slug) }}" class="btn btn-{{ $item->color_name }} btn-round">
                                                <i class="material-icons">{{ $item->icon }}</i>
                                                <b>{{ $item->name }}</b>
                                            </a>
                                            @endif
                                            @empty
                                             <a href="#" class="btn btn-danger btn-round">
                                                 <i class="material-icons">info</i>
                                                 <b>Pas de category ici</b>
                                             </a>
                                        @endforelse

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-content tab-space">
                        <div class="tab-pane active" id="pillAll">
                            <div class="row">
                                @if(count($informatiques) > 0)
                                    @foreach($informatiques as $item)
                                <div class="col-md-6 col-lg-4">
                                    <div class="rotating-card-container manual-flip">
                                        <div class="card card-rotate">
                                            <div class="front" style="background-image: url('assets/img/bg9.jpg');">
                                                <div class="card-body">
                                                    <h5 class="card-category card-category-social">
                                                        <i class="material-icons">label</i> Grand Titre
                                                    </h5>
                                                    <h4 class="card-title">
                                                        <a href="#pablo">"Dribbble just acquired Crew, a very interesting startup..."</a>
                                                    </h4>
                                                    <p class="card-description">
                                                        Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                                                    </p>
                                                    <div class="stats text-center">
                                                        <button type="button" name="button" class="btn btn-success btn-fill btn-round btn-rotate">
                                                            <i class="material-icons">refresh</i> Rotate
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <div class="author">
                                                        <a href="#pablo">
                                                            <img src="assets/img/face.jpg" alt="..." class="avatar img-raised">
                                                            <span>Boclair</span>
                                                        </a>
                                                    </div>
                                                   <favorite></favorite>
                                                </div>
                                            </div>
                                            <div class="back back-background" style="background-image: url('assets/img/kit/pro/examples/card-blog5.jpg');">
                                                <div class="card-body">
                                                    <h5 class="card-title">
                                                        Envie d'en savoir plus?
                                                    </h5>
                                                    <p class="card-description">Vous avez le choix de plus d'informations, d'écrire et d'appeler <span>Boclair</span></p>
                                                    <br>
                                                    <div class="footer text-center">
                                                        <a href="{{ url( $item->path() )  }}" class="btn btn-info btn-just-icon btn-fill btn-round">
                                                            <i class="material-icons">visibility</i>
                                                        </a>
                                                        <a href="#pablo" class="btn btn-success btn-just-icon btn-fill btn-round btn-wd">
                                                            <i class="material-icons">message</i>
                                                        </a>
                                                        <a href="#pablo" class="btn btn-danger btn-just-icon btn-fill btn-round">
                                                            <i class="material-icons">phone</i>
                                                        </a>
                                                    </div>
                                                    <br>
                                                    <button type="button" name="button" class="btn btn-link btn-round btn-rotate">
                                                        <i class="material-icons">keyboard_backspace</i> Back
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>


                        <!-- Pagination -->
                        <div class="row">
                            <div class="ml-auto mr-auto text-center">
                                <ul class="pagination pagination-primary">
                                    <!--
    color-classes: "pagination-primary", "pagination-info", "pagination-success", "pagination-warning", "pagination-danger"
    -->
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link"> Prev</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link">1</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link">...</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link">5</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link">6</a>
                                    </li>
                                    <li class="active page-item">
                                        <a href="javascript:void(0);" class="page-link">7</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link">8</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link">9</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link">...</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link">12</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="javascript:void(0);" class="page-link"> Next</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- Phone Modal -->
            <!--<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-medium" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
                    </div>
                    <div class="modal-body text-center">
                        <h5>Vous pouvez contactez le vendeur à ce numéro de télephone:</h5>
                        <p><b style="color: red;">+33 56987456</b></p>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-danger success btn-round" data-dismiss="modal"><b>Close</b></button>
                    </div>
                </div>
            </div>
        </div>-->
        <!-- Message Modal -->
        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-medium" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
                    </div>
                    <div class="modal-body text-center">
                        <h5>Vous pouvez contactez le vendeur à cette adresse E-mail:</h5>
                        <p><b style="color: red;">abc@yahoo.fr</b></p>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-danger success btn-round" data-dismiss="modal"><b>Close</b></button>
                    </div>
                </div>
            </div>
        </div>

@include('inc._footer')

@endsection

@section('scripts')
@parent
    <script type="text/javascript">

        // View function
        $('#phoneModal').on('show.bs.modal', function (event) {


            var button = $(event.relatedTarget)
            var phone = button.data('myphone')
            var lk_id = button.data('lkid')
            var modal = $(this)

            modal.find('.modal-body #phone').val(phone);
            modal.find('.modal-body #lk_id').val(lk_id);


        });

    </script>

@endsection
