@extends('inc.app')
<?php $titleTag = htmlspecialchars($informatique->title); ?>
@section('title',"- $titleTag")
@section('title', '- Informatique')

@section('style')

@endsection
@section('navbar')

<nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top  navbar-expand-lg bg-{!! $informatique->color_name   !!}" color-on-scroll="100" id="sectionsNav">
   @endsection
   @section('content')
<div class="about-us" id="app">

@if(count($slidesorders) > 0)
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($slidesorders as $item)
                <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                    <div class="page-header header-filter header-small" data-parallax="true"
                         style="background-image: url(&apos;{{ URL::to('assets/img/slides/order/'.$item->photo) }}&apos;);">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 ml-auto mr-auto text-center">
                                    <h2 class="title">
                                        <b>{{ $item->slide_title }}</b>
                                        <h4>{{ $item->slide_subtitle }}</h4>
                                    </h2>
                                    <br>
                                    @if($item->slide_btn_status != null)
                                        <div class="buttons">
                                            <a href="{{ $item->slide_btn_link }}"
                                               class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                                <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                                <b>{{ $item->slide_btn_title }}</b>
                                            </a>
                                        </div>
                                    @endif
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@else
    <div class="page-header header-filter header-small" data-parallax="true"
         style="background-image: url(&apos;{{ URL::to('assets/img/image.jpg')}}&apos;);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h1 class="title">Description Orders</h1>
                    <h5 class="title">Set Up your Slide in your Dashboard</h5>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="main main-raised">
        <div class="container">

            <!-- Description Order -->
            <div class="cd-section" id="projects">
                <div class="projects-1" id="projects-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="tab-content mr-auto ml-auto">
                                    <div class="tab-pane" id="product-page1">
                                        <div class="card card-product">
                                            <div class="card-header card-header-image">
                                                <img class="img img-raised"
                                                     src="../assets/img/sidebar-1.jpg"
                                                     width="150" height="400">
                                            </div>
                                            <div class="card-body"></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane active" id="product-page2">
                                        <div class="card card-product">
                                            <div class="card-header card-header-image">
                                                <img class="img img-raised"
                                                     src="../assets/img/sidebar-2.jpg"
                                                     width="150" height="400">
                                            </div>
                                            <div class="card-body"></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="product-page3">
                                        <div class="card card-product">
                                            <div class="card-header card-header-image">
                                                <img class="img img-raised"
                                                     src="../assets/img/sidebar-3.jpg"
                                                     width="150" height="400">
                                            </div>
                                            <div class="card-body"></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="product-page4">
                                        <div class="card card-product">
                                            <div class="card-header card-header-image">
                                                <img class="img img-raised"
                                                     src="../assets/img/sidebar-4.jpg"
                                                     width="150" height="400">
                                            </div>
                                            <div class="card-body"></div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav flexi-nav" data-tabs="tabs" id="flexiselDemo1">
                                    <li class="nav-item">
                                        <a href="#product-page1" class="nav-link"
                                           data-toggle="tab">
                                            <img src="../assets/img/sidebar-1.jpg" width="100"
                                                 height="100">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#product-page2" class="nav-link"
                                           data-toggle="tab">
                                            <img src="../assets/img/sidebar-2.jpg" width="100"
                                                 height="100">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#product-page3" class="nav-link"
                                           data-toggle="tab">
                                            <img src="../assets/img/sidebar-3.jpg" width="100"
                                                 height="100">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#product-page4" class="nav-link"
                                           data-toggle="tab">
                                            <img src="../assets/img/sidebar-4.jpg" width="100"
                                                 height="100">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 ml-auto mr-auto">
                                <h2 class="title text-center">
                                    <b>Informatique</b>
                                    <br>
                                    @foreach($informatique->tags as $tag)
                                        @if($tag)
                                            <span class="badge badge-rose">{{ $tag->name}}</span>
                                        @else
                                            <strong class="card-title">Tags don't exist</strong>
                                        @endif
                                    @endforeach
                                </h2>
                                <h2 class="title text-center">
                                    <b>{{ $informatique->visits()->count() }} </b>
                                    {{  $informatique->visits()->seconds(1)->increment() }} <i class="material-icons">visibility</i>
                                </h2>
                                <hr>
                                <h4 class="text-center">
                                    @if($informatique->menucategoryorder)
                                        <strong class="card-title">{{ $informatique->menucategoryorder->name  }}</strong>
                                    @else
                                        <strong class="card-title">Categories don't
                                            exist</strong>
                                    @endif

                                </h4>
                                <hr>
                                <h6 class="text-center card-category">
                                    <b>{!! $informatique->title   !!} </b>
                                </h6>
                                <hr>
                                <br>
                                <div id="accordion" role="tablist">
                                    <div class="card card-collapse">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <h5 class="mb-0">
                                                <a data-toggle="collapse" href="#collapseOne"
                                                   aria-expanded="true"
                                                   aria-controls="collapseOne">
                                                    <b>Description</b>
                                                    <i class="material-icons">keyboard_arrow_down</i>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse show"
                                             role="tabpanel" aria-labelledby="headingOne"
                                             data-parent="#accordion">
                                            <div class="card-body">
                                                {!! $markdown->parse($informatique->body) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-collapse">
                                        <div class="card-header" role="tab" id="headingTwo">
                                            <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse"
                                                   href="#collapseTwo" aria-expanded="false"
                                                   aria-controls="collapseTwo">
                                                    <b>Information</b><i class="material-icons">keyboard_arrow_down</i>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" role="tabpanel"
                                             aria-labelledby="headingTwo"
                                             data-parent="#accordion">
                                            <div class="card-body">
                                                An infusion of West Coast cool and New York
                                                attitude, Rebecca Minkoff is synonymous with It
                                                girl style. Minkoff burst on the fashion scene
                                                with her best-selling &apos;Morning After Bag&apos;
                                                and later expanded her offering with the Rebecca
                                                Minkoff Collection - a range of luxe city
                                                staples with a &quot;downtown romantic&quot;
                                                theme.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-collapse">
                                        <div class="card-header" role="tab" id="headingThree">
                                            <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse"
                                                   href="#collapseThree" aria-expanded="false"
                                                   aria-controls="collapseThree">
                                                    <b>Details</b>
                                                    <i class="material-icons">keyboard_arrow_down</i>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseThree" class="collapse" role="tabpanel"
                                             aria-labelledby="headingThree"
                                             data-parent="#accordion">
                                            <div class="card-body">
                                                <ul>
                                                    <li>Storm and midnight-blue stretch
                                                        cotton-blend
                                                    </li>
                                                    <li>Notch lapels, functioning buttoned
                                                        cuffs, two front flap pockets, single
                                                        vent, internal pocket
                                                    </li>
                                                    <li>Two button fastening</li>
                                                    <li>84% cotton, 14% nylon, 2% elastane</li>
                                                    <li>Dry clean</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>


                                    <br>
                                    <div class="submit">
                                        <div class="text-center">

                                            <button type="button" class="btn btn-{{ $informatique->color_name }} btn-raised" data-toggle="modal" data-target="#phoneModal" title="View phone">
                                                <span class="btn-label">
                                                    <i class="material-icons">phone</i>
                                                </span>
                                                <b>Phone</b>
                                            </button>
                                            <button type="submit" class="btn btn-warning btn-raised">
                                                <span class="btn-label">
                                                    <i class="material-icons">message</i>
                                                </span>
                                                <b>Email</b>
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6 text-center mr-auto ml-auto"
                                 style="margin: 30px;">
                                <h3 class="title">
                                    <b class="title-red">You may also be interested in</b>
                                </h3>
                                <hr>
                            </div>
                            <div class="container">
                                <div class="row">

                                    @if(count($informatiques) > 0)
                                        @foreach($informatiques as $item)
                                            <div class="col-sm-6 col-md-3">
                                                <div class="card card-product">
                                                    <div class="card-header card-header-image">
                                                        <a href="{{ url( $item->path() ) }}">
                                                            <img class="img"
                                                                 src="../assets/img/image.jpg">
                                                        </a>
                                                    </div>
                                                    <div class="card-body">
                                                        @if($item->menucategoryorder)
                                                            <h4 class="card-title">
                                                                <a href="{{ url( $item->path() )  }}"> {{ $item->menucategoryorder->name }}</a>
                                                            </h4>
                                                        @else
                                                            <h4 class="card-title">
                                                                <a href="#pablo">Categories
                                                                    don't exist</a>
                                                            </h4>
                                                        @endif

                                                        <div class="card-description">
                                                            {!! str_limit($item->body, 79 ,'[...]') !!}
                                                            <br>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer justify-content-between">
                                                        <div class="stats">
                                                            <a href="{{ url( $item->path() )  }}"
                                                               class="btn btn-info btn-just-icon btn-link"
                                                               title="" rel="tooltip"
                                                               data-original-title="Click and Visit">
                                                                <i class="material-icons">visibility</i>
                                                            </a>
                                                        </div>
                                                        <div class="stats">
                                                            <a href="#pablo"
                                                               class="btn btn-danger btn-just-icon btn-link"
                                                               title="" rel="tooltip"
                                                               data-original-title="Click and Call the seller">
                                                                <i class="material-icons">call</i>
                                                            </a>
                                                        </div>
                                                        <div class="stats">
                                                            <a href="#pablo"
                                                               class="btn btn-success btn-just-icon btn-link"
                                                               title="" rel="tooltip"
                                                               data-original-title="Click and Send the Email">
                                                                <i class="material-icons">email</i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>

                        <!-- comments -->
                            <div class="row">
                                <div class="col-md-10 ml-auto mr-auto">
                                    <div class="media-area">
                                        <h3 class="title text-center">10 Comments</h3>

                                        <comments></comments>


                                        <!--<div class="pagination-area">
                                            <ul class="pagination justify-content-center text-center">
                                                <li class="page-item">
                                                    <a href="#pablo" class="page-link">&#xAB;</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#pablo" class="page-link">1</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#pablo" class="page-link">2</a>
                                                </li>
                                                <li class="active page-item">
                                                    <a href="#pablo" class="page-link">3</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#pablo" class="page-link">4</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#pablo" class="page-link">5</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#pablo" class="page-link">&#xBB;</a>
                                                </li>
                                            </ul>
                                        </div>-->
                                    </div>

                                    @guest
                                        <!--<h3 class="text-center">
                                            <h4 class="title text-center">Please
                                                <a href="{{ route('login') }}" class="text-info" data-toggle="modal" data-target="#loginModal">sign in</a> or
                                                <a href="{{route('register')}}" class="text-info">Create an account</a> to leave a comment
                                            </h4>
                                        </h3>-->
                                    @else
                                        <!--
                                        <add-comment></add-comment>
                                        -->
                                @endguest
                                    <!-- end media-post -->
                                    <!--<div class="media media-post">
                                        <a class="author float-left" href="#pablo">
                                            <div class="avatar">
                                                <img class="media-object" alt="64x64" src="./assets/img/face.jpg">
                                            </div>
                                        </a>
                                        <div class="media-body">
                                            <form class="form">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" placeholder="Your Name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" placeholder="Your email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <textarea class="form-control" placeholder="Write some nice stuff or nothing..." rows="6"></textarea>
                                                <div class="media-footer">
                                                    <h6>Sign in with</h6>
                                                    <a href="" class="btn btn-just-icon btn-round btn-twitter">
                                                        <i class="fa fa-twitter"></i>
                                                    </a>
                                                    <a href="" class="btn btn-just-icon btn-round btn-facebook">
                                                        <i class="fa fa-facebook-square"></i>
                                                    </a>
                                                    <a href="" class="btn btn-just-icon btn-round btn-google">
                                                        <i class="fa fa-google-plus-square"></i>
                                                    </a>
                                                    <a href="#pablo" class="btn btn-primary float-right">Post Comment</a>
                                                </div>
                                            </form>
                                        </div>
                                        <!- end media-body
                                    </div>-->
                                    <!-- end media-post -->
                                </div>
                            </div>
                        <!-- End comments -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('inc._footer')
<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="restoreLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="restoreLabel"><b>For more information contact this number:</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="card-title">{{ $informatique->phone  }}</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><b>No, Cancel</b></button>

                <a href="tel:{{ $informatique->phone  }}" class="btn btn-outline-{{ $informatique->color_name }}">
            <span class="btn-label">
            <i class="material-icons">phone</i>
        </span>
                    <b>Yes, Call</b>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

    @parent

    <script>
        $(document).ready(function () {
            $("#flexiselDemo1").flexisel({
                visibleItems: 4,
                itemsToScroll: 1,
                animationSpeed: 400,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    portrait: {
                        changePoint: 480,
                        visibleItems: 3
                    },
                    landscape: {
                        changePoint: 640,
                        visibleItems: 3
                    },
                    tablet: {
                        changePoint: 768,
                        visibleItems: 3
                    }
                }
            });
        });
    </script>

@endsection