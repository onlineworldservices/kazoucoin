<div class="row">
    <div class="col-md-10 ml-auto mr-auto text-center">
        <ul class="nav nav-pills nav-pills-icons nav-pills-rose">

            @if(count($categoryorders) > 0)
                @foreach($categoryorders as $item)

                    <li class="nav-item">
                        <a class="nav-link"
                           href="{{ $item->link_category }}">
                            <i class="material-icons">{{ $item->icon }}</i>
                            <b>{{ $item->name }}</b>
                        </a>
                    </li>
                @endforeach
            @endif
            <!--<li class="nav-item">
                <a class="nav-link " href="/orders">
                    <i class="material-icons">blur_on</i>
                    <b>All</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="/orders/informatiques">
                    <i class="material-icons">important_devices</i>
                    <b>Informatique</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#pill3" data-toggle="tab">
                    <i class="material-icons">accessibility</i>
                    <b>Mode</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#pill3" data-toggle="tab">
                    <i class="material-icons">devices_other</i>
                    <b>High Tech</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#pill3" data-toggle="tab">
                    <i class="material-icons">home</i>
                    <b>Maison</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#pill3" data-toggle="tab">
                    <i class="material-icons">face</i>
                    <b>Beautè</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#pill3" data-toggle="tab">
                    <i class="material-icons">layers</i>
                    <b>Autres</b>
                </a>
            </li>-->


        </ul>
    </div>
</div>
<br>
