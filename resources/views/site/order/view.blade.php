@extends('inc.app')

@section('title', '- All Orders')

@section('style')

@endsection

@section('navbar')

@guest
<nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top  navbar-expand-lg bg-warning" color-on-scroll="100" id="sectionsNav">
@else
<nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top  navbar-expand-lg bg-{{ Auth::user()->color_name }}" color-on-scroll="100" id="sectionsNav">
@endguest
    
@endsection
    
@section('content')
   
<div class="about-us">
    <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ URL::to('assets/img/image.jpg')}}&apos;);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h1 class="title">All Orders</h1>
                    <h5 class="title">Set Up your Slide in your Dashboard</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="main main-raised">
        <div class="container">

            <!-- Teams Creators -->
            <div class="about-team team-1">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title">
                            <b>Les articles en exposition</b>
                        </h2>
                        <h5 class="description">Dans cette partie, vous trouvez tous les articles en exposition par les marchants de partout le monde. Ne perdez pas de temp et contactez-les !!!</h5>
                    </div>
                </div>

                <div class="row">

                </div>
            </div>
        </div>
    </div>
</div>

    
@include('inc.footer')
    
@endsection

@section('scripts')
@parent

@endsection