@extends('inc.app')
@section('title', '- Help & Customer Service')
@section('style')
@endsection

@section('content')
	@include('inc.nav_bar')
    <div class="landing-page sidebar-collapse" id="app">
        <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ $user->avatarcover }}&apos;);">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h1 class="title">Any Questions?</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="main main-raised">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col-md-10 ml-auto mr-auto text-center">
                            <h2 class="kazoucoin-title title-red">Help & Customer Service</h2>
                            <h5 class="kazoucoin-subtitle title-grey">Need help or advice? Do you have a question about a topic?</h5>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <!--  nav pills -->
                    <div id="navigation-pills">
                        <div class="row">
                            <ul class="nav nav-pills nav-pills-warning ml-auto mr-auto  nav-pills-icons text-center">
                                <li class="nav-item">
                                    <a id="button_message" class="nav-link active" href="#pillMessage" data-toggle="tab">
                                        <i class="material-icons">mail</i>
                                        <span>Messages</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="button_faqs" class="nav-link" href="#pillFaqs" data-toggle="tab">
                                        <i class="material-icons">contact_support</i>
                                        <span>FAQs</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="button_chat" class="nav-link" href="#pillChat" data-toggle="tab">
                                        <i class="material-icons">chat</i>
                                        <span>Chat</span>
                                    </a>
                                </li>
                            </ul>
                            <br>
                            <div class="col-md-10 ml-auto mr-auto">
                                <br>
                                <aide-contact-messages></aide-contact-messages>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('inc._footer')

@endsection

@section('scripts')
@parent

@endsection
