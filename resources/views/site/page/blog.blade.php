@extends('inc.app')
@section('title', '- Blog du Site')
@section('style')
@endsection
@section('content')
    <div id="app">

        <router-view></router-view>
    </div>
    @include('cookieConsent::index')
    @include('inc.contactsIcons')
    @include('inc.chat_section')
@endsection

@section('scripts')

@endsection



