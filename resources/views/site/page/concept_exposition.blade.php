@extends('inc.app')
@section('title', '- Notre Concept Utilisation')

@section('style')

@endsection

@section('content')
    <div class="about-us" style="background-image: url(../assets/img/bg9.jpg); background-size: cover;">

        @include('inc.nav_bar')

        @if(count($slidesconcepts) > 0)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($slidesconcepts as $item)
                        <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                            <div class="page-header header-filter header-small" data-parallax="true"
                                 style="background-image: url(&apos;{{ url($item->photo) }}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 col-lg-10 ml-auto mr-auto text-center">
                                            <h2 class="title">
                                                <b>{{ $item->slide_title }}</b>
                                                <h4>{{ $item->slide_subtitle }}</h4>
                                            </h2>
                                            <br>
                                            @if($item->slide_btn_status != null)
                                                <div class="buttons">
                                                    <a id="button_hover" href="{{ $item->slide_btn_link }}"
                                                       class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                                        <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                                        <b class="title_hover">{{ $item->slide_btn_title }}</b>
                                                    </a>
                                                </div>
                                            @endif
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="page-header header-filter header-small" data-parallax="true"
                 style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-lg-8 ml-auto mr-auto text-center">
                            <h1 class="title">Notre Concept</h1>
                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="main main-raised">
            <div class="container">
                <!-- Concept Plateforme -->
                <div class="cd-section" id="features">
                    <div class="container">
                        <div class="features-1">
                            <div class="row">
                                <div class="col-md-10 col-lg-10 ml-auto mr-auto">
                                    <h2 class="kazoucoin-title title-red">The Concept {{ config('app.name') }}</h2>
                                    <h5 class="kazoucoin-subtitle title-grey">On {{ config('app.name') }}, The Online Shopping showcase,
                                        we offer you the opportunity to post a wide selection of your products (High
                                        Tech, Multimedia, Electrical Appliances, Fashion Trend, and others) among the
                                        largest brands at the best price, exceptional quality and controlled and to be
                                        contacted directly and easily by the interested parties of your region in the
                                        good terms and in a few clicks.</h5>
                                </div>
                            </div>
                            <div class="row">
                                @if(count($informations) > 0)
                                    @foreach($informations as $item)
                                        <div class="col-md-4 col-lg-4">
                                            <div class="card card-pricing card-plain">
                                                <div class="card-body">
                                                    <div class="icon icon-{!! $item->color_name !!}">
                                                        <i class="material-icons">{!! $item->icon !!}</i>
                                                    </div>
                                                    <h3 class="card-title">{!! $item->title !!}</h3>
                                                    <p class="card-description">
                                                        {!! htmlspecialchars_decode(str_limit($item->body, 250,'...'))!!}
                                                    </p>
                                                    <a id="new-user"
                                                       href="{{ route('user.informations') }}#{!! $item->slug !!}"
                                                       class="btn btn-{!! $item->color_name !!} btn-round">
                                                        <b>Read More</b>
                                                        <i class="material-icons size-profil">chevron_right</i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="about-services features-2">
                        <div class="row">
                            <div class="col-md-10 col-lg-10 ml-auto mr-auto text-center">
                                <h2 class="kazoucoin-title title-yellow">3 Steps to create your Online Store
                                    at {{ config('app.name') }}</h2>
                            </div>
                        </div>
                        <div class="row">
                            @if(count($howtoregisters) > 0)
                                @foreach($howtoregisters as $item)
                                    <div class="col-md-4 col-lg-4">
                                        <div class="info info-horizontal">
                                            <div class="icon icon-{!! $item->color_name !!}">
                                                <i class="material-icons">{!! $item->icon !!}</i>
                                            </div>
                                            <div class="description text-justify">
                                                <h4 class="info-title">{!! $item->title !!}</h4>
                                                <p class="text-justify">{!! str_limit($item->body, 250,'...') !!}</p>
                                                <a href="{{ route('user.registration') }}#{!! $item->slug !!}">Find more
                                                    ...</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Teams Creators -->
    <div class="about-team team-1">
        <div class="container">
            <div class="row ml-auto mr-auto">
                <div class="col-md-4 ml-auto mr-auto text-center">
                    <h2 class="kazoucoin-title title-red title-no-uppercase">
                        <b>The Founders</b>
                    </h2>
                    <h5 class="kazoucoin-subtitle title-grey">In a few lines, the founders of {{ config('app.name') }} who had this idea
                        of innovation for sale to the public with the implementation of best practices online and
                        offline.</h5>
                </div>
                @if(count($abouts) > 0)
                    @foreach($abouts as $about)
                        <div class="col-md-4">
                            <div class="card card-profile card-plain">
                                <div class="card-avatar">
                                    <a href="https://www.linkedin.com/in/{{$about->linklink}}" target="_blank">
                                        <img class="img" src="{{ URL::to( $about->photo )}}?{{ time() }}">
                                    </a>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">{{$about->fullname}}</h4>
                                    <h6 class="category text-muted">{!! $about->role !!}</h6>
                                    <p class="card-description" style="color: white;">
                                        {!! str_limit($about->body, 150,'&raquo') !!}
                                    </p>
                                </div>
                                <div class="card-footer justify-content-center">
                                    <!-- Linkedin Link -->
                                    @if($about->linklink != null)
                                        <a href="https://www.linkedin.com/in/{{$about->linklink}}"
                                           class="btn btn-just-icon btn-round btn-linkedin" target="_blank">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    @endif

                                <!-- Facebook Link -->
                                    @if($about->fblink != null)
                                        <a href="https://www.facebook.com/{{$about->fblink}}"
                                           class="btn btn-just-icon btn-round btn-facebook" target="_blank">
                                            <i class="fa fa-facebook-square"></i>
                                        </a>
                                    @endif

                                <!-- Tweeter Link -->
                                    @if($about->twlink != null)
                                        <a href="https://www.twitter.com/{{$about->twlink}}"
                                           class="btn btn-just-icon btn-round btn-twitter" target="_blank">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    @endif

                                <!-- Instagram Link -->
                                    @if($about->instlink != null)
                                        <a href="https://www.instagram.com/{{$about->instlink}}"
                                           class="btn btn-just-icon btn-round btn-instagram" target="_blank">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <br>
        <!-- Work with Us -->
        <div class="about-contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 ml-auto mr-auto text-center">
                        <h2 class="kazoucoin-title title-grey">
                            <b>Want to work with us?</b>
                        </h2>
                        <h5 class="kazoucoin-subtitle title-grey">You can contact us for any collaboration with our team. We
                            will get back to you in a couple of hours.</h5>
                        <br>
                        @include('inc.alert')
                        {!! Form::open(array('route' => 'user.save_work','files'=> 'true','method'=>'POST','class' =>'contact-form')) !!}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="bmd-label-floating">Your Fullname</label>
                                    {!! Form::text('name', null, array('class' => 'form-control','id'=>'name','placeholder' => '', 'required' => '')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email" class="bmd-label-floating">Your Email</label>
                                    {!! Form::email('email', null, array('class' => 'form-control','id' => 'email','placeholder' => '')) !!}
                                </div>
                            </div>
                            <div class="col-md-3" style="margin:10px;">
                                <div class="form-group">
                                    <select class="selectpicker form-control" data-style="select-with-transition"
                                            title="Choice Speciality" data-size="7" aria-hidden="true"
                                            name="speciality_id">

                                        @if(count($specialities) > 0)
                                            @foreach($specialities as $item)
                                                <option value="{{ $item->id }}">{{ $item->speciality_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <button id="button_hover" class="btn btn-success btn-round">
                                    <i class="material-icons">done_all</i>
                                    <b class="title_hover">Send</b>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('inc.footer')
@endsection

@section('scripts')
    @parent
@endsection
