@extends('inc.app')

@section('title', '- All Events')

@section('style')

@endsection

@section('content')

    <div class="about-us">
        @include('inc.nav_bar')

        @if(count($slidesevenements) > 0)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($slidesevenements as $item)
                        <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                            <div class="page-header header-filter header-small" data-parallax="true"
                                 style="background-image: url(&apos;{{ url($item['photo']) }}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 ml-auto mr-auto text-center">
                                            <h2 class="title">
                                                <b>{{ $item['slide_title'] }}</b>
                                            </h2>
                                            <h4>{{ $item['slide_subtitle']}}</h4>
                                            <br>
                                            @if($item['slide_btn_status'] != null)
                                                <div class="buttons">
                                                    <a href="{{ $item['slide_btn_link'] }}"
                                                       class="btn btn-{{ $item['slide_btn_color'] }} btn-{{ $item['slide_btn_style1'] }} btn-{{ $item['slide_btn_style'] }} btn-md ">
                                                        <i class="material-icons">{{ $item['slide_btn_icon'] }}</i>
                                                        <b>{{ $item['slide_btn_title'] }}</b>
                                                    </a>
                                                </div>
                                            @endif
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="page-header header-filter header-small" data-parallax="true"
                 style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h1 class="title">Events page</h1>
                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="main main-raised">
            <div class="container">

                <!-- All Events -->
                <div class="cd-section" id="projects">
                    <div class="projects-1" id="projects-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 ml-auto mr-auto text-center">
                                    <h2 class="kazoucoin-title title-red">
                                        Les Evénements en Exposition
                                    </h2>
                                    <br>
                                    <h5 class="kazoucoin-subtitle title-grey">Restez brancher sur {{ config('app.name') }} afin de
                                        profiter de nos événements extraordinaires tels que les offres concerts, sports
                                        & loisirs, événements de mariage et bien autre. Notre plateforme innovante rend
                                        le shopping en ligne un vrai jeu d'enfant.</h5>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-10 ml-auto mr-auto text-center">
                                <ul class="nav nav-pills nav-pills-icons nav-pills-info">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#pillAll" data-toggle="tab">
                                            <i class="material-icons">blur_on</i>
                                            <b>All</b>
                                        </a>
                                    </li>

                                    @if(count($categoryevents) > 0)
                                        @foreach($categoryevents as $item)
                                            <li class="nav-item">
                                                <a class="nav-link" href="/{!! $item->link_category !!}">
                                                    <i class="material-icons">{{ $item->icon }}</i>
                                                    <b>{{ $item->name }}</b>
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif

                                </ul>
                                <br><br>

                                <div class="row">
                                    <div class="col-md-12 ml-auto mr-auto text-center">
                                        <ul class="nav nav-pills nav-pills-icons nav-pills-rose">

                                            @if(count($menucategoryevents) > 0)
                                                @foreach($menucategoryevents as $item)
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#pill1">{{ $item->name }}</a>
                                                    </li>
                                                @endforeach
                                            @endif


                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <!-- Pagination -->
                <div class="row">
                    <div class="ml-auto mr-auto text-center">
                        <ul class="pagination pagination-primary">

                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link"> Prev</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">1</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">...</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">5</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">6</a>
                            </li>
                            <li class="active page-item">
                                <a href="javascript:void(0);" class="page-link">7</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">8</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">9</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">...</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">12</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link"> Next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('inc._footer')

@endsection

@section('scripts')
    @parent

@endsection
