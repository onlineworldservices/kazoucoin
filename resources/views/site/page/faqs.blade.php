@extends('inc.app')

@section('title', '- FAQs')

@section('style')

@endsection

@section('content')

<div class="about-us" id="app">
    @include('inc.nav_bar')

    @if(count($slidesfaqs) > 0)
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($slidesfaqs as $item)
            <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ url($item['photo']) }}&apos;);">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 ml-auto mr-auto text-center">
                                <h2 class="title">
                                    <b>{{ $item['slide_title'] }}</b>
                                    <h4>{{ $item['slide_subtitle'] }}</h4>
                                </h2>
                                <br>
                                @if($item['slide_btn_status'] != null)
                                <div class="buttons">
                                    <a href="{{ $item['slide_btn_link'] }}" class="btn btn-{{ $item['slide_btn_color'] }} btn-{{ $item['slide_btn_style1'] }} btn-{{ $item['slide_btn_style'] }} btn-md ">
                                        <i class="material-icons">{{ $item['slide_btn_icon'] }}</i>
                                        <b>{{ $item['slide_btn_title'] }}</b>
                                    </a>
                                </div>
                                @endif
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @else
    <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h1 class="title">Our FAQs</h1>
                    <h5 class="title">Set Up your Slide in your Dashboard</h5>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="main main-raised">
        <div class="container">
            <br>
            @guest

            @else
            @if(Auth::user()->email_verified_at === null)
            <verify-link></verify-link>
            @endif()
            @endguest
            <!-- Foire Aux Questions (FAQ) -->
            <div class="section cd-section" id="javascriptComponents">
                <div class="container">
                    <div id="collapse">
                        <div class="row">
                            <div class="col-md-10 ml-auto mr-auto text-center">
                                <h2 class="kazoucoin-title title-red">
                                    Frequently Asked Questions (FAQs)
                                </h2>
                                <br>
                                <h5 class="kazoucoin-subtitle title-grey">In this section, you will find a guide to Frequently Asked Questions / Answers.</h5>
                            </div>
                        </div>
                        <br>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 ml-auto mr-auto text-center">
                                    @if(count($categoryfaqs) > 0)
                                    @foreach($categoryfaqs as $item)
                                    <a id="button_hover" href="{{ route('category_faq',$item->slug) }}" class="btn btn-{{ $item->color_name }} btn-round">
                                        <i class="material-icons">{{ $item->icon }}</i>
                                        <b class="title_hover">{{ $item->name }}</b>
                                    </a>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-11 ml-auto mr-auto">
                                <div id="accordion" role="tablist">
                                    @if(count($faqs) > 0)
                                    @foreach($faqs as $item)
                                    @if($item['status'] != null)
                                    <div class="card card-collapse">
                                        <div class="card-header" role="tab" id="headingFaq{{ $item['id'] }}">
                                            <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" href="#collapseFaq{{ $item['id'] }}" aria-expanded="false" aria-controls="collapseFaq{{ $item['id'] }}">
                                                    {!! $item['title'] !!}
                                                    <i class="material-icons">keyboard_arrow_down</i>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseFaq{{ $item['id'] }}" class="collapse" role="tabpanel" aria-labelledby="headingFaq{{ $item['id'] }}" data-parent="#accordion">
                                            <div class="card-body text-justify">
                                                {!! $item['body'] !!}
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <!-- Problème Non Résolu -->
                            <div class="col-md-4 col-lg-4 mr-auto ml-auto">
                                <div class="card card-pricing card-plain">
                                    <div class="card-body">
                                        <div class="icon icon-info">
                                            <i class="material-icons">contact_support</i>
                                        </div>
                                        <h3 class="card-title">Other Topics?</h3>
                                        <a id="button_hover" href="{{ route('aide_contact_messages')}}" class="btn btn-info btn-round">
                                            <i class="material-icons">mail</i>
                                            <b class="title_hover">Message</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Autres Sujets -->
                            <div class="col-md-4 col-lg-4 mr-auto ml-auto">
                                <div class="card card-pricing card-plain">
                                    <div class="card-body">
                                        <div class="icon icon-danger">
                                            <i class="material-icons">clear</i>
                                        </div>
                                        <h3 class="card-title">Problem Not Resolved?</h3>
                                        <a id="button_hover" href="/contact-us" class="btn btn-danger btn-round">
                                            <i class="material-icons">perm_phone_msg</i>
                                            <b class="title_hover">Contact-Us</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row text-center">
                            <news-letter></news-letter>
                            <vue-progress-bar></vue-progress-bar>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('inc.footer')

@endsection

@section('scripts')
@parent

@endsection
