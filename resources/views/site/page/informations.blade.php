@extends('inc.app')
@section('title', '- Informations Site')
@section('style')
@endsection

@section('content')
<div class="landing-page" id="app">
    @include('inc.nav_bar')
    @if(count($slidesconcepts) > 0)
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($slidesconcepts as $item)
            <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                <div class="page-header header-filter header-small" data-parallax="true"
                     style="background-image: url(&apos;{{ URL::to( $item->photo ) }}&apos;);">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-lg-10 ml-auto mr-auto text-center">
                                <h2 class="title">
                                    <b>{{ $item->slide_title }}</b>
                                    <h4>{{ $item->slide_subtitle }}</h4>
                                </h2>
                                <br>
                                @if($item->slide_btn_status != null)
                                <div class="buttons">
                                    <a href="{{ $item->slide_btn_link }}"
                                       class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                        <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                        <b>{{ $item->slide_btn_title }}</b>
                                    </a>
                                </div>
                                @endif
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @else
    <div class="page-header header-filter header-small" data-parallax="true"
         style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 ml-auto mr-auto text-center">
                    <h1 class="title">Informations</h1>
                    <h5 class="title">Set Up your Slide in your Dashboard</h5>
                </div>
            </div>
        </div>
    </div>
    @endif
    @include('inc.login_modal')
    <div class="main main-raised">
        <div class="container">
            <!-- Comment s'enregistrer sur notre Plateforme -->
            <div class="features-3">
                <div class="row">
                    <div class="col-md-12 mr-auto ml-auto">
                        <div class="category">
                            <h3 class="kazoucoin-title title-red text-center">
                                <b>Informations</b>
                            </h3>
                            @if(count($informations) > 0)
                                @foreach($informations as $item)
                                    <div class="info info-horizontal" id="{!! $item->slug !!}">
                                        <div class="icon icon-{!! $item->color_name !!}">
                                            <i class="material-icons">{!! $item->icon !!}</i>
                                        </div>
                                        <div class="description text-justify">
                                            <h4 class="info-title">{!! $item->title !!}</h4>
                                            <p>{!! htmlspecialchars_decode($item->body) !!}</p>
                                        </div>
                                    </div>
                                <br>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @guest
            @include('inc.deuxBoutons')
        @endguest
    </div>
</div>
@include('inc.footer')
@endsection

@section('scripts')
@parent
@endsection
