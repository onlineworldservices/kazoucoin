@extends('inc.app')
@section('title', '- Mentions Légales Site')
@section('style')
@endsection

@section('content')
    <div class="landing-page">
        @include('inc.nav_bar')
        @if(count($slidesmentionslegales) > 0)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" id="app">
                    @foreach($slidesmentionslegales as $item)
                        <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                            <div class="page-header header-filter header-small" data-parallax="true"
                                 style="background-image: url(&apos;{{ $item['photo'] }}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 col-lg-10 ml-auto mr-auto text-center">
                                            <h2 class="title">
                                                <b>{{ $item['slide_title'] }}</b>
                                                <h4>{{ $item['slide_subtitle'] }}</h4>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="page-header header-filter header-small" data-parallax="true"
                 style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-lg-8 ml-auto mr-auto text-center">
                            <h1 class="title">Legals Notice</h1>
                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @include('inc.login_modal')
        <div class="main main-raised">
            <div class="container">
                <div class="features-3">
                    <div class="row">
                        <div class="col-md-12 mr-auto ml-auto">
                            <div class="category">
                                <h3 class="kazoucoin-title title-red text-center">
                                    <b>Legals Notice</b>
                                </h3>
                                @if(count($legalnotices) > 0)
                                    @foreach($legalnotices as $item)
                                        <br>
                                        <p class="text-justify">
                                            {!! $markdown->parse($item->body) !!}
                                        </p>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @guest
                @include('inc.deuxBoutons')
            @endguest
        </div>
    </div>
    @include('inc.footer')
@endsection

@section('scripts')
    @parent
@endsection
