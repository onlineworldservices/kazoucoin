@extends('inc.app')

@section('title', '- Orders')

@section('style')

@endsection

@section('content')

    <div class="about-us">

        @include('inc.nav_bar')

        @if(count($slidesorders) > 0)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($slidesorders as $item)
                        <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                            <div class="page-header header-filter header-small" data-parallax="true"
                                 style="background-image: url(&apos;{{ $item->photo }}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 ml-auto mr-auto text-center">
                                            <h2 class="title">
                                                <b>{{ $item->slide_title }}</b>
                                                <h4>{{ $item->slide_subtitle }}</h4>
                                            </h2>
                                            <br>
                                            @if($item->slide_btn_status != null)
                                                <div class="buttons">
                                                    <a href="{{ $item->slide_btn_link }}"
                                                       class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                                        <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                                        <b>{{ $item->slide_btn_title }}</b>
                                                    </a>
                                                </div>
                                            @endif
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="page-header header-filter header-small" data-parallax="true"
                 style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h1 class="title">All Orders</h1>
                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="main main-raised">
            <div class="container">

                <!-- All Orders -->
                <div class="cd-section" id="projects">
                    <div class="projects-1" id="projects-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 ml-auto mr-auto text-center">
                                    <h2 class="kazoucoin-title title-red">
                                        Les Articles en Exposition
                                    </h2>
                                    <br>
                                    <h5 class="kazoucoin-subtitle title-grey">Retrouvez à portée de main tous les meilleurs articles
                                        high-tech comme la gamme Samsung Galaxy, les mobiles Tecno, les smartphones ou
                                        encore les tablettes de poche, ainsi tout le nécessaire pour remplir et décorer
                                        votre maison, et aussi tout l'univers de la Mode : chaussures, vêtements et
                                        accessoires pour toute la famille.</h5>
                                </div>
                            </div>
                        </div>

                    @include('site.order.nav_orders')
                    <!--<div class="row">
<div class="col-md-12 ml-auto mr-auto text-center">
<ul class="nav nav-pills nav-pills-icons nav-pills-info">
<li class="nav-item">
<a class="nav-link active" href="#pill1" data-toggle="tab">Monthly</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#pill2" >Yearly</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#pill2" >Moto</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#sac" >Sac</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#pill2" >Case</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#pill2" >Yearly</a>
</li>
</ul>
</div>
</div>-->

                    </div>
                </div>
                <div class="tab-content tab-space">
                    <div class="tab-pane active" id="pillAll">
                        <div class="row">
                            <div class="col-md-4 expo">
                                <div class="card card-blog">
                                    <div class="card-header card-header-image">
                                        <span class="notify-badge-new"><b>New</b></span>
                                        <a href="#pablo">
                                            <img src="../assets/img-error/clint-mckoy.jpg"
                                                 style="width:318px; height:212px" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                        <h4 class="card-title text-justify">
                                            <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                        </h4>
                                    </div>
                                    <br>
                                    <div class="price-container text-center">
                                        <span class="price price-old"> <b>€1,430</b></span>
                                        <span class="price price-new"> <b>€743</b></span>
                                        <span class="notify-solde"><b>- 40%</b></span>
                                    </div>
                                    <br>
                                    <div class="card-footer">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span class="description">Tania Andrew</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                            <i class="material-icons icon icon-warning">visibility</i> <b>45</b> &#xB7;
                                            <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 expo">
                                <div class="card card-blog">
                                    <div class="card-header card-header-image">
                                        <span class="notify-badge-new"><b>New</b></span>
                                        <a href="#pablo">
                                            <img src="../assets/assets-for-demo/section-components/laptop-basics.png"
                                                 style="width:318px; height:212px" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                        <h4 class="card-title text-justify">
                                            <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                        </h4>
                                    </div>
                                    <br>
                                    <div class="price-container text-center">
                                        <span class="price price-old"> <b>€1,430</b></span>
                                    </div>
                                    <br>
                                    <div class="card-footer">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span class="description">Tania Andrew</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                            <i class="material-icons icon icon-warning">visibility</i> <b>45</b> &#xB7;
                                            <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 expo">
                                <div class="card card-blog">
                                    <div class="card-header card-header-image">
                                        <a href="#pablo">
                                            <img src="../assets/assets-for-demo/section-components/responsive.png"
                                                 style="width:318px; height:212px" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                        <h4 class="card-title text-justify">
                                            <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                        </h4>
                                    </div>
                                    <br>
                                    <div class="price-container text-center">
                                        <span class="price price-new"> <b>€743</b></span>
                                    </div>
                                    <br>
                                    <div class="card-footer">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span class="description">Tania Andrew</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                            <i class="material-icons icon icon-warning">visibility</i> <b>45</b> &#xB7;
                                            <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 expo">
                                <div class="card card-blog">
                                    <div class="card-header card-header-image">
                                        <a href="#pablo">
                                            <img src="../assets/img-error/clint-mckoy.jpg"
                                                 style="width:318px; height:212px" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                        <h4 class="card-title text-justify">
                                            <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                        </h4>
                                    </div>
                                    <br>
                                    <div class="price-container text-center">
                                        <span class="price price-old"> <b>€1,430</b></span>
                                        <span class="price price-new"> <b>€743</b></span>
                                        <span class="notify-solde"><b>- 60%</b></span>
                                    </div>
                                    <br>
                                    <div class="card-footer">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span class="description">Tania Andrew</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                            <i class="material-icons icon icon-warning">visibility</i> <b>45</b> &#xB7;
                                            <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 expo">
                                <div class="card card-blog">
                                    <div class="card-header card-header-image">
                                        <span class="notify-badge-new"><b>New</b></span>
                                        <a href="#pablo">
                                            <img src="../assets/assets-for-demo/section-components/laptop-basics.png"
                                                 style="width:318px; height:212px" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                        <h4 class="card-title text-justify">
                                            <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                        </h4>
                                    </div>
                                    <br>
                                    <div class="price-container text-center">
                                        <span class="price price-old"> <b>€1,430</b></span>
                                        <span class="price price-new"> <b>€743</b></span>
                                        <span class="notify-solde"><b>- 40%</b></span>
                                    </div>
                                    <br>
                                    <div class="card-footer">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span class="description">Tania Andrew</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                            <i class="material-icons icon icon-warning">visibility</i> <b>45</b> &#xB7;
                                            <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 expo">
                                <div class="card card-blog">
                                    <div class="card-header card-header-image">
                                        <span class="notify-badge-new"><b>New</b></span>
                                        <a href="#pablo">
                                            <img src="../assets/assets-for-demo/section-components/responsive.png"
                                                 style="width:318px; height:212px" alt="">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                        <h4 class="card-title text-justify">
                                            <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                        </h4>
                                    </div>
                                    <br>
                                    <div class="price-container text-center">
                                        <span class="price price-old"> <b>€1,430</b></span>
                                        <span class="price price-new"> <b>€743</b></span>
                                        <span class="notify-solde"><b>- 40%</b></span>
                                    </div>
                                    <br>
                                    <div class="card-footer">
                                        <div class="author">
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                     alt="..." class="avatar img-raised">
                                                <span class="description">Tania Andrew</span>
                                            </a>
                                        </div>
                                        <div class="stats ml-auto">
                                            <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                            <i class="material-icons icon icon-warning">visibility</i> <b>45</b> &#xB7;
                                            <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Pagination -->
                <div class="row">
                    <div class="ml-auto mr-auto text-center">
                        <ul class="pagination pagination-primary">
                            <!--
color-classes: "pagination-primary", "pagination-info", "pagination-success", "pagination-warning", "pagination-danger"
-->
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link"> Prev</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">1</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">...</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">5</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">6</a>
                            </li>
                            <li class="active page-item">
                                <a href="javascript:void(0);" class="page-link">7</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">8</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">9</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">...</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link">12</a>
                            </li>
                            <li class="page-item">
                                <a href="javascript:void(0);" class="page-link"> Next</a>
                            </li>
                        </ul>
                    </div>
                    <br>
                </div>
                <br>
            </div>
        </div>
    </div>
    @include('inc.footer')

@endsection

@section('scripts')
    @parent

@endsection
