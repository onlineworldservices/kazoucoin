@extends('inc.app')

@section('title', '- All Others Services')

@section('style')

@endsection


@section('content')

    <div class="about-us">
        @include('inc.nav_bar')

        @if(count($slidesothers) > 0)
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($slidesothers as $item)
                        <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                            <div class="page-header header-filter header-small" data-parallax="true"
                                 style="background-image: url(&apos;{{ url($item->photo) }}&apos;);">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 ml-auto mr-auto text-center">
                                            <h2 class="title">
                                                <b>{{ $item->slide_title }}</b>
                                                <h4>{{ $item->slide_subtitle }}</h4>
                                            </h2>
                                            <br>
                                            @if($item->slide_btn_status != null)
                                                <div class="buttons">
                                                    <a href="{{ $item->slide_btn_link }}"
                                                       class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                                        <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                                        <b>{{ $item->slide_btn_title }}</b>
                                                    </a>
                                                </div>
                                            @endif
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="page-header header-filter header-small" data-parallax="true"
                 style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h1 class="title">All Other Services</h1>
                            <h5 class="title">Set Up your Slide in your Dashboard</h5>
                        </div>
                    </div>
                </div>
            </div>
        @endif


        <div class="main main-raised">
            <div class="container">

                <!-- All Others -->
                <div class="cd-section" id="projects">
                    <div class="projects-1" id="projects-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 ml-auto mr-auto text-center">
                                    <h2 class="kazoucoin-title title-red">
                                        Autres Services
                                    </h2>
                                    <br>
                                    <h5 class="kazoucoin-subtitle title-grey">Dans cette partie, vous trouvez tous les articles en
                                        exposition par les marchants de partout le monde. Ne perdez pas de temp et
                                        contactez-les !!!</h5>
                                </div>
                            </div>
                            <br><br>
                            <div class="tab-content tab-space">
                                <div class="tab-pane active" id="pillAll">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-4">
                                            <div class="rotating-card-container manual-flip">
                                                <div class="card card-rotate">
                                                    <div class="front"
                                                         style="background-image: url('assets/img/bg9.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-category card-category-social">
                                                                <i class="material-icons">label</i> Grand Titre
                                                            </h5>
                                                            <h4 class="card-title">
                                                                <a href="#pablo">"Dribbble just acquired Crew, a very
                                                                    interesting startup..."</a>
                                                            </h4>
                                                            <p class="card-description">
                                                                Don't be scared of the truth because we need to restart
                                                                the human foundation in truth And I love you like Kanye
                                                                loves Kanye I love Rick Owens’ bed design but the back
                                                                is...
                                                            </p>
                                                            <div class="stats text-center">
                                                                <button type="button" name="button"
                                                                        class="btn btn-success btn-fill btn-round btn-rotate">
                                                                    <i class="material-icons">refresh</i> Rotate
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="author">
                                                                <a href="#pablo">
                                                                    <img src="assets/img/face.jpg" alt="..."
                                                                         class="avatar img-raised">
                                                                    <span>Boclair</span>
                                                                </a>
                                                            </div>
                                                            <div class="stats ml-auto">
                                                                <i class="material-icons">thumb_up_alt</i>2.4K
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="back back-background"
                                                         style="background-image: url('assets/img/kit/pro/examples/card-blog5.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-title">
                                                                Envie d'en savoir plus?
                                                            </h5>
                                                            <p class="card-description">Vous avez le choix de plus
                                                                d'informations, d'écrire et d'appeller
                                                                <span>Boclair</span></p>
                                                            <br>
                                                            <div class="footer text-center">
                                                                <a href="#pablo"
                                                                   class="btn btn-info btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">visibility</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-success btn-just-icon btn-fill btn-round btn-wd">
                                                                    <i class="material-icons">message</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-danger btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">phone</i>
                                                                </a>
                                                            </div>
                                                            <br>
                                                            <button type="button" name="button"
                                                                    class="btn btn-link btn-round btn-rotate">
                                                                <i class="material-icons">keyboard_backspace</i> Back
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-4">
                                            <div class="rotating-card-container manual-flip">
                                                <div class="card card-rotate">
                                                    <div class="front"
                                                         style="background-image: url('assets/img/bg9.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-category card-category-social">
                                                                <i class="material-icons">label</i> Grand Titre
                                                            </h5>
                                                            <h4 class="card-title">
                                                                <a href="#pablo">"Dribbble just acquired Crew, a very
                                                                    interesting startup..."</a>
                                                            </h4>
                                                            <p class="card-description">
                                                                Don't be scared of the truth because we need to restart
                                                                the human foundation in truth And I love you like Kanye
                                                                loves Kanye I love Rick Owens’ bed design but the back
                                                                is...
                                                            </p>
                                                            <div class="stats text-center">
                                                                <button type="button" name="button"
                                                                        class="btn btn-success btn-fill btn-round btn-rotate">
                                                                    <i class="material-icons">refresh</i> Rotate
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="author">
                                                                <a href="#pablo">
                                                                    <img src="assets/img/face.jpg" alt="..."
                                                                         class="avatar img-raised">
                                                                    <span>Boclair</span>
                                                                </a>
                                                            </div>
                                                            <div class="stats ml-auto">
                                                                <i class="material-icons">thumb_up_alt</i>2.4K
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="back back-background"
                                                         style="background-image: url('assets/img/kit/pro/examples/card-blog5.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-title">
                                                                Envie d'en savoir plus?
                                                            </h5>
                                                            <p class="card-description">Vous avez le choix de plus
                                                                d'informations, d'écrire et d'appeller
                                                                <span>Boclair</span></p>
                                                            <br>
                                                            <div class="footer text-center">
                                                                <a href="#pablo"
                                                                   class="btn btn-info btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">visibility</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-success btn-just-icon btn-fill btn-round btn-wd">
                                                                    <i class="material-icons">message</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-danger btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">phone</i>
                                                                </a>
                                                            </div>
                                                            <br>
                                                            <button type="button" name="button"
                                                                    class="btn btn-link btn-round btn-rotate">
                                                                <i class="material-icons">keyboard_backspace</i> Back
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-4">
                                            <div class="rotating-card-container manual-flip">
                                                <div class="card card-rotate">
                                                    <div class="front"
                                                         style="background-image: url('assets/img/bg9.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-category card-category-social">
                                                                <i class="material-icons">label</i> Grand Titre
                                                            </h5>
                                                            <h4 class="card-title">
                                                                <a href="#pablo">"Dribbble just acquired Crew, a very
                                                                    interesting startup..."</a>
                                                            </h4>
                                                            <p class="card-description">
                                                                Don't be scared of the truth because we need to restart
                                                                the human foundation in truth And I love you like Kanye
                                                                loves Kanye I love Rick Owens’ bed design but the back
                                                                is...
                                                            </p>
                                                            <div class="stats text-center">
                                                                <button type="button" name="button"
                                                                        class="btn btn-success btn-fill btn-round btn-rotate">
                                                                    <i class="material-icons">refresh</i> Rotate
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="author">
                                                                <a href="#pablo">
                                                                    <img src="assets/img/face.jpg" alt="..."
                                                                         class="avatar img-raised">
                                                                    <span>Boclair</span>
                                                                </a>
                                                            </div>
                                                            <div class="stats ml-auto">
                                                                <i class="material-icons">thumb_up_alt</i>2.4K
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="back back-background"
                                                         style="background-image: url('assets/img/kit/pro/examples/card-blog5.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-title">
                                                                Envie d'en savoir plus?
                                                            </h5>
                                                            <p class="card-description">Vous avez le choix de plus
                                                                d'informations, d'écrire et d'appeller
                                                                <span>Boclair</span></p>
                                                            <br>
                                                            <div class="footer text-center">
                                                                <a href="#pablo"
                                                                   class="btn btn-info btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">visibility</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-success btn-just-icon btn-fill btn-round btn-wd">
                                                                    <i class="material-icons">message</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-danger btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">phone</i>
                                                                </a>
                                                            </div>
                                                            <br>
                                                            <button type="button" name="button"
                                                                    class="btn btn-link btn-round btn-rotate">
                                                                <i class="material-icons">keyboard_backspace</i> Back
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-4">
                                            <div class="rotating-card-container manual-flip">
                                                <div class="card card-rotate">
                                                    <div class="front"
                                                         style="background-image: url('assets/img/bg9.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-category card-category-social">
                                                                <i class="material-icons">label</i> Grand Titre
                                                            </h5>
                                                            <h4 class="card-title">
                                                                <a href="#pablo">"Dribbble just acquired Crew, a very
                                                                    interesting startup..."</a>
                                                            </h4>
                                                            <p class="card-description">
                                                                Don't be scared of the truth because we need to restart
                                                                the human foundation in truth And I love you like Kanye
                                                                loves Kanye I love Rick Owens’ bed design but the back
                                                                is...
                                                            </p>
                                                            <div class="stats text-center">
                                                                <button type="button" name="button"
                                                                        class="btn btn-success btn-fill btn-round btn-rotate">
                                                                    <i class="material-icons">refresh</i> Rotate
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="author">
                                                                <a href="#pablo">
                                                                    <img src="assets/img/face.jpg" alt="..."
                                                                         class="avatar img-raised">
                                                                    <span>Randrin</span>
                                                                </a>
                                                            </div>
                                                            <div class="stats ml-auto">
                                                                <i class="material-icons">thumb_up_alt</i>200
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="back back-background"
                                                         style="background-image: url('assets/img/kit/pro/examples/card-blog5.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-title">
                                                                Envie d'en savoir plus?
                                                            </h5>
                                                            <p class="card-description">Vous avez le choix de plus
                                                                d'informations, d'écrire et d'appeller
                                                                <span>Randrin</span></p>
                                                            <br>
                                                            <div class="footer text-center">
                                                                <a href="#pablo"
                                                                   class="btn btn-info btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">visibility</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-success btn-just-icon btn-fill btn-round btn-wd">
                                                                    <i class="material-icons">message</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-danger btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">phone</i>
                                                                </a>
                                                            </div>
                                                            <br>
                                                            <button type="button" name="button"
                                                                    class="btn btn-link btn-round btn-rotate">
                                                                <i class="material-icons">keyboard_backspace</i> Back
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-4">
                                            <div class="rotating-card-container manual-flip">
                                                <div class="card card-rotate">
                                                    <div class="front"
                                                         style="background-image: url('assets/img/bg9.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-category card-category-social">
                                                                <i class="material-icons">label</i> Grand Titre
                                                            </h5>
                                                            <h4 class="card-title">
                                                                <a href="#pablo">"Dribbble just acquired Crew, a very
                                                                    interesting startup..."</a>
                                                            </h4>
                                                            <p class="card-description">
                                                                Don't be scared of the truth because we need to restart
                                                                the human foundation in truth And I love you like Kanye
                                                                loves Kanye I love Rick Owens’ bed design but the back
                                                                is...
                                                            </p>
                                                            <div class="stats text-center">
                                                                <button type="button" name="button"
                                                                        class="btn btn-success btn-fill btn-round btn-rotate">
                                                                    <i class="material-icons">refresh</i> Rotate
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="author">
                                                                <a href="#pablo">
                                                                    <img src="assets/img/face.jpg" alt="..."
                                                                         class="avatar img-raised">
                                                                    <span>Alex</span>
                                                                </a>
                                                            </div>
                                                            <div class="stats ml-auto">
                                                                <i class="material-icons">thumb_up_alt</i>1.2K
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="back back-background"
                                                         style="background-image: url('assets/img/kit/pro/examples/card-blog5.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-title">
                                                                Envie d'en savoir plus?
                                                            </h5>
                                                            <p class="card-description">Vous avez le choix de plus
                                                                d'informations, d'écrire et d'appeller <span>Alex</span>
                                                            </p>
                                                            <br>
                                                            <div class="footer text-center">
                                                                <a href="#pablo"
                                                                   class="btn btn-info btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">visibility</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-success btn-just-icon btn-fill btn-round btn-wd">
                                                                    <i class="material-icons">message</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-danger btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">phone</i>
                                                                </a>
                                                            </div>
                                                            <br>
                                                            <button type="button" name="button"
                                                                    class="btn btn-link btn-round btn-rotate">
                                                                <i class="material-icons">keyboard_backspace</i> Back
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-4">
                                            <div class="rotating-card-container manual-flip">
                                                <div class="card card-rotate">
                                                    <div class="front"
                                                         style="background-image: url('assets/img/bg9.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-category card-category-social">
                                                                <i class="material-icons">label</i> Grand Titre
                                                            </h5>
                                                            <h4 class="card-title">
                                                                <a href="#pablo">"Dribbble just acquired Crew, a very
                                                                    interesting startup..."</a>
                                                            </h4>
                                                            <p class="card-description">
                                                                Don't be scared of the truth because we need to restart
                                                                the human foundation in truth And I love you like Kanye
                                                                loves Kanye I love Rick Owens’ bed design but the back
                                                                is...
                                                            </p>
                                                            <div class="stats text-center">
                                                                <button type="button" name="button"
                                                                        class="btn btn-success btn-fill btn-round btn-rotate">
                                                                    <i class="material-icons">refresh</i> Rotate
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="author">
                                                                <a href="#pablo">
                                                                    <img src="assets/img/face.jpg" alt="..."
                                                                         class="avatar img-raised">
                                                                    <span>Boclair</span>
                                                                </a>
                                                            </div>
                                                            <div class="stats ml-auto">
                                                                <i class="material-icons">thumb_up_alt</i>2.4K
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="back back-background"
                                                         style="background-image: url('assets/img/kit/pro/examples/card-blog5.jpg');">
                                                        <div class="card-body">
                                                            <h5 class="card-title">
                                                                Envie d'en savoir plus?
                                                            </h5>
                                                            <p class="card-description">Vous avez le choix de plus
                                                                d'informations, d'écrire et d'appeller
                                                                <span>Boclair</span></p>
                                                            <br>
                                                            <div class="footer text-center">
                                                                <a href="#pablo"
                                                                   class="btn btn-info btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">visibility</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-success btn-just-icon btn-fill btn-round btn-wd">
                                                                    <i class="material-icons">message</i>
                                                                </a>
                                                                <a href="#pablo"
                                                                   class="btn btn-danger btn-just-icon btn-fill btn-round">
                                                                    <i class="material-icons">phone</i>
                                                                </a>
                                                            </div>
                                                            <br>
                                                            <button type="button" name="button"
                                                                    class="btn btn-link btn-round btn-rotate">
                                                                <i class="material-icons">keyboard_backspace</i> Back
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pill2">
                                    <div class="col-md-4">
                                        <div class="card card-plain">
                                            <a href="http://www.creative-tim.com/product/get-shit-done-pro"
                                               target="_blank">
                                                <div class="card-header card-header-image">
                                                    <img
                                                        src="http://s3.amazonaws.com/creativetim_bucket/products/26/original/opt_gsdk_new_thumbnail.jpg">
                                                </div>
                                            </a>
                                            <div class="card-body ">
                                                <a href="http://www.creative-tim.com/product/get-shit-done-pro"
                                                   target="_blank">
                                                    <h4 class="card-title">Get Shit Done Kit PRO</h4>
                                                </a>
                                                <h6 class="card-category">Premium UI Kit</h6>
                                                <p class="card-description">
                                                    Get Shit Done Kit Pro it&apos;s a Bootstrap Kit that comes with a
                                                    huge number of customisable components. They are pixel perfect,
                                                    light and easy to use and combine with other elements.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pill3">
                                </div>

                                <!-- Pagination -->
                                <div class="row">
                                    <div class="ml-auto mr-auto text-center">
                                        <ul class="pagination pagination-primary">
                                            <!--
   lasses: "pagination-primary", "pagination-info", "pagination-success", "pagination-warning", "pagination-danger"
   -->

                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link"> Prev</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link">1</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link">...</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link">5</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link">6</a>
                                            </li>
                                            <li class="active page-item">
                                                <a href="javascript:void(0);" class="page-link">7</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link">8</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link">9</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link">...</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link">12</a>
                                            </li>
                                            <li class="page-item">
                                                <a href="javascript:void(0);" class="page-link"> Next</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('inc.footer')

@endsection

@section('scripts')
    @parent

@endsection
