@extends('inc.app')
<?php $titleTag = htmlspecialchars($presentation->title); ?>
@section('title',"| $titleTag" )

@section('style')

@endsection

@section('content')
<div class="about-us ">
    @include('inc.nav_bar')
    <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ url($presentation->photo) }}&apos;);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h2 class="title title-red">{!! $presentation->title !!}</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="main main-raised">
        <div class="container">
            <div class="section section-text">
                <div class="row">
                    <div class="col-md-10 ml-auto mr-auto text-justify">
                        {!! $markdown->parse($presentation->body) !!}
                    </div>
                </div>
            </div>
        </div>
        @guest
            @include('inc.deuxBoutons')
        @endguest
    </div>
</div>
@include('inc.footer')
@endsection

@section('scripts')

@endsection
