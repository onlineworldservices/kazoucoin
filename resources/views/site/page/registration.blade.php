@extends('inc.app')
@section('title', '- How to Register')
@section('style')
@endsection

@section('content')
<div class="landing-page" id="app">
    @include('inc.nav_bar')
    @if(count($slidesconcepts) > 0)
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($slidesconcepts as $item)
            <div class="carousel-item {{ ($loop->first)? 'active':'' }}">
                <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ url($item->photo) }}&apos;);">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-lg-10 ml-auto mr-auto text-center">
                                <h2 class="title">
                                    <b>{{ $item->slide_title }}</b>
                                    <h4>{{ $item->slide_subtitle }}</h4>
                                </h2>
                                <br>
                                @if($item->slide_btn_status != null)
                                <div class="buttons">
                                    <a href="{{ $item->slide_btn_link }}" class="btn btn-{{ $item->slide_btn_color }} btn-{{ $item->slide_btn_style1 }} btn-{{ $item->slide_btn_style }} btn-md ">
                                        <i class="material-icons">{{ $item->slide_btn_icon }}</i>
                                        <b>{{ $item->slide_btn_title }}</b>
                                    </a>
                                </div>
                                @endif
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @else
    <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 ml-auto mr-auto text-center">
                    <h1 class="title">How to register</h1>
                    <h5 class="title">Set Up your Slide in your Dashboard</h5>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="main main-raised">
        <div class="container">

            <!-- Comment s'enregistrer sur notre Plateforme -->
            <div class="cd-section" id="features">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ml-auto mr-auto">
                            <br>
                            <h3 class="kazoucoin-title title-red text-center">
                                <b>Three Step to register</b>
                            </h3>
                            <br>
                            @if(count($howtoregisters) > 0)
                            @foreach($howtoregisters as $item)
                            <div class="card card-plain card-blog" id="{!! $item->slug !!}">
                                <div class="row">
                                    <div class="col-md-8 expo">
                                        <h6 class="card-category text-info">
                                            <i class="material-icons" style="font-size: 45px;">{!! $item->icon_number !!}</i>
                                        </h6>
                                        <h3 class="card-title">
                                            <span>{!! $item->title !!}</span>
                                        </h3>
                                        <p class="card-description text-justify">
                                            {!! $markdown->parse($item->body) !!}
                                        </p>
                                    </div>
                                    <div class="col-md-4 expo">
                                        <div class="card-header card-header-image">
                                            <img class="img img-raised" width="150" height="350" src="{{ URL::to($item->image) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                @guest
                    @include('inc.deuxBoutons')
                @endguest
            </div>
        </div>
    </div>
</div>
@include('inc.footer')
@endsection

@section('scripts')
@parent
@endsection
