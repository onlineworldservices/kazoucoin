@extends('inc.app')
@section('title', '- Plan du Site')
@section('style')
@endsection
@section('content')
    <div id="app">
        @include('inc.nav_bar')
        <router-view></router-view>
    </div>
    @include('inc.footer')
    @include('inc.admin.components.multi_services')
@endsection

@section('scripts')

@endsection



