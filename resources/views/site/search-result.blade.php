@extends('inc.app')
@section('title', '- Recherche Articles')
@section('style')
@endsection

@section('content')
    <div class="landing-page" id="app">
        @include('inc.nav_bar')
        <div class="page-header header-filter header-small" data-parallax="true"
             style="background-image: url(&apos;{{ URL::to('https://www.kazoucoin.com/assets/img/photo.jpg')}}&apos;);">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h1 class="title">All Research</h1>
                        <h5 class="title">Set Up your Slide in your Dashboard</h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="main main-raised">
            <div class="section">
                <div class="container-fluid">
                    <div class="row mr-auto ml-auto">
                        <div class="col-md-3 filter-search">
                            <h3 class="section-title">
                                Filtres
                                <button class="btn btn-default btn-fab btn-fab-mini btn-link pull-right" rel="tooltip"
                                        title="" data-original-title="Reset Filter">
                                    <i class="material-icons">cached</i>
                                </button>
                            </h3>
                            <input type="text" value="France, Paris,Make Up,Rouge,Pomande"
                                   class="tagsinput form-control" data-role="tagsinput" data-color="warning">
                            <div class="card card-refine card-plain card-rose">
                                <div class="card-body">
                                    <div id="accordion" role="tablist">
                                        <div class="card card-collapse">
                                            <div class="card-header" role="tab" id="headingOne">
                                                <h5 class="mb-0">
                                                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="false"
                                                       aria-controls="collapseOne" data-parent="#accordion">
                                                        Prix
                                                        <i class="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" class="collapse show" role="tabpanel"
                                                 aria-labelledby="headingOne">
                                                <div class="card-body card-refine">
                                                    <span id="price-left" class="price-left pull-left"
                                                          data-currency="€">€42</span>
                                                    <span id="price-right" class="price-right pull-right"
                                                          data-currency="€">€880</span>
                                                    <div class="clearfix"></div>
                                                    <div id="sliderRefine"
                                                         class="slider slider-rose noUi-target noUi-ltr noUi-horizontal"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-collapse">
                                            <div class="card-header" role="tab" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" href="#collapseTwo"
                                                       aria-expanded="false" aria-controls="collapseTwo">
                                                        Pays
                                                        <i class="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse show" role="tabpanel"
                                                 aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value=""
                                                                   checked> France (45)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Italy (22)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Allemagne (15)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            USA (5)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Belgique (85)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Cameroun (23)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Canada (11)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Angleterre (3)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-collapse">
                                            <div class="card-header" role="tab" id="headingThree">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" href="#collapseThree"
                                                       aria-expanded="false" aria-controls="collapseThree">
                                                        Catègories
                                                        <i class="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseThree" class="collapse" role="tabpanel"
                                                 aria-labelledby="headingThree">
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value=""
                                                                   checked> Blazers (45)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Casual Shirts (22)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Formal Shirts (15)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Jeans (5)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Polos (85)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Pyjamas (23)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Shorts (11)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Trousers (3)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-collapse">
                                            <div class="card-header" role="tab" id="headingFour">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" href="#collapseFour"
                                                       aria-expanded="false" aria-controls="collapseFour">
                                                        Articles/Services
                                                        <i class="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseFour" class="collapse" role="tabpanel"
                                                 aria-labelledby="headingFour">
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value=""
                                                                   checked> Blazers (45)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Casual Shirts (22)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Formal Shirts (15)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Jeans (5)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Polos (85)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Pyjamas (23)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Shorts (11)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Trousers (3)
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-collapse">
                                            <div class="card-header" role="tab" id="headingFive">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" href="#collapseFive"
                                                       aria-expanded="false" aria-controls="collapseFive">
                                                        Marchants
                                                        <i class="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseFive" class="collapse" role="tabpanel"
                                                 aria-labelledby="headingFive">
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value=""
                                                                   checked> All
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Polo Ralph Lauren
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Wooyoungmi
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Alexander McQueen
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Tom Ford
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            AMI
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Berena
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Thom Sweeney
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Burberry Prorsum
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Calvin Klein
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Kingsman
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Club Monaco
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Dolce &amp; Gabbana
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Gucci
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Biglioli
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Lanvin
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Loro Piana
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Massimo Alba
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-collapse">
                                            <div class="card-header" role="tab" id="headingSix">
                                                <h5 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" href="#collapseSix"
                                                       aria-expanded="false" aria-controls="collapseSix">
                                                        Couleurs
                                                        <i class="material-icons">keyboard_arrow_down</i>
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseSix" class="collapse" role="tabpanel"
                                                 aria-labelledby="headingSix">
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value=""
                                                                   checked> All
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Black
                                                            <span class="catalog-filter-color color-black"></span>
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Blue
                                                            <span class="catalog-filter-color color-blue"></span>
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Yellow
                                                            <span class="catalog-filter-color color-yellow"></span>
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Gray
                                                            <span class="catalog-filter-color color-gray"></span>
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Green
                                                            <span class="catalog-filter-color color-green"></span>
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            White
                                                            <span class="catalog-filter-color color-white"></span>
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">
                                                            Purple
                                                            <span class="catalog-filter-color color-purple"></span>
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="result-search">
                                <h3><strong>64</strong> articles <span class="search-description">Make Up</span>
                                    disponible à <span class="search-description">Milan</span></h3>
                                <br>
                                <a id="button_hover" href="{{ url('/') }}">
                                    <i class="material-icons">keyboard_arrow_left</i>
                                    <b class="title_hover">Acceuil</b>
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <a href="#pablo">
                                                <img src="../assets/img-error/clint-mckoy.jpg" class="img-search"
                                                     alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-new"> <b>€1,430</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <span class="notify-badge-new"><b>New</b></span>
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/photo.jpg"
                                                     class="img-search" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-new"> <b>€743</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <span class="notify-badge-sold"><b>Promo</b></span>
                                            <a href="#pablo">
                                                <img src="../assets/img-error/clint-mckoy.jpg" class="img-search"
                                                     alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-old"> <b>€1,430</b></span>
                                            <span class="price price-new"> <b>€743</b></span>
                                            <span class="notify-solde"><b>- 40%</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <span class="notify-badge-sold"><b>Promo</b></span>
                                            <span class="notify-badge-new"><b>New</b></span>
                                            <a href="#pablo">
                                                <img src="../assets/img-error/clint-mckoy.jpg" class="img-search"
                                                     alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-old"> <b>€1,430</b></span>
                                            <span class="price price-new"> <b>€743</b></span>
                                            <span class="notify-solde"><b>- 40%</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <span class="notify-badge-sold"><b>Promo</b></span>
                                            <a href="#pablo">
                                                <img src="../assets/img-error/clint-mckoy.jpg" class="img-search"
                                                     alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-old"> <b>€1,430</b></span>
                                            <span class="price price-new"> <b>€743</b></span>
                                            <span class="notify-solde"><b>- 40%</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <a href="#pablo">
                                                <img src="../assets/img-error/clint-mckoy.jpg" class="img-search"
                                                     alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-new"> <b>€1,430</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <a href="#pablo">
                                                <img src="../assets/img-error/clint-mckoy.jpg" class="img-search"
                                                     alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-new"> <b>€1,430</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <span class="notify-badge-new"><b>New</b></span>
                                            <a href="#pablo">
                                                <img src="https://www.kazoucoin.com/assets/img/photo.jpg"
                                                     class="img-search" alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-new"> <b>€743</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 expo">
                                    <div class="card card-blog">
                                        <div class="card-header card-header-image">
                                            <span class="notify-badge-sold"><b>Promo</b></span>
                                            <a href="#pablo">
                                                <img src="../assets/img-error/clint-mckoy.jpg" class="img-search"
                                                     alt="">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-category text-warning text-center"><b>Category</b></h6>
                                            <h4 class="card-title text-center">
                                                <a href="#pablo">Titre Article en 2 lignes max et après ...</a>
                                            </h4>
                                        </div>
                                        <br>
                                        <div class="price-container text-center">
                                            <span class="price price-old"> <b>€1,430</b></span>
                                            <span class="price price-new"> <b>€743</b></span>
                                            <span class="notify-solde"><b>- 40%</b></span>
                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <div class="author">
                                                <a href="#pablo">
                                                    <img src="https://www.kazoucoin.com/assets/img/default-avatar.png"
                                                         title="Pablo" alt="..." class="avatar img-raised">
                                                </a>
                                            </div>
                                            <div class="stats ml-auto">
                                                <i class="material-icons icon icon-info">thumb_up</i> <b>2.4K</b> &#xB7;
                                                <i class="material-icons icon icon-warning">visibility</i> <b>45</b>
                                                &#xB7;
                                                <i class="material-icons icon icon-success">chat</i> <b>45</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <!-- Pagination -->
                            <div class="row">
                                <div class="ml-auto mr-auto text-center">
                                    <ul class="pagination pagination-primary">
                                        <!--
            color-classes: "pagination-primary", "pagination-info", "pagination-success", "pagination-warning", "pagination-danger"
            -->
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link"> Prev</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">1</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">...</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">5</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">6</a>
                                        </li>
                                        <li class="active page-item">
                                            <a href="javascript:void(0);" class="page-link">7</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">8</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">9</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">...</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link">12</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="javascript:void(0);" class="page-link"> Next</a>
                                        </li>
                                    </ul>
                                </div>
                                <br>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('inc.footer')
@endsection

@section('scripts')
    @parent
@endsection
