@extends('inc.app')

@section('style')

@endsection

@section('navbar')

<nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-warning" color-on-scroll="100" id="sectionsNav">
    @endsection
    @section('content')
    <div class="profile-page sidebar-collapse">

        <div class="page-header header-filter" data-parallax="true" style="background-image: url('../assets/img/city-profile.jpg');"></div>
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto">
                            <div class="profile">



                                @if(count($informatiques) > 0)
                                    @foreach($informatiques as $item)

                                        <div class="col-md-6 col-lg-4">
                                            <div class="rotating-card-container manual-flip">
                                                <div class="card card-rotate">
                                                    <div class="front" style="background-image: url('{{ url($item->user->avatar)  }}');">
                                                        <div class="card-body">
                                                            @foreach($item->menucategoryorders as $category)
                                                                <h5 class="card-category card-category-social">
                                                                    <i class="material-icons">label</i>  {{ $category->name }}
                                                                </h5>
                                                            @endforeach

                                                            <div class="author">
                                                                <a href="{{ route('/', $item->user->username) }}">
                                                                    <img src="{{ url($item->user->avatar)  }}" alt="..." class="avatar img-raised">
                                                                    <span>{{ str_limit($item->user->name, 13,'...')}}</span>
                                                                </a>
                                                            </div>
                                                            <div class="stats ml-auto">
                                                                <i class="material-icons">thumb_up_alt</i>2.4K
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                @endif



                </div>
            </div>
        </div>
    @include('inc._footer')
    </div>









                <div class="card-body">
                    @include('inc.alert')
                    {!! Form::model($user,['files'=> 'true', 'class' => 'form-horizontal']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">{!! config('app.name') !!} </label>
                                <input type="text" class="form-control" disabled>
                            </div>
                        </div>
                    </div>



                    <avatar :user="{{ $user }}"></avatar>

                    <div class="col-md-12">
                        <!-- Tabs with icons on Card -->
                        <div class="card card-nav-tabs">
                            <div class="card-header card-header-{{Auth::user()->color_name}}">
                                <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#profile" data-toggle="tab">
                                                    <i class="material-icons">face</i>
                                                    <b>Profile</b>
                                                </a>
                                            </li>
                                            <!--<li class="nav-item">
                                                <a class="nav-link" href="#messages" data-toggle="tab">
                                                    <i class="material-icons">lock_outline</i>
                                                    <b> Change password</b>
                                                </a>
                                            </li>-->
                                            <li class="nav-item">
                                                <a class="nav-link" href="#settings" data-toggle="tab">
                                                    <i class="material-icons">build</i>
                                                    <b>About me</b>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body ">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('Pseudo')}}</label>
                                                    {!! Form::text('username', null, ['class' => 'form-control','id' => 'username']) !!}
                                                    @if ($errors->has('username'))
                                                        <span class="invalid-feedback">
                                                           <strong>{{ $errors->first('username') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('Last name')}}</label>
                                                    {!! Form::text('name', null, ['class' => 'form-control','id' => 'name']) !!}
                                                    @if ($errors->has('name'))
                                                        <span class="invalid-feedback">
                                                           <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('Email')}}</label>
                                                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback">
                                                           <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('First name')}}</label>
                                                    {!! Form::text('first_name', null, ['class' => 'form-control','id' => 'first_name']) !!}
                                                    @if ($errors->has('first_name'))
                                                        <span class="invalid-feedback">
                                                           <strong>{{ $errors->first('first_name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="work" class="bmd-label-floating">{{ __('Work ?')}}</label>
                                                    {!! Form::text('work', null, ['class' => 'form-control','id'=>'work' ]) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="work" class="bmd-label-floating">{{ __('Your phone number')}}</label>
                                                    {!! Form::text('cellphone', null, ['class' => 'form-control','id'=>'cellphone' ]) !!}
                                                    @if ($errors->has('cellphone'))
                                                        <span class="invalid-feedback">
                                                           <strong>{{ $errors->first('cellphone') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="birthday" class="bmd-label-floating">Birthday date</label>
                                                    {!! Form::text('my_birthday', $user->my_birthday ? $user->my_birthday->format('d/m/y') : null , ['class' => 'form-control datepicker']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 ml-auto mr-auto row-block">
                                                <label for="color_name" class="bmd-label-floating">Color</label>
                                                {!! Form::select('color_name', [
                                                'info' => 'Info', 'danger' => 'Danger','warning' => 'Warning','rose'=>'Rose','dark'=>'Dark','success'=>'Success','primary'=>'Primary'
                                                ], null, ['class' => 'form-control selectpicker' ,'data-style' => 'select-with-transition','title' => 'Choose Color', 'data-size'=>'6']) !!}
                                            </div>
                                            <div class="col-lg-3 ml-auto mr-auto row-block">
                                                <label for="gender" class="bmd-label-floating">Country</label>
                                                {!! Form::select('country', [

                                                'Afghanistan'                                       => "Afghanistan",
                                                'Åland Islands'                                     => "Åland Islands",
                                                'Albania'                                           => "Albania",
                                                'Algeria'                                           => "Algeria",
                                                'American Samoa'                                    => "American Samoa",
                                                'Andorra'                                           => "Andorra",
                                                'Angola'                                            => "Angola",
                                                'Anguilla'                                          => "Anguilla",
                                                'Antarctica'                                        => "Antarctica",
                                                'Antigua and Barbuda'                               => "Antigua and Barbuda",
                                                'Argentina'                                         => "Argentina",
                                                'Armenia'                                           => "Armenia",
                                                'Aruba'                                             => "Aruba",
                                                'Australia'                                         => "Australia",
                                                'Austria'                                           => "Austria",
                                                'Azerbaijan'                                        => "Azerbaijan",
                                                'Bahamas'                                           => "Bahamas",
                                                'Bahrain'                                           => "Bahrain",
                                                'Bangladesh'                                        => "Bangladesh",
                                                'Barbados'                                          => "Barbados",
                                                'Belarus'                                           => "Belarus",
                                                'Belgium'                                           => "Belgium",
                                                'Belize'                                            => "Belize",
                                                'Benin'                                             => "Benin",
                                                'Bermuda'                                           => "Bermuda",
                                                'Bhutan'                                            => "Bhutan",
                                                'Bolivia'                                           => "Bolivia",
                                                'Bosnia and Herzegovina'                            => "Bosnia and Herzegovina",
                                                'Botswana'                                          => "Botswana",
                                                'Bouvet Island'                                     => "Bouvet Island",
                                                'Brazil'                                            => "Brazil",
                                                'British Indian Ocean Territory'                    => "British Indian Ocean Territory",
                                                'Brunei Darussalam'                                 => "Brunei Darussalam",
                                                'Bulgaria'                                          => "Bulgaria",
                                                'Burkina Faso'                                      => "Burkina Faso",
                                                'Burundi'                                           => "Burundi",
                                                'Cambodia'                                          => "Cambodia",
                                                'Cameroon'                                          => "Cameroon",
                                                'Canada'                                            => "Canada",
                                                'Cape Verde'                                        => "Cape Verde",
                                                'Cayman Islands'                                    => "Cayman Islands",
                                                'Central African Republic'                          => "Central African Republic",
                                                'Chad'                                              => "Chad",
                                                'Chile'                                             => "Chile",
                                                'China'                                             => "China",
                                                'Christmas Island'                                  => "Christmas Island",
                                                'Cocos (Keeling) Islands'                           => "Cocos (Keeling) Islands",
                                                'Colombia'                                          => "Colombia",
                                                'Comoros'                                           => "Comoros",
                                                'Congo'                                             => "Congo",
                                                'Congo, The Democratic Republic of the'             => "Congo, The Democratic Republic of the",
                                                'Cook Islands'                                      => "Cook Islands",
                                                'Costa Rica'                                        => "Costa Rica",
                                                'Cote D\'Ivoire'                                    => "Cote D'Ivoire",
                                                'Croatia'                                           => "Croatia",
                                                'Cuba'                                              => "Cuba",
                                                'Cyprus'                                            => "Cyprus",
                                                'Czech Republic'                                    => "Czech Republic",
                                                'Denmark'                                           => "Denmark",
                                                'Djibouti'                                          => "Djibouti",
                                                'Dominica'                                          => "Dominica",
                                                'Dominican Republic'                                => "Dominican Republic",
                                                'Ecuador'                                           => "Ecuador",
                                                'Egypt'                                             => "Egypt",
                                                'El Salvador'                                       => "El Salvador",
                                                'Equatorial Guinea'                                 => "Equatorial Guinea",
                                                'Eritrea'                                           => "Eritrea",
                                                'Estonia'                                           => "Estonia",
                                                'Ethiopia'                                          => "Ethiopia",
                                                'Falkland Islands (Malvinas)'                       => "Falkland Islands (Malvinas)",
                                                'Faroe Islands'                                     => "Faroe Islands",
                                                'Fiji'                                              => "Fiji",
                                                'Finland'                                           => "Finland",
                                                'France'                                            => "France",
                                                'French Guiana'                                     => "French Guiana",
                                                'French Polynesia'                                  => "French Polynesia",
                                                'French Southern Territories'                       => "French Southern Territories",
                                                'Gabon'                                             => "Gabon",
                                                'Gambia'                                            => "Gambia",
                                                'Georgia'                                           => "Georgia",
                                                'Germany'                                           => "Germany",
                                                'Ghana'                                             => "Ghana",
                                                'Gibraltar'                                         => "Gibraltar",
                                                'Greece'                                            => "Greece",
                                                'Greenland'                                         => "Greenland",
                                                'Grenada'                                           => "Grenada",
                                                'Guadeloupe'                                        => "Guadeloupe",
                                                'Guam'                                              => "Guam",
                                                'Guatemala'                                         => "Guatemala",
                                                'Guernsey'                                          => "Guernsey",
                                                'Guinea'                                            => "Guinea",
                                                'Guinea-Bissau'                                     => "Guinea-Bissau",
                                                'Guyana'                                            => "Guyana",
                                                'Haiti'                                             => "Haiti",
                                                'HM'                                                => "Heard Island and Mcdonald Islands",
                                                'Holy See (Vatican City State)'                     => "Holy See (Vatican City State)",
                                                'Honduras'                                          => "Honduras",
                                                'Hong Kong'                                         => "Hong Kong",
                                                'Hungary'                                           => "Hungary",
                                                'Iceland'                                           => "Iceland",
                                                'India'                                             => "India",
                                                'Indonesia'                                         => "Indonesia",
                                                'Iran, Islamic Republic Of'                         => "Iran, Islamic Republic Of",
                                                'Iraq'                                              => "Iraq",
                                                'Ireland'                                           => "Ireland",
                                                'Isle of Man'                                       => "Isle of Man",
                                                'Israel'                                            => "Israel",
                                                'Italy'                                             => "Italy",
                                                'Jamaica'                                           => "Jamaica",
                                                'Japan'                                             => "Japan",
                                                'Jersey'                                            => "Jersey",
                                                'Jordan'                                            => "Jordan",
                                                'Kazakhstan'                                        => "Kazakhstan",
                                                'Kenya'                                             => "Kenya",
                                                'Kiribati'                                          => "Kiribati",
                                                'Democratic People\'s Republic of Korea'            => "Democratic People's Republic of Korea",
                                                'Korea, Republic of'                                => "Korea, Republic of",
                                                'Kosovo'                                            => "Kosovo",
                                                'Kuwait'                                            => "Kuwait",
                                                'Kyrgyzstan'                                        => "Kyrgyzstan",
                                                'Lao People\'s Democratic Republic'                 => "Lao People's Democratic Republic",
                                                'Latvia'                                            => "Latvia",
                                                'Lebanon'                                           => "Lebanon",
                                                'Lesotho'                                           => "Lesotho",
                                                'Liberia'                                           => "Liberia",
                                                'LY'                                                => "Libyan Arab Jamahiriya",
                                                'Liechtenstein'                                     => "Liechtenstein",
                                                'Lithuania'                                         => "Lithuania",
                                                'Luxembourg'                                        => "Luxembourg",
                                                'Macao'                                             => "Macao",
                                                'Macedonia, The Former Yugoslav Republic of'        => "Macedonia, The Former Yugoslav Republic of",
                                                'Madagascar'                                        => "Madagascar",
                                                'Malawi'                                            => "Malawi",
                                                'Malaysia'                                          => "Malaysia",
                                                'Maldives'                                          => "Maldives",
                                                'Mali'                                              => "Mali",
                                                'Malta'                                             => "Malta",
                                                'Marshall Islan'                                    => "Marshall Islands",
                                                'Martinique'                                        => "Martinique",
                                                'Mauritania'                                        => "Mauritania",
                                                'Mauritius'                                         => "Mauritius",
                                                'Mayotte'                                           => "Mayotte",
                                                'Mexico'                                            => "Mexico",
                                                'Micronesia, Federated States of'                   => "Micronesia, Federated States of",
                                                'Moldova, Republic of'                              => "Moldova, Republic of",
                                                'Monaco'                                            => "Monaco",
                                                'Mongolia'                                          => "Mongolia",
                                                'Montenegro'                                        => "Montenegro",
                                                'Montserrat'                                        => "Montserrat",
                                                'Morocco'                                           => "Morocco",
                                                'Mozambique'                                        => "Mozambique",
                                                'Myanmar'                                           => "Myanmar",
                                                'Namibia'                                           => "Namibia",
                                                'Nauru'                                             => "Nauru",
                                                'Nepal'                                             => "Nepal",
                                                'Netherlands'                                       => "Netherlands",
                                                'Netherlands Antilles'                              => "Netherlands Antilles",
                                                'New Caledonia'                                     => "New Caledonia",
                                                'New Zealand'                                       => "New Zealand",
                                                'Nicaragua",'                                       => "Nicaragua",
                                                'Niger'                                             => "Niger",
                                                'Nigeria'                                           => "Nigeria",
                                                'Niue'                                              => "Niue",
                                                'Norfolk Island'                                    => "Norfolk Island",
                                                'Northern Mariana Islands'                          => "Northern Mariana Islands",
                                                'Norway'                                            => "Norway",
                                                'Oman'                                              => "Oman",
                                                'Pakistan'                                          => "Pakistan",
                                                'Palau'                                             => "Palau",
                                                'Palestinian Territory, Occupied'                   => "Palestinian Territory, Occupied",
                                                'Panama'                                            => "Panama",
                                                'Papua New Guinea'                                  => "Papua New Guinea",
                                                'Paraguay'                                          => "Paraguay",
                                                'Peru'                                              => "Peru",
                                                'Philippines'                                       => "Philippines",
                                                'Pitcairn'                                          => "Pitcairn",
                                                'Poland'                                            => "Poland",
                                                'Portugal'                                          => "Portugal",
                                                'Puerto Ri'                                         => "Puerto Rico",
                                                'Qatar'                                             => "Qatar",
                                                'Reunion'                                           => "Reunion",
                                                'Romania'                                           => "Romania",
                                                'Russian Federation'                                => "Russian Federation",
                                                'Rwanda'                                            => "Rwanda",
                                                'Saint Helena'                                      => "Saint Helena",
                                                'Saint Kitts and Nevis'                             => "Saint Kitts and Nevis",
                                                'Saint Lucia'                                       => "Saint Lucia",
                                                'Saint Pierre and Miquelon'                         => "Saint Pierre and Miquelon",
                                                'Saint Vincent and the Grenadines'                  => "Saint Vincent and the Grenadines",
                                                'Samoa'                                             => "Samoa",
                                                'San Marino'                                        => "San Marino",
                                                'Sao Tome and Principe'                             => "Sao Tome and Principe",
                                                'Saudi Arabia'                                      => "Saudi Arabia",
                                                'Senegal,'                                          => "Senegal",
                                                'Serbia,'                                           => "Serbia",
                                                'Seychelles,'                                       => "Seychelles",
                                                'Sierra Leone'                                      => "Sierra Leone",
                                                'Singapore,'                                        => "Singapore",
                                                'Slovakia,'                                         => "Slovakia",
                                                'Slovenia,'                                         => "Slovenia",
                                                'Solomon Islands'                                   => "Solomon Islands",
                                                'Somalia'                                           => "Somalia",
                                                'South Africa'                                      => "South Africa",
                                                'South Georgia and the South Sandwich Islands'      => "South Georgia and the South Sandwich Islands",
                                                'Spain'                                             => "Spain",
                                                'Sri Lanka'                                         => "Sri Lanka",
                                                'Sudan'                                             => "Sudan",
                                                'Suriname'                                          => "Suriname",
                                                'Svalbard and Jan Mayen'                            => "Svalbard and Jan Mayen",
                                                'Swaziland'                                         => "Swaziland",
                                                'Sweden'                                            => "Sweden",
                                                'Switzerland'                                       => "Switzerland",
                                                'Syrian Arab Republic'                              => "Syrian Arab Republic",
                                                'Taiwan'                                            => "Taiwan",
                                                'Tajikistan'                                        => "Tajikistan",
                                                'Tanzania, United Republic of'                      => "Tanzania, United Republic of",
                                                'Thailand'                                          => "Thailand",
                                                'Timor-Leste'                                       => "Timor-Leste",
                                                'Togo'                                              => "Togo",
                                                'Tokelau'                                           => "Tokelau",
                                                'Tonga'                                             => "Tonga",
                                                'Trinidad and Tobago'                               => "Trinidad and Tobago",
                                                'Tunisia'                                           => "Tunisia",
                                                'Turkey'                                            => "Turkey",
                                                'Turkmenistan'                                      => "Turkmenistan",
                                                'Turks and Caicos Islands'                          => "Turks and Caicos Islands",
                                                'Tuvalu'                                            => "Tuvalu",
                                                'Uganda'                                            => "Uganda",
                                                'Ukraine'                                           => "Ukraine",
                                                'United Arab Emirates'                              => "United Arab Emirates",
                                                'United Kingdom'                                    => "United Kingdom",
                                                'United States'                                     => "United States",
                                                'United States Minor Outlying Islands'              => "United States Minor Outlying Islands",
                                                'Uruguay'                                           => "Uruguay",
                                                'Uzbekistan'                                        => "Uzbekistan",
                                                'Vanuatu'                                           => "Vanuatu",
                                                'Venezuela'                                         => "Venezuela",
                                                'Viet Nam'                                          => "Viet Nam",
                                                'Virgin Islands, British'                           => "Virgin Islands, British",
                                                'Virgin Islands, U.S.'                              => "Virgin Islands, U.S.",
                                                'Wallis and Futuna'                                 => "Wallis and Futuna",
                                                'Western Sahara'                                    => "Western Sahara",
                                                'Yemen'                                             => "Yemen",
                                                'Zambia'                                            => "Zambia",
                                                'Zimbabwe'                                          => "Zimbabwe"

                                                ], null, ['class' => 'form-control selectpicker show-tick' ,'data-style' => 'select-with-transition','title' => 'Choose Country', 'data-size'=>'6','data-live-search'=>'true']) !!}
                                            </div>
                                            <div class="col-lg-3 ml-auto mr-auto row-block">
                                                <label for="gender" class="bmd-label-floating">Sex</label>
                                                {!! Form::select('sex', ['Female' => 'Female', 'Male' => 'Male'], null, ['class' => 'form-control selectpicker' ,'data-style' => 'select-with-transition','title' => 'Choose Sex','required']) !!}
                                            </div>
                                            <div class="col-lg-3 ml-auto mr-auto row-block">
                                                <label for="color_style" class="bmd-label-floating">color menu</label>
                                                {!! Form::select('color_style', ['purple' => 'Purple', 'azure' => 'Azure','green' => 'Green','orange' => 'Orange','danger' =>'Danger'], null, ['class' => 'form-control selectpicker' ,'data-style' => 'select-with-transition','title' => 'Choose color style']) !!}
                                            </div>
                                        </div>
                                    </div>
                                <!--
                                        <div class="tab-pane" id="messages">
                                            <div class="row">

                                                <div class="input-group form-control-lg">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                          <i class="material-icons">lock_outline</i>
                                                        </span>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                                        <div class="col-sm-10 checkbox-radios">
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="password_options" value="keep"
                                                                           checked> Do Not Change Password
                                                                    <span class="circle">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="password_options"
                                                                           value="manual"> Manuel Set New
                                                                    Password
                                                                    <span class="circle">
                                                                        <span class="check"></span>
                                                                     </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="password" class="form-control" id="password"
                                                   placeholder="Password( Select manuel set new Password! )"
                                                   name="password">
                                            @if ($errors->has('password'))
                                    <span class="help-block">
                                                  <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                                    </span>
                                            @endif
                                    <br/>
                                </div>
-->
                                    <div class="tab-pane" id="settings">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="media-body">
                                                    <label for="body" class="bmd-label-floating">About Me</label>
                                                    {!! Form::textarea('body', null, ['id'=>'ckeditor','class' => 'form-control', 'placeholder' => 'Tell something about you ....']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Tabs with icons on Card -->
                    </div>
                    <hr>
                    <div class="submit">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success btn-raised btn-round">
                                <span class="btn-label">
                                    <i class="material-icons">save_alt</i>
                                </span>
                                <b>Update profile</b>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>





@endsection

@section('scripts')

@endsection
