@extends('inc.app')
<?php $titleTag = htmlspecialchars($user->name); ?>
@section('title',"- $titleTag" )

@section('style')

@endsection

@section('content')
<div class="profile-page sidebar-collapse" id="app">
    @include('inc.nav_bar')

    <div class="page-header header-filter"  data-parallax="true" style="background-image: url(&apos;{{ url($user->avatarcover) }}&apos;); background-size: cover; background-position: top center;"></div>
    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto">
                        <div class="profile">
                            <div class="avatar">
                               @if($user->avatar)
                                   <img src="{{ url($user->avatar)  }}" alt="{!! $user->username !!}" class="img-raised rounded-circle img-fluid">
                               @endif
                            </div>
                            <div class="name">


                                @if(!Auth::guest())
                                    @if(Auth::user()->id === $user->id)
                                        <a href="{{route('myaccount.profile')}}"
                                           class="btn btn-fab btn-rose btn-round">
                                            <i class="material-icons">settings</i>
                                        </a>
                                    @endif
                                @endif

                                <h3 class="title">{{ $user->name }}</h3>
                                <p>Member Since {{ $user->created_at->format('j F Y') }}</p>
                                <p>Age {{ $user->age }} years old</p>
                                <h6>{!! $user->work  !!}</h6>
                                @if($user->fblink != null)
                                    <a href="https://facebook.com/{!! $user->fblink !!}"
                                       class="btn btn-just-icon btn-link btn-facebook" target="_blank"><i class="fa fa-facebook-f"></i></a>
                                @endif

                                @if($user->instalink != null)
                                    <a href="https://instagram.com/{!! $user->instalink !!}"
                                       class="btn btn-just-icon btn-link btn-instagram" target="_blank"><i class="fa fa-instagram"></i></a>
                                @endif

                                @if($user->twlink != null)
                                    <a href="https://twitter.com/{!! $user->twlink !!}"
                                       class="btn btn-just-icon btn-link btn-twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="description text-center">
                    <p>{!! $user->body !!}</p>
                </div>
                @if(!Auth::guest())
                    @if(Auth::user()->id === $user->id)
                        <div class="submit">
                            <div class="text-center">
                                @if (session('resent'))
                                    <div class="alert alert-rose" role="alert">
                                        {{ __('A fresh verification link has been sent to your email address please check your email address for confirmation.') }}
                                    </div>
                                @endif
                                @if(Auth::user()->email_verified_at === null)
                                <button id="btnEmailConfirmation" class="btn btn-success btn-raised btn-round btn-lg">
                                    <span class="btn-label">
                                        <i class="material-icons">category</i>
                                    </span>
                                    <b>Add new post</b>
                                </button>
                                    @else
                                 <button id="button_hover" class="btn btn-success btn-raised btn-round btn-lg"
                                         data-toggle="modal" data-target="#selectModal">
                                     <span class="btn-label">
                                         <i class="material-icons">category</i>
                                     </span>
                                     <b class="title_hover">Add new post</b>
                                 </button>
                                 @endif
                            </div>
                        </div>
                    @endif
                @endif

                <br/>
                <div class="tab-content tab-space">
                    <div class="tab-pane active work" id="work">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto ">
                                <div class="page-categories">
                                    <ul class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center"
                                        role="tablist">
                                        <li class="nav-item">
                                            <a id="button_orders" class="nav-link" data-toggle="tab" href="#orders"
                                               role="tablist">
                                                <i class="material-icons">redeem</i>
                                                <span>My Orders</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a id="button_events" class="nav-link active" data-toggle="tab" href="#events"
                                               role="tablist">
                                                <i class="material-icons">event_available</i>
                                                <span>My Events</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a id="button_anothers" class="nav-link" data-toggle="tab" href="#anothers"
                                               role="tablist">
                                                <i class="material-icons">bubble_chart</i>
                                                <span>Anothers</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <br>
                                    <div class="tab-content tab-space tab-subcategories">
                                        <div class="tab-pane" id="orders">
                                            <div class="card">
                                                <div class="card-header card-header-info text-center">
                                                    <h4 class="card-title">All About Orders</h4>
                                                    <p class="card-category">
                                                        More Information Here
                                                    </p>
                                                </div>
                                                <div class="card-body">
                                                    Collaboratively administrate empowered markets via
                                                    plug-and-play networks. Dynamically procrastinate B2C
                                                    users after installed base benefits.
                                                    <br>
                                                    <br> Dramatically visualize customer directed
                                                    convergence without revolutionary ROI.
                                                    Collaboratively administrate empowered markets via
                                                    plug-and-play networks. Dynamically procrastinate B2C
                                                    users after installed base benefits.
                                                    <br>
                                                    <br> Dramatically visualize customer directed
                                                    convergence without revolutionary ROI.
                                                    Collaboratively administrate empowered markets via
                                                    plug-and-play networks. Dynamically procrastinate B2C
                                                    users after installed base benefits.
                                                    <br>
                                                    <br> Dramatically visualize customer directed
                                                    convergence without revolutionary ROI.
                                                    Collaboratively administrate empowered markets via
                                                    plug-and-play networks. Dynamically procrastinate B2C
                                                    users after installed base benefits.
                                                    <br>
                                                    <br> Dramatically visualize customer directed
                                                    convergence without revolutionary ROI.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane active" id="events">
                                            <div class="card">
                                                <div class="card-header card-header-info text-center">
                                                    <h4 class="card-title">All About Events</h4>
                                                    <p class="card-category">
                                                        More Information Here
                                                    </p>
                                                </div>
                                                <div class="card-body">
                                                    Efficiently unleash cross-media information without
                                                    cross-media value. Quickly maximize timely deliverables
                                                    for real-time schemas.
                                                    <br>
                                                    <br> Dramatically maintain clicks-and-mortar solutions
                                                    without functional solutions.
                                                    Efficiently unleash cross-media information without
                                                    cross-media value. Quickly maximize timely deliverables
                                                    for real-time schemas.
                                                    <br>
                                                    <br> Dramatically maintain clicks-and-mortar solutions
                                                    without functional solutions.
                                                    Collaboratively administrate empowered markets via
                                                    plug-and-play networks. Dynamically procrastinate B2C
                                                    users after installed base benefits.
                                                    <br>
                                                    <br> Dramatically visualize customer directed
                                                    convergence without revolutionary ROI.
                                                    Collaboratively administrate empowered markets via
                                                    plug-and-play networks. Dynamically procrastinate B2C
                                                    users after installed base benefits.
                                                    <br>
                                                    <br> Dramatically visualize customer directed
                                                    convergence without revolutionary ROI.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="anothers">
                                            <div class="card">
                                                <div class="card-header card-header-info text-center">
                                                    <h4 class="card-title">All About Others</h4>
                                                    <p class="card-category">
                                                        More Information Here
                                                    </p>
                                                </div>
                                                <div class="card-body">
                                                    Completely synergize resource taxing relationships via
                                                    premier niche markets. Professionally cultivate
                                                    one-to-one customer service with robust ideas.
                                                    <br>
                                                    <br>Dynamically innovate resource-leveling customer
                                                    service for state of the art customer service.
                                                    Collaboratively administrate empowered markets via
                                                    plug-and-play networks. Dynamically procrastinate B2C
                                                    users after installed base benefits.
                                                    <br>
                                                    <br> Dramatically visualize customer directed
                                                    convergence without revolutionary ROI.
                                                    Collaboratively administrate empowered markets via
                                                    plug-and-play networks. Dynamically procrastinate B2C
                                                    users after installed base benefits.
                                                    <br>
                                                    <br> Dramatically visualize customer directed
                                                    convergence without revolutionary ROI.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 mr-auto ml-auto stats">
                                <h4 class="title">Stats</h4>
                                <ul class="list-unstyled">
                                    <li><b class="title-red">4</b> Collections</li>
                                    <li><b class="title-red">331</b> Influencers</li>
                                    <li><b class="title-red">1.2K</b> Likes</li>
                                </ul>
                                <hr>
                                <h4 class="title">About his Work</h4>
                                <p class="description">French luxury footwear and fashion. The footwear has
                                    incorporated shiny, red-lacquered soles that have become his
                                    signature.</p>
                                <hr>
                                <h4 class="title">Focus</h4>
                                <span class="badge badge-primary">Footwear</span>
                                <span class="badge badge-rose">Luxury</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('inc.login_modal')
    <div class="modal fade " id="selectModal" tabindex="-1" role="dialog" aria-labelledby="selectLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    @if(count($categoryevents) > 0)
                        <hr>
                        <div class="modal-header">
                            <h5 class="modal-title text-center" id="deleteLabel"><b>Please select your
                                    category</b></h5>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ml-auto mr-auto text-center">
                                @foreach($categoryevents as $item)
                                    <a href="/{{ $item->link_category_create }}"
                                       class="btn btn-{{ $item->color_name }} btn-round">
                                        <i class="material-icons">{{ $item->icon }}</i>
                                        <b>{{ $item->name }}</b>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="modal-header">
                        <h5 class="modal-title text-center" id="deleteLabel"><b>Please select your category for order</b>
                        </h5>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ml-auto mr-auto text-center">
                            @if(count($categoryorders) > 0)
                                @foreach($categoryorders as $item)
                                    <a href="/orders/{{ $item->link_category_create }}"
                                       class="btn btn-{{ $item->color_name }}">
                                        <i class="material-icons">{{ $item->icon }}</i>
                                        <b>{{ $item->name }}</b>
                                    </a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <hr>
                    <p>
                        <a href="#" class="tooltip-test" title="Tooltip">This link</a>
                        and <a href="#" class="tooltip-test" title="Tooltip">that link</a>
                        have tooltips on hover.
                    </p>
                </div>

            </div>
        </div>
    </div>
@include('inc._footer')
</div>

@endsection
@section('scripts')


<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });
    });
</script>
<script>
    $(document).ready(function () {
        // Initialise the wizard
        demo.initMaterialWizard();
        setTimeout(function () {
            $('.card.card-wizard').addClass('active');
        }, 600);
    });
</script>
<script type="text/javascript">
    /* ***** script btn email confirmation ****/
    $("#btnEmailConfirmation").click(function() {
        sweetAlert({
            title:'Action',
            text: 'Your email address is not confirm! send again email confirmation',
            //type:'question',
            animation: false,
            customClass: 'animated bounceIn',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Send Again',
            cancelButtonText: 'Cancel',
            showCancelButton: true,
            reverseButtons: true
        },function(isConfirm){
            alert('ok');
        });
        $('.swal2-confirm').click(function(){
            window.location.href = "{{ route('verification.resend') }}";
        });
    });
</script>
@endsection
