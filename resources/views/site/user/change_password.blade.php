@extends('inc.app')
<?php $titleTag = htmlspecialchars($user->name); ?>
@section('title',"- $titleTag")

@section('style')

    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

@endsection
@section('navbar')

<nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg bg-{{$user->color_name}}" color-on-scroll="100" id="sectionsNav">
@endsection
@section('content')
<div class="signup-page sidebar-collapse">
    <div class="page-header header-filter"
         style="background-image: url(&apos;{{ url(Auth::user()->avatarcover)  }}&apos;); background-size: cover; background-position: top center;">
        <div class="container" id="app">
            <div class="row">
                <div class="col-md-6 col-sm-6 ml-auto mr-auto">
                    <router-view></router-view>
                </div>
            </div>

        </div>
        @include('inc._footer')
    </div>
</div>
@endsection
@section('scripts')
<!-- Show password -->
@endsection
