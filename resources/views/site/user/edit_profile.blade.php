@extends('inc.app')
<?php $titleTag = htmlspecialchars($user->name); ?>
@section('title',"- $titleTag")

@section('style')

@endsection
@section('content')
<div class="signup-page sidebar-collapse">
	@include('inc.nav_bar')

<div class="page-header header-filter" style="background-image: url(&apos;{{ $user->avatarcover }}&apos;); background-size: cover; background-position: top center;">
<div class="container" id="app">
    <router-view></router-view>
</div>
@include('inc._footer')
</div>
</div>
@endsection
@section('scripts')


<script>
	$(document).ready(function () {

		$(".datepicker").datetimepicker({
			format: "DD/MM/YYYY",
			icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-chevron-up",
				down: "fa fa-chevron-down",
				previous: "fa fa-chevron-left",
				next: "fa fa-chevron-right",
				today: "fa fa-screenshot",
				clear: "fa fa-trash",
				close: "fa fa-remove"
			}
		})

	});
</script>
@endsection
