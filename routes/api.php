<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::apiResource('admin/link', 'Admin\LinkController');
Route::apiResource('contact', 'Api\NewsletterController');
//Api Tag
Route::apiResource('admin/tag', 'Api\TagController');
Route::get('countries', 'Admin\Account\CountryController@api');
//Route::get('admin/tag/active', 'Api\TagController@active')->name('tag.active');
//Api permissions
//Route::apiResource('admin/permissions', 'Api\PermissionController');
//Api colors
//Route::apiResource('admin/colors', 'Api\ColorController');
//Route::apiResource('countries','Admin\Account\CountryController');


//Ici les information vont etre utilise dans la partie beauter

//Route::apiResource('/beautes', 'Site\Order\BeauteController');
//Route::get('menucategoryorders/beautes', 'Site\Order\BeauteController@menucategoryorder');
//Route::get('colors/beautes', 'Site\Order\BeauteController@color');
//

Route::get('store/orders/orders-user/{username}', 'MyaccountController@GetOrdersByUser');
//Route::get('orders/beautes/{id}', 'Site\Order\BeauteController@show');
//Route::get('orders/beautes/{beaute}', 'Site\Order\BeauteController@show');

Route::namespace('Site\Order')->group(function () {
    Route::get('/orders/beautes', 'BeauteController@GetUserBeaute');
    Route::get('/orders/beautes/{beaute}', 'BeauteController@show');
    Route::put('/orders/beautes/{beaute}', 'BeauteController@update');
    Route::get('/beautes/topic/{menucategoryorder}', 'BeauteController@getItembycategory');

});
