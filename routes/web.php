
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
/** Dashboard */
require(__DIR__ . DIRECTORY_SEPARATOR . 'web' .DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'index.php');

Route::get('/search', 'SearchController@index')->name('search');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/conversations', 'ConversationsController@index')->name('conversations');

Route::group(['namespace' => 'Auth'], function () {

    // Admin Invitation
    Route::get('invite', 'AdmininviteController@invite')->name('invite');
    Route::post('invite', 'AdmininviteController@process')->name('process');

    // {token} is a required parameter that will be exposed to us in the controller method
    Route::get('accept/{token}', 'AdmininviteController@accept')->name('accept');

    // Social Auth
    Route::get('oauth/{driver}', 'SocialAuthController@redirectToProvider')->name('social.oauth');
    Route::get('oauth/{driver}/callback', 'SocialAuthController@handleProviderCallback')->name('social.callback');

});

Route::group(['middleware' => 'web'], function () {

    //User Profile
    Route::get('/profiles/users', 'MyaccountController@usersfollow');
    Route::post('/profiles/toggle', 'MyaccountController@toggle');

    //Logout User
    Route::get('user/logout', 'Auth\LoginController@UserLogout')->name('user.logout');

});


Route::get('invitations/{invitation_id}/{action}', 'Api\AcceptController@accept')->name('invitations.send');

//User
Auth::routes(['verify' => true]);
Route::group(['namespace' => 'User'], function () {

    //Conditions utilisation Routes
    Route::get('/terms_conditions/', 'ConditionController@index');

    //Presentation Route
    Route::get('presentation/{presentation}', 'PresentationController@presentation')->name('presentation');

    //Orders Route
    Route::get('orders', 'OrdersController@index')->name('orders');

    //Faqs Route
    Route::get('faqs', 'FaqsController@index')->name('faqs');
    Route::get('faqs/topics/{category}', 'FaqsController@categoryfaq')->name('category_faq');

    //Evenements Route
    Route::get('/evenements', 'EvenementsController@index')->name('evenements');

    //Others (autres services) Route
    Route::get('others', 'OthersController@index')->name('others');

    //Testimonials Route
   // Route::get('testimonial', 'TestimonialControlle@index')->name('testimonial');

    //About Us Route
    Route::get('about', 'AboutController@index')->name('about-us');

    //Work with Us Route
    Route::post('/save-work', 'AboutController@save_work')->name('user.save_work');

    //Events Route
    Route::get('topic/events', 'EventsController@index')->name('events');
    Route::get('topic/events/{event}', 'EventsController@event')->name('topic.events');
    Route::get('topics/{category}', 'EventsController@category')->name('topic.category');
    Route::get('topics/tag/{tag}', 'EventsController@tag')->name('topic.tag');

    //Articles Route
    Route::get('topic/articles', 'ArticlesController@index')->name('articles');
    Route::get('topic/articles/{article}', 'ArticlesController@article')->name('topic.articles');
    Route::get('topics/{category}', 'ArticlesController@category')->name('topic.category');
    Route::get('topics/tag/{tag}', 'ArticlesController@tag')->name('topic.tag');

    //Cookies Informations Site Route
    Route::get('/cookies-site', 'CookiesinformationsController@user')->name('user.cookies');
    Route::resource('admin/cookies-site','CookiesinformationsController');
    Route::get('/admin/cookies-site/lm/{cookie}', 'CookiesinformationsController@vector')->name('cookies-site.view');
    Route::get('/admin/cookies-site/view/{slug}', 'CookiesinformationsController@view');
    Route::get('/admin/api/cookies-site', 'CookiesinformationsController@api');
    Route::get('/admin/change_status_cookies/{id}', 'CookiesinformationsController@disable');

    //Concept Site (d'Exposition articles) Route
    Route::get('/our_concept', 'LegalmentionpagesController@concept_page')->name('concept');

    //How to register Route
    Route::get('/how_to_register', 'LegalmentionpagesController@registration_info_page')->name('user.registration');

    //Informations Site Route
    Route::get('/informations', 'LegalmentionpagesController@information_page')->name('user.informations');

    // Site Plan Route
    Route::get('/site_plan', 'SiteplanController@index')->name('site.plan');

    // Blog Route
    Route::get('/blog', 'BlogController@index')->name('blog');

    //Mention Légales Site Route
    Route::get('/legal_notice', 'LegalmentionpagesController@legal_notice_page')->name('legal_notice');

    //Licenses Site Route
    Route::get('/licenses', 'LegalmentionpagesController@licence_page')->name('site.licences');

    //Politiques Privacy Site Route
    Route::get('/policy_privacy', 'LegalmentionpagesController@policy_privacy_page')->name('user.privacy');


    Route::post('ajaxRequest', 'EventsController@ajaxRequest')->name('ajaxRequest');

    //Route::get('product/like/{id}', ['as' => 'product.like', 'uses' => 'LikeController@likeProduct']);
    // Route::get('event/like/{id}', ['as' => 'event.like', 'uses' => 'LikeController@likeEvent']);


    //Route::get('topic/events/tag/{tag}','BlogController@tag')->name('tag');
    //Route::get('to/category/{category}','BlogController@category')->name('category');



    Route::resource('admin/messages', 'MessageController');
    Route::get('admin/messages/msg/{message}', 'MessageController@message')->name('messages.view');
    Route::get('/admin/messages/view/{slug}', 'MessageController@view');
    Route::get('admin/api/messages', 'MessageController@api');
    Route::get('admin/messages/m/send', 'MessageController@send')->name('messages.send');
    Route::get('/messages/red_confirm/{id}', 'MessageController@active');
    Route::get('/messages/discard_red/{slug}', 'MessageController@disable');


});


// API
Route::group(['namespace' => 'Api'], function () {

    Route::resource('/newsletter', 'NewsletterController');

    //Route Contact Us
    Route::get('/contact-us', 'ContactController@create')->name('contact_us');
    Route::post('/contact-us/save', 'ContactController@store')->name('contact.store');

    //Route Assistance Messages
    Route::resource('admin/demande/contact/assistance/all-assistance', 'AidecontactmessagesController');
    Route::get('admin/api/demande/contact/assistance/all-assistance', 'AidecontactmessagesController@api');
    Route::get('admin/demande/contact/assistance/active_all-assistance/{id}', 'AidecontactmessagesController@active');
    Route::get('admin/demande/contact/assistance/disable_all-assistance/{id}', 'AidecontactmessagesController@disable');
    Route::get('/aide_contact_messages', 'AidecontactmessagesController@create')->name('aide_contact_messages');
    Route::get('aide_contact_messages/contact_themes', 'AidecontactmessagesController@themes');
    Route::get('aide_contact_messages/contact_demandes', 'AidecontactmessagesController@demandes');
    Route::post('aide_contact_messages/save', 'AidecontactmessagesController@store');
});

//Reset password
Route::get('user/change_password', 'Admin\Account\ChangePasswordController@showChangePasswordFormUser')->name('user.change_password');

//POST Subscribe Route
Route::post('/subscribe_email', 'Admin\Partials\NewletterSubscribeController@store')->name('newlettersubscribe');

//Route Site
Route::group(['namespace' => 'Site','middleware' => 'web'], function () {

    //Ici sont les routes qui sont les categories Orders
    Route::group(['namespace' => 'Order'], function () {

        //Get Beautes Admin
        Route::get('/admin/orders/beautes', 'BeauteController@dashboard')->name('admin.beaute');
        Route::get('api/admin/orders/beautes', 'BeauteController@getbeautedashboard');
        Route::get('/api/orders/admin/active_beautes/{id}', 'BeauteController@adminactive');
        Route::get('/api/orders/admin/disable_beautes/{id}', 'BeauteController@admindisable');

        //Beaute Routes
        Route::resource('orders/beautes', 'BeauteController');
        Route::get('api/orders/user/like_beautes/{id}', 'BeauteController@likePost');
        Route::post('orders/beautes/image','ImageController@store');
        Route::get('orders/api/beautes', 'BeauteController@getbeautes');
        Route::get('orders/api/beautes/menucategoryorders', 'BeauteController@menucategoryorder');
        Route::get('orders/api/beautes/categoryorders', 'BeauteController@categoryorder');
        Route::get('orders/api/beautes/colors', 'BeauteController@color');
        Route::get('orders/beautes/topic/{menucategoryorder}', 'BeauteController@getcategory')->name('category.beaute');
        Route::get('orders/beautes/{menucategoryorder}/show/{beaute}', 'BeauteController@view')->name('view.beaute');
        Route::get('/api/orders/user/active_beautes/{id}', 'BeauteController@useractive');
        Route::get('/api/orders/user/disable_beautes/{id}', 'BeauteController@userdisable');

        //Informatiques Routes
        Route::resource('orders/informatiques', 'InformatiquesController');

        //Categoryorder Route
        Route::get('orders/informatiques/topic/{menucategoryorder}', 'InformatiquesController@getcategory')->name('topic.menucategoryorder');
        Route::get('orders/informatiques/{menucategoryorder}/show/{informatique}', 'InformatiquesController@view')->name('view.informatique');
        //Route::get('topics/tag/{tag}', 'InformatiquesController@tag')->name('topic.tag');

    });

    //Comments
    Route::post('orders/informatiques/topic/{menucategoryorder}/{informatique}/comment', 'CommentsController@store')->name('comment_post');
   Route::get('orders/informatiques/topic/{menucategoryorder}/{informatique}/comment', 'CommentsController@index');
   Route::patch('orders/informatiques/topic/{menucategoryorder}/{informatique}/{comment}/comment', 'CommentsController@update');
   Route::delete('orders/informatiques/topic/{menucategoryorder}/{informatique}/{comment}/comment', 'CommentsController@destroy');

    //Ici sont les routes qui sont les categories Events
    Route::group(['namespace' => 'Orders'], function () {

        //Conditions Routes
        Route::resource('/articles', 'ArticlesController');

    });
});


//Events Route
Route::resource('events', 'EventsController');

//Articles Route
Route::resource('articles', 'ArticlesController');

// functionne bien
Route::get('/@{username}', ['as' => '/', 'uses' => 'MyaccountController@viewProfile']);
Route::get('orders/store/{username}', 'MyaccountController@orders')->name('orders.users');

Route::group(['prefix' => 'account', 'middleware' => 'verified'], function () {

    /* *************************** Init Route **************/
    Route::get('/', 'MyaccountController@index');
    Route::get('home', 'MyaccountController@home')->name('myaccount.home');
    Route::get('profile', 'UsersController@edit')->name('myaccount.profile');
    //Route::post('profile', 'UsersController@update');

    /* **************************** end *********************/


    //Route::get('/conversations', 'ConversationsController@index')->name('conversations');
    //Route::get('/conversations/{user}', 'ConversationsController@index')->name('conversations.show');
    // Route::get('/conversations/{user}', 'ConversationsController@index');


    Route::resource('messages', 'MessagesController');


    Route::get('/contacts', 'ContactsController@get');
    Route::get('/conversation/{id}', 'ContactsController@getMessagesFor');
    Route::post('/conversation/send', 'ContactsController@send');


    Route::resource('posts', 'PostsController');

});


//Route::get('/site', function (){
//
//    return view('site.test');
//});



/*
 * Route administrations
 */
Route::group(['middleware' => 'verified','namespace' => 'Admin', 'prefix' => 'admin'], function () {

    //Dashboard Route
    Route::get('/', 'DashboardController@index')->name('admin');
   // Route::get('/home', 'DashboardController@index')->name('admin');

    //Activity Log Route
    Route::get('/all-activity-log-page', 'ActivitylogController@index')->name('all_activity');
    Route::get('/api/all-activity-log-page', 'ActivitylogController@api');

    //Administrators Route
    Route::resource('/administrators', 'AdministratorsController');
    Route::get('/administrators/u/datatable', 'AdministratorsController@datatables');
    Route::get('/administrators/profile/{id}', 'AdministratorsController@view');
    Route::get('/api/administrators', 'AdministratorsController@api');
    Route::get('/api/all/users', 'AdministratorsController@user');
    Route::get('/administrators-export', 'AdministratorsController@export')->name('administrators_export');

    //Roles Route
    Route::resource('roles', 'RolesController');
    Route::get('/api/roles', 'RolesController@api');
    Route::get('/api/userRoles', 'RolesController@apiRoles');
    Route::delete('delete-multiple-role', ['as' => 'role.multiple-delete', 'uses' => 'RolesController@deleteMultiple']);

    //Tags Route
    Route::resource('tag', 'TagController');
    Route::get('/api/tag', 'TagController@api');
    Route::get('/change_status_tags/{id}', 'TagController@status');

    //Notes administrator Route
    Route::resource('notes', 'NoteController');
    Route::get('/api/notes', 'NoteController@api');
    Route::get('/active_note/{id}', 'NoteController@active');
    Route::get('/confirm_note_task/{id}', 'NoteController@taskconfirm');
    Route::get('/disable_note/{id}', 'NoteController@disable');


    //Permissions Route
    Route::resource('/permissions', 'PermissionsController');
    Route::get('/api/permissions', 'PermissionsController@api');

    //Users Route
    Route::resource('/users', 'UsersregisterController');
    Route::get('/api/all_users', 'UsersregisterController@api');
    Route::get('/api/users', 'UsersregisterController@user');
    Route::get('/users-export', 'UsersregisterController@export')->name('users_export');

    //Links Route
    Route::resource('/links', 'LinkController');
    Route::get('/api/links', 'LinkController@api');

    Route::group(['namespace' => 'Events'], function () {

        //Event Routes
        Route::resource('/event', 'EventsController');
    });

    Route::group(['namespace' => 'Aide', 'prefix' => 'aide'], function () {

        //Aide & Contact Themes Routes
        Route::resource('/contact_themes', 'AidecontactthemeController');
        Route::get('/api/contact_themes', 'AidecontactthemeController@api');
        Route::get('/active_contact_themes/{id}', 'AidecontactthemeController@active');
        Route::get('/disable_contact_themes/{id}', 'AidecontactthemeController@disable');

    });

    Route::group(['namespace' => 'Demande', 'prefix' => 'demande'], function () {

        //Aide & Contact Demandes Routes
        Route::resource('/contact_demandes', 'AidecontactdemandeController');
        Route::get('/api/contact_demandes', 'AidecontactdemandeController@api');
        Route::get('/active_contact_demandes/{id}', 'AidecontactdemandeController@active');
        Route::get('/disable_contact_demandes/{id}', 'AidecontactdemandeController@disable');

    });


    Route::group(['namespace' => 'Articles'], function () {

        //Conditions Routes
        Route::resource('/articles', 'ArticlesController');
    });

    Route::group(['namespace' => 'Categories'], function () {

        //Categories Orders Routes
        Route::resource('category-order', 'CategoryorderController');
        Route::get('/api/category-order', 'CategoryorderController@api');
        Route::get('/active_category-order/{id}', 'CategoryorderController@active');
        Route::get('/disable_category-order/{id}', 'CategoryorderController@disable');

        //Categories Faqs Routes
        Route::resource('category-faq', 'CategoryfaqController');
        Route::get('/api/category-faq', 'CategoryfaqController@api');
        Route::get('/change_status_category-faqs/{id}', 'CategoryfaqController@status');

        //Categories Events Routes
        Route::resource('/category-event', 'CategoryeventController');
        Route::get('/api/category-event', 'CategoryeventController@api');
        Route::get('/unactive_category-event/{id}', 'CategoryeventController@unactive_category');
        Route::get('/active_category-event/{id}', 'CategoryeventController@active_category');

        //Categories Menu Orders Routes
        Route::resource('/category-menu-order', 'MenucategoryorderController');
        Route::get('/api/category-menu-order', 'MenucategoryorderController@api');
        Route::get('/active_category-menu-order/{id}', 'MenucategoryorderController@active');
        Route::get('/disable_category-menu-order/{id}', 'MenucategoryorderController@disable');

        //Categories Menu Events Routes
        Route::resource('/category-menu-event', 'MenucategoryeventController');
        Route::get('/api/category-menu-event', 'MenucategoryeventController@api');
        Route::get('/unactive_category-menu-event/{id}', 'MenucategoryeventController@unactive_category');
        Route::get('/active_category-menu-event/{id}', 'MenucategoryeventController@active_category');
    });

    Route::group(['namespace' => 'Partials'], function () {

        //Subscribe Route
        Route::get('/subscribe_email', 'SubscribeemailController@index')->name('emailsubscribe');
        Route::get('/api/subscribe_email', 'SubscribeemailController@api');
        Route::get('/subscribe_email-export', 'SubscribeemailController@export')->name('emailsubscribe_export');



        //Evenements Route
        Route::get('eventments/{event_id}/send', ['uses' => 'EventmentsController@send', 'as' => 'eventments.send']);
        Route::resource('eventments', 'EventmentsController');
        Route::post('eventments_mass_destroy', ['uses' => 'EventmentsController@massDestroy', 'as' => 'eventments.mass_destroy']);
        Route::post('eventments_restore/{id}', ['uses' => 'EventmentsController@restore', 'as' => 'eventments.restore']);
        Route::delete('eventments_perma_del/{id}', ['uses' => 'EventmentsController@perma_del', 'as' => 'eventments.perma_del']);

        //Basket
        Route::resource('/basket', 'BasketController');



        //Privileges Route
        Route::resource('privileges', 'PrivilegeController');
        Route::get('/api/privileges/{name_privilege}', 'PrivilegeController@api');


        Route::group(['namespace' => 'Profiles'], function () {

            //Profile Home Page Route
            Route::resource('/homepageprofile','HomepageprofileController');
            Route::get('/api/homepageprofile', 'HomepageprofileController@api');
            Route::get('/unactive_home_page/{id}', 'HomepageprofileController@unactive_home_page');
            Route::get('/active_home_page/{id}', 'HomepageprofileController@active_home_page');
        });
    });

    //Agenda Route
    Route::get('/agenda', 'Agenda\AgendaController@index')->name('agenda');






    Route::group(['namespace' => 'Account'], function () {

        //Country Route
        Route::resource('countries','CountryController');

        //Account Profile Route
        Route::get('account', 'AccountController@index')->name('admin.account');
        Route::get('profile/{username}', 'AccountController@show')->name('user.profile');
        Route::get('api/profile/{username}', 'AccountController@userShow');
        Route::get('edit/profile', 'AccountController@edit')->name('admin.edit_profile');
        Route::get('api/account/profile', 'AccountController@userID');
        Route::get('api/account/user', 'AccountController@user');
        Route::put('api/account/user', 'AccountController@update');

        //Change Password Route
        Route::get('change_password', 'ChangePasswordController@showChangePasswordForm')->name('admin.change_password');
        Route::put('change_password', 'ChangePasswordController@changePassword');
    });

    Route::group(['namespace' => 'Sites'], function () {

        //Orders Route
        Route::resource('admin_orders', 'OrdersController');
    });

});






