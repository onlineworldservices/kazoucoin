<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:50
 */

//Abouts Route
Route::resource('/abouts', 'AboutController');
Route::get('/api/abouts', 'AboutController@api');
Route::get('/active_abouts/{id}', 'AboutController@active');
Route::get('/disable_abouts/{id}', 'AboutController@disable');