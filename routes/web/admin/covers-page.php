<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:52
 */

//Cover Page Administrator Route
Route::resource('covers-page', 'CoverpageController');
Route::get('/api/covers-page', 'CoverpageController@api');
Route::get('/active_covers-page/{id}', 'CoverpageController@active');
Route::get('/disable_covers-page/{id}', 'CoverpageController@disable');