<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:51
 */

//Faqs Route
Route::resource('/faqs', 'FaqsController');
Route::get('/faqs/v/sites', 'FaqsController@adminsites')->name('faqs.admin_sites');
Route::get('/api/faqs', 'FaqsController@api');
Route::get('/api/site/faqs', 'FaqsController@apiadminsites');
Route::get('/api/admin/faqs/{faq}','FaqsController@show');
Route::get('/change_status_faqs/{id}', 'FaqsController@status');

