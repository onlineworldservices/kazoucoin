<?php

Route::group(['middleware' => 'verified','namespace' => 'Admin','prefix' => 'admin'], function(){


    require(__DIR__ . DIRECTORY_SEPARATOR . 'info'. DIRECTORY_SEPARATOR . 'index.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'partial'. DIRECTORY_SEPARATOR . 'index.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'abouts.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'covers-page.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'faqs.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'presentations.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'testimonials.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'priorities.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'status.php');

});
