<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:09
 */

//Terms & Conditions Routes
Route::resource('/conditions', 'ConditionController');
Route::get('/conditions/lm/{condition}', 'ConditionController@vector')->name('conditions.view');
Route::get('/conditions/view/{slug}', 'ConditionController@view');
Route::get('/api/conditions', 'ConditionController@api');
Route::get('/unactive_condition/{id}', 'ConditionController@unactive_condition');
Route::get('/active_condition/{id}', 'ConditionController@active_condition');