<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 19:49
 */

//Terms & Conditions Routes
Route::get('documentations', 'DocumentationController@index')->name('documentations.index');
Route::get('/api/documentations', 'DocumentationController@api');
Route::get('documentations/create', 'DocumentationController@create')->name('documentations.create');
Route::post('documentations', 'DocumentationController@store')->name('documentations.store');
Route::get('documentations/{documentation}/edit', 'DocumentationController@edit')->name('documentations.edit');
Route::put('documentations/{documentation}', 'DocumentationController@update')->name('documentations.update');
Route::delete('documentations/{documentation}', 'DocumentationController@destroy')->name('documentations.destroy');
Route::get(
    '/documentations/{documentation}/getdocumentation',
    'DocumentationController@getDocumentation'
)->name('documentations.getdocumentation');
