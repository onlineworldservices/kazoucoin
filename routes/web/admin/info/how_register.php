<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:12
 */

//How to register Route
Route::resource('/how_register', 'HowtoregisterController');
Route::get('/api/how_register', 'HowtoregisterController@api');
Route::get('/how_register/hw/{howtoregister}', 'HowtoregisterController@vector')->name('howtoregister.view');
Route::get('/how_register/view/{slug}', 'HowtoregisterController@view');
Route::get('/unactive_how_register/{id}', 'HowtoregisterController@unactive_how_register');
Route::get('/active_how_register/{id}', 'HowtoregisterController@active_how_register');




