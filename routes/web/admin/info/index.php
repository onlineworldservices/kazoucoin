<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:06
 */

Route::group(['namespace' => 'Info'], function(){


    require(__DIR__ . DIRECTORY_SEPARATOR . 'conditions.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'documentations.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'how_register.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'informations.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'legal_notice.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'licence_site.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'policy_privacy.php');

});
