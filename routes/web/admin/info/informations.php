<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:05
 */

//Information Route
Route::resource('/informations', 'InformationController');
Route::get('/api/information_site', 'InformationController@api');
Route::get('/informations/view/{slug}', 'InformationController@view');
Route::get('/informations/inf/{information}', 'InformationController@vector')->name('information.view');
Route::get('/unactive_information/{id}', 'InformationController@unactive_information');
Route::get('/active_information/{id}', 'InformationController@active_information');
