<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:10
 */

//Legal Notice Route
Route::resource('/legal_notice', 'LegalnoticeController');
Route::get('/legal_notice/lm/{legalnotice}', 'LegalnoticeController@vector')->name('legalnotice.view');
Route::get('/legal_notice/view/{slug}', 'LegalnoticeController@view');
Route::get('/api/legal_notice', 'LegalnoticeController@api');
Route::get('/active_legal_notice/{id}', 'LegalnoticeController@active');
Route::get('/disable_legal_notice/{id}', 'LegalnoticeController@disable');