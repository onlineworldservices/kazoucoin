<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:13
 */


//Licence Site Route
Route::resource('/licence_site', 'LicencesiteController');
Route::get('/licence_site/lm/{licence}', 'LicencesiteController@vector')->name('licence_site.view');
Route::get('/licence_site/view/{slug}', 'LicencesiteController@view');
Route::get('/api/licence_site', 'LicencesiteController@api');
Route::get('/active_licence_site/{id}', 'LicencesiteController@active');
Route::get('/disable_licence_site/{id}', 'LicencesiteController@disable');