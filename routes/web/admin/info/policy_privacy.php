<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:11
 */


//Policy & Privacy Route
Route::resource('/policy_privacy', 'PolicyprivacyController');
Route::get('/policy_privacy/lm/{policyprivacy}', 'PolicyprivacyController@vector')->name('policy_privacy.view');
Route::get('/policy_privacy/view/{slug}', 'PolicyprivacyController@view');
Route::get('/api/policy_privacy', 'PolicyprivacyController@api');
Route::get('/active_policy_privacy/{id}', 'PolicyprivacyController@active');
Route::get('/disable_policy_privacy/{id}', 'PolicyprivacyController@disable');