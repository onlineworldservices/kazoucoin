<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:25
 */

//Color Route
Route::resource('colors', 'ColorController');
Route::get('/api/colors', 'ColorController@api');
Route::get('/colors-export', 'ColorController@export')->name('colors_export');
Route::get('/change_status_colors/{id}', 'ColorController@status');

