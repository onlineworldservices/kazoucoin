<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:29
 */

//Contact us Route
Route::resource('/all-contact', 'ContactController');
Route::get('/api/all-contact', 'ContactController@api');
Route::get('/all-contact/msg/{contact}', 'ContactController@contact')->name('contacts.view');
Route::get('/all-contact/view/{slug}', 'ContactController@view');
Route::get('/all-contact/red_confirm/{id}', 'ContactController@active');
Route::get('/all-contact/discard_red/{id}', 'ContactController@disable');