
<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:25
 */

//Devises Route
Route::resource('devises', 'DevisesController');
Route::get('/api/devises', 'DevisesController@api');
Route::get('/show/devises', 'DevisesController@devise');
Route::get('/change_status_devises/{id}', 'DevisesController@status');