<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:19
 */

Route::group(['namespace' => 'Partials'], function(){


    require(__DIR__ . DIRECTORY_SEPARATOR . 'slides'. DIRECTORY_SEPARATOR . 'index.php');


    require(__DIR__ . DIRECTORY_SEPARATOR . 'colors.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'contact.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'devises.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'speciality.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'tasks.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'works.php');
});
