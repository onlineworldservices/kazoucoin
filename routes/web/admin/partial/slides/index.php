<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:35
 */

Route::group(['namespace' => 'Slides'], function(){


    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide-about-us.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_concept.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_contact-us.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_cookie.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_evenement.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_faq.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_home.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_licence.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_mentionslegale.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_order.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_other.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_policy.php');

    require(__DIR__ . DIRECTORY_SEPARATOR . 'slide_testimonial.php');

});
