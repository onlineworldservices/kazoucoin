<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:37
 */

//Slide About page Route
Route::resource('/slide-about-us', 'SlidesaboutController');
Route::get('/api/slide-about-us', 'SlidesaboutController@api');
Route::get('/active-slide-about/{id}', 'SlidesaboutController@active_slide_about');
Route::get('/unactive-slide-about/{id}', 'SlidesaboutController@unactive_slide_about');