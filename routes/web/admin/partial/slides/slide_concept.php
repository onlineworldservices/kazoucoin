<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:38
 */


//Slide Concept page Route
Route::resource('/slide_concept', 'SlidesconceptController');
Route::get('/api/slide_concept', 'SlidesconceptController@api');
Route::get('/unactive-slide-concept/{id}', 'SlidesconceptController@unactive_slide');
Route::get('/active-slide-concept/{id}', 'SlidesconceptController@active_slide');