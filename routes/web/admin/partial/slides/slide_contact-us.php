<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:38
 */


//Slide Contact-us page Route
Route::resource('/slide_contact-us', 'SlidescontactController');
Route::get('/api/slide_contact-us', 'SlidescontactController@api');
Route::get('/unactive_slide-contact/{id}', 'SlidescontactController@unactive_slide');
Route::get('/active_slide-contact/{id}', 'SlidescontactController@active_slide');
