<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:39
 */

//Slide Cookies page Route
Route::resource('/slide_cookie', 'SlidescookieController');
Route::get('/api/slide_cookie', 'SlidescookieController@api');
Route::get('/unactive-slide-cookie/{id}', 'SlidescookieController@unactive_slide');
Route::get('/active-slide-cookie/{id}', 'SlidescookieController@active_slide');
