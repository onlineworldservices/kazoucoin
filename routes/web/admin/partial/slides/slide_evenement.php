<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:39
 */


//Slide Evenements page Route
Route::resource('/slide_evenement', 'SlidesevenementController');
Route::get('/api/slide_evenement', 'SlidesevenementController@api');
Route::get('/unactive-slide-evenement/{id}', 'SlidesevenementController@unactive_slide');
Route::get('/active-slide-evenement/{id}', 'SlidesevenementController@active_slide');
