<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:41
 */

//Slide Faqs page Route
Route::resource('/slide_faq', 'SlidesfaqController');
Route::get('/api/slide_faq', 'SlidesfaqController@api');
Route::get('/unactive-slide-faq/{id}', 'SlidesfaqController@unactive_slide');
Route::get('/active-slide-faq/{id}', 'SlidesfaqController@active_slide');