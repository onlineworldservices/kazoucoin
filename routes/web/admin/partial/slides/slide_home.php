<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:42
 */

//Slide Home page Route
Route::resource('/slide_home', 'SlideshomeController');
Route::get('/api/slide_home', 'SlideshomeController@api');
Route::get('/unactive-slide-home/{id}', 'SlideshomeController@unactive_slide');
Route::get('/active-slide-home/{id}', 'SlideshomeController@active_slide');