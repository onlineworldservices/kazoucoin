<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:42
 */


//Slide Licences page Route
Route::resource('/slide_licence', 'SlideslicenceController');
Route::get('/api/slide_licence', 'SlideslicenceController@api');
Route::get('/unactive-slide-licence/{id}', 'SlideslicenceController@unactive_slide');
Route::get('/active-slide-licence/{id}', 'SlideslicenceController@active_slide');
