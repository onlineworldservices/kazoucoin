<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:42
 */


//Slide Mentions Legales page Route
Route::resource('/slide_mentionslegale', 'SlidesmentionslegaleController');
Route::get('/api/slide_mentionslegale', 'SlidesmentionslegaleController@api');
Route::get('/unactive-slide-mentionslegale/{id}', 'SlidesmentionslegaleController@unactive_slide');
Route::get('/active-slide-mentionslegale/{id}', 'SlidesmentionslegaleController@active_slide');
