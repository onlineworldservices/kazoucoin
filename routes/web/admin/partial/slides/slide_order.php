<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:43
 */





//Slide Orders page Route
Route::resource('/slide_order', 'SlidesorderController');
Route::get('/api/slide_order', 'SlidesorderController@api');
Route::get('/unactive-slide-order/{id}', 'SlidesorderController@unactive_slide');
Route::get('/active-slide-order/{id}', 'SlidesorderController@active_slide');
