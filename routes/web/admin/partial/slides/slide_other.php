<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:43
 */


//Slide Others page Route
Route::resource('/slide_other', 'SlidesotherController');
Route::get('/api/slide_other', 'SlidesotherController@api');
Route::get('/unactive-slide-other/{id}', 'SlidesotherController@unactive_slide');
Route::get('/active-slide-other/{id}', 'SlidesotherController@active_slide');