<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:43
 */



//Slide Policy page Route
Route::resource('/slide_policy', 'SlidespolicyController');
Route::get('/api/slide_policy', 'SlidespolicyController@api');
Route::get('/unactive-slide-policy/{id}', 'SlidespolicyController@unactive_slide');
Route::get('/active-slide-policy/{id}', 'SlidespolicyController@active_slide');