<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:44
 */



//Slide Testimonial Route
Route::resource('/slide_testimonial', 'SlidestestimonialController');
Route::get('/api/slide_testimonial', 'SlidestestimonialController@api');
Route::get('/unactive_slide-testimonial/{id}', 'SlidestestimonialController@unactive_slide');
Route::get('/active_slide-testimonial/{id}', 'SlidestestimonialController@active_slide');
