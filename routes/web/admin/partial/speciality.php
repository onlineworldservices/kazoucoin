<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:28
 */

//Speciality contact Route
Route::resource('contact/speciality', 'SpecialityController');
Route::get('/api/speciality', 'SpecialityController@api');
Route::get('/unactive_speciality/{id}', 'SpecialityController@unactive_speciality');
Route::get('/active_speciality/{id}', 'SpecialityController@active_speciality');
