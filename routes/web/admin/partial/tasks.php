<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:21
 */

//Tasks Route
Route::resource('tasks', 'TaskController');
Route::get('/u/tasks{username}', 'TaskController@view')->name('tasks.view');
Route::get('api/tasks/u/{username}', 'TaskController@usertask');
Route::get('/api/tasks', 'TaskController@api');
Route::put('/update_progress_tasks/{id}', 'TaskController@updateProgress');
Route::put('/update_description_tasks/{id}', 'TaskController@updateDescription');