<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:31
 */

//Work with us Route
Route::resource('contact/all-work_with-us', 'WorkController');
Route::get('/api/contact/all-work_with-us', 'WorkController@api');