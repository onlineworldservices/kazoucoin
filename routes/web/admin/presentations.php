<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:51
 */

//Presentation Routes
Route::resource('/presentations', 'PresentationController');
Route::get('/presentations/lm/{presentation}', 'PresentationController@vector')->name('presentations.view');
Route::get('/presentations/view/{slug}', 'PresentationController@view');
Route::get('/api/presentations', 'PresentationController@api');
Route::get('/active_presentations/{id}', 'PresentationController@active');
Route::get('/disable_presentations/{id}', 'PresentationController@disable');


