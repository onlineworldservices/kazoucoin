<?php

// Priorities Route
Route::resource('/priorities', 'PriorityController');
Route::get('/api/priorities', 'PriorityController@api');
Route::get('/priorities-export', 'PriorityController@export')->name('priorities_export');
Route::get('/change_status_priorities/{id}', 'PriorityController@status');