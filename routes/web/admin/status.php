<?php

// Status Route
Route::resource('/status', 'StatusController');
Route::get('/api/status', 'StatusController@api');
Route::get('/status-export', 'StatusController@export')->name('status_export');
Route::get('/change_status_status_tasks/{id}', 'StatusController@status');