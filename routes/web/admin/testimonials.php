<?php
/**
 * Created by PhpStorm.
 * User: boclairtemgoua
 * Date: 2019-09-03
 * Time: 20:52
 */

//Testimonials Route
Route::resource('/testimonials', 'TestimonialController');
Route::get('/testimonials/tm/{testimonial}', 'TestimonialController@vector')->name('testimonials.view');
Route::get('/testimonials/view/{slug}', 'TestimonialController@view');
Route::get('/api/testimonials', 'TestimonialController@api');
Route::get('/unactive_testimonials/{id}', 'TestimonialController@disable');
Route::get('/active_testimonials/{id}', 'TestimonialController@active');